<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    	'admin/vendors/bootstrap/dist/css/bootstrap.min.css',
		'admin/vendors/font-awesome/css/font-awesome.min.css',
		'admin/vendors/nprogress/nprogress.css',
		'admin/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css',
		'admin/vendors/bootstrap-daterangepicker/daterangepicker.css',
		'admin/build/css/custom.min.css',
		'admin/vendors/iCheck/skins/flat/green.css',
		'admin/vendors/jqvmap/dist/jqvmap.min.css',
		'admin/vendors/bootstrap-daterangepicker/daterangepicker.css',
		'admin/vendors/pnotify/dist/pnotify.nonblock.css',
		'admin/vendors/pnotify/dist/pnotify.buttons.css',
		'admin/vendors/pnotify/dist/pnotify.css',
		'admin/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css',
		'admin/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css',
		'admin/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css',
		'admin/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.cs',
		'admin/css/hover.css',
        
    ];
    public $js = [
		//'admin/vendors/bootstrap/dist/js/bootstrap.min.js',
		//'admin/vendors/datatables.net/js/jquery.dataTables.min.js',
		'admin/vendors/fastclick/lib/fastclick.js',
		'admin/vendors/nprogress/nprogress.js',
		'js/datatables/jquery.dataTables.min.js',
        'js/datatables/dataTables.bootstrap.min.js',
		'admin/vendors/Chart.js/dist/Chart.min.js',
		'admin/vendors/jquery-sparkline/dist/jquery.sparkline.min.js',
		'admin/vendors/raphael/raphael.min.js',
		'admin/vendors/morris.js/morris.min.js',
		'admin/vendors/gauge.js/dist/gauge.min.js',
		'admin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js',
		'admin/vendors/iCheck/icheck.min.js',
		'admin/vendors/skycons/skycons.js',
		'admin/vendors/Flot/jquery.flot.js',
		'admin/vendors/Flot/jquery.flot.pie.js',
		'admin/vendors/Flot/jquery.flot.time.js',
		'admin/vendors/Flot/jquery.flot.stack.js',
		'admin/vendors/Flot/jquery.flot.resize.js',
		'admin/vendors/flot.orderbars/js/jquery.flot.orderBars.js',
		'admin/vendors/flot-spline/js/jquery.flot.spline.min.js',
		'admin/vendors/flot.curvedlines/curvedLines.js',
		'admin/vendors/DateJS/build/date.js',
		'admin/vendors/moment/min/moment.min.js',
		'admin/vendors/bootstrap-daterangepicker/daterangepicker.js',
		'admin/build/js/custom.min.js',
		'admin/vendors/datatables.net/js/jquery.dataTables.min.js',
		'admin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js',		
		'admin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js',
		'admin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js',
		'admin/vendors/datatables.net-buttons/js/buttons.flash.min.js',
		'admin/vendors/datatables.net-buttons/js/buttons.html5.min.js',
		'admin/vendors/datatables.net-buttons/js/buttons.print.min.js',
		'admin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js',
		'admin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js',
		'admin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js',
		'admin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js',
		'admin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js',
		'admin/vendors/jszip/dist/jszip.min.js',
		'admin/vendors/pdfmake/build/pdfmake.min.js',
		'admin/vendors/pdfmake/build/vfs_fonts.js',
		
		'admin/js/dashboard.js',
		
		
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
