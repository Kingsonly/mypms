<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class WebSiteAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'vendors/animate.css/animate.min.css',
		'assets/fonts/megatron/styles.css',
		'assets/css/main-interior.css'
    ];
    public $js = [
		'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js',
		'vendors/jquery/dist/jquery.min.js',
		'vendors/bootstrap/dist/js/bootstrap.min.js',
		'vendors/waypoints/lib/jquery.waypoints.min.js',
		'vendors/waypoints/lib/shortcuts/sticky.min.js',
		'vendors/smoothscroll/SmoothScroll.js',
		'vendors/wow/dist/wow.min.js',
		'vendors/parallax.js/parallax.js',
		'vendors/magnific-popup/dist/jquery.magnific-popup.min.js',
		'vendors/jquery-modal/jquery.modal.min.js',
		'assets/js/main.js',
		'vendors/imagesloaded/imagesloaded.pkgd.min.js',
		'vendors/isotope/dist/isotope.pkgd.min.js',
		'vendors/bootstrap-progressbar/bootstrap-progressbar.min.js',
		'vendors/slick-carousel/slick/slick.js',
		'vendors/jquery-countTo/jquery.countTo.js',
		
		
    ];
    public $depends = [
        'yii\web\YiiAsset',
        
    ];
}

