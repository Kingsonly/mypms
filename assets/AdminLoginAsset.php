<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AdminLoginAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
		'vendors/admin/vendors/bootstrap/dist/css/bootstrap.min.css',
		'vendors/admin/vendors/font-awesome/css/font-awesome.min.css',
		'vendors/admin/vendors/nprogress/nprogress.css',
		'vendors/admin/vendors/animate.css/animate.min.css',
		'build/css/custom.min.css'
        
    ];
    public $js = [
		
		
		
    ];
    public $depends = [
        'yii\web\YiiAsset',
		//'yii\bootstrap\BootstrapAsset',
        
    ];
}
