<?php

namespace app\controllers;

use Yii;
use app\models\RoomBooking;
use app\models\Email;
use app\models\Address;
use app\models\Telephone;
use app\models\Person;
use app\models\States;
use app\models\Entity;

use app\models\Countries;
use app\models\StatusType;
use app\models\Guest;
use app\models\Room;
use app\models\ReciptRelationship;
use app\models\Recipt;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Expression;

/**
 * RoomBookingController implements the CRUD actions for RoomBooking model.
 */
class RoomBookingController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RoomBooking models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = RoomBooking::find()->all();

        return $this->render('index', [
            'model' => $model,
        ]);
    }

    /**
     * Displays a single RoomBooking model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
		$model = new ReciptRelationship;
		$reciptmodel = new Recipt;
		$roomBookingModel = new RoomBooking;
		$fetchsenarioId = $model->find()->where(['recipt_id'=>$id])->one();
		$fetchReceiptDetails = $reciptmodel->find()->where(['recipt_id'=>$id])->one();
		$getSenarioId = $fetchsenarioId->senarior_id;
		$fetchRoomBookingDetails = $roomBookingModel->findOne($getSenarioId);
		$timeIn = new \DateTime($fetchRoomBookingDetails->time_in);
		$timeOut = new \DateTime($fetchRoomBookingDetails->time_out);
		$interval = date_diff($timeIn, $timeOut);
		$totalDays = $interval->format('%a');
		$getTariff = $fetchRoomBookingDetails->room->roomTariff->tariff;
		$amountDue = $totalDays * $getTariff *1.20;
        return $this->render('view', [
            'model' => $fetchsenarioId,
			'getRoomBookingDetails' => $fetchRoomBookingDetails,
			'getReceiptDetails' => $fetchReceiptDetails,
			'amountDue' => $amountDue
			
        ]);
    }

    /**
     * Creates a new RoomBooking model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RoomBooking();
        $personModel = new Person();
        $emailModel = new Email();
        $telephoneModel = new Telephone();
        $addressModel = new Address();
        $statesModel = new States();
        $countriesModel = new Countries();
        $statusTypeModel = new StatusType();
        $guestModel = new Guest();
        $entityModel = new Entity();
        $receiptModel = new Recipt();
        $ReciptRelationshipModel = new ReciptRelationship();
		//$model->isNotNew = false;
		
		// check to make sure no form is empty  if valid create a new entity id based on specifications 
        if ($model->load(Yii::$app->request->post()) && $personModel->load(Yii::$app->request->post()) && $guestModel->load(Yii::$app->request->post()) && $emailModel->load(Yii::$app->request->post()) && $telephoneModel->load(Yii::$app->request->post())  && $addressModel->load(Yii::$app->request->post())  && $model->validate() && $personModel->validate() && $emailModel->validate() && $telephoneModel->validate()  && $addressModel->validate() && $guestModel->validate() ) {
			$timeIn = new \DateTime($model->time_in);
			$timeOut = new \DateTime($model->time_out);
			$interval = date_diff($timeIn, $timeOut);
			$model->number_of_nights = (int)$interval->format('%d days');
			$dateNow = new Expression('NOW()'); // use for the time gueest was created 
			$userId = Yii::$app->user->identity->id;

			//assing entity id to all entity component
			$guestModel->entity_id = $model->entityIds;//entityModelId;
			$guestModel->registration_date = $dateNow;
			$guestModel->user_id = $userId;
			$guestModel->save(false);// save the data for guest in the database 
			$entityId = $guestModel->entity_id ;

			// assing relevat ids eg like guest id 
			$personModel->entity_id = $entityId;
			$emailModel->entity_id = $entityId;
			$telephoneModel->entity_id = $entityId;
			$addressModel->entity_id = $entityId;
			$model->user_id = $userId;
			$model->entity_id = $entityId;
			$model->tariff_id = 1; // take not change room tarrrif id to the right content 

			// save data into the db

			$personModel->save();
			$emailModel->save();
			$telephoneModel->save();
			$addressModel->save();


			//select room to update status
			$roomModel = Room::findOne($model->room_id);
			// make change of status based on status type 
			$selectStatusType = $statusTypeModel->find()->where(['status_title'=>'occupied'])->one();
			$roomModel->status = $selectStatusType->status_id;
			$roomModel->save();
			$model->save(false);
			/*
				/// insert value into the receipt table
				$receiptModel->receipt_issue_date = new Expression('NOW()');
				$receiptModel->recipt_number =rand(3009,10000000);
				$receiptModel->amount = $model->amount_paid ;
				$receiptModel->save();
				// create a recipt relationship
				$ReciptRelationshipModel->recipt_id = $receiptModel->recipt_id;
				$ReciptRelationshipModel->senarior_id = $model->booking_id;
				$ReciptRelationshipModel->senarior_type = 'roombooking';
				$ReciptRelationshipModel->save();       			
				*/
			return $this->redirect(['view', 'id' => $model->reciptId]);
			
			
		} else {
            return $this->renderAjax('create', [
                'model' => $model,
				'personModel' => $personModel,
        		'emailModel' => $emailModel,
        		'telephoneModel' => $telephoneModel,
        		'addressModel' => $addressModel,
        		'statesModel' => $statesModel,
        		'countriesModel' => $countriesModel,
        		'statusTypeModel' => $statusTypeModel,
        		'guestModel' => $guestModel,
            ]);
        }
    }

    /**
     * Updates an existing RoomBooking model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->booking_id]);
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing RoomBooking model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUnbook($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->booking_id]);
        } else {
            return $this->renderAjax('unbook', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RoomBooking model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
	
    /**
     * Finds the RoomBooking model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RoomBooking the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RoomBooking::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	
}
