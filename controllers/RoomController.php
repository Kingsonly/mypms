<?php

namespace app\controllers;

use Yii;
use app\models\Room;
use app\models\RoomTariff;
use app\models\RoomType;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\RoomBooking;
use app\models\StatusType;

/**
 * RoomController implements the CRUD actions for room model.
 */
class RoomController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all room models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new Room();
        $viewModel = Room::find()->all();
		$roomOneOccupant = 
        $roomTariffModel = RoomTariff::find()->all();
		
		$roomTypeModel = RoomType::find()->all();

        $date = date('Y-m-d');
        $prev_date = date('Y-m-d', strtotime($date .' -1 day'));
        $next_date = date('Y-m-d', strtotime($date .' +1 day'));

            for ($i = 1; $i<=187; $i++) {
        ${'roomuser'.$i} = RoomBooking::find()
            ->select('entity_id')
            ->where(['>','time_in',$prev_date])
            ->andWhere(['<','time_in',$next_date])
            ->andWhere(['=','room_id',$i])
            ->one();
        }

        return $this->render('index', [
            'model' => $model,
            'viewModel' => $viewModel,
			'roomTariffModel' => $roomTariffModel,
			'roomTypeModel' => $roomTypeModel,  
            'roomuser1' => $roomuser1,
            'roomuser2' => $roomuser2,
            'roomuser3' => $roomuser3,
            'roomuser4' => $roomuser4,
            'roomuser5' => $roomuser5,
            'roomuser6' => $roomuser6,
            'roomuser7' => $roomuser7,
            'roomuser8' => $roomuser8,
            'roomuser9' => $roomuser9,
            'roomuser10' => $roomuser10,
            'roomuser11' => $roomuser11,
            'roomuser12' => $roomuser12,
            'roomuser13' => $roomuser13,
            'roomuser14' => $roomuser14,
            'roomuser15' => $roomuser15,
            'roomuser16' => $roomuser16,
            'roomuser17' => $roomuser17,
            'roomuser18' => $roomuser18,
            'roomuser19' => $roomuser19,
            'roomuser20' => $roomuser20,
            'roomuser21' => $roomuser21,
            'roomuser22' => $roomuser22,
            'roomuser23' => $roomuser23,
            'roomuser24' => $roomuser24,
            'roomuser25' => $roomuser25,
            'roomuser26' => $roomuser26,
            'roomuser27' => $roomuser27,
            'roomuser28' => $roomuser28,
            'roomuser29' => $roomuser29,
            'roomuser30' => $roomuser30,
            'roomuser31' => $roomuser31,
            'roomuser32' => $roomuser32,
            'roomuser33' => $roomuser33,
            'roomuser34' => $roomuser34,
            'roomuser35' => $roomuser35,
            'roomuser36' => $roomuser36,
            'roomuser37' => $roomuser37,
            'roomuser38' => $roomuser38,
            'roomuser39' => $roomuser39,
            'roomuser40' => $roomuser40,
            'roomuser41' => $roomuser41,
            'roomuser42' => $roomuser42,
            'roomuser43' => $roomuser43,
            'roomuser44' => $roomuser44,
            'roomuser45' => $roomuser45,
            'roomuser46' => $roomuser46,
            'roomuser47' => $roomuser47,
            'roomuser48' => $roomuser48,
            'roomuser49' => $roomuser49,
            'roomuser50' => $roomuser50,
            'roomuser51' => $roomuser51,
            'roomuser52' => $roomuser52,
            'roomuser53' => $roomuser53,
            'roomuser54' => $roomuser54,
            'roomuser55' => $roomuser55,
            'roomuser56' => $roomuser56,
            'roomuser57' => $roomuser57,
            'roomuser58' => $roomuser58,
            'roomuser59' => $roomuser59,
            'roomuser60' => $roomuser60,
            'roomuser61' => $roomuser61,
            'roomuser62' => $roomuser62,
            'roomuser63' => $roomuser63,
            'roomuser64' => $roomuser64,
            'roomuser65' => $roomuser65,
            'roomuser66' => $roomuser66,
            'roomuser67' => $roomuser67,
            'roomuser68' => $roomuser68,
            'roomuser69' => $roomuser69,
            'roomuser70' => $roomuser70,
            'roomuser71' => $roomuser71,
            'roomuser72' => $roomuser72,
            'roomuser73' => $roomuser73,
            'roomuser74' => $roomuser74,
            'roomuser75' => $roomuser75,
            'roomuser76' => $roomuser76,
            'roomuser77' => $roomuser77,
            'roomuser78' => $roomuser78,
            'roomuser79' => $roomuser79,
            'roomuser80' => $roomuser80,
            'roomuser81' => $roomuser81,
            'roomuser82' => $roomuser82,
            'roomuser83' => $roomuser83,
            'roomuser84' => $roomuser84,
            'roomuser85' => $roomuser85,
            'roomuser86' => $roomuser86,
            'roomuser87' => $roomuser87,
            'roomuser88' => $roomuser88,
            'roomuser89' => $roomuser89,
            'roomuser90' => $roomuser90,
            'roomuser91' => $roomuser91,
            'roomuser92' => $roomuser92,
            'roomuser93' => $roomuser93,
            'roomuser94' => $roomuser94,
            'roomuser95' => $roomuser95,
            'roomuser96' => $roomuser96,
            'roomuser97' => $roomuser97,
            'roomuser98' => $roomuser98,
            'roomuser99' => $roomuser99,
            'roomuser100' => $roomuser100,
            'roomuser101' => $roomuser101,
            'roomuser102' => $roomuser102,
            'roomuser103' => $roomuser103,
            'roomuser104' => $roomuser104,
            'roomuser105' => $roomuser105,
            'roomuser106' => $roomuser106,
            'roomuser107' => $roomuser107,
            'roomuser108' => $roomuser108,
            'roomuser109' => $roomuser109,
            'roomuser110' => $roomuser110,
            'roomuser111' => $roomuser111,
            'roomuser112' => $roomuser112,
            'roomuser113' => $roomuser113,
            'roomuser114' => $roomuser114,
            'roomuser115' => $roomuser115,
            'roomuser116' => $roomuser116,
            'roomuser117' => $roomuser117,
            'roomuser118' => $roomuser118,
            'roomuser119' => $roomuser119,
            'roomuser120' => $roomuser120,
            'roomuser121' => $roomuser121,
            'roomuser122' => $roomuser122,
            'roomuser123' => $roomuser123,
            'roomuser124' => $roomuser124,
            'roomuser125' => $roomuser125,
            'roomuser126' => $roomuser126,
            'roomuser127' => $roomuser127,
            'roomuser128' => $roomuser128,
            'roomuser129' => $roomuser129,
            'roomuser130' => $roomuser130,
            'roomuser131' => $roomuser131,
            'roomuser132' => $roomuser132,
            'roomuser133' => $roomuser133,
            'roomuser134' => $roomuser134,
            'roomuser135' => $roomuser135,
            'roomuser136' => $roomuser136,
            'roomuser137' => $roomuser137,
            'roomuser138' => $roomuser138,
            'roomuser139' => $roomuser139,
            'roomuser140' => $roomuser140,
            'roomuser141' => $roomuser141,
            'roomuser142' => $roomuser142,
            'roomuser143' => $roomuser143,
            'roomuser144' => $roomuser144,
            'roomuser145' => $roomuser145,
            'roomuser146' => $roomuser146,
            'roomuser147' => $roomuser147,
            'roomuser148' => $roomuser148,
            'roomuser149' => $roomuser149,
            'roomuser150' => $roomuser150,
            'roomuser151' => $roomuser151,
            'roomuser152' => $roomuser152,
            'roomuser153' => $roomuser153,
            'roomuser154' => $roomuser154,
            'roomuser155' => $roomuser155,
            'roomuser156' => $roomuser156,
            'roomuser157' => $roomuser157,
            'roomuser158' => $roomuser158,
            'roomuser159' => $roomuser159,
            'roomuser160' => $roomuser160,
            'roomuser161' => $roomuser161,
            'roomuser162' => $roomuser162,
            'roomuser163' => $roomuser163,
            'roomuser164' => $roomuser164,
            'roomuser165' => $roomuser165,
            'roomuser166' => $roomuser166,
            'roomuser167' => $roomuser167,
            'roomuser168' => $roomuser168,
            'roomuser169' => $roomuser169,
            'roomuser170' => $roomuser170,
            'roomuser171' => $roomuser171,
            'roomuser172' => $roomuser172,
            'roomuser173' => $roomuser173,
            'roomuser174' => $roomuser174,
            'roomuser175' => $roomuser175,
            'roomuser176' => $roomuser176,
            'roomuser177' => $roomuser177,
            'roomuser178' => $roomuser178,
            'roomuser179' => $roomuser179,
            'roomuser180' => $roomuser180,
            'roomuser181' => $roomuser181,
            'roomuser182' => $roomuser182,
            'roomuser183' => $roomuser183,
            'roomuser184' => $roomuser184,
            'roomuser185' => $roomuser185,
            'roomuser186' => $roomuser186,
            'roomuser187' => $roomuser187,

        ]);
    }
    /**
     * Displays a single room model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
		$model = RoomBooking::find()->where(['room_id'=>$id])->all();
        return $this->renderAjax('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new room model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new room();

        $statusTypeModel = new StatusType();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
                'statusTypeModel' => $statusTypeModel,
            ]);
        }
    }

    /**
     * Updates an existing room model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $statusTypeModel = new StatusType();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
                'statusTypeModel' => $statusTypeModel,
            ]);
        }
    }

    /**
     * Updates an existing room status model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionStatus($id)
    {
        $model = $this->findModel($id);
        $statusTypeModel = new StatusType();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->renderAjax('status', [
                'model' => $model,
                'statusTypeModel' => $statusTypeModel,
            ]);
        }
    }

    public function actionRoomStatusReport()
    {
        $roomStatusReport = Room::find()->all();
        return $this->render('room-status-report', [
            'roomStatusReport' => $roomStatusReport,
        ]);
    }

    public function actionCompanyWiseRoomReport()
    {
        $year = date('Y');
        $month = date('m');
        $start = '01';
        $time1 = $year.'-'.$month.'-'.$start;
        $time2 = date('Y-m-d');

        $companyWiseRoomReport = RoomBooking::find()
            ->where(['>','time_in',DATE($time1)])
            ->andWhere(['<=','time_in',DATE($time2)])
            ->andWhere(['=','status_id',11])
            ->all();
        return $this->render('company-wise-room-report', [
            'companyWiseRoomReport' => $companyWiseRoomReport,
        ]);
    }

    public function actionGuestWiseRoomReport()
    {
        $year = date('Y');
        $month = date('m');
        $start = '01';
        $time1 = $year.'-'.$month.'-'.$start;
        $time2 = date('Y-m-d');
        $guestWiseRoomReport = RoomBooking::find()
            ->where(['>','time_in',DATE($time1)])
            ->andWhere(['<=','time_in',DATE($time2)])
            ->andWhere(['!=','status_id',11])
            ->all();
        return $this->render('guest-wise-room-report', [
            'guestWiseRoomReport' => $guestWiseRoomReport,
        ]);
    }

    public function actionRoomStatisticsReport()
    {
        $year = date('Y');
        $lastYear = $year-1;
        $roomStatisticsReport = Room::find()
            ->all();
            for ($i = 1; $i<=187; $i++) {
        ${'roomstats'.$i} = RoomBooking::find()
            ->Where('YEAR(time_in) = '.$year.' AND room_id = '.$i)
            ->count();
        ${'roomstats2'.$i} = RoomBooking::find()
            ->Where('YEAR(time_in) = '.$lastYear.' AND room_id = '.$i)
            ->count();
        }

        return $this->render('room-statistics-report', [
            'roomStatisticsReport' => $roomStatisticsReport,
            'roomstats1' => $roomstats1,
            'roomstats2' => $roomstats2,
            'roomstats3' => $roomstats3,
            'roomstats4' => $roomstats4,
            'roomstats5' => $roomstats5,
            'roomstats6' => $roomstats6,
            'roomstats7' => $roomstats7,
            'roomstats8' => $roomstats8,
            'roomstats9' => $roomstats9,
            'roomstats10' => $roomstats10,
            'roomstats11' => $roomstats11,
            'roomstats12' => $roomstats12,
            'roomstats13' => $roomstats13,
            'roomstats14' => $roomstats14,
            'roomstats15' => $roomstats15,
            'roomstats16' => $roomstats16,
            'roomstats17' => $roomstats17,
            'roomstats18' => $roomstats18,
            'roomstats19' => $roomstats19,
            'roomstats20' => $roomstats20,
            'roomstats21' => $roomstats21,
            'roomstats22' => $roomstats22,
            'roomstats23' => $roomstats23,
            'roomstats24' => $roomstats24,
            'roomstats25' => $roomstats25,
            'roomstats26' => $roomstats26,
            'roomstats27' => $roomstats27,
            'roomstats28' => $roomstats28,
            'roomstats29' => $roomstats29,
            'roomstats30' => $roomstats30,
            'roomstats31' => $roomstats31,
            'roomstats32' => $roomstats32,
            'roomstats33' => $roomstats33,
            'roomstats34' => $roomstats34,
            'roomstats35' => $roomstats35,
            'roomstats36' => $roomstats36,
            'roomstats37' => $roomstats37,
            'roomstats38' => $roomstats38,
            'roomstats39' => $roomstats39,
            'roomstats40' => $roomstats40,
            'roomstats41' => $roomstats41,
            'roomstats42' => $roomstats42,
            'roomstats43' => $roomstats43,
            'roomstats44' => $roomstats44,
            'roomstats45' => $roomstats45,
            'roomstats46' => $roomstats46,
            'roomstats47' => $roomstats47,
            'roomstats48' => $roomstats48,
            'roomstats49' => $roomstats49,
            'roomstats50' => $roomstats50,
            'roomstats51' => $roomstats51,
            'roomstats52' => $roomstats52,
            'roomstats53' => $roomstats53,
            'roomstats54' => $roomstats54,
            'roomstats55' => $roomstats55,
            'roomstats56' => $roomstats56,
            'roomstats57' => $roomstats57,
            'roomstats58' => $roomstats58,
            'roomstats59' => $roomstats59,
            'roomstats60' => $roomstats60,
            'roomstats61' => $roomstats61,
            'roomstats62' => $roomstats62,
            'roomstats63' => $roomstats63,
            'roomstats64' => $roomstats64,
            'roomstats65' => $roomstats65,
            'roomstats66' => $roomstats66,
            'roomstats67' => $roomstats67,
            'roomstats68' => $roomstats68,
            'roomstats69' => $roomstats69,
            'roomstats70' => $roomstats70,
            'roomstats71' => $roomstats71,
            'roomstats72' => $roomstats72,
            'roomstats73' => $roomstats73,
            'roomstats74' => $roomstats74,
            'roomstats75' => $roomstats75,
            'roomstats76' => $roomstats76,
            'roomstats77' => $roomstats77,
            'roomstats78' => $roomstats78,
            'roomstats79' => $roomstats79,
            'roomstats80' => $roomstats80,
            'roomstats81' => $roomstats81,
            'roomstats82' => $roomstats82,
            'roomstats83' => $roomstats83,
            'roomstats84' => $roomstats84,
            'roomstats85' => $roomstats85,
            'roomstats86' => $roomstats86,
            'roomstats87' => $roomstats87,
            'roomstats88' => $roomstats88,
            'roomstats89' => $roomstats89,
            'roomstats90' => $roomstats90,
            'roomstats91' => $roomstats91,
            'roomstats92' => $roomstats92,
            'roomstats93' => $roomstats93,
            'roomstats94' => $roomstats94,
            'roomstats95' => $roomstats95,
            'roomstats96' => $roomstats96,
            'roomstats97' => $roomstats97,
            'roomstats98' => $roomstats98,
            'roomstats99' => $roomstats99,
            'roomstats100' => $roomstats100,
            'roomstats101' => $roomstats101,
            'roomstats102' => $roomstats102,
            'roomstats103' => $roomstats103,
            'roomstats104' => $roomstats104,
            'roomstats105' => $roomstats105,
            'roomstats106' => $roomstats106,
            'roomstats107' => $roomstats107,
            'roomstats108' => $roomstats108,
            'roomstats109' => $roomstats109,
            'roomstats110' => $roomstats110,
            'roomstats111' => $roomstats111,
            'roomstats112' => $roomstats112,
            'roomstats113' => $roomstats113,
            'roomstats114' => $roomstats114,
            'roomstats115' => $roomstats115,
            'roomstats116' => $roomstats116,
            'roomstats117' => $roomstats117,
            'roomstats118' => $roomstats118,
            'roomstats119' => $roomstats119,
            'roomstats120' => $roomstats120,
            'roomstats121' => $roomstats121,
            'roomstats122' => $roomstats122,
            'roomstats123' => $roomstats123,
            'roomstats124' => $roomstats124,
            'roomstats125' => $roomstats125,
            'roomstats126' => $roomstats126,
            'roomstats127' => $roomstats127,
            'roomstats128' => $roomstats128,
            'roomstats129' => $roomstats129,
            'roomstats130' => $roomstats130,
            'roomstats131' => $roomstats131,
            'roomstats132' => $roomstats132,
            'roomstats133' => $roomstats133,
            'roomstats134' => $roomstats134,
            'roomstats135' => $roomstats135,
            'roomstats136' => $roomstats136,
            'roomstats137' => $roomstats137,
            'roomstats138' => $roomstats138,
            'roomstats139' => $roomstats139,
            'roomstats140' => $roomstats140,
            'roomstats141' => $roomstats141,
            'roomstats142' => $roomstats142,
            'roomstats143' => $roomstats143,
            'roomstats144' => $roomstats144,
            'roomstats145' => $roomstats145,
            'roomstats146' => $roomstats146,
            'roomstats147' => $roomstats147,
            'roomstats148' => $roomstats148,
            'roomstats149' => $roomstats149,
            'roomstats150' => $roomstats150,
            'roomstats151' => $roomstats151,
            'roomstats152' => $roomstats152,
            'roomstats153' => $roomstats153,
            'roomstats154' => $roomstats154,
            'roomstats155' => $roomstats155,
            'roomstats156' => $roomstats156,
            'roomstats157' => $roomstats157,
            'roomstats158' => $roomstats158,
            'roomstats159' => $roomstats159,
            'roomstats160' => $roomstats160,
            'roomstats161' => $roomstats161,
            'roomstats162' => $roomstats162,
            'roomstats163' => $roomstats163,
            'roomstats164' => $roomstats164,
            'roomstats165' => $roomstats165,
            'roomstats166' => $roomstats166,
            'roomstats167' => $roomstats167,
            'roomstats168' => $roomstats168,
            'roomstats169' => $roomstats169,
            'roomstats170' => $roomstats170,
            'roomstats171' => $roomstats171,
            'roomstats172' => $roomstats172,
            'roomstats173' => $roomstats173,
            'roomstats174' => $roomstats174,
            'roomstats175' => $roomstats175,
            'roomstats176' => $roomstats176,
            'roomstats177' => $roomstats177,
            'roomstats178' => $roomstats178,
            'roomstats179' => $roomstats179,
            'roomstats180' => $roomstats180,
            'roomstats181' => $roomstats181,
            'roomstats182' => $roomstats182,
            'roomstats183' => $roomstats183,
            'roomstats184' => $roomstats184,
            'roomstats185' => $roomstats185,
            'roomstats186' => $roomstats186,
            'roomstats187' => $roomstats187,
             'roomstats21' => $roomstats21,
            'roomstats22' => $roomstats22,
            'roomstats23' => $roomstats23,
            'roomstats24' => $roomstats24,
            'roomstats25' => $roomstats25,
            'roomstats26' => $roomstats26,
            'roomstats27' => $roomstats27,
            'roomstats28' => $roomstats28,
            'roomstats29' => $roomstats29,
            'roomstats210' => $roomstats210,
            'roomstats211' => $roomstats211,
            'roomstats212' => $roomstats212,
            'roomstats213' => $roomstats213,
            'roomstats214' => $roomstats214,
            'roomstats215' => $roomstats215,
            'roomstats216' => $roomstats216,
            'roomstats217' => $roomstats217,
            'roomstats218' => $roomstats218,
            'roomstats219' => $roomstats219,
            'roomstats220' => $roomstats220,
            'roomstats221' => $roomstats221,
            'roomstats222' => $roomstats222,
            'roomstats223' => $roomstats223,
            'roomstats224' => $roomstats224,
            'roomstats225' => $roomstats225,
            'roomstats226' => $roomstats226,
            'roomstats227' => $roomstats227,
            'roomstats228' => $roomstats228,
            'roomstats229' => $roomstats229,
            'roomstats230' => $roomstats230,
            'roomstats231' => $roomstats231,
            'roomstats232' => $roomstats232,
            'roomstats233' => $roomstats233,
            'roomstats234' => $roomstats234,
            'roomstats235' => $roomstats235,
            'roomstats236' => $roomstats236,
            'roomstats237' => $roomstats237,
            'roomstats238' => $roomstats238,
            'roomstats239' => $roomstats239,
            'roomstats240' => $roomstats240,
            'roomstats241' => $roomstats241,
            'roomstats242' => $roomstats242,
            'roomstats243' => $roomstats243,
            'roomstats244' => $roomstats244,
            'roomstats245' => $roomstats245,
            'roomstats246' => $roomstats246,
            'roomstats247' => $roomstats247,
            'roomstats248' => $roomstats248,
            'roomstats249' => $roomstats249,
            'roomstats250' => $roomstats250,
            'roomstats251' => $roomstats251,
            'roomstats252' => $roomstats252,
            'roomstats253' => $roomstats253,
            'roomstats254' => $roomstats254,
            'roomstats255' => $roomstats255,
            'roomstats256' => $roomstats256,
            'roomstats257' => $roomstats257,
            'roomstats258' => $roomstats258,
            'roomstats259' => $roomstats259,
            'roomstats260' => $roomstats260,
            'roomstats261' => $roomstats261,
            'roomstats262' => $roomstats262,
            'roomstats263' => $roomstats263,
            'roomstats264' => $roomstats264,
            'roomstats265' => $roomstats265,
            'roomstats266' => $roomstats266,
            'roomstats267' => $roomstats267,
            'roomstats268' => $roomstats268,
            'roomstats269' => $roomstats269,
            'roomstats270' => $roomstats270,
            'roomstats271' => $roomstats271,
            'roomstats272' => $roomstats272,
            'roomstats273' => $roomstats273,
            'roomstats274' => $roomstats274,
            'roomstats275' => $roomstats275,
            'roomstats276' => $roomstats276,
            'roomstats277' => $roomstats277,
            'roomstats278' => $roomstats278,
            'roomstats279' => $roomstats279,
            'roomstats280' => $roomstats280,
            'roomstats281' => $roomstats281,
            'roomstats282' => $roomstats282,
            'roomstats283' => $roomstats283,
            'roomstats284' => $roomstats284,
            'roomstats285' => $roomstats285,
            'roomstats286' => $roomstats286,
            'roomstats287' => $roomstats287,
            'roomstats288' => $roomstats288,
            'roomstats289' => $roomstats289,
            'roomstats290' => $roomstats290,
            'roomstats291' => $roomstats291,
            'roomstats292' => $roomstats292,
            'roomstats293' => $roomstats293,
            'roomstats294' => $roomstats294,
            'roomstats295' => $roomstats295,
            'roomstats296' => $roomstats296,
            'roomstats297' => $roomstats297,
            'roomstats298' => $roomstats298,
            'roomstats299' => $roomstats299,
            'roomstats2100' => $roomstats2100,
            'roomstats2101' => $roomstats2101,
            'roomstats2102' => $roomstats2102,
            'roomstats2103' => $roomstats2103,
            'roomstats2104' => $roomstats2104,
            'roomstats2105' => $roomstats2105,
            'roomstats2106' => $roomstats2106,
            'roomstats2107' => $roomstats2107,
            'roomstats2108' => $roomstats2108,
            'roomstats2109' => $roomstats2109,
            'roomstats2110' => $roomstats2110,
            'roomstats2111' => $roomstats2111,
            'roomstats2112' => $roomstats2112,
            'roomstats2113' => $roomstats2113,
            'roomstats2114' => $roomstats2114,
            'roomstats2115' => $roomstats2115,
            'roomstats2116' => $roomstats2116,
            'roomstats2117' => $roomstats2117,
            'roomstats2118' => $roomstats2118,
            'roomstats2119' => $roomstats2119,
            'roomstats2120' => $roomstats2120,
            'roomstats2121' => $roomstats2121,
            'roomstats2122' => $roomstats2122,
            'roomstats2123' => $roomstats2123,
            'roomstats2124' => $roomstats2124,
            'roomstats2125' => $roomstats2125,
            'roomstats2126' => $roomstats2126,
            'roomstats2127' => $roomstats2127,
            'roomstats2128' => $roomstats2128,
            'roomstats2129' => $roomstats2129,
            'roomstats2130' => $roomstats2130,
            'roomstats2131' => $roomstats2131,
            'roomstats2132' => $roomstats2132,
            'roomstats2133' => $roomstats2133,
            'roomstats2134' => $roomstats2134,
            'roomstats2135' => $roomstats2135,
            'roomstats2136' => $roomstats2136,
            'roomstats2137' => $roomstats2137,
            'roomstats2138' => $roomstats2138,
            'roomstats2139' => $roomstats2139,
            'roomstats2140' => $roomstats2140,
            'roomstats2141' => $roomstats2141,
            'roomstats2142' => $roomstats2142,
            'roomstats2143' => $roomstats2143,
            'roomstats2144' => $roomstats2144,
            'roomstats2145' => $roomstats2145,
            'roomstats2146' => $roomstats2146,
            'roomstats2147' => $roomstats2147,
            'roomstats2148' => $roomstats2148,
            'roomstats2149' => $roomstats2149,
            'roomstats2150' => $roomstats2150,
            'roomstats2151' => $roomstats2151,
            'roomstats2152' => $roomstats2152,
            'roomstats2153' => $roomstats2153,
            'roomstats2154' => $roomstats2154,
            'roomstats2155' => $roomstats2155,
            'roomstats2156' => $roomstats2156,
            'roomstats2157' => $roomstats2157,
            'roomstats2158' => $roomstats2158,
            'roomstats2159' => $roomstats2159,
            'roomstats2160' => $roomstats2160,
            'roomstats2161' => $roomstats2161,
            'roomstats2162' => $roomstats2162,
            'roomstats2163' => $roomstats2163,
            'roomstats2164' => $roomstats2164,
            'roomstats2165' => $roomstats2165,
            'roomstats2166' => $roomstats2166,
            'roomstats2167' => $roomstats2167,
            'roomstats2168' => $roomstats2168,
            'roomstats2169' => $roomstats2169,
            'roomstats2170' => $roomstats2170,
            'roomstats2171' => $roomstats2171,
            'roomstats2172' => $roomstats2172,
            'roomstats2173' => $roomstats2173,
            'roomstats2174' => $roomstats2174,
            'roomstats2175' => $roomstats2175,
            'roomstats2176' => $roomstats2176,
            'roomstats2177' => $roomstats2177,
            'roomstats2178' => $roomstats2178,
            'roomstats2179' => $roomstats2179,
            'roomstats2180' => $roomstats2180,
            'roomstats2181' => $roomstats2181,
            'roomstats2182' => $roomstats2182,
            'roomstats2183' => $roomstats2183,
            'roomstats2184' => $roomstats2184,
            'roomstats2185' => $roomstats2185,
            'roomstats2186' => $roomstats2186,
            'roomstats2187' => $roomstats2187,
        ]);
    }

    public function actionRoomHistoryReport()
    {
        $today = date('Y-m-d');
        $roomHistoryReport = RoomBooking::find()
            ->where(['<=','time_in',DATE($today)])
            ->andWhere(['>','time_out',DATE($today)])
            ->all();
        return $this->render('room-history-report', [
            'roomHistoryReport' => $roomHistoryReport,
        ]);
    }

    public function actionArrivalDepartureReport()
    {
        $today = date('Y-m-d');
        $arrivalReport = RoomBooking::find()
            ->where(['=','time_in',DATE($today)])
            ->all();
        $arrivalReportTotal = RoomBooking::find()
            ->where(['=','time_in',DATE($today)])
            ->count();
        $departureReport = RoomBooking::find()
            ->where(['=','time_out',DATE($today)])
            ->all();
        $departureReportTotal = RoomBooking::find()
            ->where(['=','time_out',DATE($today)])
            ->count();
        $stayOverReport = RoomBooking::find()
            ->where(['<=','time_in',DATE($today)])
            ->andWhere(['>','time_out',DATE($today)])
            ->andWhere(['!=','time_in',DATE($today)])
            ->all();
        $stayOverReportTotal = RoomBooking::find()
            ->where(['<=','time_in',DATE($today)])
            ->andWhere(['>','time_out',DATE($today)])
            ->andWhere(['!=','time_in',DATE($today)])
            ->count();
        return $this->render('arrival-departure-report', [
            'arrivalReport' => $arrivalReport,
            'departureReport' => $departureReport,
            'stayOverReport' => $stayOverReport,
            'arrivalReportTotal' => $arrivalReportTotal,
            'departureReportTotal' => $departureReportTotal,
            'stayOverReportTotal' => $stayOverReportTotal,
        ]);
    }

    public function actionGuestInhouseReport()
    {
        $today = date('Y-m-d');
        $guestInhouseReport = RoomBooking::find()
            ->where(['<=','time_in',DATE($today)])
            ->andWhere(['>','time_out',DATE($today)])
            ->all();
        $guestInhouseReportTotal = RoomBooking::find()
            ->where(['<=','time_in',DATE($today)])
            ->andWhere(['>','time_out',DATE($today)])
            ->count();
        return $this->render('guest-inhouse-report', [
            'guestInhouseReport' => $guestInhouseReport,
            'guestInhouseReportTotal' => $guestInhouseReportTotal,
        ]);
    }

    public function actionRoomChart()
    {
                $day = date('Y-m-d');
                $todaysdate = strtotime($day);
                $today = date("D", $todaysdate);
                if ($today == 'Sun') {
                $day1 = $day;
                $day2 = DATE($day) +1;
                $day3 = DATE($day) +2;
                $day4 = DATE($day) +3;
                $day5 = DATE($day) +4;
                $day6 = DATE($day) +5;
                $day7 = DATE($day) +6;
                } elseif ($today == 'Mon') {
                $day1 = date('Y-m-d', strtotime(' -1 day')); 
                $day2 = $day;
                $day3 = date('Y-m-d', strtotime(' +1 day')); 
                $day4 = date('Y-m-d', strtotime(' +2 day')); 
                $day5 = date('Y-m-d', strtotime(' +3 day')); 
                $day6 = date('Y-m-d', strtotime(' +4 day'));
                $day7 = date('Y-m-d', strtotime(' +5 day'));      
                }elseif ($today == 'Tue') {
                $day1 = date('Y-m-d', strtotime(' -2 day')); 
                $day2 = date('Y-m-d', strtotime(' -1 day')); 
                $day3 = $day;
                $day4 = date('Y-m-d', strtotime(' +1 day')); 
                $day5 = date('Y-m-d', strtotime(' +2 day')); 
                $day6 = date('Y-m-d', strtotime(' +3 day')); 
                $day7 = date('Y-m-d', strtotime(' +4 day'));                  
                }elseif ($today == 'Wed') {
                $day1 = date('Y-m-d', strtotime(' -3 day'));
                $day2 = date('Y-m-d', strtotime(' -2 day')); 
                $day3 = date('Y-m-d', strtotime(' -1 day')); 
                $day4 = $day;
                $day5 = date('Y-m-d', strtotime(' +1 day')); 
                $day6 = date('Y-m-d', strtotime(' +2 day')); 
                $day7 = date('Y-m-d', strtotime(' +3 day'));                 
                }elseif ($today == 'Thu') {
                $day1 = date('Y-m-d', strtotime(' -4 day'));
                $day2 = date('Y-m-d', strtotime(' -3 day'));
                $day3 = date('Y-m-d', strtotime(' -2 day')); 
                $day4 = date('Y-m-d', strtotime(' -1 day')); 
                $day5 = $day;
                $day6 = date('Y-m-d', strtotime(' +1 day')); 
                $day7 = date('Y-m-d', strtotime(' +2 day'));                  
                }elseif ($today == 'Fri') {
                $day1 = date('Y-m-d', strtotime(' -5 day'));
                $day2 = date('Y-m-d', strtotime(' -4 day'));
                $day3 = date('Y-m-d', strtotime(' -3 day'));
                $day4 = date('Y-m-d', strtotime(' -2 day')); 
                $day5 = date('Y-m-d', strtotime(' -1 day')); 
                $day6 = $day; 
                $day7 = date('Y-m-d', strtotime(' +1 day'));                  
                }elseif ($today == 'Sat') {
                $day1 = date('Y-m-d', strtotime(' -6 day'));
                $day2 = date('Y-m-d', strtotime(' -5 day'));
                $day3 = date('Y-m-d', strtotime(' -4 day'));
                $day4 = date('Y-m-d', strtotime(' -3 day'));
                $day5 = date('Y-m-d', strtotime(' -2 day')); 
                $day6 = date('Y-m-d', strtotime(' -1 day')); 
                $day7 = $day;                
                }
                for ($i=1; $i <=187 ; $i++) { 
        $monday[] = RoomBooking::find()
            ->where(['>','time_out',DATE($day1)])
            ->andWhere(['=','room_id',$i])
            ->all();
        $tuesday = RoomBooking::find()
            ->where(['<=','time_in',DATE($day2)])
            ->andWhere(['>','time_out',DATE($day2)])
            ->andWhere(['=','room_id',$i])
            ->all();
        $wednesday = RoomBooking::find()
            ->where(['<=','time_in',DATE($day3)])
            ->andWhere(['>','time_out',DATE($day3)])
            ->andWhere(['=','room_id',$i])
            ->all();
        $thursday = RoomBooking::find()
            ->where(['<=','time_in',DATE($day4)])
            ->andWhere(['>','time_out',DATE($day4)])
            ->andWhere(['=','room_id',$i])
            ->all();
        $friday = RoomBooking::find()
            ->where(['<=','time_in',DATE($day5)])
            ->andWhere(['>','time_out',DATE($day5)])
            ->andWhere(['=','room_id',$i])
            ->all();
        $saturday = RoomBooking::find()
            ->where(['<=','time_in',DATE($day6)])
            ->andWhere(['>','time_out',DATE($day6)])
            ->andWhere(['=','room_id',$i])
            ->all();
        $sunday = RoomBooking::find()
            ->where(['<=','time_in',DATE($day7)])
            ->andWhere(['>','time_out',DATE($day7)])
            ->andWhere(['=','room_id',$i])
            ->all();
        }
        $roomList = Room::find()
            ->all();
        return $this->render('room-chart', [
            'monday' => $monday,
            'tuesday' => $tuesday,
            'wednesday' => $wednesday,
            'thursday' => $thursday,
            'friday' => $friday,
            'saturday' => $saturday,
            'sunday' => $sunday,
            'roomList' => $roomList,
        ]);
    }

    public function actionRoomAvailabilityReport()
    {
        $today = date('Y-m-d');
        $occupied = Room::find()
            ->where(['=','status',2])
            ->all();
        $outOfService = Room::find()
            ->where(['=','status',6])
            ->all();
        $outOfOrder = Room::find()
            ->where(['=','status',4])
            ->all();
        $vacantButDirty = Room::find()
            ->where(['=','status',7])
            ->all();
        $unoccupied = Room::find()
            ->where(['=','status',5])
            ->all();
        return $this->render('room-availability-report', [
            'occupied' => $occupied,
            'outOfOrder' => $outOfOrder,
            'outOfService' => $outOfService,
            'vacantButDirty' => $vacantButDirty,
            'unoccupied' => $unoccupied,
        ]);
    }

    /**
     * Deletes an existing room model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the room model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return room the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = room::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
