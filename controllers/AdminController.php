<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

class AdminController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actionSay($message = 'Hello')
    {
        return $this->render('say', ['message' => $message]);
    }
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
		$this->layout = 'main';
        return $this->render('dashboard');
    }

    /**
     * Displays reports.
     *
     * @return string
     */
    public function actionReports()
    {
        $this->layout = 'main';
        return $this->render('reports');
    }

    /**
     * Displays records page.
     *
     * @return string
     */
    public function actionRecords()
    {
        $this->layout = 'main';
        return $this->render('records');
    }

    /**
     * Displays ledgers.
     *
     * @return string
     */
    public function actionFolio()
    {
        $this->layout = 'main';
        return $this->render('folio');
    }
}
