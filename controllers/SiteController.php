<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\States;
use yii\helpers\Url;
use yii\helpers\Json;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actionSay($message = 'Hello')
    {
        return $this->render('say', ['message' => $message]);
    }
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
		$this->layout = 'websiteindex';
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
		$this->layout = 'websiteindex';
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
		$this->layout = 'adminlogin';
        if (!Yii::$app->user->isGuest) {
            return Yii::$app->response->redirect(Url::to(['/admin']));
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return Yii::$app->response->redirect(Url::to(['/admin']));
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
		$this->layout = 'websiteindex';
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
		$this->layout = 'websitemain';
        return $this->render('about');
    }

    /**
     * Displays price page.
     *
     * @return string
     */
    public function actionPrice()
    {
        $this->layout = 'websitemain';
        return $this->render('price');
    }

    /**
     * Displays gallery page.
     *
     * @return string
     */
    public function actionGallery()
    {
        $this->layout = 'websitemain';
        return $this->render('gallery');
    }
	
	public function actionState()
    {
        $states = new States;
    	$out = [];
        $parents = Yii::$app->request->post('depdrop_all_params');
		
			$out = $states->find()->select(['state_id As id','state_name  As name'])->where(['country_id'=>$parents])->asArray()->all(); 
			echo Json::encode(['output'=>$out, 'selected'=>'1']);    
        	return;
    }


    /**
     * Displays gallery page.
     *
     * @return string
     */
    public function actionContact_us()
    {
        $this->layout = 'websitemain';
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['customerEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact_us', [
            'model' => $model,
        ]);
    }
}
