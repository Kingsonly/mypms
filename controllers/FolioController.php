<?php

namespace app\controllers;

use Yii;
use app\models\FolioCredit;
use app\models\FolioDebit;
use app\models\Folio;
use app\models\Entity;
use app\models\RestaurantPrice;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Expression;

/**
 * RestaurantBookingController implements the CRUD actions for RestaurantBooking model.
 */
class FolioController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    
	
	public function actionIndex(){
		$folioModel = new Folio();
		$getFolioDetails = $folioModel->find()->all();
		return $this->render('index', [
            'folio' => $getFolioDetails,
        ]);
	}
 
    public function actionView($id,$entity)
    {
		$fetchEntity = Entity::findOne($entity);
		$getPersonString = $fetchEntity->people->personString;
        return $this->renderAjax('view', [
            'debit' => $this->findDebitFolioModel($id),
            'credit' => $this->findCreditFolioModel($id),
			'debitSum' => FolioDebit::find()->where(['folio_id'=>$id])->sum('amount'),
            'creditSum' => FolioCredit::find()->where(['folio_id'=>$id])->sum('amount'),
			'entity' => $fetchEntity,
        ]);
    }

   
	
     protected function findDebitFolioModel($id)
    {
        if (($model = FolioDebit::find()->where(['folio_id'=>$id])->all()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	protected function findCreditFolioModel($id)
    {
        if (($model = FolioCredit::find()->where(['folio_id'=>$id])->all()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
