<?php

namespace app\controllers;

use Yii;
use app\models\Userdb;
use app\models\Person;
use app\models\Email;
use app\models\Telephone;
use app\models\Address;
use app\models\StatusType;
use app\models\Role;
use app\models\States;
use app\models\Entity;
use app\models\Countries;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserdbController implements the CRUD actions for Userdb model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Userdb models.
     * @return mixed
     */
    public function actionIndex()
    {

        $viewModel = Userdb::find()->all();

        return $this->render('index', [
            'viewModel' => $viewModel,
        ]);
    }

    /**
     * Displays a single Userdb model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Userdb model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Userdb();
        $personModel = new Person();
        $emailModel = new Email();
        $telephoneModel = new Telephone();
        $addressModel = new Address();
        $roleModel = new Role();
        $statusTypeModel = new StatusType();
        $statesModel = new States();
        $countriesModel = new Countries();
        $entityModel = new Entity();
		
		// check to make sure no form is empty  if valid create a new entity id based on specifications 
        if ($model->load(Yii::$app->request->post()) && $personModel->load(Yii::$app->request->post()) && $emailModel->load(Yii::$app->request->post()) && $telephoneModel->load(Yii::$app->request->post())  && $addressModel->load(Yii::$app->request->post())  && $model->validate() && $personModel->validate() && $emailModel->validate() && $telephoneModel->validate()  && $addressModel->validate() ) {
			$entityModel->type = 'user';
			if($entityModel->save()){
				$entityModelid = $entityModel->entity_id;
				
				//assing entity id to all entity component
				$model->entity_id = $entityModelid;
				$personModel->entity_id = $entityModelid;
        		$emailModel->entity_id = $entityModelid;
        		$telephoneModel->entity_id = $entityModelid;
        		$addressModel->entity_id = $entityModelid;
				
				// save data into the db
				$model->save(false);
				$personModel->save(false);
        		$emailModel->save(false);
        		$telephoneModel->save(false);
        		$addressModel->save(false);
       			return $this->redirect(['view', 'id' => $model->user_id]);
			}
            
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
				'personModel' => $personModel,
				'emailModel' => $emailModel, 
				'telephoneModel' => $telephoneModel, 
				'addressModel' => $addressModel, 
				'roleModel' => $roleModel, 
				'statusTypeModel' => $statusTypeModel, 
				'statesModel' => $statesModel, 
				'countriesModel' => $countriesModel, 
            ]);
        }
    }

    /**
     * Updates an existing Userdb model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $personModel = new Person($id);
        $emailModel = new Email();
        $telephoneModel = new Telephone();
        $addressModel = new Address();
        $roleModel = new Role();
        $statusTypeModel = new StatusType();
        $statesModel = new States();
        $countriesModel = new Countries();
        $entityModel = new Entity();

        // check to make sure no form is empty  if valid create a new entity id based on specifications 
        if ($model->load(Yii::$app->request->post()) && $personModel->load(Yii::$app->request->post()) && $emailModel->load(Yii::$app->request->post()) && $telephoneModel->load(Yii::$app->request->post())  && $addressModel->load(Yii::$app->request->post())  && $model->validate() && $personModel->validate() && $emailModel->validate() && $telephoneModel->validate()  && $addressModel->validate() ) {
            $entityModel->type = 'user';
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
                $entityModelid = $entityModel->entity_id;
                
                //assing entity id to all entity component
                $model->entity_id = $entityModelid;
                $personModel->entity_id = $entityModelid;
                $emailModel->entity_id = $entityModelid;
                $telephoneModel->entity_id = $entityModelid;
                $addressModel->entity_id = $entityModelid;
                
                // save data into the db
                $model->save(false);
                $personModel->save(false);
                $emailModel->save(false);
                $telephoneModel->save(false);
                $addressModel->save(false);
            return $this->redirect(['view', 'id' => $model->user_id]);
        }
        } else {
            return $this->render('update', [
                'model' => $model,
                'personModel' => $personModel,
                'emailModel' => $emailModel, 
                'telephoneModel' => $telephoneModel, 
                'addressModel' => $addressModel, 
                'roleModel' => $roleModel, 
                'statusTypeModel' => $statusTypeModel, 
                'statesModel' => $statesModel, 
                'countriesModel' => $countriesModel, 
            ]);
        }
    }

    /**
     * Deletes an existing Userdb model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Userdb model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Userdb the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Userdb::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
