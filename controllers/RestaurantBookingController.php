<?php

namespace app\controllers;

use Yii;
use app\models\RestaurantBooking;
use app\models\RestaurantProduct;
use app\models\Telephone;
use app\models\Guest;
use app\models\RoomBooking;
use app\models\RestaurantPrice;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Expression;
use app\models\ReciptRelationship;
use app\models\Recipt;

/**
 * RestaurantBookingController implements the CRUD actions for RestaurantBooking model.
 */
class RestaurantBookingController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RestaurantBooking models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new RestaurantBooking();
        $viewModel = RestaurantBooking::find()->all();
        $restaurantProductModel = RestaurantProduct::find()->all();
        
        $restaurantPriceModel = RestaurantPrice::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
        return $this->render('index', [
            'model' => $model,
            'viewModel' => $viewModel,
            'restaurantProductModel' => $restaurantProductModel,
            'restaurantPriceModel' => $restaurantPriceModel,
        ]);
        } else {
            return $this->render('index', [
            'model' => $model,
            'viewModel' => $viewModel,
            'restaurantProductModel' => $restaurantProductModel,
            'restaurantPriceModel' => $restaurantPriceModel,
            ]);
        }
    }

    /**
     * Displays a single RestaurantBooking model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RestaurantBooking model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RestaurantBooking();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->booking_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing RestaurantBooking model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->booking_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RestaurantBooking model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
	
	public function actionAddtocart($id,$quantity)
	{
		

		$model = RestaurantProduct::findOne($id);
		if ($model) {
			\Yii::$app->cart->put($model, $quantity);
			//return $this->redirect(['cart-view']);
		}else{
			throw new NotFoundHttpException();
		}
		
	}
	
	public function actionRemovefromcart($id)
	{
		

		$model = RestaurantProduct::findOne($id);
		if ($model) {
			\Yii::$app->cart->remove($model);
			//return $this->redirect(['cart-view']);
		}else{
			throw new NotFoundHttpException();
		}
		
	}
	
	public function actionSearchGuestNumber($number)
	{
		
		$telephoneModel = new Telephone();
		$guestModel = new Guest();
		$roomBookingModel = new RoomBooking();
		
		$findTelephone = $telephoneModel
			->find()->select(['entity_id'])
			->where(['number'=>$number])
			->one();
		
		$data = [];
		//$numberDetails = !empty($findTelephone->entity_id)?$findTelephone->entity_id:'false';
		if(!empty($findTelephone->entity_id) ){
			
			$getGuestEntityId = $guestModel->find()
				->where(['entity_id'=>$findTelephone->entity_id])
				->one();
			
			$getPresentGuestBooking = $roomBookingModel
				->find()
				->where(['entity_id' => $findTelephone->entity_id ])
				->orderBy(['booking_id' => SORT_ASC])
				->one();
			
			\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			
			$data['roomId'] = $getPresentGuestBooking->room_id;
			$data['entityId'] = $getGuestEntityId->entity_id;
			//$data['guestName'] = $getGuestEntityId->person;
			
			
			return $data;
			
			
		} else{
			$data=0;
			return $data;
		}
		
	}
	
	
	public function actionClearcart()
	{
		\Yii::$app->cart->removeAll();
		return 'cart Cleared';	
	}
	
	public function actionPosCheckout()
	{
		$model = new RestaurantBooking();
		$model->user_id = Yii::$app->user->identity->id; ;
		$model->time_of_order = new Expression('NOW()') ;
		if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			\Yii::$app->cart->removeAll();
			return $this->redirect(['reciept', 'id' => $model->reciptId]);
            //return $this->redirect(['view', 'id' => $model->booking_id]);
		} else {
			return $this->renderAjax('checkout',[
				'model'=>$model,
			]);
		}
	}
	
	public function actionPosCheckoutNonGuest()
	{
		$model = new RestaurantBooking();
		$model->user_id = Yii::$app->user->identity->id; ;
		$model->time_of_order = new Expression('NOW()') ;
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->cart->removeAll();
			return $this->redirect(['reciept', 'id' => $model->reciptId]);
            //return $this->redirect(['view', 'id' => $model->booking_id]);
		} else {
			return $this->renderAjax('checkout',[
				'model'=>$model,
			]);
		}
	}

	public function actionReciept($id)
	{
		$model = new ReciptRelationship;
		$reciptmodel = new Recipt;
		$roomBookingModel = new RestaurantBooking;
		$fetchsenarioId = $model->find()->where(['recipt_id'=>$id])->one();
		$fetchReceiptDetails = $reciptmodel->find()->where(['recipt_id'=>$id])->one();
		$getSenarioId = $fetchsenarioId->senarior_id;
		$fetchRoomBookingDetails = $roomBookingModel->findOne($getSenarioId);

		return $this->render('reciept', [
			'model' => $fetchsenarioId,
			'getRoomBookingDetails' => $fetchRoomBookingDetails,
			'getReceiptDetails' => $fetchReceiptDetails,
			'amountDue' => Yii::$app->session->getFlash('amountDue') ,

		]);
	}
	
	
    /**
     * Finds the RestaurantBooking model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RestaurantBooking the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RestaurantBooking::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
