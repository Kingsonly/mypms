<?php

namespace app\controllers;

use Yii;
use app\models\RoomBooking;
use app\models\Email;
use app\models\Address;
use app\models\Telephone;
use app\models\Person;
use app\models\States;
use app\models\Entity;

use app\models\ReciptRelationship;
use app\models\Recipt;
use app\models\Countries;
use app\models\StatusType;
use app\models\RoomType;
use app\models\RoomTariff;
use app\models\Guest;
use app\models\Room;
use app\models\Reservations;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Expression;

/**
 * ReservationsController implements the CRUD actions for Reservations model.
 */
class ReservationsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
        'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
        'delete' => ['POST'],
        ],
        ],
        ];
    }

    /**
     * Lists all Reservations models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Reservations::find(),
            ]);

        $thisYear = date('Y');
        $totalJanRes = Reservations::find()
        ->where('YEAR(datetime) = '.$thisYear.' AND MONTH(datetime) = 01')
        ->andWhere('time_in' > 'datetime')->count();

        $totalFebRes = Reservations::find()
        ->where('YEAR(datetime) = '.$thisYear.' AND MONTH(datetime) = 02')
        ->andWhere('time_in' > 'datetime')->count();

        $totalMarRes = Reservations::find()
        ->where('YEAR(datetime) = '.$thisYear.' AND MONTH(datetime) = 03')
        ->andWhere('time_in' > 'datetime')->count();

        $totalAprRes = Reservations::find()
        ->where('YEAR(datetime) = '.$thisYear.' AND MONTH(datetime) = 04')
        ->andWhere('time_in' > 'datetime')->count();

        $totalMayRes = Reservations::find()
        ->where('YEAR(datetime) = '.$thisYear.' AND MONTH(datetime) = 05')
        ->andWhere('time_in' > 'datetime')->count();

        $totalJunRes = Reservations::find()
        ->where('YEAR(datetime) = '.$thisYear.' AND MONTH(datetime) = 06')
        ->andWhere('time_in' > 'datetime')->count();

        $totalJulRes = Reservations::find()
        ->where('YEAR(datetime) = '.$thisYear.' AND MONTH(datetime) = 07')
        ->andWhere('time_in' > 'datetime')->count();

        $totalAugRes = Reservations::find()
        ->where('YEAR(datetime) = '.$thisYear.' AND MONTH(datetime) = 08')
        ->andWhere('time_in' > 'datetime')->count();

        $totalSepRes = Reservations::find()
        ->where('YEAR(datetime) = '.$thisYear.' AND MONTH(datetime) = 09')
        ->andWhere('time_in' > 'datetime')->count();

        $totalOctRes = Reservations::find()
        ->where('YEAR(datetime) = '.$thisYear.' AND MONTH(datetime) = 10')
        ->andWhere('time_in' > 'datetime')->count();

        $totalNovRes = Reservations::find()
        ->where('YEAR(datetime) = '.$thisYear.' AND MONTH(datetime) = 11')
        ->andWhere('time_in' > 'datetime')->count();

        $totalDecRes = Reservations::find()
        ->where('YEAR(datetime) = '.$thisYear.' AND MONTH(datetime) = 12')
        ->andWhere('time_in' > 'datetime')->count();

        $model = new Reservations();
        $roomModel = new \app\models\Room;

        $viewModel = Reservations::find()
        ->where(['>','time_in','datetime'])
        ->andWhere(['>','time_in',new Expression('NOW()')])
        ->all();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'totalJanRes' => $totalJanRes,
            'totalFebRes' => $totalFebRes,
            'totalMarRes' => $totalMarRes,
            'totalAprRes' => $totalAprRes,
            'totalMayRes' => $totalMayRes,
            'totalJunRes' => $totalJunRes,
            'totalJulRes' => $totalJulRes,
            'totalAugRes' => $totalAugRes,
            'totalSepRes' => $totalSepRes,
            'totalOctRes' => $totalOctRes,
            'totalNovRes' => $totalNovRes,
            'totalDecRes' => $totalDecRes,
            'model' => $model,
            'viewModel' => $viewModel,
            'now' => new Expression('NOW()')
            ]);	
    }


    /**
     * Displays a single Reservations model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            ]);
    }

    /**
     * Creates a new Reservations model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
     public function actionCreate()
    {
        $model = new RoomBooking();
        $personModel = new Person();
        $emailModel = new Email();
        $telephoneModel = new Telephone();
        $addressModel = new Address();
        $statesModel = new States();
        $countriesModel = new Countries();
        $statusTypeModel = new StatusType();
        $guestModel = new Guest();
        $entityModel = new Entity();
        $receiptModel = new Recipt();
        $roomModel = new Room();
        $ReciptRelationshipModel = new ReciptRelationship();
        //$model->isNotNew = false;
        
        // check to make sure no form is empty  if valid create a new entity id based on specifications 
        if ($model->load(Yii::$app->request->post()) && $personModel->load(Yii::$app->request->post()) && $guestModel->load(Yii::$app->request->post()) && $emailModel->load(Yii::$app->request->post()) && $telephoneModel->load(Yii::$app->request->post())  && $addressModel->load(Yii::$app->request->post())  && $model->validate() && $personModel->validate() && $emailModel->validate() && $telephoneModel->validate()  && $addressModel->validate() && $guestModel->validate() ) {
            $timeIn = new \DateTime($model->time_in);
            $timeOut = new \DateTime($model->time_out);
            $interval = date_diff($timeIn, $timeOut);
            $model->number_of_nights = (int)$interval->format('%d days');
            $dateNow = new Expression('NOW()'); // use for the time gueest was created 
            $userId = Yii::$app->user->identity->id;

            //assing entity id to all entity component
            $guestModel->entity_id = $model->entityIds;//entityModelId;
            $guestModel->registration_date = $dateNow;
            $guestModel->user_id = $userId;
            $guestModel->save(false);// save the data for guest in the database 
            $entityId = $guestModel->entity_id ;

            // assing relevat ids eg like guest id 
            $personModel->entity_id = $entityId;
            $emailModel->entity_id = $entityId;
            $telephoneModel->entity_id = $entityId;
            $addressModel->entity_id = $entityId;
            $model->user_id = $userId;
            $model->entity_id = $entityId;
            $model->tariff_id = 1; // take not change room tarrrif id to the right content 

            // save data into the db

            $personModel->save();
            $emailModel->save();
            $telephoneModel->save();
            $addressModel->save();


            //select room to update status
            $roomModel = Room::findOne($model->room_id);
            // make change of status based on status type 
            $selectStatusType = $statusTypeModel->find()->where(['status_title'=>'occupied'])->one();
            $roomModel->status = $selectStatusType->status_id;
            $roomModel->save();
            $model->save();
            /*
                /// insert value into the receipt table
                $receiptModel->receipt_issue_date = new Expression('NOW()');
                $receiptModel->recipt_number =rand(3009,10000000);
                $receiptModel->amount = $model->amount_paid ;
                $receiptModel->save();
                // create a recipt relationship
                $ReciptRelationshipModel->recipt_id = $receiptModel->recipt_id;
                $ReciptRelationshipModel->senarior_id = $model->booking_id;
                $ReciptRelationshipModel->senarior_type = 'roombooking';
                $ReciptRelationshipModel->save();                   
                */
            return $this->redirect(['room-booking/view', 'id' => $model->reciptId]);
            
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
                'roomModel' => $roomModel,
                'personModel' => $personModel,
                'emailModel' => $emailModel,
                'telephoneModel' => $telephoneModel,
                'addressModel' => $addressModel,
                'statesModel' => $statesModel,
                'countriesModel' => $countriesModel,
                'statusTypeModel' => $statusTypeModel,
                'guestModel' => $guestModel,
            ]);
        }
    }

    /**
     * Updates an existing Reservations model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->booking_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                ]);
        }
    }
    /**
     * Updates an existing Reservations model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionReservations($month)
    {

        $thisYear = date('Y');
        if ($month == 'JANUARY') {
            $thisMonth = 01;
        } elseif ($month == 'FEBRUARY') {
            $thisMonth = 02;
        } elseif ($month == 'MARCH') {
            $thisMonth = 03;
        } elseif ($month == 'APRIL') {
            $thisMonth = 04;
        } elseif ($month == 'MAY') {
            $thisMonth = 05;
        } elseif ($month == 'JUNE') {
            $thisMonth = 06;
        } elseif ($month == 'JULY') {
            $thisMonth = 07;
        } elseif ($month == 'AUGUST') {
            $thisMonth = 8;
        } elseif ($month == 'SEPTEMBER') {
            $thisMonth = 9;
        } elseif ($month == 'OCTOBER') {
            $thisMonth = 10;
        } elseif ($month == 'NOVEMBER') {
            $thisMonth = 11;
        } elseif ($month == 'DECEMBER') {
            $thisMonth = 12;
        }
        $firstWeek = Reservations::find()
        ->where('YEAR(datetime) = '.$thisYear.' AND DAY(datetime) >= 01 AND DAY(datetime) <=07')
        ->andWhere('MONTH(datetime) = '.$thisMonth)
        ->andWhere('time_in' > 'datetime')
        ->count();
        $secondWeek = Reservations::find()
        ->where('YEAR(datetime) = '.$thisYear.' AND DAY(datetime) >= 08 AND DAY(datetime) <=14')
        ->andWhere('MONTH(datetime) = '.$thisMonth)
        ->andWhere('time_in' > 'datetime')
        ->count();
        $thirdWeek = Reservations::find()
        ->where('YEAR(datetime) = '.$thisYear.' AND DAY(datetime) >= 15 AND DAY(datetime) <=21')
        ->andWhere('MONTH(datetime) = '.$thisMonth)
        ->andWhere('time_in' > 'datetime')
        ->count();
        $fourthWeek = Reservations::find()
        ->where('YEAR(datetime) = '.$thisYear.' AND DAY(datetime) >= 22 AND DAY(datetime) <=28')
        ->andWhere('MONTH(datetime) = '.$thisMonth)
        ->andWhere('time_in' > 'datetime')
        ->count();
        $lastWeek = Reservations::find()
        ->where('YEAR(datetime) = '.$thisYear.' AND DAY(datetime) >= 29 AND DAY(datetime) <=31')
        ->andWhere('MONTH(datetime) = '.$thisMonth)
        ->andWhere('time_in' > 'datetime')
        ->count();
        return $this->render('reservations', [
            'firstWeek' => $firstWeek,
            'secondWeek' => $secondWeek,
            'thirdWeek' => $thirdWeek,
            'fourthWeek' => $fourthWeek,
            'lastWeek' => $lastWeek
            ]);
    }


    public function actionArrivalListReport($startDate,$endDate)
    {

        $arrivalListModel = Reservations::find()
        ->where(['>','time_in',$startDate])
        ->andWhere(['<','time_out',$endDate])
        ->all();
        return $this->render('arrival-list-report', [
            'arrivalListModel' => $arrivalListModel,
            ]);
    }

    public function actionAvailabilityChart()
    {
        $day1 = date('Y-m-').'01';
        $day2 = date('Y-m-').'02';
        $day3 = date('Y-m-').'03';
        $day4 = date('Y-m-').'04';
        $day5 = date('Y-m-').'05';
        $day6 = date('Y-m-').'06';
        $day7 = date('Y-m-').'07';
        $day8 = date('Y-m-').'08';
        $day9 = date('Y-m-').'09';
        $day10 = date('Y-m-').'10';
        $day11 = date('Y-m-').'11';
        $day12 = date('Y-m-').'12';
        $day13 = date('Y-m-').'13';
        $day14 = date('Y-m-').'14';
        $day15 = date('Y-m-').'15';
        $day16 = date('Y-m-').'16';
        $day17 = date('Y-m-').'17';
        $day18 = date('Y-m-').'18';
        $day19 = date('Y-m-').'19';
        $day20 = date('Y-m-').'20';
        $day21 = date('Y-m-').'21';
        $day22 = date('Y-m-').'22';
        $day23 = date('Y-m-').'23';
        $day24 = date('Y-m-').'24';
        $day25 = date('Y-m-').'25';
        $day26 = date('Y-m-').'26';
        $day27 = date('Y-m-').'27';
        $day28 = date('Y-m-').'28';
        $day29 = date('Y-m-').'29';
        $day30 = date('Y-m-').'30';
        $day31 = date('Y-m-').'31';
        $day32 = date('Y-m-').'32';

        for ($y=1; $y <= 187; $y++) { 
        ${'roomcheck'.$y} = array();
        for ($i = 1; $i<=31; $i++) {
            $x = $i+1;
            ${'roomcheck'.$y}[] = RoomBooking::find()
            ->where('room_id = '.$y)
            ->andwhere(['>=','time_in', ${'day'.$i}])
            ->andWhere(['<','time_in', ${'day'.$x}])
            ->count();
        }
        }

      

        $roomsListModel = Room::find()->all();
        $availabilityChartModel = Reservations::find()
            // ->where('time_in >= '.$start.' AND time_out <= '.$end)
        ->all();
        return $this->render('availability-chart', [
            'roomsListModel' => $roomsListModel,
            'roomcheck1' => $roomcheck1,
            'roomcheck2' => $roomcheck2,
            'roomcheck3' => $roomcheck3,
            'roomcheck4' => $roomcheck4,
            'roomcheck5' => $roomcheck5,
            'roomcheck6' => $roomcheck6,
            'roomcheck7' => $roomcheck7,
            'roomcheck8' => $roomcheck8,
            'roomcheck9' => $roomcheck9,
            'roomcheck10' => $roomcheck10,
            'roomcheck11' => $roomcheck11,
            'roomcheck12' => $roomcheck12,
            'roomcheck13' => $roomcheck13,
            'roomcheck14' => $roomcheck14,
            'roomcheck15' => $roomcheck15,
            'roomcheck16' => $roomcheck16,
            'roomcheck17' => $roomcheck17,
            'roomcheck18' => $roomcheck18,
            'roomcheck19' => $roomcheck19,
            'roomcheck20' => $roomcheck20,
            'roomcheck21' => $roomcheck21,
            'roomcheck22' => $roomcheck22,
            'roomcheck23' => $roomcheck23,
            'roomcheck24' => $roomcheck24,
            'roomcheck25' => $roomcheck25,
            'roomcheck26' => $roomcheck26,
            'roomcheck27' => $roomcheck27,
            'roomcheck28' => $roomcheck28,
            'roomcheck29' => $roomcheck29,
            'roomcheck30' => $roomcheck30,
            'roomcheck31' => $roomcheck31,
            'roomcheck32' => $roomcheck32,
            'roomcheck33' => $roomcheck33,
            'roomcheck34' => $roomcheck34,
            'roomcheck35' => $roomcheck35,
            'roomcheck36' => $roomcheck36,
            'roomcheck37' => $roomcheck37,
            'roomcheck38' => $roomcheck38,
            'roomcheck39' => $roomcheck39,
            'roomcheck40' => $roomcheck40,
            'roomcheck41' => $roomcheck41,
            'roomcheck42' => $roomcheck42,
            'roomcheck43' => $roomcheck43,
            'roomcheck44' => $roomcheck44,
            'roomcheck45' => $roomcheck45,
            'roomcheck46' => $roomcheck46,
            'roomcheck47' => $roomcheck47,
            'roomcheck48' => $roomcheck48,
            'roomcheck49' => $roomcheck49,
            'roomcheck50' => $roomcheck50,
            'roomcheck51' => $roomcheck51,
            'roomcheck52' => $roomcheck52,
            'roomcheck53' => $roomcheck53,
            'roomcheck54' => $roomcheck54,
            'roomcheck55' => $roomcheck55,
            'roomcheck56' => $roomcheck56,
            'roomcheck57' => $roomcheck57,
            'roomcheck58' => $roomcheck58,
            'roomcheck59' => $roomcheck59,
            'roomcheck60' => $roomcheck60,
            'roomcheck61' => $roomcheck61,
            'roomcheck62' => $roomcheck62,
            'roomcheck63' => $roomcheck63,
            'roomcheck64' => $roomcheck64,
            'roomcheck65' => $roomcheck65,
            'roomcheck66' => $roomcheck66,
            'roomcheck67' => $roomcheck67,
            'roomcheck68' => $roomcheck68,
            'roomcheck69' => $roomcheck69,
            'roomcheck70' => $roomcheck70,
            'roomcheck71' => $roomcheck71,
            'roomcheck72' => $roomcheck72,
            'roomcheck73' => $roomcheck73,
            'roomcheck74' => $roomcheck74,
            'roomcheck75' => $roomcheck75,
            'roomcheck76' => $roomcheck76,
            'roomcheck77' => $roomcheck77,
            'roomcheck78' => $roomcheck78,
            'roomcheck79' => $roomcheck79,
            'roomcheck80' => $roomcheck80,
            'roomcheck81' => $roomcheck81,
            'roomcheck82' => $roomcheck82,
            'roomcheck83' => $roomcheck83,
            'roomcheck84' => $roomcheck84,
            'roomcheck85' => $roomcheck85,
            'roomcheck86' => $roomcheck86,
            'roomcheck87' => $roomcheck87,
            'roomcheck88' => $roomcheck88,
            'roomcheck89' => $roomcheck89,
            'roomcheck90' => $roomcheck90,
            'roomcheck91' => $roomcheck91,
            'roomcheck92' => $roomcheck92,
            'roomcheck93' => $roomcheck93,
            'roomcheck94' => $roomcheck94,
            'roomcheck95' => $roomcheck95,
            'roomcheck96' => $roomcheck96,
            'roomcheck97' => $roomcheck97,
            'roomcheck98' => $roomcheck98,
            'roomcheck99' => $roomcheck99,
            'roomcheck100' => $roomcheck100,
            'roomcheck101' => $roomcheck101,
            'roomcheck102' => $roomcheck102,
            'roomcheck103' => $roomcheck103,
            'roomcheck104' => $roomcheck104,
            'roomcheck105' => $roomcheck105,
            'roomcheck106' => $roomcheck106,
            'roomcheck107' => $roomcheck107,
            'roomcheck108' => $roomcheck108,
            'roomcheck109' => $roomcheck109,
            'roomcheck110' => $roomcheck110,
            'roomcheck111' => $roomcheck111,
            'roomcheck112' => $roomcheck112,
            'roomcheck113' => $roomcheck113,
            'roomcheck114' => $roomcheck114,
            'roomcheck115' => $roomcheck115,
            'roomcheck116' => $roomcheck116,
            'roomcheck117' => $roomcheck117,
            'roomcheck118' => $roomcheck118,
            'roomcheck119' => $roomcheck119,
            'roomcheck120' => $roomcheck120,
            'roomcheck121' => $roomcheck121,
            'roomcheck122' => $roomcheck122,
            'roomcheck123' => $roomcheck123,
            'roomcheck124' => $roomcheck124,
            'roomcheck125' => $roomcheck125,
            'roomcheck126' => $roomcheck126,
            'roomcheck127' => $roomcheck127,
            'roomcheck128' => $roomcheck128,
            'roomcheck129' => $roomcheck129,
            'roomcheck130' => $roomcheck130,
            'roomcheck131' => $roomcheck131,
            'roomcheck132' => $roomcheck132,
            'roomcheck133' => $roomcheck133,
            'roomcheck134' => $roomcheck134,
            'roomcheck135' => $roomcheck135,
            'roomcheck136' => $roomcheck136,
            'roomcheck137' => $roomcheck137,
            'roomcheck138' => $roomcheck138,
            'roomcheck139' => $roomcheck139,
            'roomcheck140' => $roomcheck140,
            'roomcheck141' => $roomcheck141,
            'roomcheck142' => $roomcheck142,
            'roomcheck143' => $roomcheck143,
            'roomcheck144' => $roomcheck144,
            'roomcheck145' => $roomcheck145,
            'roomcheck146' => $roomcheck146,
            'roomcheck147' => $roomcheck147,
            'roomcheck148' => $roomcheck148,
            'roomcheck149' => $roomcheck149,
            'roomcheck150' => $roomcheck150,
            'roomcheck151' => $roomcheck151,
            'roomcheck152' => $roomcheck152,
            'roomcheck153' => $roomcheck153,
            'roomcheck154' => $roomcheck154,
            'roomcheck155' => $roomcheck155,
            'roomcheck156' => $roomcheck156,
            'roomcheck157' => $roomcheck157,
            'roomcheck158' => $roomcheck158,
            'roomcheck159' => $roomcheck159,
            'roomcheck160' => $roomcheck160,
            'roomcheck161' => $roomcheck161,
            'roomcheck162' => $roomcheck162,
            'roomcheck163' => $roomcheck163,
            'roomcheck164' => $roomcheck164,
            'roomcheck165' => $roomcheck165,
            'roomcheck166' => $roomcheck166,
            'roomcheck167' => $roomcheck167,
            'roomcheck168' => $roomcheck168,
            'roomcheck169' => $roomcheck169,
            'roomcheck170' => $roomcheck170,
            'roomcheck171' => $roomcheck171,
            'roomcheck172' => $roomcheck172,
            'roomcheck173' => $roomcheck173,
            'roomcheck174' => $roomcheck174,
            'roomcheck175' => $roomcheck175,
            'roomcheck176' => $roomcheck176,
            'roomcheck177' => $roomcheck177,
            'roomcheck178' => $roomcheck178,
            'roomcheck179' => $roomcheck179,
            'roomcheck180' => $roomcheck180,
            'roomcheck181' => $roomcheck181,
            'roomcheck182' => $roomcheck182,
            'roomcheck183' => $roomcheck183,
            'roomcheck184' => $roomcheck184,
            'roomcheck185' => $roomcheck185,
            'roomcheck186' => $roomcheck186,
            'roomcheck187' => $roomcheck187,
            'availabilityChartModel' => $availabilityChartModel,
            ]);
    }

    public function actionMonthlyInventoryReport()
    {
      $month = date('m');
      $firstDay = 01;
      if ($month = 9 || $month = 4 || $month = 6 || $month = 11) {
        $lastDay = 30;
    } if ($month = 2) {
        $lastDay =29;
    } if ($month != 9 || $month != 4 || $month != 6 || $month != 11 || $month != 2){
        $lastDay = 31;
    }
        $day1 = date('Y-m-').'01';
        $day2 = date('Y-m-').'02';
        $day3 = date('Y-m-').'03';
        $day4 = date('Y-m-').'04';
        $day5 = date('Y-m-').'05';
        $day6 = date('Y-m-').'06';
        $day7 = date('Y-m-').'07';
        $day8 = date('Y-m-').'08';
        $day9 = date('Y-m-').'09';
        $day10 = date('Y-m-').'10';
        $day11 = date('Y-m-').'11';
        $day12 = date('Y-m-').'12';
        $day13 = date('Y-m-').'13';
        $day14 = date('Y-m-').'14';
        $day15 = date('Y-m-').'15';
        $day16 = date('Y-m-').'16';
        $day17 = date('Y-m-').'17';
        $day18 = date('Y-m-').'18';
        $day19 = date('Y-m-').'19';
        $day20 = date('Y-m-').'20';
        $day21 = date('Y-m-').'21';
        $day22 = date('Y-m-').'22';
        $day23 = date('Y-m-').'23';
        $day24 = date('Y-m-').'24';
        $day25 = date('Y-m-').'25';
        $day26 = date('Y-m-').'26';
        $day27 = date('Y-m-').'27';
        $day28 = date('Y-m-').'28';
        $day29 = date('Y-m-').'29';
        $day30 = date('Y-m-').'30';
        $day31 = date('Y-m-').'31';
        $day32 = date('Y-m-').'32';

        $roomInventoryReport1 = array();
        for ($i = 1; $i<=$lastDay; $i++) {
            $x = $i+1;
            $roomInventoryReport1[] = RoomBooking::find()
            ->where('room_type = 1')
            ->andwhere(['>=','time_in', ${'day'.$i}])
            ->andWhere(['<','time_in', ${'day'.$x}])
            ->count();
        }

        $roomInventoryReport2 = array();
        for ($i = 1; $i<=$lastDay; $i++) {
            $x = $i+1;
            $roomInventoryReport2[] = RoomBooking::find()
            ->where('room_type = 2')
            ->andwhere(['>=','time_in', ${'day'.$i}])
            ->andWhere(['<','time_in', ${'day'.$x}])
            ->count();
        }

        $roomInventoryReport3 = array();
        for ($i = 1; $i<=$lastDay; $i++) {
            $x = $i+1;
            $roomInventoryReport3[] = RoomBooking::find()
            ->where('room_type = 3')
            ->andwhere(['>=','time_in', ${'day'.$i}])
            ->andWhere(['<','time_in', ${'day'.$x}])
            ->count();
        }

    return $this->render('monthly-inventory-report', [
        'roomInventoryReport1' => $roomInventoryReport1,
        'roomInventoryReport2' => $roomInventoryReport2,
        'roomInventoryReport3' => $roomInventoryReport3,
        ]);
}

    public function actionReservationReport()
    {
        $thisMonth = date('m');
        $reservationReport = RoomBooking::find()
        ->where(['>','time_in','datetime'])
        ->andWhere(['>','time_in',new Expression('NOW()')])
        ->andWhere('MONTH(time_in) = '.$thisMonth)
        ->all();

    return $this->render('reservation-report', [
        'reservationReport' => $reservationReport,
        ]);
    }


    /**
     * Deletes an existing Reservations model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    
    /**
     * Finds the Reservations model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Reservations the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Reservations::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
