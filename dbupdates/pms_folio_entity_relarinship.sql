-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 23, 2017 at 10:58 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mypms`
--

-- --------------------------------------------------------

--
-- Table structure for table `pms_folio_entity_relarinship`
--

CREATE TABLE `pms_folio_entity_relarinship` (
  `relationship_folio_id` int(255) NOT NULL,
  `relationship_entity_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pms_folio_entity_relarinship`
--

INSERT INTO `pms_folio_entity_relarinship` (`relationship_folio_id`, `relationship_entity_id`) VALUES
(74, 115),
(75, 116),
(76, 117),
(77, 118),
(78, 119);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pms_folio_entity_relarinship`
--
ALTER TABLE `pms_folio_entity_relarinship`
  ADD KEY `folio_id_link` (`relationship_folio_id`),
  ADD KEY `entity_id_link` (`relationship_entity_id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pms_folio_entity_relarinship`
--
ALTER TABLE `pms_folio_entity_relarinship`
  ADD CONSTRAINT `entity_id_link` FOREIGN KEY (`relationship_entity_id`) REFERENCES `pms_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `folio_id_link` FOREIGN KEY (`relationship_folio_id`) REFERENCES `pms_folio` (`folio_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
