-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 14, 2017 at 02:44 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.0.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mypms`
--

-- --------------------------------------------------------

--
-- Table structure for table `pms_status_type`
--

CREATE TABLE `pms_status_type` (
  `status_id` int(11) NOT NULL,
  `status_category` varchar(50) NOT NULL,
  `status_title` varchar(50) NOT NULL,
  `status_color` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pms_status_type`
--

INSERT INTO `pms_status_type` (`status_id`, `status_category`, `status_title`, `status_color`) VALUES
(2, 'room', 'occupied', 1),
(3, 'user', 'online', 1),
(4, 'room', 'out-of-order', 2),
(5, 'room', 'unoccupied', 3),
(6, 'room', 'out-of-service', 4),
(7, 'room', 'vacant-but-dirty', 5),
(8, 'booking', 'Single Occupancy', 1),
(9, 'booking', 'Dual Occupancy', 1),
(10, 'booking', 'Multiple Occupancy', 1),
(11, 'booking', 'Corporate', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pms_status_type`
--
ALTER TABLE `pms_status_type`
  ADD PRIMARY KEY (`status_id`),
  ADD KEY `status_color` (`status_color`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pms_status_type`
--
ALTER TABLE `pms_status_type`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `pms_status_type`
--
ALTER TABLE `pms_status_type`
  ADD CONSTRAINT `status` FOREIGN KEY (`status_color`) REFERENCES `pms_color` (`color_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `status_color` FOREIGN KEY (`status_color`) REFERENCES `pms_color` (`color_id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
