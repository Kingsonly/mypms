-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 01, 2017 at 10:54 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.0.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mypms`
--

-- --------------------------------------------------------

--
-- Table structure for table `pms_address`
--

CREATE TABLE `pms_address` (
  `address_id` int(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `state_id` int(50) NOT NULL,
  `country_id` int(50) NOT NULL,
  `entity_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pms_address`
--

INSERT INTO `pms_address` (`address_id`, `address`, `state_id`, `country_id`, `entity_id`) VALUES
(1, 'ncce mabushi', 243, 156, 2),
(2, 'ncce mabushi', 240, 1, 3),
(3, 'ncce mabushi', 244, 5, 4),
(4, 'ncce mabushi', 248, 1, 5),
(5, 'ncce mabushi', 246, 4, 6),
(6, 'ncce mabushi', 245, 1, 7),
(7, 'ncce mabushi', 243, 1, 8),
(8, 'ncce mabushi', 244, 17, 15),
(9, 'ncce mabushi', 245, 7, 17),
(10, 'ncce mabushi', 245, 1, 18),
(11, 'ncce mabushi', 240, 1, 20),
(12, 'ncce mabushi', 240, 1, 25),
(13, 'ncce mabushi', 240, 1, 26),
(14, 'ncce mabushi', 240, 1, 27),
(15, 'ncce mabushi', 240, 1, 28),
(16, 'ncce mabushi', 240, 1, 29),
(17, 'ncce mabushi', 256, 15, 30),
(18, 'ncce mabushi', 240, 1, 31),
(19, 'ncce mabushi', 240, 1, 32),
(20, 'ncce mabushi', 240, 1, 33),
(21, 'ncce mabushi', 255, 13, 34),
(22, 'ncce mabushi', 252, 15, 35),
(23, 'ncce mabushi', 254, 15, 36),
(24, 'ncce mabushi', 240, 1, 37),
(25, 'ncce mabushi', 240, 1, 38),
(26, 'ncce mabushi', 243, 4, 39),
(27, 'ncce mabushi', 246, 5, 40),
(28, 'ncce mabushi', 245, 5, 42),
(29, 'ncce mabushi', 240, 14, 43),
(30, 'ncce mabushi', 255, 14, 45),
(31, 'ncce mabushi', 241, 7, 47),
(32, 'ncce mabushi', 240, 1, 49),
(33, 'ncce mabushi', 243, 1, 51),
(34, 'ncce mabushi', 240, 1, 55),
(35, 'ncce mabushi', 244, 1, 57),
(36, 'ncce mabushi', 240, 1, 59),
(37, 'ncce mabushi', 240, 1, 60),
(38, 'ncce mabushi', 240, 1, 61),
(39, 'ncce mabushi', 240, 1, 63),
(40, 'ncce mabushi', 240, 1, 65),
(41, 'Dawaki Abuja', 253, 15, 67),
(42, 'Minna', 244, 1, 69),
(43, 'Suleja', 258, 16, 74),
(44, 'Minna', 255, 18, 76),
(45, 'Suleja', 254, 1, 82),
(46, 'Abuja', 245, 15, 86),
(47, 'Abuja', 254, 1, 88),
(48, 'Abuja', 244, 19, 90),
(49, 'Minna', 256, 13, 92),
(50, 'Minna', 243, 1, 94),
(51, 'Abuja', 240, 1, 96),
(52, 'Mabushi', 240, 1, 98),
(53, 'Abuja', 240, 1, 100),
(54, 'Minna', 240, 1, 102),
(55, 'Minna', 240, 1, 104);

-- --------------------------------------------------------

--
-- Table structure for table `pms_bookout_return`
--

CREATE TABLE `pms_bookout_return` (
  `booking_id` int(255) NOT NULL,
  `product_id` int(255) NOT NULL,
  `client_id` int(255) NOT NULL,
  `number_out` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `status` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_check_in_out`
--

CREATE TABLE `pms_check_in_out` (
  `in_out_id` int(255) NOT NULL,
  `room_number_id` int(255) NOT NULL,
  `guess_id` int(255) NOT NULL,
  `date` date NOT NULL,
  `user_id` int(255) NOT NULL,
  `status` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_client`
--

CREATE TABLE `pms_client` (
  `client_id` int(11) NOT NULL,
  `entity_id` int(11) NOT NULL,
  `account_number` int(11) NOT NULL,
  `amount_paid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_color`
--

CREATE TABLE `pms_color` (
  `color_id` int(50) NOT NULL,
  `color_class_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pms_color`
--

INSERT INTO `pms_color` (`color_id`, `color_class_name`) VALUES
(1, 'green'),
(2, 'red'),
(3, '#FFC107'),
(4, '#800000'),
(5, '#2196F3');

-- --------------------------------------------------------

--
-- Table structure for table `pms_countries`
--

CREATE TABLE `pms_countries` (
  `country_id` int(11) NOT NULL,
  `country_name` varchar(30) CHARACTER SET utf8 NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:Blocked, 1:Active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pms_countries`
--

INSERT INTO `pms_countries` (`country_id`, `country_name`, `status`) VALUES
(1, 'Aruba', 1),
(2, 'Afghanistan', 1),
(3, 'Angola', 1),
(4, 'Anguilla', 1),
(5, 'Albania', 1),
(6, 'Andorra', 1),
(7, 'Netherlands Antilles', 1),
(8, 'United Arab Emirates', 1),
(9, 'Argentina', 1),
(10, 'Armenia', 1),
(11, 'American Samoa', 1),
(12, 'Antarctica', 1),
(13, 'French Southern territories', 1),
(14, 'Antigua and Barbuda', 1),
(15, 'Australia', 1),
(16, 'Austria', 1),
(17, 'Azerbaijan', 1),
(18, 'Burundi', 1),
(19, 'Belgium', 1),
(20, 'Benin', 1),
(21, 'Burkina Faso', 1),
(22, 'Bangladesh', 1),
(23, 'Bulgaria', 1),
(24, 'Bahrain', 1),
(25, 'Bahamas', 1),
(26, 'Bosnia and Herzegovina', 1),
(27, 'Belarus', 1),
(28, 'Belize', 1),
(29, 'Bermuda', 1),
(30, 'Bolivia', 1),
(31, 'Brazil', 1),
(32, 'Barbados', 1),
(33, 'Brunei', 1),
(34, 'Bhutan', 1),
(35, 'Bouvet Island', 1),
(36, 'Botswana', 1),
(37, 'Central African Republic', 1),
(38, 'Canada', 1),
(39, 'Cocos (Keeling) Islands', 1),
(40, 'Switzerland', 1),
(41, 'Chile', 1),
(42, 'China', 1),
(43, 'CÃ´te dâ€™Ivoire', 1),
(44, 'Cameroon', 1),
(45, 'Congo, The Democratic Republic', 1),
(46, 'Congo', 1),
(47, 'Cook Islands', 1),
(48, 'Colombia', 1),
(49, 'Comoros', 1),
(50, 'Cape Verde', 1),
(51, 'Costa Rica', 1),
(52, 'Cuba', 1),
(53, 'Christmas Island', 1),
(54, 'Cayman Islands', 1),
(55, 'Cyprus', 1),
(56, 'Czech Republic', 1),
(57, 'Germany', 1),
(58, 'Djibouti', 1),
(59, 'Dominica', 1),
(60, 'Denmark', 1),
(61, 'Dominican Republic', 1),
(62, 'Algeria', 1),
(63, 'Ecuador', 1),
(64, 'Egypt', 1),
(65, 'Eritrea', 1),
(66, 'Western Sahara', 1),
(67, 'Spain', 1),
(68, 'Estonia', 1),
(69, 'Ethiopia', 1),
(70, 'Finland', 1),
(71, 'Fiji Islands', 1),
(72, 'Falkland Islands', 1),
(73, 'France', 1),
(74, 'Faroe Islands', 1),
(75, 'Micronesia, Federated States o', 1),
(76, 'Gabon', 1),
(77, 'United Kingdom', 1),
(78, 'Georgia', 1),
(79, 'Ghana', 1),
(80, 'Gibraltar', 1),
(81, 'Guinea', 1),
(82, 'Guadeloupe', 1),
(83, 'Gambia', 1),
(84, 'Guinea-Bissau', 1),
(85, 'Equatorial Guinea', 1),
(86, 'Greece', 1),
(87, 'Grenada', 1),
(88, 'Greenland', 1),
(89, 'Guatemala', 1),
(90, 'French Guiana', 1),
(91, 'Guam', 1),
(92, 'Guyana', 1),
(93, 'Hong Kong', 1),
(94, 'Heard Island and McDonald Isla', 1),
(95, 'Honduras', 1),
(96, 'Croatia', 1),
(97, 'Haiti', 1),
(98, 'Hungary', 1),
(99, 'Indonesia', 1),
(100, 'India', 1),
(101, 'British Indian Ocean Territory', 1),
(102, 'Ireland', 1),
(103, 'Iran', 1),
(104, 'Iraq', 1),
(105, 'Iceland', 1),
(106, 'Israel', 1),
(107, 'Italy', 1),
(108, 'Jamaica', 1),
(109, 'Jordan', 1),
(110, 'Japan', 1),
(111, 'Kazakstan', 1),
(112, 'Kenya', 1),
(113, 'Kyrgyzstan', 1),
(114, 'Cambodia', 1),
(115, 'Kiribati', 1),
(116, 'Saint Kitts and Nevis', 1),
(117, 'South Korea', 1),
(118, 'Kuwait', 1),
(119, 'Laos', 1),
(120, 'Lebanon', 1),
(121, 'Liberia', 1),
(122, 'Libyan Arab Jamahiriya', 1),
(123, 'Saint Lucia', 1),
(124, 'Liechtenstein', 1),
(125, 'Sri Lanka', 1),
(126, 'Lesotho', 1),
(127, 'Lithuania', 1),
(128, 'Luxembourg', 1),
(129, 'Latvia', 1),
(130, 'Macao', 1),
(131, 'Morocco', 1),
(132, 'Monaco', 1),
(133, 'Moldova', 1),
(134, 'Madagascar', 1),
(135, 'Maldives', 1),
(136, 'Mexico', 1),
(137, 'Marshall Islands', 1),
(138, 'Macedonia', 1),
(139, 'Mali', 1),
(140, 'Malta', 1),
(141, 'Myanmar', 1),
(142, 'Mongolia', 1),
(143, 'Northern Mariana Islands', 1),
(144, 'Mozambique', 1),
(145, 'Mauritania', 1),
(146, 'Montserrat', 1),
(147, 'Martinique', 1),
(148, 'Mauritius', 1),
(149, 'Malawi', 1),
(150, 'Malaysia', 1),
(151, 'Mayotte', 1),
(152, 'Namibia', 1),
(153, 'New Caledonia', 1),
(154, 'Niger', 1),
(155, 'Norfolk Island', 1),
(156, 'Nigeria', 1),
(157, 'Nicaragua', 1),
(158, 'Niue', 1),
(159, 'Netherlands', 1),
(160, 'Norway', 1),
(161, 'Nepal', 1),
(162, 'Nauru', 1),
(163, 'New Zealand', 1),
(164, 'Oman', 1),
(165, 'Pakistan', 1),
(166, 'Panama', 1),
(167, 'Pitcairn', 1),
(168, 'Peru', 1),
(169, 'Philippines', 1),
(170, 'Palau', 1),
(171, 'Papua New Guinea', 1),
(172, 'Poland', 1),
(173, 'Puerto Rico', 1),
(174, 'North Korea', 1),
(175, 'Portugal', 1),
(176, 'Paraguay', 1),
(177, 'Palestine', 1),
(178, 'French Polynesia', 1),
(179, 'Qatar', 1),
(180, 'RÃ©union', 1),
(181, 'Romania', 1),
(182, 'Russian Federation', 1),
(183, 'Rwanda', 1),
(184, 'Saudi Arabia', 1),
(185, 'Sudan', 1),
(186, 'Senegal', 1),
(187, 'Singapore', 1),
(188, 'South Georgia and the South Sa', 1),
(189, 'Saint Helena', 1),
(190, 'Svalbard and Jan Mayen', 1),
(191, 'Solomon Islands', 1),
(192, 'Sierra Leone', 1),
(193, 'El Salvador', 1),
(194, 'San Marino', 1),
(195, 'Somalia', 1),
(196, 'Saint Pierre and Miquelon', 1),
(197, 'Sao Tome and Principe', 1),
(198, 'Suriname', 1),
(199, 'Slovakia', 1),
(200, 'Slovenia', 1),
(201, 'Sweden', 1),
(202, 'Swaziland', 1),
(203, 'Seychelles', 1),
(204, 'Syria', 1),
(205, 'Turks and Caicos Islands', 1),
(206, 'Chad', 1),
(207, 'Togo', 1),
(208, 'Thailand', 1),
(209, 'Tajikistan', 1),
(210, 'Tokelau', 1),
(211, 'Turkmenistan', 1),
(212, 'East Timor', 1),
(213, 'Tonga', 1),
(214, 'Trinidad and Tobago', 1),
(215, 'Tunisia', 1),
(216, 'Turkey', 1),
(217, 'Tuvalu', 1),
(218, 'Taiwan', 1),
(219, 'Tanzania', 1),
(220, 'Uganda', 1),
(221, 'Ukraine', 1),
(222, 'United States Minor Outlying I', 1),
(223, 'Uruguay', 1),
(224, 'United States', 1),
(225, 'Uzbekistan', 1),
(226, 'Holy See (Vatican City State)', 1),
(227, 'Saint Vincent and the Grenadin', 1),
(228, 'Venezuela', 1),
(229, 'Virgin Islands, British', 1),
(230, 'Virgin Islands, U.S.', 1),
(231, 'Vietnam', 1),
(232, 'Vanuatu', 1),
(233, 'Wallis and Futuna', 1),
(234, 'Samoa', 1),
(235, 'Yemen', 1),
(236, 'Yugoslavia', 1),
(237, 'South Africa', 1),
(238, 'Zambia', 1),
(239, 'Zimbabwe', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pms_email`
--

CREATE TABLE `pms_email` (
  `email_id` int(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `entity_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pms_email`
--

INSERT INTO `pms_email` (`email_id`, `address`, `entity_id`) VALUES
(1, 'kingsfirst@gmail.com', 2),
(2, 'kingsfirst@gmail.com', 3),
(3, 'asdasdas', 4),
(4, 'p.soribe@epsolun.com', 5),
(5, 'kingsfirst@gmail.com', 6),
(6, 'p.soribe@epsolun.com', 7),
(7, 'weewer', 8),
(8, 'efwefwefwrr', 15),
(9, 'p.soribe@epsolun.com', 17),
(10, 'p.soribe@epsolun.com', 18),
(11, 'p.soribe@epsolun.com', 20),
(12, 'weewer', 25),
(13, 'weewer', 26),
(14, 'p.soribe@epsolun.com', 27),
(15, 'p.soribe@epsolun.com', 28),
(16, 'p.soribe@epsolun.com', 29),
(17, 'p.soribe@epsolun.com', 30),
(18, 'p.soribe@epsolun.com', 31),
(19, 'p.soribe@epsolun.com', 32),
(20, 'p.soribe@epsolun.com', 33),
(21, 'p.soribe@epsolun.com', 34),
(22, 'p.soribe@epsolun.com', 35),
(23, 'p.soribe@epsolun.com', 36),
(24, 'p.soribe@epsolun.com', 37),
(25, 'p.soribe@epsolun.com', 38),
(26, 'p.soribe@epsolun.com', 39),
(27, 'p.soribe@epsolun.com', 40),
(28, 'p.soribe@epsolun.com', 42),
(29, 'p.soribe@epsolun.com', 43),
(30, 'p.soribe@epsolun.com', 45),
(31, 'p.soribe@epsolun.com', 47),
(32, 'p.soribe@epsolun.com', 49),
(33, 'p.soribe@epsolun.com', 51),
(34, 'p.soribe@epsolun.com', 55),
(35, 'p.soribe@epsolun.com', 57),
(36, 'p.soribe@epsolun.com', 59),
(37, 'p.soribe@epsolun.com', 60),
(38, 'p.soribe@epsolun.com', 61),
(39, 'p.soribe@epsolun.com', 63),
(40, 'p.soribe@epsolun.com', 65),
(41, 'esejoyakhaine@gmail.com', 67),
(42, 'bunmi@gmail.com', 69),
(43, 'steph@gmail.com', 74),
(44, 'bunmi@gmail.com', 76),
(45, 'steph@gmail.com', 82),
(46, 'enose@gmail.com', 86),
(47, 'victor@gnail.com', 88),
(48, 'ese@gmail.com', 90),
(49, 'bunmi@gmail.com', 92),
(50, 'ese@gmail.com', 94),
(51, 'ese@gmail.com', 96),
(52, 'kingsonly13c@gmail.com', 98),
(53, 'dahlia@gmail.com', 100),
(54, 'efe@gmail.com', 102),
(55, 'dami@gmail.com', 104);

-- --------------------------------------------------------

--
-- Table structure for table `pms_entity`
--

CREATE TABLE `pms_entity` (
  `entity_id` int(255) NOT NULL,
  `type` enum('guest','client','user') NOT NULL,
  `folio` int(255) NOT NULL,
  `folio_status` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pms_entity`
--

INSERT INTO `pms_entity` (`entity_id`, `type`, `folio`, `folio_status`) VALUES
(1, 'user', 0, 0),
(2, 'user', 0, 0),
(3, 'user', 0, 0),
(4, 'user', 0, 0),
(5, 'user', 0, 0),
(6, 'user', 0, 0),
(7, 'user', 0, 0),
(8, 'user', 0, 0),
(9, 'guest', 0, 0),
(10, 'guest', 0, 0),
(11, 'guest', 0, 0),
(12, 'guest', 0, 0),
(13, 'guest', 0, 0),
(14, 'guest', 0, 0),
(15, 'guest', 0, 0),
(16, 'guest', 0, 0),
(17, 'guest', 0, 0),
(18, 'guest', 0, 0),
(19, 'guest', 0, 0),
(20, 'guest', 0, 0),
(21, 'guest', 0, 0),
(22, 'guest', 0, 0),
(23, 'guest', 0, 0),
(24, 'guest', 0, 0),
(25, 'guest', 0, 0),
(26, 'guest', 0, 0),
(27, 'guest', 0, 0),
(28, 'guest', 0, 0),
(29, 'guest', 0, 0),
(30, 'guest', 0, 0),
(31, 'guest', 0, 0),
(32, 'guest', 0, 0),
(33, 'guest', 0, 0),
(34, 'guest', 0, 0),
(35, 'guest', 0, 0),
(36, 'guest', 0, 0),
(37, 'guest', 0, 0),
(38, 'guest', 0, 0),
(39, 'guest', 0, 0),
(40, 'guest', 0, 0),
(41, 'guest', 0, 0),
(42, 'guest', 0, 0),
(43, 'guest', 3, 0),
(44, 'guest', 4, 0),
(45, 'guest', 5, 0),
(46, 'guest', 6, 0),
(47, 'guest', 7, 0),
(48, 'guest', 8, 0),
(49, 'guest', 9, 0),
(50, 'guest', 10, 0),
(51, 'guest', 11, 0),
(52, 'guest', 12, 0),
(53, 'guest', 13, 0),
(54, 'guest', 14, 0),
(55, 'guest', 15, 0),
(56, 'guest', 16, 0),
(57, 'guest', 17, 0),
(58, 'guest', 18, 0),
(59, 'guest', 19, 0),
(60, 'guest', 20, 0),
(61, 'guest', 21, 0),
(62, 'guest', 22, 0),
(63, 'guest', 23, 0),
(64, 'guest', 24, 0),
(65, 'guest', 25, 0),
(66, 'guest', 26, 0),
(67, 'guest', 27, 0),
(68, 'guest', 28, 0),
(69, 'guest', 29, 0),
(70, 'guest', 30, 0),
(71, 'guest', 31, 0),
(72, 'guest', 0, 0),
(73, 'guest', 32, 0),
(74, 'guest', 33, 0),
(75, 'guest', 34, 0),
(76, 'guest', 35, 0),
(77, 'guest', 36, 0),
(78, 'guest', 37, 0),
(79, 'guest', 38, 0),
(80, 'guest', 39, 0),
(81, 'guest', 40, 0),
(82, 'guest', 41, 0),
(83, 'guest', 42, 0),
(84, 'guest', 43, 0),
(85, 'guest', 44, 0),
(86, 'guest', 45, 0),
(87, 'guest', 46, 0),
(88, 'guest', 47, 0),
(89, 'guest', 48, 0),
(90, 'guest', 49, 0),
(91, 'guest', 50, 0),
(92, 'guest', 51, 0),
(93, 'guest', 52, 0),
(94, 'guest', 53, 0),
(95, 'guest', 54, 0),
(96, 'guest', 55, 0),
(97, 'guest', 56, 0),
(98, 'guest', 57, 0),
(99, 'guest', 58, 0),
(100, 'guest', 59, 0),
(101, 'guest', 60, 0),
(102, 'guest', 61, 0),
(103, 'guest', 62, 0),
(104, 'guest', 63, 0),
(105, 'guest', 64, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pms_folio`
--

CREATE TABLE `pms_folio` (
  `folio_id` int(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `creation_date` datetime NOT NULL,
  `status` int(255) NOT NULL,
  `user` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pms_folio`
--

INSERT INTO `pms_folio` (`folio_id`, `amount`, `creation_date`, `status`, `user`) VALUES
(1, '', '2017-10-08 19:12:03', 0, 3),
(2, '', '2017-10-08 19:12:54', 0, 3),
(3, '', '2017-10-08 19:13:28', 0, 3),
(4, '', '2017-10-08 19:13:29', 0, 3),
(5, '', '2017-10-08 19:24:00', 0, 3),
(6, '', '2017-10-08 19:24:00', 0, 3),
(7, '', '2017-10-08 19:59:04', 0, 3),
(8, '', '2017-10-08 19:59:04', 0, 3),
(9, '', '2017-10-08 20:12:29', 0, 3),
(10, '', '2017-10-08 20:12:29', 0, 3),
(11, '', '2017-10-08 20:19:07', 0, 3),
(12, '', '2017-10-08 20:19:07', 0, 3),
(13, '', '2017-10-08 20:31:46', 0, 3),
(14, '', '2017-10-08 20:31:46', 0, 3),
(15, '', '2017-10-08 20:32:40', 0, 3),
(16, '', '2017-10-08 20:32:40', 0, 3),
(17, '', '2017-10-08 20:54:41', 0, 3),
(18, '', '2017-10-08 20:54:41', 0, 3),
(19, '', '2017-10-08 20:57:45', 0, 3),
(20, '', '2017-10-08 20:59:20', 0, 3),
(21, '', '2017-10-08 21:02:22', 0, 3),
(22, '', '2017-10-08 21:02:22', 0, 3),
(23, '', '2017-10-08 21:04:31', 0, 3),
(24, '', '2017-10-08 21:04:32', 0, 3),
(25, '', '2017-10-08 21:15:40', 0, 3),
(26, '', '2017-10-08 21:15:40', 0, 3),
(27, '', '2017-10-09 14:30:42', 0, 3),
(28, '', '2017-10-09 14:30:43', 0, 3),
(29, '', '2017-10-09 14:37:37', 0, 3),
(30, '', '2017-10-09 14:37:39', 0, 3),
(31, '', '2017-10-12 00:20:36', 0, 3),
(32, '', '2017-10-12 00:20:37', 0, 3),
(33, '', '2017-10-12 01:18:20', 0, 3),
(34, '', '2017-10-12 01:18:21', 0, 3),
(35, '', '2017-10-15 09:09:57', 0, 3),
(36, '', '2017-10-15 09:09:59', 0, 3),
(37, '', '2017-10-15 09:24:31', 0, 3),
(38, '', '2017-10-15 09:24:45', 0, 3),
(39, '', '2017-10-15 09:26:44', 0, 3),
(40, '', '2017-10-15 09:27:23', 0, 3),
(41, '', '2017-10-15 09:27:36', 0, 3),
(42, '', '2017-10-15 09:27:37', 0, 3),
(43, '', '2017-10-16 15:37:11', 0, 3),
(44, '', '2017-10-16 15:38:18', 0, 3),
(45, '', '2017-10-16 15:38:39', 0, 3),
(46, '', '2017-10-16 15:38:49', 0, 3),
(47, '', '2017-10-16 15:44:05', 0, 3),
(48, '', '2017-10-16 15:44:06', 0, 3),
(49, '', '2017-10-16 21:55:32', 0, 3),
(50, '', '2017-10-16 21:55:34', 0, 3),
(51, '', '2017-10-16 22:05:28', 0, 3),
(52, '', '2017-10-16 22:05:28', 0, 3),
(53, '', '2017-10-17 08:35:18', 0, 3),
(54, '', '2017-10-17 08:35:20', 0, 3),
(55, '', '2017-10-17 19:51:10', 0, 3),
(56, '', '2017-10-17 19:51:13', 0, 3),
(57, '', '2017-10-19 23:19:45', 0, 3),
(58, '', '2017-10-19 23:19:57', 0, 3),
(59, '', '2017-10-19 23:21:51', 0, 3),
(60, '', '2017-10-19 23:21:53', 0, 3),
(61, '', '2017-10-21 05:31:23', 0, 3),
(62, '', '2017-10-21 05:31:25', 0, 3),
(63, '', '2017-10-21 05:32:49', 0, 3),
(64, '', '2017-10-21 05:32:51', 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `pms_folio_credit`
--

CREATE TABLE `pms_folio_credit` (
  `folio_id` int(255) NOT NULL,
  `entity_id` int(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `senario_id` int(255) NOT NULL,
  `senario_type` varchar(255) NOT NULL,
  `date_posted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pms_folio_credit`
--

INSERT INTO `pms_folio_credit` (`folio_id`, `entity_id`, `amount`, `senario_id`, `senario_type`, `date_posted`) VALUES
(33, 51, '8000', 36, 'RoomBooking', '2017-10-12 01:18:22'),
(35, 76, '9000', 37, 'RoomBooking', '2017-10-15 09:10:00'),
(41, 82, '9000', 38, 'RoomBooking', '2017-10-15 09:27:38'),
(45, 86, '10000', 39, 'RoomBooking', '2017-10-16 15:38:50'),
(47, 88, '10000', 40, 'RoomBooking', '2017-10-16 15:44:07'),
(49, 90, '60000', 1, 'RoomBooking', '2017-10-16 21:55:35'),
(51, 92, '5600', 2, 'RoomBooking', '2017-10-16 22:05:30'),
(53, 94, '7250', 3, 'RoomBooking', '2017-10-17 08:35:21'),
(55, 96, '5700', 4, 'RoomBooking', '2017-10-17 19:51:15'),
(57, 98, '5600', 5, 'RoomBooking', '2017-10-19 23:19:58'),
(59, 100, '7000', 6, 'RoomBooking', '2017-10-19 23:21:54'),
(61, 102, '10000', 7, 'RoomBooking', '2017-10-21 05:31:26'),
(63, 104, '9450', 8, 'RoomBooking', '2017-10-21 05:32:51');

-- --------------------------------------------------------

--
-- Table structure for table `pms_folio_debit`
--

CREATE TABLE `pms_folio_debit` (
  `folio_id` int(255) NOT NULL,
  `entity_id` int(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `senario_id` int(255) NOT NULL,
  `senario_type` varchar(255) NOT NULL,
  `date_posted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pms_folio_debit`
--

INSERT INTO `pms_folio_debit` (`folio_id`, `entity_id`, `amount`, `senario_id`, `senario_type`, `date_posted`) VALUES
(33, 51, '8000', 36, 'RoomBooking', '2017-10-12 01:18:22'),
(35, 76, '97200000', 37, 'RoomBooking', '2017-10-15 09:09:59'),
(41, 82, '97200000', 38, 'RoomBooking', '2017-10-15 09:27:38'),
(45, 86, '172800000', 39, 'RoomBooking', '2017-10-16 15:38:50'),
(47, 88, '172800000', 40, 'RoomBooking', '2017-10-16 15:44:07'),
(49, 90, '97200000', 1, 'RoomBooking', '2017-10-16 21:55:35'),
(51, 92, '97200000', 2, 'RoomBooking', '2017-10-16 22:05:30'),
(53, 94, '97200000', 3, 'RoomBooking', '2017-10-17 08:35:21'),
(55, 96, '97200000', 4, 'RoomBooking', '2017-10-17 19:51:15'),
(57, 98, '97200000', 5, 'RoomBooking', '2017-10-19 23:19:58'),
(59, 100, '97200000', 6, 'RoomBooking', '2017-10-19 23:21:54'),
(61, 102, '172800000', 7, 'RoomBooking', '2017-10-21 05:31:26'),
(63, 104, '97200000', 8, 'RoomBooking', '2017-10-21 05:32:51');

-- --------------------------------------------------------

--
-- Table structure for table `pms_guest`
--

CREATE TABLE `pms_guest` (
  `id` int(255) NOT NULL,
  `entity_id` int(255) NOT NULL,
  `registration_date` date NOT NULL,
  `user_id` int(255) NOT NULL,
  `status_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pms_guest`
--

INSERT INTO `pms_guest` (`id`, `entity_id`, `registration_date`, `user_id`, `status_id`) VALUES
(1, 9, '2017-09-23', 3, 3),
(2, 10, '2017-09-23', 3, 3),
(3, 11, '2017-09-23', 3, 3),
(4, 12, '2017-09-23', 3, 3),
(5, 13, '2017-09-23', 3, 3),
(6, 14, '2017-09-23', 3, 3),
(7, 15, '2017-09-23', 3, 3),
(8, 16, '2017-09-23', 3, 2),
(9, 17, '2017-09-23', 3, 2),
(10, 18, '2017-09-23', 3, 3),
(11, 19, '2017-09-23', 3, 2),
(12, 20, '2017-09-23', 3, 2),
(13, 21, '2017-09-23', 3, 2),
(14, 22, '2017-09-23', 3, 2),
(15, 23, '2017-09-23', 3, 2),
(16, 24, '2017-09-23', 3, 2),
(17, 25, '2017-09-23', 3, 2),
(18, 26, '2017-09-23', 3, 2),
(19, 27, '2017-09-23', 3, 2),
(20, 28, '2017-09-23', 3, 2),
(21, 29, '2017-09-23', 3, 2),
(22, 30, '2017-09-23', 3, 3),
(23, 31, '2017-09-23', 3, 2),
(24, 32, '2017-09-23', 3, 2),
(25, 33, '2017-09-23', 3, 2),
(26, 34, '2017-09-24', 3, 2),
(27, 35, '2017-09-24', 3, 2),
(28, 36, '2017-09-24', 3, 2),
(29, 37, '2017-09-24', 3, 2),
(30, 38, '2017-09-25', 3, 2),
(31, 39, '2017-09-26', 3, 2),
(32, 40, '2017-10-04', 3, 3),
(33, 41, '2017-10-04', 3, 3),
(34, 42, '2017-10-04', 3, 3),
(35, 43, '2017-10-08', 3, 3),
(36, 45, '2017-10-08', 3, 3),
(37, 47, '2017-10-08', 3, 3),
(38, 49, '2017-10-08', 3, 4),
(39, 51, '2017-10-08', 3, 2),
(40, 53, '2017-10-08', 3, 3),
(41, 55, '2017-10-08', 3, 2),
(42, 57, '2017-10-08', 3, 3),
(43, 59, '2017-10-08', 3, 3),
(44, 60, '2017-10-08', 3, 4),
(45, 61, '2017-10-08', 3, 2),
(46, 63, '2017-10-08', 3, 2),
(47, 65, '2017-10-08', 3, 2),
(48, 67, '2017-10-09', 3, 2),
(49, 69, '2017-10-09', 3, 2),
(50, 72, '2017-10-12', 3, 2),
(51, 74, '2017-10-12', 3, 2),
(52, 76, '2017-10-15', 3, 9),
(53, 82, '2017-10-15', 3, 9),
(54, 86, '2017-10-16', 3, 11),
(55, 88, '2017-10-16', 3, 9),
(56, 90, '2017-10-16', 3, 9),
(57, 92, '2017-10-16', 3, 10),
(58, 94, '2017-10-17', 3, 9),
(59, 96, '2017-10-17', 3, 11),
(60, 98, '2017-10-19', 3, 9),
(61, 100, '2017-10-19', 3, 9),
(62, 102, '2017-10-21', 3, 9),
(63, 104, '2017-10-21', 3, 10);

-- --------------------------------------------------------

--
-- Table structure for table `pms_ledger`
--

CREATE TABLE `pms_ledger` (
  `ledger_id` int(50) NOT NULL,
  `corporation_name` varchar(50) NOT NULL,
  `amount` varchar(50) NOT NULL,
  `status` int(50) NOT NULL,
  `creation_date` date NOT NULL,
  `user_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_ledger_relationship`
--

CREATE TABLE `pms_ledger_relationship` (
  `ledger_id` int(50) NOT NULL COMMENT 'ledger id',
  `guest_id` int(50) NOT NULL COMMENT 'guest id ',
  `ledger_type` enum('debit','credit') NOT NULL COMMENT 'credit/debit',
  `status` int(50) NOT NULL COMMENT 'opened/closed'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_order`
--

CREATE TABLE `pms_order` (
  `id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `due_amount` int(255) NOT NULL,
  `created` datetime NOT NULL,
  `status` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_person`
--

CREATE TABLE `pms_person` (
  `person_id` int(11) NOT NULL,
  `surname` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `entity_id` int(11) NOT NULL,
  `photo` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pms_person`
--

INSERT INTO `pms_person` (`person_id`, `surname`, `first_name`, `entity_id`, `photo`) VALUES
(1, 'achumie', 'kingsley', 1, ''),
(2, 'achumie', 'kingsley', 2, ''),
(3, 'john ', 'emeka', 3, ''),
(4, 'achumie', 'corporation', 4, ''),
(5, 'iwugeriygwiy', 'wuhiuwrygh', 5, ''),
(6, '6u5u56', '567uhhrt', 6, ''),
(7, 'test', 'test', 7, ''),
(8, 'uguyg', 'iuhiuhiu', 8, ''),
(9, 'corporation', 'aedasfdf', 15, ''),
(10, 'werwer', 'aedasfdf', 17, ''),
(11, 'werwer', 'aedasfdf', 18, ''),
(12, 'werwer', 'aedasfdf', 20, ''),
(13, 'werwer', 'aedasfdf', 25, ''),
(14, 'werwer', 'aedasfdf', 26, ''),
(15, 'werwer', 'aedasfdf', 27, ''),
(16, 'werwer', 'aedasfdf', 28, ''),
(17, 'werwer', 'aedasfdf', 29, ''),
(18, 'werwer', 'edasd', 30, ''),
(19, 'werwer', 'aedasfdf', 31, ''),
(20, 'werwer', 'aedasfdf', 32, ''),
(21, 'werwer', 'aedasfdf', 33, ''),
(22, 'werwer', 'aedasfdf', 34, ''),
(23, 'werwer', 'aedasfdf', 35, ''),
(24, 'werwer', 'aedasfdf', 36, ''),
(25, 'werwer', 'aedasfdf', 37, ''),
(26, 'werwer', 'aedasfdf', 38, ''),
(27, 'asdasdasd', 'aedasfdf', 39, ''),
(28, 'mr test', 'mr test', 40, ''),
(29, 'mr test2', 'mr test2', 42, ''),
(30, 'test', 'test', 43, ''),
(31, 'me', 'me', 45, ''),
(32, 'ytrfytfyhfv', 'iygiujgkj', 47, ''),
(33, 'bboy', 'bboy', 49, ''),
(34, 'werwer', 'aedasfdf', 51, ''),
(35, 'werwer', 'aedasfdf', 55, ''),
(36, 'werwer', 'aedasfdf', 57, ''),
(37, 'werwer', 'aedasfdf', 59, ''),
(38, 'werwer', 'aedasfdf', 60, ''),
(39, 'werwer', 'aedasfdf', 61, ''),
(40, 'werwer', 'aedasfdf', 63, ''),
(41, 'werwer', 'aedasfdf', 65, ''),
(42, 'Akhaine', 'Dahlia', 67, ''),
(43, 'Bunmi', 'Obebe', 69, ''),
(44, 'Chineye', 'Stephanie', 74, ''),
(45, 'Obebe', 'Bunmi', 76, ''),
(46, 'Chineye', 'Stephanie', 82, ''),
(47, 'Akhaine', 'Emuata', 86, ''),
(48, 'Emuata A.', 'Victor', 88, ''),
(49, 'Dahlia', 'Akhaine', 90, ''),
(50, 'Bunmi', 'Obebe', 92, ''),
(51, 'Ese', 'Akhaine', 94, ''),
(52, 'Akhaine', 'Dahlia', 96, ''),
(53, 'Kingsley', 'Achumie', 98, ''),
(54, 'Dahlia', 'Akhaine', 100, ''),
(55, 'Omo', 'efe', 102, ''),
(56, 'Dami', 'lola', 104, '');

-- --------------------------------------------------------

--
-- Table structure for table `pms_product_inventory`
--

CREATE TABLE `pms_product_inventory` (
  `product_id` int(255) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `vendor_id` int(255) NOT NULL,
  `product_quantity` int(255) NOT NULL,
  `purchase_date` date NOT NULL,
  `amount_purchased` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_recipt`
--

CREATE TABLE `pms_recipt` (
  `recipt_id` int(50) NOT NULL,
  `recipt_number` int(50) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `receipt_issue_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pms_recipt`
--

INSERT INTO `pms_recipt` (`recipt_id`, `recipt_number`, `amount`, `receipt_issue_date`) VALUES
(1, 725671, '100', '2017-09-24 14:50:17'),
(2, 8169412, '100', '2017-09-24 18:02:20'),
(3, 6708692, '100', '2017-09-25 21:11:14'),
(4, 4938644, '100', '2017-09-26 19:29:48'),
(5, 2949105, '679', '2017-10-04 04:19:19'),
(6, 5684948, '100', '2017-10-04 04:37:04'),
(7, 3833599, '100', '2017-10-08 19:13:29'),
(8, 6392498, '100', '2017-10-08 19:24:00'),
(9, 8135188, '100', '2017-10-08 19:59:04'),
(10, 2078426, '100', '2017-10-08 20:12:29'),
(11, 4989222, '100', '2017-10-08 20:19:07'),
(12, 7520118, '100', '2017-10-08 20:32:40'),
(13, 3704449, '100', '2017-10-08 20:54:41'),
(14, 8154334, '100', '2017-10-08 20:57:45'),
(15, 452189, '100', '2017-10-08 20:59:20'),
(16, 745977, '100', '2017-10-08 21:02:22'),
(17, 194537, '100', '2017-10-08 21:04:32'),
(18, 2756465, '100', '2017-10-08 21:15:40'),
(19, 8836410, '7000', '2017-10-09 14:30:44'),
(20, 1655648, '9000', '2017-10-09 14:37:44'),
(21, 127894, '8000', '2017-10-12 01:18:22'),
(22, 130978, '9000', '2017-10-15 09:10:00'),
(23, 180406, '9000', '2017-10-15 09:27:38'),
(24, 43098, '10000', '2017-10-16 15:38:51'),
(25, 12657, '10000', '2017-10-16 15:44:08'),
(26, 140232, '60000', '2017-10-16 21:55:35'),
(27, 20929, '5600', '2017-10-16 22:05:31'),
(28, 22611, '7250', '2017-10-17 08:35:22'),
(29, 22038, '5700', '2017-10-17 19:51:16'),
(30, 152614, '5600', '2017-10-19 23:19:59'),
(31, 159598, '7000', '2017-10-19 23:21:54'),
(32, 121068, '10000', '2017-10-21 05:31:27'),
(33, 55822, '9450', '2017-10-21 05:32:51');

-- --------------------------------------------------------

--
-- Table structure for table `pms_recipt_relationship`
--

CREATE TABLE `pms_recipt_relationship` (
  `recipt_id` int(50) NOT NULL,
  `senarior_id` int(50) NOT NULL,
  `senarior_type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pms_recipt_relationship`
--

INSERT INTO `pms_recipt_relationship` (`recipt_id`, `senarior_id`, `senarior_type`) VALUES
(1, 21, 'roombooking'),
(2, 22, 'roombooking'),
(3, 23, 'roombooking'),
(4, 24, 'roombooking'),
(6, 25, 'roombooking'),
(12, 26, 'roombooking'),
(13, 27, 'roombooking'),
(14, 28, 'roombooking'),
(15, 29, 'roombooking'),
(16, 30, 'roombooking'),
(17, 31, 'roombooking'),
(18, 32, 'roombooking'),
(19, 33, 'roombooking'),
(20, 34, 'roombooking'),
(21, 36, 'RoomBooking'),
(22, 37, 'RoomBooking'),
(23, 38, 'RoomBooking'),
(24, 39, 'RoomBooking'),
(25, 40, 'RoomBooking'),
(26, 1, 'RoomBooking'),
(27, 2, 'RoomBooking'),
(28, 3, 'RoomBooking'),
(29, 4, 'RoomBooking'),
(30, 5, 'RoomBooking'),
(31, 6, 'RoomBooking'),
(32, 7, 'RoomBooking'),
(33, 8, 'RoomBooking');

-- --------------------------------------------------------

--
-- Table structure for table `pms_restaurant_booking`
--

CREATE TABLE `pms_restaurant_booking` (
  `booking_id` int(255) NOT NULL,
  `room_number_id` int(255) NOT NULL,
  `product_order` longblob NOT NULL,
  `entity_id` int(255) NOT NULL,
  `amount_paid` varchar(255) NOT NULL,
  `time_of_order` datetime(6) NOT NULL,
  `user_id` int(255) NOT NULL,
  `status` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_restaurant_price`
--

CREATE TABLE `pms_restaurant_price` (
  `price_id` int(255) NOT NULL,
  `product_price` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pms_restaurant_price`
--

INSERT INTO `pms_restaurant_price` (`price_id`, `product_price`) VALUES
(1, 100);

-- --------------------------------------------------------

--
-- Table structure for table `pms_restaurant_product`
--

CREATE TABLE `pms_restaurant_product` (
  `product_id` int(255) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `price_id` int(255) NOT NULL,
  `product_quantity` int(255) NOT NULL,
  `product_status` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pms_restaurant_product`
--

INSERT INTO `pms_restaurant_product` (`product_id`, `product_name`, `price_id`, `product_quantity`, `product_status`) VALUES
(4, 'rice', 1, 2, 2),
(6, 'beans', 1, 87, 2);

-- --------------------------------------------------------

--
-- Table structure for table `pms_role`
--

CREATE TABLE `pms_role` (
  `role_id` int(50) NOT NULL,
  `role_number` int(50) NOT NULL,
  `role_description` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pms_role`
--

INSERT INTO `pms_role` (`role_id`, `role_number`, `role_description`) VALUES
(1, 30, 'sales boy');

-- --------------------------------------------------------

--
-- Table structure for table `pms_room`
--

CREATE TABLE `pms_room` (
  `room_id` int(255) NOT NULL,
  `room_number` varchar(100) NOT NULL,
  `room_type` int(11) NOT NULL,
  `room_tariff_id` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pms_room`
--

INSERT INTO `pms_room` (`room_id`, `room_number`, `room_type`, `room_tariff_id`, `status`) VALUES
(1, '1', 1, 1, 2),
(2, '2', 1, 1, 2),
(3, '3', 1, 1, 5),
(4, '4', 1, 1, 5),
(5, '5', 1, 1, 5),
(6, '6', 1, 1, 5),
(7, '7', 1, 1, 5),
(8, '8', 1, 1, 5),
(9, '9', 1, 1, 5),
(10, '10', 1, 1, 5),
(11, '11', 1, 1, 5),
(12, '12', 1, 1, 5),
(13, '13', 1, 1, 2),
(14, '14', 1, 1, 5),
(15, '15', 1, 1, 5),
(16, '16', 1, 1, 5),
(17, '17', 1, 1, 5),
(18, '18', 1, 1, 5),
(19, '19', 1, 1, 5),
(20, '20', 1, 1, 5),
(21, '21', 1, 1, 5),
(22, '22', 1, 1, 5),
(23, '23', 1, 1, 5),
(24, '24', 1, 1, 5),
(25, '25', 1, 1, 5),
(26, '26', 1, 1, 5),
(27, '27', 1, 1, 5),
(28, '28', 1, 1, 5),
(29, '29', 1, 1, 5),
(30, '30', 1, 1, 5),
(31, '31', 1, 1, 5),
(32, '32', 1, 1, 5),
(33, '33', 1, 1, 5),
(34, '34', 1, 1, 5),
(35, '35', 1, 1, 5),
(36, '36', 1, 1, 5),
(37, '37', 1, 1, 5),
(38, '38', 1, 1, 5),
(39, '39', 1, 1, 5),
(40, '40', 1, 1, 5),
(41, '41', 1, 1, 5),
(42, '42', 1, 1, 2),
(43, '43', 1, 1, 5),
(44, '44', 1, 1, 5),
(45, '45', 1, 1, 5),
(46, '46', 1, 1, 5),
(47, '47', 1, 1, 5),
(48, '48', 1, 1, 5),
(49, '49', 1, 1, 5),
(50, '50', 1, 1, 5),
(51, '51', 1, 1, 5),
(52, '52', 1, 1, 5),
(53, '53', 1, 1, 5),
(54, '54', 1, 1, 5),
(55, '55', 1, 1, 5),
(56, '56', 1, 1, 5),
(57, '57', 1, 1, 5),
(58, '58', 1, 1, 5),
(59, '59', 1, 1, 5),
(60, '60', 1, 1, 5),
(61, '61', 1, 1, 5),
(62, '62', 1, 1, 5),
(63, '63', 1, 1, 5),
(64, '64', 1, 1, 5),
(65, '65', 1, 1, 5),
(66, '66', 1, 1, 5),
(67, '67', 1, 1, 5),
(68, '68', 1, 1, 5),
(69, '69', 1, 1, 5),
(70, '70', 1, 1, 5),
(71, '71', 1, 1, 5),
(72, '72', 1, 1, 5),
(73, '73', 1, 1, 5),
(74, '74', 1, 1, 5),
(75, '75', 1, 1, 5),
(76, '76', 1, 1, 5),
(77, '77', 1, 1, 5),
(78, '78', 1, 1, 5),
(79, '79', 1, 1, 2),
(80, '80', 1, 1, 5),
(81, '81', 1, 1, 5),
(82, '82', 1, 1, 5),
(83, '83', 1, 1, 5),
(84, '84', 1, 1, 5),
(85, '85', 1, 1, 5),
(86, '86', 1, 1, 5),
(87, '87', 1, 1, 5),
(88, '88', 1, 1, 5),
(89, '89', 1, 1, 5),
(90, '90', 1, 1, 5),
(91, '91', 1, 1, 5),
(92, '92', 1, 1, 5),
(93, '93', 1, 1, 5),
(94, '94', 1, 1, 5),
(95, '95', 1, 1, 5),
(96, '96', 1, 1, 5),
(97, '97', 1, 1, 5),
(98, '98', 1, 1, 5),
(99, '99', 1, 1, 5),
(100, '100', 1, 1, 5),
(101, '101', 1, 1, 5),
(102, '102', 1, 1, 5),
(103, '103', 1, 1, 5),
(104, '104', 1, 1, 5),
(105, '105', 1, 1, 5),
(106, '106', 1, 1, 5),
(107, '107', 1, 1, 5),
(108, '108', 1, 1, 5),
(109, '109', 1, 1, 5),
(110, '110', 1, 1, 5),
(111, '111', 1, 1, 5),
(112, '112', 1, 1, 5),
(113, '113', 2, 2, 5),
(114, '114', 2, 2, 5),
(115, '115', 2, 2, 5),
(116, '116', 2, 2, 5),
(117, '117', 2, 2, 5),
(118, '118', 2, 2, 5),
(119, '119', 2, 2, 5),
(120, '120', 2, 2, 5),
(121, '121', 2, 2, 5),
(122, '122', 2, 2, 5),
(123, '123', 2, 2, 5),
(124, '124', 2, 2, 5),
(125, '125', 2, 2, 5),
(126, '126', 2, 2, 5),
(127, '127', 2, 2, 2),
(128, '128', 2, 2, 5),
(129, '129', 2, 2, 5),
(130, '130', 2, 2, 5),
(131, '131', 2, 2, 5),
(132, '132', 2, 2, 5),
(133, '133', 2, 2, 5),
(134, '134', 2, 2, 5),
(135, '135', 2, 2, 5),
(136, '136', 2, 2, 5),
(137, '137', 2, 2, 5),
(138, '138', 2, 2, 5),
(139, '139', 2, 2, 5),
(140, '140', 2, 2, 5),
(141, '141', 2, 2, 5),
(142, '142', 2, 2, 5),
(143, '143', 2, 2, 5),
(144, '144', 2, 2, 5),
(145, '145', 2, 2, 5),
(146, '146', 2, 2, 5),
(147, '147', 2, 2, 5),
(148, '148', 2, 2, 5),
(149, '149', 2, 2, 5),
(150, '150', 2, 2, 5),
(151, '151', 2, 2, 5),
(152, '152', 2, 2, 5),
(153, '153', 2, 2, 5),
(154, '154', 2, 2, 5),
(155, '155', 2, 2, 5),
(156, '156', 3, 3, 5),
(157, '157', 3, 3, 5),
(158, '158', 3, 3, 5),
(159, '159', 3, 3, 5),
(160, '160', 3, 3, 5),
(161, '161', 3, 3, 5),
(162, '162', 3, 3, 5),
(163, '163', 3, 3, 5),
(164, '164', 3, 3, 5),
(165, '165', 3, 3, 5),
(166, '166', 3, 3, 5),
(167, '167', 3, 3, 5),
(168, '168', 3, 3, 5),
(169, '169', 3, 3, 5),
(170, '170', 3, 3, 5),
(171, '171', 3, 3, 5),
(172, '172', 3, 3, 5),
(173, '173', 3, 3, 5),
(174, '174', 3, 3, 5),
(175, '175', 3, 3, 5),
(176, '176', 3, 3, 5),
(177, '177', 3, 3, 5),
(178, '178', 3, 3, 5),
(179, '179', 3, 3, 5),
(180, '180', 3, 3, 5),
(181, '181', 3, 3, 5),
(182, '182', 3, 3, 5),
(183, '183', 3, 3, 5),
(184, '184', 3, 3, 5),
(185, '185', 3, 3, 5),
(186, '186', 3, 3, 5),
(187, '187', 3, 3, 5);

-- --------------------------------------------------------

--
-- Table structure for table `pms_room_booking`
--

CREATE TABLE `pms_room_booking` (
  `booking_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `entity_id` int(255) NOT NULL,
  `tariff_id` int(11) NOT NULL,
  `room_type` int(255) NOT NULL,
  `amount_paid` int(11) NOT NULL,
  `time_in` datetime NOT NULL,
  `time_out` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `number_of_nights` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pms_room_booking`
--

INSERT INTO `pms_room_booking` (`booking_id`, `room_id`, `entity_id`, `tariff_id`, `room_type`, `amount_paid`, `time_in`, `time_out`, `user_id`, `status_id`, `datetime`, `number_of_nights`) VALUES
(1, 1, 90, 1, 1, 60000, '2017-10-18 00:00:00', '2017-10-20 00:00:00', 3, 5, '2017-10-16 20:55:34', 2),
(2, 13, 92, 1, 1, 5600, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 2, '2017-10-16 21:05:29', 0),
(3, 42, 94, 1, 1, 7250, '2017-10-17 00:00:00', '2017-10-18 00:00:00', 3, 2, '2017-10-17 07:35:21', 1),
(4, 10, 96, 1, 1, 5700, '2017-10-17 00:00:00', '2017-10-19 00:00:00', 3, 2, '2017-10-17 18:51:14', 2),
(5, 1, 98, 1, 1, 5600, '2017-10-19 00:00:00', '2017-10-21 00:00:00', 3, 2, '2017-10-19 22:19:57', 2),
(6, 2, 100, 1, 1, 7000, '2017-10-21 00:00:00', '2017-10-24 00:00:00', 3, 9, '2017-10-19 22:21:53', 3),
(7, 127, 102, 1, 0, 10000, '2017-10-23 00:00:00', '2017-10-25 00:00:00', 3, 2, '2017-10-21 04:31:25', 2),
(8, 79, 104, 1, 0, 9450, '2017-10-27 00:00:00', '2017-10-30 00:00:00', 3, 2, '2017-10-21 04:32:51', 3);

-- --------------------------------------------------------

--
-- Table structure for table `pms_room_tariff`
--

CREATE TABLE `pms_room_tariff` (
  `tariff_id` int(255) NOT NULL,
  `tariff` varchar(255) NOT NULL COMMENT 'amout'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pms_room_tariff`
--

INSERT INTO `pms_room_tariff` (`tariff_id`, `tariff`) VALUES
(1, '9000'),
(2, '12000'),
(3, '18000');

-- --------------------------------------------------------

--
-- Table structure for table `pms_room_type`
--

CREATE TABLE `pms_room_type` (
  `room_type_id` int(255) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pms_room_type`
--

INSERT INTO `pms_room_type` (`room_type_id`, `type`) VALUES
(1, 'Classic'),
(2, 'Superior'),
(3, 'Business Suite');

-- --------------------------------------------------------

--
-- Table structure for table `pms_states`
--

CREATE TABLE `pms_states` (
  `state_id` int(11) NOT NULL,
  `state_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `country_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:Blocked, 1:Active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pms_states`
--

INSERT INTO `pms_states` (`state_id`, `state_name`, `country_id`, `status`) VALUES
(240, 'â€“', 1, 1),
(241, 'Balkh', 2, 1),
(242, 'Herat', 2, 1),
(243, 'Kabol', 2, 1),
(244, 'Qandahar', 2, 1),
(245, 'Benguela', 3, 1),
(246, 'Huambo', 3, 1),
(247, 'Luanda', 3, 1),
(248, 'Namibe', 3, 1),
(249, 'â€“', 4, 1),
(250, 'Tirana', 5, 1),
(251, 'Andorra la Vella', 6, 1),
(252, 'CuraÃ§ao', 7, 1),
(253, 'Abu Dhabi', 8, 1),
(254, 'Ajman', 8, 1),
(255, 'Dubai', 8, 1),
(256, 'Sharja', 8, 1),
(257, 'Buenos Aires', 9, 1),
(258, 'Catamarca', 9, 1),
(259, 'CÃ³rdoba', 9, 1),
(260, 'Chaco', 9, 1),
(261, 'Chubut', 9, 1),
(262, 'Corrientes', 9, 1),
(263, 'Distrito Federal', 9, 1),
(264, 'Entre Rios', 9, 1),
(265, 'Formosa', 9, 1),
(266, 'Jujuy', 9, 1),
(267, 'La Rioja', 9, 1),
(268, 'Mendoza', 9, 1),
(269, 'Misiones', 9, 1),
(270, 'NeuquÃ©n', 9, 1),
(271, 'Salta', 9, 1),
(272, 'San Juan', 9, 1),
(273, 'San Luis', 9, 1),
(274, 'Santa FÃ©', 9, 1),
(275, 'Santiago del Estero', 9, 1),
(276, 'TucumÃ¡n', 9, 1),
(277, 'Lori', 10, 1),
(278, 'Yerevan', 10, 1),
(279, 'Å irak', 10, 1),
(280, 'Tutuila', 11, 1),
(281, 'St John', 14, 1),
(282, 'Capital Region', 15, 1),
(283, 'New South Wales', 15, 1),
(284, 'Queensland', 15, 1),
(285, 'South Australia', 15, 1),
(286, 'Tasmania', 15, 1),
(287, 'Victoria', 15, 1),
(288, 'West Australia', 15, 1),
(289, 'KÃ¤rnten', 16, 1),
(290, 'North Austria', 16, 1),
(291, 'Salzburg', 16, 1),
(292, 'Steiermark', 16, 1),
(293, 'Tiroli', 16, 1),
(294, 'Wien', 16, 1),
(295, 'Baki', 17, 1),
(296, 'GÃ¤ncÃ¤', 17, 1),
(297, 'MingÃ¤Ã§evir', 17, 1),
(298, 'Sumqayit', 17, 1),
(299, 'Bujumbura', 18, 1),
(300, 'Antwerpen', 19, 1),
(301, 'Bryssel', 19, 1),
(302, 'East Flanderi', 19, 1),
(303, 'Hainaut', 19, 1),
(304, 'LiÃ¨ge', 19, 1),
(305, 'Namur', 19, 1),
(306, 'West Flanderi', 19, 1),
(307, 'Atacora', 20, 1),
(308, 'Atlantique', 20, 1),
(309, 'Borgou', 20, 1),
(310, 'OuÃ©mÃ©', 20, 1),
(311, 'BoulkiemdÃ©', 21, 1),
(312, 'Houet', 21, 1),
(313, 'Kadiogo', 21, 1),
(314, 'Barisal', 22, 1),
(315, 'Chittagong', 22, 1),
(316, 'Dhaka', 22, 1),
(317, 'Khulna', 22, 1),
(318, 'Rajshahi', 22, 1),
(319, 'Sylhet', 22, 1),
(320, 'Burgas', 23, 1),
(321, 'Grad Sofija', 23, 1),
(322, 'Haskovo', 23, 1),
(323, 'Lovec', 23, 1),
(324, 'Plovdiv', 23, 1),
(325, 'Ruse', 23, 1),
(326, 'Varna', 23, 1),
(327, 'al-Manama', 24, 1),
(328, 'New Providence', 25, 1),
(329, 'Federaatio', 26, 1),
(330, 'Republika Srpska', 26, 1),
(331, 'Brest', 27, 1),
(332, 'Gomel', 27, 1),
(333, 'Grodno', 27, 1),
(334, 'Horad Minsk', 27, 1),
(335, 'Minsk', 27, 1),
(336, 'Mogiljov', 27, 1),
(337, 'Vitebsk', 27, 1),
(338, 'Belize City', 28, 1),
(339, 'Cayo', 28, 1),
(340, 'Hamilton', 29, 1),
(341, 'Saint GeorgeÂ´s', 29, 1),
(342, 'Chuquisaca', 30, 1),
(343, 'Cochabamba', 30, 1),
(344, 'La Paz', 30, 1),
(345, 'Oruro', 30, 1),
(346, 'PotosÃ­', 30, 1),
(347, 'Santa Cruz', 30, 1),
(348, 'Tarija', 30, 1),
(349, 'Acre', 31, 1),
(350, 'Alagoas', 31, 1),
(351, 'AmapÃ¡', 31, 1),
(352, 'Amazonas', 31, 1),
(353, 'Bahia', 31, 1),
(354, 'CearÃ¡', 31, 1),
(355, 'Distrito Federal', 31, 1),
(356, 'EspÃ­rito Santo', 31, 1),
(357, 'GoiÃ¡s', 31, 1),
(358, 'MaranhÃ£o', 31, 1),
(359, 'Mato Grosso', 31, 1),
(360, 'Mato Grosso do Sul', 31, 1),
(361, 'Minas Gerais', 31, 1),
(362, 'ParaÃ­ba', 31, 1),
(363, 'ParanÃ¡', 31, 1),
(364, 'ParÃ¡', 31, 1),
(365, 'Pernambuco', 31, 1),
(366, 'PiauÃ­', 31, 1),
(367, 'Rio de Janeiro', 31, 1),
(368, 'Rio Grande do Norte', 31, 1),
(369, 'Rio Grande do Sul', 31, 1),
(370, 'RondÃ´nia', 31, 1),
(371, 'Roraima', 31, 1),
(372, 'Santa Catarina', 31, 1),
(373, 'SÃ£o Paulo', 31, 1),
(374, 'Sergipe', 31, 1),
(375, 'Tocantins', 31, 1),
(376, 'St Michael', 32, 1),
(377, 'Brunei and Muara', 33, 1),
(378, 'Thimphu', 34, 1),
(379, 'Francistown', 36, 1),
(380, 'Gaborone', 36, 1),
(381, 'Bangui', 37, 1),
(382, 'Alberta', 38, 1),
(383, 'British Colombia', 38, 1),
(384, 'Manitoba', 38, 1),
(385, 'Newfoundland', 38, 1),
(386, 'Nova Scotia', 38, 1),
(387, 'Ontario', 38, 1),
(388, 'QuÃ©bec', 38, 1),
(389, 'Saskatchewan', 38, 1),
(390, 'Home Island', 39, 1),
(391, 'West Island', 39, 1),
(392, 'Basel-Stadt', 40, 1),
(393, 'Bern', 40, 1),
(394, 'Geneve', 40, 1),
(395, 'Vaud', 40, 1),
(396, 'ZÃ¼rich', 40, 1),
(397, 'Antofagasta', 41, 1),
(398, 'Atacama', 41, 1),
(399, 'BÃ­obÃ­o', 41, 1),
(400, 'Coquimbo', 41, 1),
(401, 'La AraucanÃ­a', 41, 1),
(402, 'Los Lagos', 41, 1),
(403, 'Magallanes', 41, 1),
(404, 'Maule', 41, 1),
(405, 'OÂ´Higgins', 41, 1),
(406, 'Santiago', 41, 1),
(407, 'TarapacÃ¡', 41, 1),
(408, 'ValparaÃ­so', 41, 1),
(409, 'Anhui', 42, 1),
(410, 'Chongqing', 42, 1),
(411, 'Fujian', 42, 1),
(412, 'Gansu', 42, 1),
(413, 'Guangdong', 42, 1),
(414, 'Guangxi', 42, 1),
(415, 'Guizhou', 42, 1),
(416, 'Hainan', 42, 1),
(417, 'Hebei', 42, 1),
(418, 'Heilongjiang', 42, 1),
(419, 'Henan', 42, 1),
(420, 'Hubei', 42, 1),
(421, 'Hunan', 42, 1),
(422, 'Inner Mongolia', 42, 1),
(423, 'Jiangsu', 42, 1),
(424, 'Jiangxi', 42, 1),
(425, 'Jilin', 42, 1),
(426, 'Liaoning', 42, 1),
(427, 'Ningxia', 42, 1),
(428, 'Peking', 42, 1),
(429, 'Qinghai', 42, 1),
(430, 'Shaanxi', 42, 1),
(431, 'Shandong', 42, 1),
(432, 'Shanghai', 42, 1),
(433, 'Shanxi', 42, 1),
(434, 'Sichuan', 42, 1),
(435, 'Tianjin', 42, 1),
(436, 'Tibet', 42, 1),
(437, 'Xinxiang', 42, 1),
(438, 'Yunnan', 42, 1),
(439, 'Zhejiang', 42, 1),
(440, 'Abidjan', 43, 1),
(441, 'BouakÃ©', 43, 1),
(442, 'Daloa', 43, 1),
(443, 'Korhogo', 43, 1),
(444, 'Yamoussoukro', 43, 1),
(445, 'Centre', 44, 1),
(446, 'ExtrÃªme-Nord', 44, 1),
(447, 'Littoral', 44, 1),
(448, 'Nord', 44, 1),
(449, 'Nord-Ouest', 44, 1),
(450, 'Ouest', 44, 1),
(451, 'Bandundu', 0, 1),
(452, 'Bas-ZaÃ¯re', 0, 1),
(453, 'East Kasai', 0, 1),
(454, 'Equateur', 0, 1),
(455, 'Haute-ZaÃ¯re', 0, 1),
(456, 'Kinshasa', 0, 1),
(457, 'North Kivu', 0, 1),
(458, 'Shaba', 0, 1),
(459, 'South Kivu', 0, 1),
(460, 'West Kasai', 0, 1),
(461, 'Brazzaville', 46, 1),
(462, 'Kouilou', 46, 1),
(463, 'Rarotonga', 47, 1),
(464, 'Antioquia', 48, 1),
(465, 'AtlÃ¡ntico', 48, 1),
(466, 'BolÃ­var', 48, 1),
(467, 'BoyacÃ¡', 48, 1),
(468, 'Caldas', 48, 1),
(469, 'CaquetÃ¡', 48, 1),
(470, 'Cauca', 48, 1),
(471, 'CÃ³rdoba', 48, 1),
(472, 'Cesar', 48, 1),
(473, 'Cundinamarca', 48, 1),
(474, 'Huila', 48, 1),
(475, 'La Guajira', 48, 1),
(476, 'Magdalena', 48, 1),
(477, 'Meta', 48, 1),
(478, 'NariÃ±o', 48, 1),
(479, 'Norte de Santander', 48, 1),
(480, 'QuindÃ­o', 48, 1),
(481, 'Risaralda', 48, 1),
(482, 'SantafÃ© de BogotÃ¡', 48, 1),
(483, 'Santander', 48, 1),
(484, 'Sucre', 48, 1),
(485, 'Tolima', 48, 1),
(486, 'Valle', 48, 1),
(487, 'Njazidja', 49, 1),
(488, 'SÃ£o Tiago', 50, 1),
(489, 'San JosÃ©', 51, 1),
(490, 'CamagÃ¼ey', 52, 1),
(491, 'Ciego de Ãvila', 52, 1),
(492, 'Cienfuegos', 52, 1),
(493, 'Granma', 52, 1),
(494, 'GuantÃ¡namo', 52, 1),
(495, 'HolguÃ­n', 52, 1),
(496, 'La Habana', 52, 1),
(497, 'Las Tunas', 52, 1),
(498, 'Matanzas', 52, 1),
(499, 'Pinar del RÃ­o', 52, 1),
(500, 'Sancti-SpÃ­ritus', 52, 1),
(501, 'Santiago de Cuba', 52, 1),
(502, 'Villa Clara', 52, 1),
(503, 'â€“', 53, 1),
(504, 'Grand Cayman', 54, 1),
(505, 'Limassol', 55, 1),
(506, 'Nicosia', 55, 1),
(507, 'HlavnÃ­ mesto Praha', 56, 1),
(508, 'JiznÃ­ Cechy', 56, 1),
(509, 'JiznÃ­ Morava', 56, 1),
(510, 'SevernÃ­ Cechy', 56, 1),
(511, 'SevernÃ­ Morava', 56, 1),
(512, 'VÃ½chodnÃ­ Cechy', 56, 1),
(513, 'ZapadnÃ­ Cechy', 56, 1),
(514, 'Anhalt Sachsen', 57, 1),
(515, 'Baden-WÃ¼rttemberg', 57, 1),
(516, 'Baijeri', 57, 1),
(517, 'Berliini', 57, 1),
(518, 'Brandenburg', 57, 1),
(519, 'Bremen', 57, 1),
(520, 'Hamburg', 57, 1),
(521, 'Hessen', 57, 1),
(522, 'Mecklenburg-Vorpomme', 57, 1),
(523, 'Niedersachsen', 57, 1),
(524, 'Nordrhein-Westfalen', 57, 1),
(525, 'Rheinland-Pfalz', 57, 1),
(526, 'Saarland', 57, 1),
(527, 'Saksi', 57, 1),
(528, 'Schleswig-Holstein', 57, 1),
(529, 'ThÃ¼ringen', 57, 1),
(530, 'Djibouti', 58, 1),
(531, 'St George', 59, 1),
(532, 'Ã…rhus', 60, 1),
(533, 'Frederiksberg', 60, 1),
(534, 'Fyn', 60, 1),
(535, 'KÃ¸benhavn', 60, 1),
(536, 'Nordjylland', 60, 1),
(537, 'Distrito Nacional', 61, 1),
(538, 'Duarte', 61, 1),
(539, 'La Romana', 61, 1),
(540, 'Puerto Plata', 61, 1),
(541, 'San Pedro de MacorÃ­', 61, 1),
(542, 'Santiago', 61, 1),
(543, 'Alger', 62, 1),
(544, 'Annaba', 62, 1),
(545, 'Batna', 62, 1),
(546, 'BÃ©char', 62, 1),
(547, 'BÃ©jaÃ¯a', 62, 1),
(548, 'Biskra', 62, 1),
(549, 'Blida', 62, 1),
(550, 'Chlef', 62, 1),
(551, 'Constantine', 62, 1),
(552, 'GhardaÃ¯a', 62, 1),
(553, 'Mostaganem', 62, 1),
(554, 'Oran', 62, 1),
(555, 'SÃ©tif', 62, 1),
(556, 'Sidi Bel AbbÃ¨s', 62, 1),
(557, 'Skikda', 62, 1),
(558, 'TÃ©bessa', 62, 1),
(559, 'Tiaret', 62, 1),
(560, 'Tlemcen', 62, 1),
(561, 'Azuay', 63, 1),
(562, 'Chimborazo', 63, 1),
(563, 'El Oro', 63, 1),
(564, 'Esmeraldas', 63, 1),
(565, 'Guayas', 63, 1),
(566, 'Imbabura', 63, 1),
(567, 'Loja', 63, 1),
(568, 'Los RÃ­os', 63, 1),
(569, 'ManabÃ­', 63, 1),
(570, 'Pichincha', 63, 1),
(571, 'Tungurahua', 63, 1),
(572, 'al-Buhayra', 64, 1),
(573, 'al-Daqahliya', 64, 1),
(574, 'al-Faiyum', 64, 1),
(575, 'al-Gharbiya', 64, 1),
(576, 'al-Minufiya', 64, 1),
(577, 'al-Minya', 64, 1),
(578, 'al-Qalyubiya', 64, 1),
(579, 'al-Sharqiya', 64, 1),
(580, 'Aleksandria', 64, 1),
(581, 'Assuan', 64, 1),
(582, 'Asyut', 64, 1),
(583, 'Bani Suwayf', 64, 1),
(584, 'Giza', 64, 1),
(585, 'Ismailia', 64, 1),
(586, 'Kafr al-Shaykh', 64, 1),
(587, 'Kairo', 64, 1),
(588, 'Luxor', 64, 1),
(589, 'Port Said', 64, 1),
(590, 'Qina', 64, 1),
(591, 'Sawhaj', 64, 1),
(592, 'Shamal Sina', 64, 1),
(593, 'Suez', 64, 1),
(594, 'Maekel', 65, 1),
(595, 'El-AaiÃºn', 66, 1),
(596, 'Andalusia', 67, 1),
(597, 'Aragonia', 67, 1),
(598, 'Asturia', 67, 1),
(599, 'Balears', 67, 1),
(600, 'Baskimaa', 67, 1),
(601, 'Canary Islands', 67, 1),
(602, 'Cantabria', 67, 1),
(603, 'Castilla and LeÃ³n', 67, 1),
(604, 'Extremadura', 67, 1),
(605, 'Galicia', 67, 1),
(606, 'Kastilia-La Mancha', 67, 1),
(607, 'Katalonia', 67, 1),
(608, 'La Rioja', 67, 1),
(609, 'Madrid', 67, 1),
(610, 'Murcia', 67, 1),
(611, 'Navarra', 67, 1),
(612, 'Valencia', 67, 1),
(613, 'Harjumaa', 68, 1),
(614, 'Tartumaa', 68, 1),
(615, 'Addis Abeba', 69, 1),
(616, 'Amhara', 69, 1),
(617, 'Dire Dawa', 69, 1),
(618, 'Oromia', 69, 1),
(619, 'Tigray', 69, 1),
(620, 'Newmaa', 70, 1),
(621, 'PÃ¤ijÃ¤t-HÃ¤me', 70, 1),
(622, 'Pirkanmaa', 70, 1),
(623, 'Pohjois-Pohjanmaa', 70, 1),
(624, 'Varsinais-Suomi', 70, 1),
(625, 'Central', 71, 1),
(626, 'East Falkland', 72, 1),
(627, 'Alsace', 73, 1),
(628, 'Aquitaine', 73, 1),
(629, 'Auvergne', 73, 1),
(630, 'ÃŽle-de-France', 73, 1),
(631, 'Basse-Normandie', 73, 1),
(632, 'Bourgogne', 73, 1),
(633, 'Bretagne', 73, 1),
(634, 'Centre', 73, 1),
(635, 'Champagne-Ardenne', 73, 1),
(636, 'Franche-ComtÃ©', 73, 1),
(637, 'Haute-Normandie', 73, 1),
(638, 'Languedoc-Roussillon', 73, 1),
(639, 'Limousin', 73, 1),
(640, 'Lorraine', 73, 1),
(641, 'Midi-PyrÃ©nÃ©es', 73, 1),
(642, 'Nord-Pas-de-Calais', 73, 1),
(643, 'Pays de la Loire', 73, 1),
(644, 'Picardie', 73, 1),
(645, 'Provence-Alpes-CÃ´te', 73, 1),
(646, 'RhÃ´ne-Alpes', 73, 1),
(647, 'Streymoyar', 74, 1),
(648, 'Chuuk', 0, 1),
(649, 'Pohnpei', 0, 1),
(650, 'Estuaire', 76, 1),
(651, 'â€“', 77, 1),
(652, 'England', 77, 1),
(653, 'Jersey', 77, 1),
(654, 'North Ireland', 77, 1),
(655, 'Scotland', 77, 1),
(656, 'Wales', 77, 1),
(657, 'Abhasia [Aphazeti]', 78, 1),
(658, 'Adzaria [AtÅ¡ara]', 78, 1),
(659, 'Imereti', 78, 1),
(660, 'Kvemo Kartli', 78, 1),
(661, 'Tbilisi', 78, 1),
(662, 'Ashanti', 79, 1),
(663, 'Greater Accra', 79, 1),
(664, 'Northern', 79, 1),
(665, 'Western', 79, 1),
(666, 'â€“', 80, 1),
(667, 'Conakry', 81, 1),
(668, 'Basse-Terre', 82, 1),
(669, 'Grande-Terre', 82, 1),
(670, 'Banjul', 83, 1),
(671, 'Kombo St Mary', 83, 1),
(672, 'Bissau', 84, 1),
(673, 'Bioko', 85, 1),
(674, 'Attika', 86, 1),
(675, 'Central Macedonia', 86, 1),
(676, 'Crete', 86, 1),
(677, 'Thessalia', 86, 1),
(678, 'West Greece', 86, 1),
(679, 'St George', 87, 1),
(680, 'Kitaa', 88, 1),
(681, 'Guatemala', 89, 1),
(682, 'Quetzaltenango', 89, 1),
(683, 'Cayenne', 90, 1),
(684, 'â€“', 91, 1),
(685, 'Georgetown', 92, 1),
(686, 'Hongkong', 93, 1),
(687, 'Kowloon and New Kowl', 93, 1),
(688, 'AtlÃ¡ntida', 95, 1),
(689, 'CortÃ©s', 95, 1),
(690, 'Distrito Central', 95, 1),
(691, 'Grad Zagreb', 96, 1),
(692, 'Osijek-Baranja', 96, 1),
(693, 'Primorje-Gorski Kota', 96, 1),
(694, 'Split-Dalmatia', 96, 1),
(695, 'Nord', 97, 1),
(696, 'Ouest', 97, 1),
(697, 'Baranya', 98, 1),
(698, 'BÃ¡cs-Kiskun', 98, 1),
(699, 'Borsod-AbaÃºj-ZemplÃ', 98, 1),
(700, 'Budapest', 98, 1),
(701, 'CsongrÃ¡d', 98, 1),
(702, 'FejÃ©r', 98, 1),
(703, 'GyÃ¶r-Moson-Sopron', 98, 1),
(704, 'HajdÃº-Bihar', 98, 1),
(705, 'Szabolcs-SzatmÃ¡r-Be', 98, 1),
(706, 'Aceh', 99, 1),
(707, 'Bali', 99, 1),
(708, 'Bengkulu', 99, 1),
(709, 'Central Java', 99, 1),
(710, 'East Java', 99, 1),
(711, 'Jakarta Raya', 99, 1),
(712, 'Jambi', 99, 1),
(713, 'Kalimantan Barat', 99, 1),
(714, 'Kalimantan Selatan', 99, 1),
(715, 'Kalimantan Tengah', 99, 1),
(716, 'Kalimantan Timur', 99, 1),
(717, 'Lampung', 99, 1),
(718, 'Molukit', 99, 1),
(719, 'Nusa Tenggara Barat', 99, 1),
(720, 'Nusa Tenggara Timur', 99, 1),
(721, 'Riau', 99, 1),
(722, 'Sulawesi Selatan', 99, 1),
(723, 'Sulawesi Tengah', 99, 1),
(724, 'Sulawesi Tenggara', 99, 1),
(725, 'Sulawesi Utara', 99, 1),
(726, 'Sumatera Barat', 99, 1),
(727, 'Sumatera Selatan', 99, 1),
(728, 'Sumatera Utara', 99, 1),
(729, 'West Irian', 99, 1),
(730, 'West Java', 99, 1),
(731, 'Yogyakarta', 99, 1),
(732, 'Andhra Pradesh', 100, 1),
(733, 'Assam', 100, 1),
(734, 'Bihar', 100, 1),
(735, 'Chandigarh', 100, 1),
(736, 'Chhatisgarh', 100, 1),
(737, 'Delhi', 100, 1),
(738, 'Gujarat', 100, 1),
(739, 'Haryana', 100, 1),
(740, 'Jammu and Kashmir', 100, 1),
(741, 'Jharkhand', 100, 1),
(742, 'Karnataka', 100, 1),
(743, 'Kerala', 100, 1),
(744, 'Madhya Pradesh', 100, 1),
(745, 'Maharashtra', 100, 1),
(746, 'Manipur', 100, 1),
(747, 'Meghalaya', 100, 1),
(748, 'Mizoram', 100, 1),
(749, 'Orissa', 100, 1),
(750, 'Pondicherry', 100, 1),
(751, 'Punjab', 100, 1),
(752, 'Rajasthan', 100, 1),
(753, 'Tamil Nadu', 100, 1),
(754, 'Tripura', 100, 1),
(755, 'Uttar Pradesh', 100, 1),
(756, 'Uttaranchal', 100, 1),
(757, 'West Bengal', 100, 1),
(758, 'Leinster', 102, 1),
(759, 'Munster', 102, 1),
(760, 'Ardebil', 103, 1),
(761, 'Bushehr', 103, 1),
(762, 'Chaharmahal va Bakht', 103, 1),
(763, 'East Azerbaidzan', 103, 1),
(764, 'Esfahan', 103, 1),
(765, 'Fars', 103, 1),
(766, 'Gilan', 103, 1),
(767, 'Golestan', 103, 1),
(768, 'Hamadan', 103, 1),
(769, 'Hormozgan', 103, 1),
(770, 'Ilam', 103, 1),
(771, 'Kerman', 103, 1),
(772, 'Kermanshah', 103, 1),
(773, 'Khorasan', 103, 1),
(774, 'Khuzestan', 103, 1),
(775, 'Kordestan', 103, 1),
(776, 'Lorestan', 103, 1),
(777, 'Markazi', 103, 1),
(778, 'Mazandaran', 103, 1),
(779, 'Qazvin', 103, 1),
(780, 'Qom', 103, 1),
(781, 'Semnan', 103, 1),
(782, 'Sistan va Baluchesta', 103, 1),
(783, 'Teheran', 103, 1),
(784, 'West Azerbaidzan', 103, 1),
(785, 'Yazd', 103, 1),
(786, 'Zanjan', 103, 1),
(787, 'al-Anbar', 104, 1),
(788, 'al-Najaf', 104, 1),
(789, 'al-Qadisiya', 104, 1),
(790, 'al-Sulaymaniya', 104, 1),
(791, 'al-Tamim', 104, 1),
(792, 'Babil', 104, 1),
(793, 'Baghdad', 104, 1),
(794, 'Basra', 104, 1),
(795, 'DhiQar', 104, 1),
(796, 'Diyala', 104, 1),
(797, 'Irbil', 104, 1),
(798, 'Karbala', 104, 1),
(799, 'Maysan', 104, 1),
(800, 'Ninawa', 104, 1),
(801, 'Wasit', 104, 1),
(802, 'HÃ¶fuÃ°borgarsvÃ¦Ã°i', 105, 1),
(803, 'Ha Darom', 106, 1),
(804, 'Ha Merkaz', 106, 1),
(805, 'Haifa', 106, 1),
(806, 'Jerusalem', 106, 1),
(807, 'Tel Aviv', 106, 1),
(808, 'Abruzzit', 107, 1),
(809, 'Apulia', 107, 1),
(810, 'Calabria', 107, 1),
(811, 'Campania', 107, 1),
(812, 'Emilia-Romagna', 107, 1),
(813, 'Friuli-Venezia Giuli', 107, 1),
(814, 'Latium', 107, 1),
(815, 'Liguria', 107, 1),
(816, 'Lombardia', 107, 1),
(817, 'Marche', 107, 1),
(818, 'Piemonte', 107, 1),
(819, 'Sardinia', 107, 1),
(820, 'Sisilia', 107, 1),
(821, 'Toscana', 107, 1),
(822, 'Trentino-Alto Adige', 107, 1),
(823, 'Umbria', 107, 1),
(824, 'Veneto', 107, 1),
(825, 'St. Andrew', 108, 1),
(826, 'St. Catherine', 108, 1),
(827, 'al-Zarqa', 109, 1),
(828, 'Amman', 109, 1),
(829, 'Irbid', 109, 1),
(830, 'Aichi', 110, 1),
(831, 'Akita', 110, 1),
(832, 'Aomori', 110, 1),
(833, 'Chiba', 110, 1),
(834, 'Ehime', 110, 1),
(835, 'Fukui', 110, 1),
(836, 'Fukuoka', 110, 1),
(837, 'Fukushima', 110, 1),
(838, 'Gifu', 110, 1),
(839, 'Gumma', 110, 1),
(840, 'Hiroshima', 110, 1),
(841, 'Hokkaido', 110, 1),
(842, 'Hyogo', 110, 1),
(843, 'Ibaragi', 110, 1),
(844, 'Ishikawa', 110, 1),
(845, 'Iwate', 110, 1),
(846, 'Kagawa', 110, 1),
(847, 'Kagoshima', 110, 1),
(848, 'Kanagawa', 110, 1),
(849, 'Kochi', 110, 1),
(850, 'Kumamoto', 110, 1),
(851, 'Kyoto', 110, 1),
(852, 'Mie', 110, 1),
(853, 'Miyagi', 110, 1),
(854, 'Miyazaki', 110, 1),
(855, 'Nagano', 110, 1),
(856, 'Nagasaki', 110, 1),
(857, 'Nara', 110, 1),
(858, 'Niigata', 110, 1),
(859, 'Oita', 110, 1),
(860, 'Okayama', 110, 1),
(861, 'Okinawa', 110, 1),
(862, 'Osaka', 110, 1),
(863, 'Saga', 110, 1),
(864, 'Saitama', 110, 1),
(865, 'Shiga', 110, 1),
(866, 'Shimane', 110, 1),
(867, 'Shizuoka', 110, 1),
(868, 'Tochigi', 110, 1),
(869, 'Tokushima', 110, 1),
(870, 'Tokyo-to', 110, 1),
(871, 'Tottori', 110, 1),
(872, 'Toyama', 110, 1),
(873, 'Wakayama', 110, 1),
(874, 'Yamagata', 110, 1),
(875, 'Yamaguchi', 110, 1),
(876, 'Yamanashi', 110, 1),
(877, 'Almaty', 111, 1),
(878, 'Almaty Qalasy', 111, 1),
(879, 'AqtÃ¶be', 111, 1),
(880, 'Astana', 111, 1),
(881, 'Atyrau', 111, 1),
(882, 'East Kazakstan', 111, 1),
(883, 'Mangghystau', 111, 1),
(884, 'North Kazakstan', 111, 1),
(885, 'Pavlodar', 111, 1),
(886, 'Qaraghandy', 111, 1),
(887, 'Qostanay', 111, 1),
(888, 'Qyzylorda', 111, 1),
(889, 'South Kazakstan', 111, 1),
(890, 'Taraz', 111, 1),
(891, 'West Kazakstan', 111, 1),
(892, 'Central', 112, 1),
(893, 'Coast', 112, 1),
(894, 'Eastern', 112, 1),
(895, 'Nairobi', 112, 1),
(896, 'Nyanza', 112, 1),
(897, 'Rift Valley', 112, 1),
(898, 'Bishkek shaary', 113, 1),
(899, 'Osh', 113, 1),
(900, 'Battambang', 114, 1),
(901, 'Phnom Penh', 114, 1),
(902, 'Siem Reap', 114, 1),
(903, 'South Tarawa', 115, 1),
(904, 'St George Basseterre', 116, 1),
(905, 'Cheju', 117, 1),
(906, 'Chollabuk', 117, 1),
(907, 'Chollanam', 117, 1),
(908, 'Chungchongbuk', 117, 1),
(909, 'Chungchongnam', 117, 1),
(910, 'Inchon', 117, 1),
(911, 'Kang-won', 117, 1),
(912, 'Kwangju', 117, 1),
(913, 'Kyonggi', 117, 1),
(914, 'Kyongsangbuk', 117, 1),
(915, 'Kyongsangnam', 117, 1),
(916, 'Pusan', 117, 1),
(917, 'Seoul', 117, 1),
(918, 'Taegu', 117, 1),
(919, 'Taejon', 117, 1),
(920, 'al-Asima', 118, 1),
(921, 'Hawalli', 118, 1),
(922, 'Savannakhet', 119, 1),
(923, 'Viangchan', 119, 1),
(924, 'al-Shamal', 120, 1),
(925, 'Beirut', 120, 1),
(926, 'Montserrado', 121, 1),
(927, 'al-Zawiya', 122, 1),
(928, 'Bengasi', 122, 1),
(929, 'Misrata', 122, 1),
(930, 'Tripoli', 122, 1),
(931, 'Castries', 123, 1),
(932, 'Schaan', 124, 1),
(933, 'Vaduz', 124, 1),
(934, 'Central', 125, 1),
(935, 'Northern', 125, 1),
(936, 'Western', 125, 1),
(937, 'Maseru', 126, 1),
(938, 'Kaunas', 127, 1),
(939, 'Klaipeda', 127, 1),
(940, 'Panevezys', 127, 1),
(941, 'Vilna', 127, 1),
(942, 'Å iauliai', 127, 1),
(943, 'Luxembourg', 128, 1),
(944, 'Daugavpils', 129, 1),
(945, 'Liepaja', 129, 1),
(946, 'Riika', 129, 1),
(947, 'Macau', 130, 1),
(948, 'Casablanca', 131, 1),
(949, 'Chaouia-Ouardigha', 131, 1),
(950, 'Doukkala-Abda', 131, 1),
(951, 'FÃ¨s-Boulemane', 131, 1),
(952, 'Gharb-Chrarda-BÃ©ni', 131, 1),
(953, 'Marrakech-Tensift-Al', 131, 1),
(954, 'MeknÃ¨s-Tafilalet', 131, 1),
(955, 'Oriental', 131, 1),
(956, 'Rabat-SalÃ©-Zammour-', 131, 1),
(957, 'Souss Massa-DraÃ¢', 131, 1),
(958, 'Tadla-Azilal', 131, 1),
(959, 'Tanger-TÃ©touan', 131, 1),
(960, 'Taza-Al Hoceima-Taou', 131, 1),
(961, 'â€“', 132, 1),
(962, 'Balti', 133, 1),
(963, 'Bender (TÃ®ghina)', 133, 1),
(964, 'Chisinau', 133, 1),
(965, 'Dnjestria', 133, 1),
(966, 'Antananarivo', 134, 1),
(967, 'Fianarantsoa', 134, 1),
(968, 'Mahajanga', 134, 1),
(969, 'Toamasina', 134, 1),
(970, 'Maale', 135, 1),
(971, 'Aguascalientes', 136, 1),
(972, 'Baja California', 136, 1),
(973, 'Baja California Sur', 136, 1),
(974, 'Campeche', 136, 1),
(975, 'Chiapas', 136, 1),
(976, 'Chihuahua', 136, 1),
(977, 'Coahuila de Zaragoza', 136, 1),
(978, 'Colima', 136, 1),
(979, 'Distrito Federal', 136, 1),
(980, 'Durango', 136, 1),
(981, 'Guanajuato', 136, 1),
(982, 'Guerrero', 136, 1),
(983, 'Hidalgo', 136, 1),
(984, 'Jalisco', 136, 1),
(985, 'MÃ©xico', 136, 1),
(986, 'MichoacÃ¡n de Ocampo', 136, 1),
(987, 'Morelos', 136, 1),
(988, 'Nayarit', 136, 1),
(989, 'Nuevo LeÃ³n', 136, 1),
(990, 'Oaxaca', 136, 1),
(991, 'Puebla', 136, 1),
(992, 'QuerÃ©taro', 136, 1),
(993, 'QuerÃ©taro de Arteag', 136, 1),
(994, 'Quintana Roo', 136, 1),
(995, 'San Luis PotosÃ­', 136, 1),
(996, 'Sinaloa', 136, 1),
(997, 'Sonora', 136, 1),
(998, 'Tabasco', 136, 1),
(999, 'Tamaulipas', 136, 1),
(1000, 'Veracruz', 136, 1),
(1001, 'Veracruz-Llave', 136, 1),
(1002, 'YucatÃ¡n', 136, 1),
(1003, 'Zacatecas', 136, 1),
(1004, 'Majuro', 137, 1),
(1005, 'Skopje', 138, 1),
(1006, 'Bamako', 139, 1),
(1007, 'Inner Harbour', 140, 1),
(1008, 'Outer Harbour', 140, 1),
(1009, 'Irrawaddy [Ayeyarwad', 141, 1),
(1010, 'Magwe [Magway]', 141, 1),
(1011, 'Mandalay', 141, 1),
(1012, 'Mon', 141, 1),
(1013, 'Pegu [Bago]', 141, 1),
(1014, 'Rakhine', 141, 1),
(1015, 'Rangoon [Yangon]', 141, 1),
(1016, 'Sagaing', 141, 1),
(1017, 'Shan', 141, 1),
(1018, 'Tenasserim [Tanintha', 141, 1),
(1019, 'Ulaanbaatar', 142, 1),
(1020, 'Saipan', 143, 1),
(1021, 'Gaza', 144, 1),
(1022, 'Inhambane', 144, 1),
(1023, 'Manica', 144, 1),
(1024, 'Maputo', 144, 1),
(1025, 'Nampula', 144, 1),
(1026, 'Sofala', 144, 1),
(1027, 'Tete', 144, 1),
(1028, 'ZambÃ©zia', 144, 1),
(1029, 'Dakhlet NouÃ¢dhibou', 145, 1),
(1030, 'Nouakchott', 145, 1),
(1031, 'Plymouth', 146, 1),
(1032, 'Fort-de-France', 147, 1),
(1033, 'Plaines Wilhelms', 148, 1),
(1034, 'Port-Louis', 148, 1),
(1035, 'Blantyre', 149, 1),
(1036, 'Lilongwe', 149, 1),
(1037, 'Johor', 150, 1),
(1038, 'Kedah', 150, 1),
(1039, 'Kelantan', 150, 1),
(1040, 'Negeri Sembilan', 150, 1),
(1041, 'Pahang', 150, 1),
(1042, 'Perak', 150, 1),
(1043, 'Pulau Pinang', 150, 1),
(1044, 'Sabah', 150, 1),
(1045, 'Sarawak', 150, 1),
(1046, 'Selangor', 150, 1),
(1047, 'Terengganu', 150, 1),
(1048, 'Wilayah Persekutuan', 150, 1),
(1049, 'Mamoutzou', 151, 1),
(1050, 'Khomas', 152, 1),
(1051, 'â€“', 153, 1),
(1052, 'Maradi', 154, 1),
(1053, 'Niamey', 154, 1),
(1054, 'Zinder', 154, 1),
(1055, 'â€“', 155, 1),
(1056, 'Anambra & Enugu & Eb', 156, 1),
(1057, 'Bauchi & Gombe', 156, 1),
(1058, 'Benue', 156, 1),
(1059, 'Borno & Yobe', 156, 1),
(1060, 'Cross River', 156, 1),
(1061, 'Edo & Delta', 156, 1),
(1062, 'Federal Capital Dist', 156, 1),
(1063, 'Imo & Abia', 156, 1),
(1064, 'Kaduna', 156, 1),
(1065, 'Kano & Jigawa', 156, 1),
(1066, 'Katsina', 156, 1),
(1067, 'Kwara & Kogi', 156, 1),
(1068, 'Lagos', 156, 1),
(1069, 'Niger', 156, 1),
(1070, 'Ogun', 156, 1),
(1071, 'Ondo & Ekiti', 156, 1),
(1072, 'Oyo & Osun', 156, 1),
(1073, 'Plateau & Nassarawa', 156, 1),
(1074, 'Rivers & Bayelsa', 156, 1),
(1075, 'Sokoto & Kebbi & Zam', 156, 1),
(1076, 'Chinandega', 157, 1),
(1077, 'LeÃ³n', 157, 1),
(1078, 'Managua', 157, 1),
(1079, 'Masaya', 157, 1),
(1080, 'â€“', 158, 1),
(1081, 'Drenthe', 159, 1),
(1082, 'Flevoland', 159, 1),
(1083, 'Gelderland', 159, 1),
(1084, 'Groningen', 159, 1),
(1085, 'Limburg', 159, 1),
(1086, 'Noord-Brabant', 159, 1),
(1087, 'Noord-Holland', 159, 1),
(1088, 'Overijssel', 159, 1),
(1089, 'Utrecht', 159, 1),
(1090, 'Zuid-Holland', 159, 1),
(1091, 'Akershus', 160, 1),
(1092, 'Hordaland', 160, 1),
(1093, 'Oslo', 160, 1),
(1094, 'Rogaland', 160, 1),
(1095, 'SÃ¸r-TrÃ¸ndelag', 160, 1),
(1096, 'Central', 161, 1),
(1097, 'Eastern', 161, 1),
(1098, 'Western', 161, 1),
(1099, 'â€“', 162, 1),
(1100, 'Auckland', 163, 1),
(1101, 'Canterbury', 163, 1),
(1102, 'Dunedin', 163, 1),
(1103, 'Hamilton', 163, 1),
(1104, 'Wellington', 163, 1),
(1105, 'al-Batina', 164, 1),
(1106, 'Masqat', 164, 1),
(1107, 'Zufar', 164, 1),
(1108, 'Baluchistan', 165, 1),
(1109, 'Islamabad', 165, 1),
(1110, 'Nothwest Border Prov', 165, 1),
(1111, 'Punjab', 165, 1),
(1112, 'Sind', 165, 1),
(1113, 'Sindh', 165, 1),
(1114, 'PanamÃ¡', 166, 1),
(1115, 'San Miguelito', 166, 1),
(1116, 'â€“', 167, 1),
(1117, 'Ancash', 168, 1),
(1118, 'Arequipa', 168, 1),
(1119, 'Ayacucho', 168, 1),
(1120, 'Cajamarca', 168, 1),
(1121, 'Callao', 168, 1),
(1122, 'Cusco', 168, 1),
(1123, 'Huanuco', 168, 1),
(1124, 'Ica', 168, 1),
(1125, 'JunÃ­n', 168, 1),
(1126, 'La Libertad', 168, 1),
(1127, 'Lambayeque', 168, 1),
(1128, 'Lima', 168, 1),
(1129, 'Loreto', 168, 1),
(1130, 'Piura', 168, 1),
(1131, 'Puno', 168, 1),
(1132, 'Tacna', 168, 1),
(1133, 'Ucayali', 168, 1),
(1134, 'ARMM', 169, 1),
(1135, 'Bicol', 169, 1),
(1136, 'Cagayan Valley', 169, 1),
(1137, 'CAR', 169, 1),
(1138, 'Caraga', 169, 1),
(1139, 'Central Luzon', 169, 1),
(1140, 'Central Mindanao', 169, 1),
(1141, 'Central Visayas', 169, 1),
(1142, 'Eastern Visayas', 169, 1),
(1143, 'Ilocos', 169, 1),
(1144, 'National Capital Reg', 169, 1),
(1145, 'Northern Mindanao', 169, 1),
(1146, 'Southern Mindanao', 169, 1),
(1147, 'Southern Tagalog', 169, 1),
(1148, 'Western Mindanao', 169, 1),
(1149, 'Western Visayas', 169, 1),
(1150, 'Koror', 170, 1),
(1151, 'National Capital Dis', 171, 1),
(1152, 'Dolnoslaskie', 172, 1),
(1153, 'Kujawsko-Pomorskie', 172, 1),
(1154, 'Lodzkie', 172, 1),
(1155, 'Lubelskie', 172, 1),
(1156, 'Lubuskie', 172, 1),
(1157, 'Malopolskie', 172, 1),
(1158, 'Mazowieckie', 172, 1),
(1159, 'Opolskie', 172, 1),
(1160, 'Podkarpackie', 172, 1),
(1161, 'Podlaskie', 172, 1),
(1162, 'Pomorskie', 172, 1),
(1163, 'Slaskie', 172, 1),
(1164, 'Swietokrzyskie', 172, 1),
(1165, 'Warminsko-Mazurskie', 172, 1),
(1166, 'Wielkopolskie', 172, 1),
(1167, 'Zachodnio-Pomorskie', 172, 1),
(1168, 'Arecibo', 173, 1),
(1169, 'BayamÃ³n', 173, 1),
(1170, 'Caguas', 173, 1),
(1171, 'Carolina', 173, 1),
(1172, 'Guaynabo', 173, 1),
(1173, 'MayagÃ¼ez', 173, 1),
(1174, 'Ponce', 173, 1),
(1175, 'San Juan', 173, 1),
(1176, 'Toa Baja', 173, 1),
(1177, 'Chagang', 174, 1),
(1178, 'Hamgyong N', 174, 1),
(1179, 'Hamgyong P', 174, 1),
(1180, 'Hwanghae N', 174, 1),
(1181, 'Hwanghae P', 174, 1),
(1182, 'Kaesong-si', 174, 1),
(1183, 'Kangwon', 174, 1),
(1184, 'Nampo-si', 174, 1),
(1185, 'Pyongan N', 174, 1),
(1186, 'Pyongan P', 174, 1),
(1187, 'Pyongyang-si', 174, 1),
(1188, 'Yanggang', 174, 1),
(1189, 'Braga', 175, 1),
(1190, 'CoÃ­mbra', 175, 1),
(1191, 'Lisboa', 175, 1),
(1192, 'Porto', 175, 1),
(1193, 'Alto ParanÃ¡', 176, 1),
(1194, 'AsunciÃ³n', 176, 1),
(1195, 'Central', 176, 1),
(1196, 'Gaza', 177, 1),
(1197, 'Hebron', 177, 1),
(1198, 'Khan Yunis', 177, 1),
(1199, 'Nablus', 177, 1),
(1200, 'North Gaza', 177, 1),
(1201, 'Rafah', 177, 1),
(1202, 'Tahiti', 178, 1),
(1203, 'Doha', 179, 1),
(1204, 'Saint-Denis', 180, 1),
(1205, 'Arad', 181, 1),
(1206, 'Arges', 181, 1),
(1207, 'Bacau', 181, 1),
(1208, 'Bihor', 181, 1),
(1209, 'Botosani', 181, 1),
(1210, 'Braila', 181, 1),
(1211, 'Brasov', 181, 1),
(1212, 'Bukarest', 181, 1),
(1213, 'Buzau', 181, 1),
(1214, 'Caras-Severin', 181, 1),
(1215, 'Cluj', 181, 1),
(1216, 'Constanta', 181, 1),
(1217, 'DÃ¢mbovita', 181, 1),
(1218, 'Dolj', 181, 1),
(1219, 'Galati', 181, 1),
(1220, 'Gorj', 181, 1),
(1221, 'Iasi', 181, 1),
(1222, 'Maramures', 181, 1),
(1223, 'Mehedinti', 181, 1),
(1224, 'Mures', 181, 1),
(1225, 'Neamt', 181, 1),
(1226, 'Prahova', 181, 1),
(1227, 'Satu Mare', 181, 1),
(1228, 'Sibiu', 181, 1),
(1229, 'Suceava', 181, 1),
(1230, 'Timis', 181, 1),
(1231, 'Tulcea', 181, 1),
(1232, 'VÃ¢lcea', 181, 1),
(1233, 'Vrancea', 181, 1),
(1234, 'Adygea', 182, 1),
(1235, 'Altai', 182, 1),
(1236, 'Amur', 182, 1),
(1237, 'Arkangeli', 182, 1),
(1238, 'Astrahan', 182, 1),
(1239, 'BaÅ¡kortostan', 182, 1),
(1240, 'Belgorod', 182, 1),
(1241, 'Brjansk', 182, 1),
(1242, 'Burjatia', 182, 1),
(1243, 'Dagestan', 182, 1),
(1244, 'Habarovsk', 182, 1),
(1245, 'Hakassia', 182, 1),
(1246, 'Hanti-Mansia', 182, 1),
(1247, 'Irkutsk', 182, 1),
(1248, 'Ivanovo', 182, 1),
(1249, 'Jaroslavl', 182, 1),
(1250, 'Kabardi-Balkaria', 182, 1),
(1251, 'Kaliningrad', 182, 1),
(1252, 'Kalmykia', 182, 1),
(1253, 'Kaluga', 182, 1),
(1254, 'KamtÅ¡atka', 182, 1),
(1255, 'KaratÅ¡ai-TÅ¡erkessi', 182, 1),
(1256, 'Karjala', 182, 1),
(1257, 'Kemerovo', 182, 1),
(1258, 'Kirov', 182, 1),
(1259, 'Komi', 182, 1),
(1260, 'Kostroma', 182, 1),
(1261, 'Krasnodar', 182, 1),
(1262, 'Krasnojarsk', 182, 1),
(1263, 'Kurgan', 182, 1),
(1264, 'Kursk', 182, 1),
(1265, 'Lipetsk', 182, 1),
(1266, 'Magadan', 182, 1),
(1267, 'Marinmaa', 182, 1),
(1268, 'Mordva', 182, 1),
(1269, 'Moscow (City)', 182, 1),
(1270, 'Moskova', 182, 1),
(1271, 'Murmansk', 182, 1),
(1272, 'Nizni Novgorod', 182, 1),
(1273, 'North Ossetia-Alania', 182, 1),
(1274, 'Novgorod', 182, 1),
(1275, 'Novosibirsk', 182, 1),
(1276, 'Omsk', 182, 1),
(1277, 'Orenburg', 182, 1),
(1278, 'Orjol', 182, 1),
(1279, 'Penza', 182, 1),
(1280, 'Perm', 182, 1),
(1281, 'Pietari', 182, 1),
(1282, 'Pihkova', 182, 1),
(1283, 'Primorje', 182, 1),
(1284, 'Rjazan', 182, 1),
(1285, 'Rostov-na-Donu', 182, 1),
(1286, 'Saha (Jakutia)', 182, 1),
(1287, 'Sahalin', 182, 1),
(1288, 'Samara', 182, 1),
(1289, 'Saratov', 182, 1),
(1290, 'Smolensk', 182, 1),
(1291, 'Stavropol', 182, 1),
(1292, 'Sverdlovsk', 182, 1),
(1293, 'Tambov', 182, 1),
(1294, 'Tatarstan', 182, 1),
(1295, 'Tjumen', 182, 1),
(1296, 'Tomsk', 182, 1),
(1297, 'Tula', 182, 1),
(1298, 'Tver', 182, 1),
(1299, 'Tyva', 182, 1),
(1300, 'TÅ¡eljabinsk', 182, 1),
(1301, 'TÅ¡etÅ¡enia', 182, 1),
(1302, 'TÅ¡ita', 182, 1),
(1303, 'TÅ¡uvassia', 182, 1),
(1304, 'Udmurtia', 182, 1),
(1305, 'Uljanovsk', 182, 1),
(1306, 'Vladimir', 182, 1),
(1307, 'Volgograd', 182, 1),
(1308, 'Vologda', 182, 1),
(1309, 'Voronez', 182, 1),
(1310, 'Yamalin Nenetsia', 182, 1),
(1311, 'Kigali', 183, 1),
(1312, 'al-Khudud al-Samaliy', 184, 1),
(1313, 'al-Qasim', 184, 1),
(1314, 'al-Sharqiya', 184, 1),
(1315, 'Asir', 184, 1),
(1316, 'Hail', 184, 1),
(1317, 'Medina', 184, 1),
(1318, 'Mekka', 184, 1),
(1319, 'Najran', 184, 1),
(1320, 'Qasim', 184, 1),
(1321, 'Riad', 184, 1),
(1322, 'Riyadh', 184, 1),
(1323, 'Tabuk', 184, 1),
(1324, 'al-Bahr al-Abyad', 185, 1),
(1325, 'al-Bahr al-Ahmar', 185, 1),
(1326, 'al-Jazira', 185, 1),
(1327, 'al-Qadarif', 185, 1),
(1328, 'Bahr al-Jabal', 185, 1),
(1329, 'Darfur al-Janubiya', 185, 1),
(1330, 'Darfur al-Shamaliya', 185, 1),
(1331, 'Kassala', 185, 1),
(1332, 'Khartum', 185, 1),
(1333, 'Kurdufan al-Shamaliy', 185, 1),
(1334, 'Cap-Vert', 186, 1),
(1335, 'Diourbel', 186, 1),
(1336, 'Kaolack', 186, 1),
(1337, 'Saint-Louis', 186, 1),
(1338, 'ThiÃ¨s', 186, 1),
(1339, 'Ziguinchor', 186, 1),
(1340, 'â€“', 187, 1),
(1341, 'Saint Helena', 189, 1),
(1342, 'LÃ¤nsimaa', 190, 1),
(1343, 'Honiara', 191, 1),
(1344, 'Western', 192, 1),
(1345, 'La Libertad', 193, 1),
(1346, 'San Miguel', 193, 1),
(1347, 'San Salvador', 193, 1),
(1348, 'Santa Ana', 193, 1),
(1349, 'San Marino', 194, 1),
(1350, 'Serravalle/Dogano', 194, 1),
(1351, 'Banaadir', 195, 1),
(1352, 'Jubbada Hoose', 195, 1),
(1353, 'Woqooyi Galbeed', 195, 1),
(1354, 'Saint-Pierre', 196, 1),
(1355, 'Aqua Grande', 197, 1),
(1356, 'Paramaribo', 198, 1),
(1357, 'Bratislava', 199, 1),
(1358, 'VÃ½chodnÃ© Slovensko', 199, 1),
(1359, 'Osrednjeslovenska', 200, 1),
(1360, 'Podravska', 200, 1),
(1361, 'Ã–rebros lÃ¤n', 201, 1),
(1362, 'East GÃ¶tanmaan lÃ¤n', 201, 1),
(1363, 'GÃ¤vleborgs lÃ¤n', 201, 1),
(1364, 'JÃ¶nkÃ¶pings lÃ¤n', 201, 1),
(1365, 'Lisboa', 201, 1),
(1366, 'SkÃ¥ne lÃ¤n', 201, 1),
(1367, 'Uppsala lÃ¤n', 201, 1),
(1368, 'VÃ¤sterbottens lÃ¤n', 201, 1),
(1369, 'VÃ¤sternorrlands lÃ¤', 201, 1),
(1370, 'VÃ¤stmanlands lÃ¤n', 201, 1),
(1371, 'West GÃ¶tanmaan lÃ¤n', 201, 1),
(1372, 'Hhohho', 202, 1),
(1373, 'MahÃ©', 203, 1),
(1374, 'al-Hasaka', 204, 1),
(1375, 'al-Raqqa', 204, 1),
(1376, 'Aleppo', 204, 1),
(1377, 'Damascus', 204, 1),
(1378, 'Damaskos', 204, 1),
(1379, 'Dayr al-Zawr', 204, 1),
(1380, 'Hama', 204, 1),
(1381, 'Hims', 204, 1),
(1382, 'Idlib', 204, 1),
(1383, 'Latakia', 204, 1),
(1384, 'Grand Turk', 205, 1),
(1385, 'Chari-Baguirmi', 206, 1),
(1386, 'Logone Occidental', 206, 1),
(1387, 'Maritime', 207, 1),
(1388, 'Bangkok', 208, 1),
(1389, 'Chiang Mai', 208, 1),
(1390, 'Khon Kaen', 208, 1),
(1391, 'Nakhon Pathom', 208, 1),
(1392, 'Nakhon Ratchasima', 208, 1),
(1393, 'Nakhon Sawan', 208, 1),
(1394, 'Nonthaburi', 208, 1),
(1395, 'Songkhla', 208, 1),
(1396, 'Ubon Ratchathani', 208, 1),
(1397, 'Udon Thani', 208, 1),
(1398, 'Karotegin', 209, 1),
(1399, 'Khujand', 209, 1),
(1400, 'Fakaofo', 210, 1),
(1401, 'Ahal', 211, 1),
(1402, 'Dashhowuz', 211, 1),
(1403, 'Lebap', 211, 1),
(1404, 'Mary', 211, 1),
(1405, 'Dili', 212, 1),
(1406, 'Tongatapu', 213, 1),
(1407, 'Caroni', 214, 1),
(1408, 'Port-of-Spain', 214, 1),
(1409, 'Ariana', 215, 1),
(1410, 'Biserta', 215, 1),
(1411, 'GabÃ¨s', 215, 1),
(1412, 'Kairouan', 215, 1),
(1413, 'Sfax', 215, 1),
(1414, 'Sousse', 215, 1),
(1415, 'Tunis', 215, 1),
(1416, 'Adana', 216, 1),
(1417, 'Adiyaman', 216, 1),
(1418, 'Afyon', 216, 1),
(1419, 'Aksaray', 216, 1),
(1420, 'Ankara', 216, 1),
(1421, 'Antalya', 216, 1),
(1422, 'Aydin', 216, 1),
(1423, 'Ã‡orum', 216, 1),
(1424, 'Balikesir', 216, 1),
(1425, 'Batman', 216, 1),
(1426, 'Bursa', 216, 1),
(1427, 'Denizli', 216, 1),
(1428, 'Diyarbakir', 216, 1),
(1429, 'Edirne', 216, 1),
(1430, 'ElÃ¢zig', 216, 1),
(1431, 'Erzincan', 216, 1),
(1432, 'Erzurum', 216, 1),
(1433, 'Eskisehir', 216, 1),
(1434, 'Gaziantep', 216, 1),
(1435, 'Hatay', 216, 1),
(1436, 'IÃ§el', 216, 1),
(1437, 'Isparta', 216, 1),
(1438, 'Istanbul', 216, 1),
(1439, 'Izmir', 216, 1),
(1440, 'Kahramanmaras', 216, 1),
(1441, 'KarabÃ¼k', 216, 1),
(1442, 'Karaman', 216, 1),
(1443, 'Kars', 216, 1),
(1444, 'Kayseri', 216, 1),
(1445, 'KÃ¼tahya', 216, 1),
(1446, 'Kilis', 216, 1),
(1447, 'Kirikkale', 216, 1),
(1448, 'Kocaeli', 216, 1),
(1449, 'Konya', 216, 1),
(1450, 'Malatya', 216, 1),
(1451, 'Manisa', 216, 1),
(1452, 'Mardin', 216, 1),
(1453, 'Ordu', 216, 1),
(1454, 'Osmaniye', 216, 1),
(1455, 'Sakarya', 216, 1),
(1456, 'Samsun', 216, 1),
(1457, 'Sanliurfa', 216, 1),
(1458, 'Siirt', 216, 1),
(1459, 'Sivas', 216, 1),
(1460, 'Tekirdag', 216, 1),
(1461, 'Tokat', 216, 1),
(1462, 'Trabzon', 216, 1),
(1463, 'Usak', 216, 1),
(1464, 'Van', 216, 1),
(1465, 'Zonguldak', 216, 1),
(1466, 'Funafuti', 217, 1),
(1468, 'Changhwa', 218, 1),
(1469, 'Chiayi', 218, 1),
(1470, 'Hsinchu', 218, 1),
(1471, 'Hualien', 218, 1),
(1472, 'Ilan', 218, 1),
(1473, 'Kaohsiung', 218, 1),
(1474, 'Keelung', 218, 1),
(1475, 'Miaoli', 218, 1),
(1476, 'Nantou', 218, 1),
(1477, 'Pingtung', 218, 1),
(1478, 'Taichung', 218, 1),
(1479, 'Tainan', 218, 1),
(1480, 'Taipei', 218, 1),
(1481, 'Taitung', 218, 1),
(1482, 'Taoyuan', 218, 1),
(1483, 'YÃ¼nlin', 218, 1),
(1484, 'Arusha', 219, 1),
(1485, 'Dar es Salaam', 219, 1),
(1486, 'Dodoma', 219, 1),
(1487, 'Kilimanjaro', 219, 1),
(1488, 'Mbeya', 219, 1),
(1489, 'Morogoro', 219, 1),
(1490, 'Mwanza', 219, 1),
(1491, 'Tabora', 219, 1),
(1492, 'Tanga', 219, 1),
(1493, 'Zanzibar West', 219, 1),
(1494, 'Central', 220, 1),
(1495, 'Dnipropetrovsk', 221, 1),
(1496, 'Donetsk', 221, 1),
(1497, 'Harkova', 221, 1),
(1498, 'Herson', 221, 1),
(1499, 'Hmelnytskyi', 221, 1),
(1500, 'Ivano-Frankivsk', 221, 1),
(1501, 'Kiova', 221, 1),
(1502, 'Kirovograd', 221, 1),
(1503, 'Krim', 221, 1),
(1504, 'Lugansk', 221, 1),
(1505, 'Lviv', 221, 1),
(1506, 'Mykolajiv', 221, 1),
(1507, 'Odesa', 221, 1),
(1508, 'Pultava', 221, 1),
(1509, 'Rivne', 221, 1),
(1510, 'Sumy', 221, 1),
(1511, 'Taka-Karpatia', 221, 1),
(1512, 'Ternopil', 221, 1),
(1513, 'TÅ¡erkasy', 221, 1),
(1514, 'TÅ¡ernigiv', 221, 1),
(1515, 'TÅ¡ernivtsi', 221, 1),
(1516, 'Vinnytsja', 221, 1),
(1517, 'Volynia', 221, 1),
(1518, 'Zaporizzja', 221, 1),
(1519, 'Zytomyr', 221, 1),
(1520, 'Montevideo', 223, 1),
(1521, 'Alabama', 224, 1),
(1522, 'Alaska', 224, 1),
(1523, 'Arizona', 224, 1),
(1524, 'Arkansas', 224, 1),
(1525, 'California', 224, 1),
(1526, 'Colorado', 224, 1),
(1527, 'Connecticut', 224, 1),
(1528, 'District of Columbia', 224, 1),
(1529, 'Florida', 224, 1),
(1530, 'Georgia', 224, 1),
(1531, 'Hawaii', 224, 1),
(1532, 'Idaho', 224, 1),
(1533, 'Illinois', 224, 1),
(1534, 'Indiana', 224, 1),
(1535, 'Iowa', 224, 1),
(1536, 'Kansas', 224, 1),
(1537, 'Kentucky', 224, 1),
(1538, 'Louisiana', 224, 1),
(1539, 'Maryland', 224, 1),
(1540, 'Massachusetts', 224, 1),
(1541, 'Michigan', 224, 1),
(1542, 'Minnesota', 224, 1),
(1543, 'Mississippi', 224, 1),
(1544, 'Missouri', 224, 1),
(1545, 'Montana', 224, 1),
(1546, 'Nebraska', 224, 1),
(1547, 'Nevada', 224, 1),
(1548, 'New Hampshire', 224, 1),
(1549, 'New Jersey', 224, 1),
(1550, 'New Mexico', 224, 1),
(1551, 'New York', 224, 1),
(1552, 'North Carolina', 224, 1),
(1553, 'Ohio', 224, 1),
(1554, 'Oklahoma', 224, 1),
(1555, 'Oregon', 224, 1),
(1556, 'Pennsylvania', 224, 1),
(1557, 'Rhode Island', 224, 1),
(1558, 'South Carolina', 224, 1),
(1559, 'South Dakota', 224, 1),
(1560, 'Tennessee', 224, 1),
(1561, 'Texas', 224, 1),
(1562, 'Utah', 224, 1),
(1563, 'Virginia', 224, 1),
(1564, 'Washington', 224, 1),
(1565, 'Wisconsin', 224, 1),
(1566, 'Andijon', 225, 1),
(1567, 'Buhoro', 225, 1),
(1568, 'Cizah', 225, 1),
(1569, 'Fargona', 225, 1),
(1570, 'Karakalpakistan', 225, 1),
(1571, 'Khorazm', 225, 1),
(1572, 'Namangan', 225, 1),
(1573, 'Navoi', 225, 1),
(1574, 'Qashqadaryo', 225, 1),
(1575, 'Samarkand', 225, 1),
(1576, 'Surkhondaryo', 225, 1),
(1577, 'Toskent', 225, 1),
(1578, 'Toskent Shahri', 225, 1),
(1579, 'â€“', 226, 1),
(1580, 'St George', 0, 1),
(1582, 'AnzoÃ¡tegui', 228, 1),
(1583, 'Apure', 228, 1),
(1584, 'Aragua', 228, 1),
(1585, 'Barinas', 228, 1),
(1586, 'BolÃ­var', 228, 1),
(1587, 'Carabobo', 228, 1),
(1588, 'Distrito Federal', 228, 1),
(1589, 'FalcÃ³n', 228, 1),
(1590, 'GuÃ¡rico', 228, 1),
(1591, 'Lara', 228, 1),
(1592, 'MÃ©rida', 228, 1),
(1593, 'Miranda', 228, 1),
(1594, 'Monagas', 228, 1),
(1595, 'Portuguesa', 228, 1),
(1596, 'Sucre', 228, 1),
(1597, 'TÃ¡chira', 228, 1),
(1598, 'Trujillo', 228, 1),
(1599, 'Yaracuy', 228, 1),
(1600, 'Zulia', 228, 1),
(1601, 'Tortola', 229, 1),
(1602, 'St Thomas', 230, 1),
(1603, 'An Giang', 231, 1),
(1604, 'Ba Ria-Vung Tau', 231, 1),
(1605, 'Bac Thai', 231, 1),
(1606, 'Binh Dinh', 231, 1),
(1607, 'Binh Thuan', 231, 1),
(1608, 'Can Tho', 231, 1),
(1609, 'Dac Lac', 231, 1),
(1610, 'Dong Nai', 231, 1),
(1611, 'Haiphong', 231, 1),
(1612, 'Hanoi', 231, 1),
(1613, 'Ho Chi Minh City', 231, 1),
(1614, 'Khanh Hoa', 231, 1),
(1615, 'Kien Giang', 231, 1),
(1616, 'Lam Dong', 231, 1),
(1617, 'Nam Ha', 231, 1),
(1618, 'Nghe An', 231, 1),
(1619, 'Quang Binh', 231, 1),
(1620, 'Quang Nam-Da Nang', 231, 1),
(1621, 'Quang Ninh', 231, 1),
(1622, 'Thua Thien-Hue', 231, 1),
(1623, 'Tien Giang', 231, 1),
(1624, 'Shefa', 232, 1),
(1625, 'Wallis', 233, 1),
(1626, 'Upolu', 234, 1),
(1627, 'Aden', 235, 1),
(1628, 'Hadramawt', 235, 1),
(1629, 'Hodeida', 235, 1),
(1630, 'Ibb', 235, 1),
(1631, 'Sanaa', 235, 1),
(1632, 'Taizz', 235, 1),
(1633, 'Central Serbia', 236, 1),
(1634, 'Kosovo and Metohija', 236, 1),
(1635, 'Montenegro', 236, 1),
(1636, 'Vojvodina', 236, 1),
(1637, 'Eastern Cape', 237, 1),
(1638, 'Free State', 237, 1),
(1639, 'Gauteng', 237, 1),
(1640, 'KwaZulu-Natal', 237, 1),
(1641, 'Mpumalanga', 237, 1),
(1642, 'North West', 237, 1),
(1643, 'Northern Cape', 237, 1),
(1644, 'Western Cape', 237, 1),
(1645, 'Central', 238, 1),
(1646, 'Copperbelt', 238, 1),
(1647, 'Lusaka', 238, 1),
(1648, 'Bulawayo', 239, 1),
(1649, 'Harare', 239, 1),
(1650, 'Manicaland', 239, 1),
(1651, 'Midlands', 239, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pms_status_type`
--

CREATE TABLE `pms_status_type` (
  `status_id` int(11) NOT NULL,
  `status_category` varchar(50) NOT NULL,
  `status_title` varchar(50) NOT NULL,
  `status_color` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pms_status_type`
--

INSERT INTO `pms_status_type` (`status_id`, `status_category`, `status_title`, `status_color`) VALUES
(2, 'room', 'occupied', 1),
(3, 'user', 'online', 1),
(4, 'room', 'out-of-order', 2),
(5, 'room', 'unoccupied', 3),
(6, 'room', 'out-of-service', 4),
(7, 'room', 'vacant-but-dirty', 5),
(9, 'booking', 'Single Booking', 1),
(10, 'booking', 'Group Booking', 1),
(11, 'booking', 'Corporate', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pms_telephone`
--

CREATE TABLE `pms_telephone` (
  `number_id` int(50) NOT NULL,
  `number` varchar(50) NOT NULL,
  `entity_id` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pms_telephone`
--

INSERT INTO `pms_telephone` (`number_id`, `number`, `entity_id`) VALUES
(1, '08153259099', 2),
(2, '08153259099', 3),
(3, '08153259099', 4),
(4, '08153259099', 5),
(5, '08153259099', 6),
(6, '08153259099', 7),
(7, '08153259099', 8),
(8, '08153259099', 15),
(9, '08153259099', 17),
(10, '08153259099', 18),
(11, '08153259099', 20),
(12, '08153259099', 25),
(13, '08153259099', 26),
(14, '08153259099', 27),
(15, '08153259099', 28),
(16, '08153259099', 29),
(17, '08153259099', 30),
(18, '08153259099', 31),
(19, '08153259099', 32),
(20, '08153259099', 33),
(21, '08153259099', 34),
(22, '08153259099', 35),
(23, '08153259099', 36),
(24, '08153259099', 37),
(25, '08153259099', 38),
(26, '08153259099', 39),
(27, '08153259099', 40),
(28, '08153259099', 42),
(29, '08153259099', 43),
(30, '08153259099', 45),
(31, '08153259099', 47),
(32, '08153259099', 49),
(33, '08153259099', 51),
(34, '08153259099', 55),
(35, '08153259099', 57),
(36, '08153259099', 59),
(37, '08153259099', 60),
(38, '08153259099', 61),
(39, '08153259099', 63),
(40, '08153259099', 65),
(41, '0902726267', 67),
(42, '0902726267', 69),
(43, '09028263738', 74),
(44, '09028263738', 76),
(45, '070746462829', 82),
(46, '09028263738', 86),
(47, '09028263738', 88),
(48, '09028263738', 90),
(49, '09028263738', 92),
(50, '070746462829', 94),
(51, '09028263738', 96),
(52, '0902726267', 98),
(53, '0902726267', 100),
(54, '0902726267', 102),
(55, '0902726267', 104);

-- --------------------------------------------------------

--
-- Table structure for table `pms_user`
--

CREATE TABLE `pms_user` (
  `user_id` int(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `entity_id` int(255) NOT NULL,
  `access_level` int(255) NOT NULL,
  `status_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pms_user`
--

INSERT INTO `pms_user` (`user_id`, `username`, `password`, `entity_id`, `access_level`, `status_id`) VALUES
(3, 'demo2', 'demo2', 1, 1, 1),
(4, 'admin', 'firstoctober', 2, 1, 3),
(5, 'emek1', 'test', 3, 1, 3),
(6, 'abc', 'abc', 4, 1, 3),
(7, 'whuijkbkbgf', '7777', 5, 1, 3),
(8, 'yrhjyr', 'ytjjyt', 6, 1, 3),
(9, '2222', '7777', 7, 1, 3),
(10, 'ojoijoi', '7777', 8, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `pms_vendor`
--

CREATE TABLE `pms_vendor` (
  `vendor_id` int(255) NOT NULL,
  `vendor_account_number` int(255) NOT NULL,
  `entity_id` int(50) NOT NULL,
  `status_id` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pms_address`
--
ALTER TABLE `pms_address`
  ADD PRIMARY KEY (`address_id`),
  ADD KEY `address_entity` (`entity_id`),
  ADD KEY `countyid1` (`country_id`),
  ADD KEY `statesid1` (`state_id`);

--
-- Indexes for table `pms_bookout_return`
--
ALTER TABLE `pms_bookout_return`
  ADD PRIMARY KEY (`booking_id`),
  ADD KEY `clientid` (`client_id`),
  ADD KEY `producti` (`product_id`),
  ADD KEY `pms_bookout_return_ibfk_1` (`status`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `pms_check_in_out`
--
ALTER TABLE `pms_check_in_out`
  ADD PRIMARY KEY (`in_out_id`),
  ADD KEY `guessids` (`guess_id`),
  ADD KEY `roomids` (`room_number_id`),
  ADD KEY `inoutinStatus` (`status`),
  ADD KEY `inoutuser` (`user_id`);

--
-- Indexes for table `pms_client`
--
ALTER TABLE `pms_client`
  ADD PRIMARY KEY (`client_id`),
  ADD KEY `client_entity` (`entity_id`);

--
-- Indexes for table `pms_color`
--
ALTER TABLE `pms_color`
  ADD PRIMARY KEY (`color_id`);

--
-- Indexes for table `pms_countries`
--
ALTER TABLE `pms_countries`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `pms_email`
--
ALTER TABLE `pms_email`
  ADD PRIMARY KEY (`email_id`),
  ADD KEY `email_entity` (`entity_id`);

--
-- Indexes for table `pms_entity`
--
ALTER TABLE `pms_entity`
  ADD PRIMARY KEY (`entity_id`);

--
-- Indexes for table `pms_folio`
--
ALTER TABLE `pms_folio`
  ADD PRIMARY KEY (`folio_id`);

--
-- Indexes for table `pms_folio_credit`
--
ALTER TABLE `pms_folio_credit`
  ADD KEY `folio_credit` (`folio_id`);

--
-- Indexes for table `pms_folio_debit`
--
ALTER TABLE `pms_folio_debit`
  ADD KEY `folio_debit` (`folio_id`);

--
-- Indexes for table `pms_guest`
--
ALTER TABLE `pms_guest`
  ADD PRIMARY KEY (`id`),
  ADD KEY `guest_entity` (`entity_id`),
  ADD KEY `guest_status` (`status_id`),
  ADD KEY `guest_user` (`user_id`);

--
-- Indexes for table `pms_ledger`
--
ALTER TABLE `pms_ledger`
  ADD PRIMARY KEY (`ledger_id`);

--
-- Indexes for table `pms_order`
--
ALTER TABLE `pms_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pms_person`
--
ALTER TABLE `pms_person`
  ADD PRIMARY KEY (`person_id`),
  ADD KEY `person_entity` (`entity_id`);

--
-- Indexes for table `pms_product_inventory`
--
ALTER TABLE `pms_product_inventory`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `vendor` (`vendor_id`);

--
-- Indexes for table `pms_recipt`
--
ALTER TABLE `pms_recipt`
  ADD PRIMARY KEY (`recipt_id`),
  ADD KEY `recieipt_status` (`receipt_issue_date`);

--
-- Indexes for table `pms_recipt_relationship`
--
ALTER TABLE `pms_recipt_relationship`
  ADD KEY `re_relationship` (`recipt_id`);

--
-- Indexes for table `pms_restaurant_booking`
--
ALTER TABLE `pms_restaurant_booking`
  ADD PRIMARY KEY (`booking_id`),
  ADD KEY `guessid` (`entity_id`),
  ADD KEY `roomnumberid` (`room_number_id`),
  ADD KEY `restaurant_status` (`status`),
  ADD KEY `restaurant_user` (`user_id`);

--
-- Indexes for table `pms_restaurant_price`
--
ALTER TABLE `pms_restaurant_price`
  ADD PRIMARY KEY (`price_id`);

--
-- Indexes for table `pms_restaurant_product`
--
ALTER TABLE `pms_restaurant_product`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `restaurantprice` (`price_id`),
  ADD KEY `restaurant_pstatus` (`product_status`);

--
-- Indexes for table `pms_role`
--
ALTER TABLE `pms_role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `pms_room`
--
ALTER TABLE `pms_room`
  ADD PRIMARY KEY (`room_id`),
  ADD KEY `room_status` (`status`),
  ADD KEY `roomtype` (`room_type`),
  ADD KEY `tariff` (`room_tariff_id`);

--
-- Indexes for table `pms_room_booking`
--
ALTER TABLE `pms_room_booking`
  ADD PRIMARY KEY (`booking_id`),
  ADD KEY `guestid` (`entity_id`),
  ADD KEY `roomid` (`room_id`),
  ADD KEY `tarrifid` (`tariff_id`),
  ADD KEY `room_booking_id` (`user_id`);

--
-- Indexes for table `pms_room_tariff`
--
ALTER TABLE `pms_room_tariff`
  ADD PRIMARY KEY (`tariff_id`);

--
-- Indexes for table `pms_room_type`
--
ALTER TABLE `pms_room_type`
  ADD PRIMARY KEY (`room_type_id`);

--
-- Indexes for table `pms_states`
--
ALTER TABLE `pms_states`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `pms_status_type`
--
ALTER TABLE `pms_status_type`
  ADD PRIMARY KEY (`status_id`),
  ADD KEY `status_color` (`status_color`);

--
-- Indexes for table `pms_telephone`
--
ALTER TABLE `pms_telephone`
  ADD PRIMARY KEY (`number_id`),
  ADD KEY `telentityid` (`entity_id`);

--
-- Indexes for table `pms_user`
--
ALTER TABLE `pms_user`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `user_entity_id` (`entity_id`);

--
-- Indexes for table `pms_vendor`
--
ALTER TABLE `pms_vendor`
  ADD PRIMARY KEY (`vendor_id`),
  ADD KEY `vendor_entity` (`entity_id`),
  ADD KEY `vendor_status` (`status_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pms_address`
--
ALTER TABLE `pms_address`
  MODIFY `address_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `pms_bookout_return`
--
ALTER TABLE `pms_bookout_return`
  MODIFY `booking_id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pms_check_in_out`
--
ALTER TABLE `pms_check_in_out`
  MODIFY `in_out_id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pms_client`
--
ALTER TABLE `pms_client`
  MODIFY `client_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pms_color`
--
ALTER TABLE `pms_color`
  MODIFY `color_id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `pms_countries`
--
ALTER TABLE `pms_countries`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;
--
-- AUTO_INCREMENT for table `pms_email`
--
ALTER TABLE `pms_email`
  MODIFY `email_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `pms_entity`
--
ALTER TABLE `pms_entity`
  MODIFY `entity_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;
--
-- AUTO_INCREMENT for table `pms_folio`
--
ALTER TABLE `pms_folio`
  MODIFY `folio_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `pms_guest`
--
ALTER TABLE `pms_guest`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `pms_ledger`
--
ALTER TABLE `pms_ledger`
  MODIFY `ledger_id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pms_order`
--
ALTER TABLE `pms_order`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pms_person`
--
ALTER TABLE `pms_person`
  MODIFY `person_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `pms_product_inventory`
--
ALTER TABLE `pms_product_inventory`
  MODIFY `product_id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pms_recipt`
--
ALTER TABLE `pms_recipt`
  MODIFY `recipt_id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `pms_restaurant_booking`
--
ALTER TABLE `pms_restaurant_booking`
  MODIFY `booking_id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pms_restaurant_price`
--
ALTER TABLE `pms_restaurant_price`
  MODIFY `price_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pms_restaurant_product`
--
ALTER TABLE `pms_restaurant_product`
  MODIFY `product_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `pms_role`
--
ALTER TABLE `pms_role`
  MODIFY `role_id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pms_room`
--
ALTER TABLE `pms_room`
  MODIFY `room_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=188;
--
-- AUTO_INCREMENT for table `pms_room_booking`
--
ALTER TABLE `pms_room_booking`
  MODIFY `booking_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `pms_room_tariff`
--
ALTER TABLE `pms_room_tariff`
  MODIFY `tariff_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `pms_room_type`
--
ALTER TABLE `pms_room_type`
  MODIFY `room_type_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `pms_states`
--
ALTER TABLE `pms_states`
  MODIFY `state_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1652;
--
-- AUTO_INCREMENT for table `pms_status_type`
--
ALTER TABLE `pms_status_type`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `pms_telephone`
--
ALTER TABLE `pms_telephone`
  MODIFY `number_id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `pms_user`
--
ALTER TABLE `pms_user`
  MODIFY `user_id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `pms_vendor`
--
ALTER TABLE `pms_vendor`
  MODIFY `vendor_id` int(255) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `pms_address`
--
ALTER TABLE `pms_address`
  ADD CONSTRAINT `address_entity` FOREIGN KEY (`entity_id`) REFERENCES `pms_entity` (`entity_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `countyid1` FOREIGN KEY (`country_id`) REFERENCES `pms_countries` (`country_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `statesid1` FOREIGN KEY (`state_id`) REFERENCES `pms_states` (`state_id`) ON UPDATE CASCADE;

--
-- Constraints for table `pms_bookout_return`
--
ALTER TABLE `pms_bookout_return`
  ADD CONSTRAINT `clientid` FOREIGN KEY (`client_id`) REFERENCES `pms_client` (`client_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_bookout_return_ibfk_1` FOREIGN KEY (`status`) REFERENCES `pms_status_type` (`status_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_bookout_return_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `pms_user` (`user_id`),
  ADD CONSTRAINT `producti` FOREIGN KEY (`product_id`) REFERENCES `pms_product_inventory` (`product_id`) ON UPDATE CASCADE;

--
-- Constraints for table `pms_check_in_out`
--
ALTER TABLE `pms_check_in_out`
  ADD CONSTRAINT `guessids` FOREIGN KEY (`guess_id`) REFERENCES `pms_guest` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `inoutinStatus` FOREIGN KEY (`status`) REFERENCES `pms_status_type` (`status_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `inoutuser` FOREIGN KEY (`user_id`) REFERENCES `pms_user` (`user_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `roomids` FOREIGN KEY (`room_number_id`) REFERENCES `pms_room` (`room_id`) ON UPDATE CASCADE;

--
-- Constraints for table `pms_client`
--
ALTER TABLE `pms_client`
  ADD CONSTRAINT `client_entity` FOREIGN KEY (`entity_id`) REFERENCES `pms_entity` (`entity_id`) ON UPDATE CASCADE;

--
-- Constraints for table `pms_email`
--
ALTER TABLE `pms_email`
  ADD CONSTRAINT `email_entity` FOREIGN KEY (`entity_id`) REFERENCES `pms_entity` (`entity_id`);

--
-- Constraints for table `pms_folio_credit`
--
ALTER TABLE `pms_folio_credit`
  ADD CONSTRAINT `folio_credit` FOREIGN KEY (`folio_id`) REFERENCES `pms_folio` (`folio_id`) ON UPDATE CASCADE;

--
-- Constraints for table `pms_folio_debit`
--
ALTER TABLE `pms_folio_debit`
  ADD CONSTRAINT `folio_debit` FOREIGN KEY (`folio_id`) REFERENCES `pms_folio` (`folio_id`) ON UPDATE CASCADE;

--
-- Constraints for table `pms_guest`
--
ALTER TABLE `pms_guest`
  ADD CONSTRAINT `guest_entity` FOREIGN KEY (`entity_id`) REFERENCES `pms_entity` (`entity_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `guest_status` FOREIGN KEY (`status_id`) REFERENCES `pms_status_type` (`status_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `guest_user` FOREIGN KEY (`user_id`) REFERENCES `pms_user` (`user_id`) ON UPDATE CASCADE;

--
-- Constraints for table `pms_person`
--
ALTER TABLE `pms_person`
  ADD CONSTRAINT `person_entity` FOREIGN KEY (`entity_id`) REFERENCES `pms_entity` (`entity_id`) ON UPDATE CASCADE;

--
-- Constraints for table `pms_product_inventory`
--
ALTER TABLE `pms_product_inventory`
  ADD CONSTRAINT `vendor` FOREIGN KEY (`vendor_id`) REFERENCES `pms_vendor` (`vendor_id`) ON UPDATE CASCADE;

--
-- Constraints for table `pms_recipt_relationship`
--
ALTER TABLE `pms_recipt_relationship`
  ADD CONSTRAINT `re_relationship` FOREIGN KEY (`recipt_id`) REFERENCES `pms_recipt` (`recipt_id`) ON UPDATE CASCADE;

--
-- Constraints for table `pms_restaurant_booking`
--
ALTER TABLE `pms_restaurant_booking`
  ADD CONSTRAINT `restaurant_status` FOREIGN KEY (`status`) REFERENCES `pms_status_type` (`status_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `restaurant_user` FOREIGN KEY (`user_id`) REFERENCES `pms_user` (`user_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `roomnumberid` FOREIGN KEY (`room_number_id`) REFERENCES `pms_room` (`room_id`) ON UPDATE CASCADE;

--
-- Constraints for table `pms_restaurant_product`
--
ALTER TABLE `pms_restaurant_product`
  ADD CONSTRAINT `restaurant_pstatus` FOREIGN KEY (`product_status`) REFERENCES `pms_status_type` (`status_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `restaurantprice` FOREIGN KEY (`price_id`) REFERENCES `pms_restaurant_price` (`price_id`) ON UPDATE CASCADE;

--
-- Constraints for table `pms_room`
--
ALTER TABLE `pms_room`
  ADD CONSTRAINT `room_status` FOREIGN KEY (`status`) REFERENCES `pms_status_type` (`status_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `roomtype` FOREIGN KEY (`room_type`) REFERENCES `pms_room_type` (`room_type_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tariff` FOREIGN KEY (`room_tariff_id`) REFERENCES `pms_room_tariff` (`tariff_id`);

--
-- Constraints for table `pms_room_booking`
--
ALTER TABLE `pms_room_booking`
  ADD CONSTRAINT `entityid` FOREIGN KEY (`entity_id`) REFERENCES `pms_entity` (`entity_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `room_booking_id` FOREIGN KEY (`user_id`) REFERENCES `pms_user` (`user_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `roomid` FOREIGN KEY (`room_id`) REFERENCES `pms_room` (`room_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tarrifid` FOREIGN KEY (`tariff_id`) REFERENCES `pms_room_tariff` (`tariff_id`);

--
-- Constraints for table `pms_status_type`
--
ALTER TABLE `pms_status_type`
  ADD CONSTRAINT `status` FOREIGN KEY (`status_color`) REFERENCES `pms_color` (`color_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `status_color` FOREIGN KEY (`status_color`) REFERENCES `pms_color` (`color_id`) ON UPDATE CASCADE;

--
-- Constraints for table `pms_telephone`
--
ALTER TABLE `pms_telephone`
  ADD CONSTRAINT `telentityid` FOREIGN KEY (`entity_id`) REFERENCES `pms_entity` (`entity_id`);

--
-- Constraints for table `pms_user`
--
ALTER TABLE `pms_user`
  ADD CONSTRAINT `user_entity_id` FOREIGN KEY (`entity_id`) REFERENCES `pms_entity` (`entity_id`) ON UPDATE CASCADE;

--
-- Constraints for table `pms_vendor`
--
ALTER TABLE `pms_vendor`
  ADD CONSTRAINT `vendor_entity` FOREIGN KEY (`entity_id`) REFERENCES `pms_entity` (`entity_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `vendor_status` FOREIGN KEY (`status_id`) REFERENCES `pms_status_type` (`status_id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
