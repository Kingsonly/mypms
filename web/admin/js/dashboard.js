$(document).ready(function(){
	

    $(document).ajaxStart(function(){
        $("#wait").css("display", "block");
    });
	
    $(document).ajaxComplete(function(){
        $("#wait").css("display", "none");
    });
     
	$(document).on('click','#modalclick',function(){
		var datas=$(this).data('url');
		$('#modalheader').html('<h3>Create New Admin</h3>');
		$('#modal').modal('show').find('#modalcontent').html(' ');
		$('#modal').modal('show').find('#modalcontent').load(datas);
	});
	
    $(document).on('click','.reservemodalclick',function(){
		var datas=$(this).data('url');
		$('#modalheader').html('<h3>Reservations</h3>');
		$('#reservemodal').modal('show').find('#reservemodalcontent').html(' ');
		$('#reservemodal').modal('show').find('#reservemodalcontent').load(datas);
	});
	
    $(document).on('click','.adminmodalclick',function(){
		var datas=$(this).data('url');
		$('#modalheader').html('<h3>Admin</h3>');
		$('#adminmodal').modal('show').find('#adminmodalcontent').html(' ');
		$('#adminmodal').modal('show').find('#adminmodalcontent').load(datas);
	});

    $(document).on('click','.guest',function(){
    	var datas=$(this).data('url');
		$('#modalheader').html('<h3>Guests</h3>');
		$('#guestmodal').modal('show').find('#guestmodalcontent').html(' ');
		$('#guestmodal').modal('show').find('#guestmodalcontent').load(datas);
	});
	
	$(document).on('click','.booking',function(){
		var datas=$(this).data('url');
		$('#modalheader').html('<h3>Rooms</h3>');
		$('#roommodal').modal('show').find('#roommodalcontent').html(' ');
		$('#roommodal').modal('show').find('#roommodalcontent').load(datas);
	});
	
	$(document).on('click','#checkOut',function(){
		$('#checkOutModal').modal('show').find('#checkOutContent').load($(this).data('url'));
		
	});
	
	$('.dropdown-toggle').dropdown();
	
});