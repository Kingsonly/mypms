<?php
/**
 * @copyright Copyright (c) 2017 Tycol Main (By Epsolun Ltd)
 */

namespace app\tenttacularclass;


use Yii;
use yii\base\InvalidCallException;
use yii\base\UnknownPropertyException;
use yii\base\Behavior;
use yii\base\Event;
use yii\base\ModelEvent;
use yii\db\BaseActiveRecord;
use yii\db\ActiveRecord;
use yii\db\Expression;
use app\models\Folio;
use app\models\FolioEntityRelarinship;
use app\models\Entity;
use yii\db\Expression as dateTImer;




/**
 * ComponentsBehavior automatically fetches all the associated component of a Parent component and stores value  
 * in a global/ public array to be used in any controller based on the AFTER_FIND_EVENT . 
 * 
 * This only applies to Component models 
 *
 * To use ComponentsBehavior, insert the following code to your ActiveRecord class:
 *
 * ```php
 * use app\tm_vendor\ComponentsBehavior;
 *
 * public function behaviors()
 * {
 *     return [
 *         ComponentsBehavior::className(),
 *     ];
 * }
 * ```
 *
 * By default, ComponentBehavior returns a nested array of subComponets which is identified by the variable *'$subComponents'.
 * 
 * 
 * By default component type are 6 and are held as a data type (string) in a '$componentType' numeric key array 
 * 
 * If the component names are different then this can be changed by changing the values of 'componentType', to achive this change 
 * 'newComponetsType' property to true  and add new component as an array of strings to 'componentTypeSetting' eg 
 *
 *	public function behaviors()
 * 	{
 *     return [
 *				[
 *					'class'=>ComponentsBehavior::className(),
 *					'componentTypeSetting' =>['payment','goods','products'],// add new components
 					"newComponetsType" => 'TRUE',// determine if componetTypeSetting  should  be treated as new components or  as the only components as such making the existing 								  default components null
					
 *				] 	
 *     		 ];
 * 	}
 *
 * 
 *
 */
class EntityBehaviour extends Behavior
{
	
    public $entityIds; 
	
	
    public function init()
    {
        parent::init();
		
		
    }
	
	/**
	 * inherit docs
     */
	public function events() 
	{
		
		return [
			
			ActiveRecord::EVENT_AFTER_VALIDATE => 'createNewEntity', // prevent possible 
			
		];
   }
	
	
	
	public function createNewEntity($event)
	{
		if(!$this->owner->getIsNewRecord()){
			
		} elseif(\yii\helpers\StringHelper::basename(get_class($this->owner)) == 'RestaurantBooking'){
			
			// create a new entity with a random number as name and phone number as random number 
			$this->creatFolioAndEntity();
			$this->owner->entity_id = $this->entityIds;
			
		} else{
			$this->creatFolioAndEntity();
			
		}
		
	}
	
	
	
	private function creatFolioAndEntity(){
		$folio = new Folio();
		$entity = new Entity();
		$relationship = new FolioEntityRelarinship();
		$dateNow = new dateTImer('NOW()'); // use for the time guest was created 
		$userId = Yii::$app->user->identity->id;
		$folio->creation_date = $dateNow;
		$folio->user = $userId;
		if($folio->save(false)){
			$folioId = $folio->folio_id;
			$entity->folio = $folioId;
			if(\yii\helpers\StringHelper::basename(get_class($this->owner)) == 'Userdb'){
				$entity->type = 'user';	
			} elseif(\yii\helpers\StringHelper::basename(get_class($this->owner)) == 'RestaurantBooking'){
				$entity->type = 'non_gest';
			} else{
				$entity->type = 'guest';
			}
			if($entity->save(false)){
				$relationship->relationship_entity_id = $entity->entity_id;
				$relationship->relationship_folio_id  = $folio->folio_id;
				$relationship->save(false);
				$this->entityIds = $entity->entity_id;

			}

		}
		}
	
	
}
