<?php
/**
 * @copyright Copyright (c) 2017 Tycol Main (By Epsolun Ltd)
 */

namespace app\tenttacularclass;


use Yii;
use yii\base\InvalidCallException;
use yii\base\UnknownPropertyException;
use yii\base\Behavior;
use yii\base\Event;
use yii\base\ModelEvent;
use yii\db\BaseActiveRecord;
use yii\db\ActiveRecord;
use yii\db\Expression;
use app\models\Folio;
use app\models\FolioCredit;
use app\models\FolioDebit;
use app\models\Room;
use app\models\Entity;
use app\models\Guest;
use app\models\Recipt;
use app\models\ReciptRelationship;
use yii\db\Expression as dateTImer;




/**
 * entityBehavior automatically fetches all the associated component of a Parent component and stores value  
 * in a global/ public array to be used in any controller based on the AFTER_FIND_EVENT . 
 * 
 * This only applies to Component models 
 *
 * To use ComponentsBehavior, insert the following code to your ActiveRecord class:
 *
 * ```php
 * use app\tm_vendor\ComponentsBehavior;
 *
 * public function behaviors()
 * {
 *     return [
 *         ComponentsBehavior::className(),
 *     ];
 * }
 * ```
 *
 * By default, ComponentBehavior returns a nested array of subComponets which is identified by the variable *'$subComponents'.
 * 
 * 
 * By default component type are 6 and are held as a data type (string) in a '$componentType' numeric key array 
 * 
 * If the component names are different then this can be changed by changing the values of 'componentType', to achive this change 
 * 'newComponetsType' property to true  and add new component as an array of strings to 'componentTypeSetting' eg 
 *
 *	public function behaviors()
 * 	{
 *     return [
 *				[
 *					'class'=>ComponentsBehavior::className(),
 *					'componentTypeSetting' =>['payment','goods','products'],// add new components
 					"newComponetsType" => 'TRUE',// determine if componetTypeSetting  should  be treated as new components or  as the only components as such making the existing 								  default components null
					
 *				] 	
 *     		 ];
 * 	}
 *
 * 
 *
 */
class FolioPosting extends Behavior
{
	
    public $entityIds; 
	
	
    public function init()
    {
        parent::init();
		// add amount due if its a room  booking 
		
	
		
		
    }
	
	/**
	 * inherit docs
     */
	public function events() 
	{
		
		return [
			
			ActiveRecord::EVENT_AFTER_INSERT => 'newPosting', // prevent possible 
			
		];
   }
	
	
	
	public function newPosting($event)
	{
		// in stanciate all required class
		if(\yii\helpers\StringHelper::basename(get_class($this->owner)) == 'RoomBooking' or  \yii\helpers\StringHelper::basename(get_class($this->owner)) == 'Reservation'){
			$timeIn = new \DateTime($this->owner->time_in);
			$timeOut = new \DateTime($this->owner->time_out);
			$interval = date_diff($timeIn, $timeOut);
			$roomTarrif = Room::findOne($this->owner->room_id);
			$tarrifAmount = $roomTarrif->roomTariff->tariff;
			$this->owner->amountDue = $tarrifAmount * $tarrifAmount * 1.20;
		}
		
		if(!empty($this->owner->amount_paid)){
			$this->debitCreditFolio();
			
		} else{
			$this->debitFolio();
		}
		
		
		
	}
	
	public function  debitFolio(){
		
		$folio = new Folio();
		$entity = new Entity();
		$folioDebit = new FolioDebit();
		$recipt = new Recipt();
		$reciptRelationship = new ReciptRelationship();
		$dateNow = new dateTImer('NOW()'); // use for the time gueest was created 
		$userId = Yii::$app->user->identity->id; // user id
	
		$getEntityId = $entity->findOne($this->owner->entity_id);
		$getFolioiId = $getEntityId->folio; 
		
		// attach values to table attributes 
		
		$folioDebit->folio_id = $getFolioiId;
		$folioDebit->entity_id = $this->owner->entity_id;
		$folioDebit->amount = $this->owner->amountDue;
		$folioDebit->senario_id = $this->owner->booking_id;
		$folioDebit->senario_type = \yii\helpers\StringHelper::basename(get_class($this->owner));
		$folioDebit->date_posted = $dateNow;
		
		if($folioDebit->save(false) ){
			//beging the recipt process and create a redirect to the recipt base on relationship etc
			$recipt->recipt_number = rand(100,189993) + $this->owner->booking_id;
			$recipt->amount = $this->owner->amount_paid;
			$recipt->receipt_issue_date = $dateNow;
			if($recipt->save(false)){
				$reciptRelationship->recipt_id = $recipt->recipt_id;
				$reciptRelationship->senarior_id = $this->owner->booking_id;
				$reciptRelationship->senarior_type = \yii\helpers\StringHelper::basename(get_class($this->owner));
				if($reciptRelationship->save(false)){
					//return Yii::$app->response->redirect(['room-booking/view', 'id' => $reciptRelationship->recipt_id]);
					$this->owner->reciptId = $reciptRelationship->recipt_id;
					Yii::$app->session->setFlash('amountDue', $folioDebit->amount); 
					
					
					//return \Yii::app()->getController()->redirect(['view', 'id' => $reciptRelationship->recipt_id]);
				}
				
			}
			
			
		}
		
		
	}
	
	public function  debitCreditFolio(){
		
		$folio = new Folio();
		$entity = new Entity();
		$folioCredit = new FolioCredit();
		$folioDebit = new FolioDebit();
		$recipt = new Recipt();
		$reciptRelationship = new ReciptRelationship();
		$dateNow = new dateTImer('NOW()'); // use for the time gueest was created 
		$userId = Yii::$app->user->identity->id; // user id
	
		$getEntityId = $entity->findOne($this->owner->entity_id);
		$getFolioiId = $getEntityId->folio; 
		
		// attach values to table attributes 
		
		$folioCredit->folio_id = $getFolioiId;
		$folioCredit->entity_id = $this->owner->entity_id;
		$folioCredit->amount = $this->owner->amount_paid;
		$folioCredit->senario_id = $this->owner->booking_id;
		$folioCredit->senario_type = \yii\helpers\StringHelper::basename(get_class($this->owner));
		$folioCredit->date_posted = $dateNow;
		
		$folioDebit->folio_id = $getFolioiId;
		$folioDebit->entity_id = $this->owner->entity_id;
		$folioDebit->amount = $this->owner->amountDue;
		$folioDebit->senario_id = $this->owner->booking_id;
		$folioDebit->senario_type = \yii\helpers\StringHelper::basename(get_class($this->owner));
		$folioDebit->date_posted = $dateNow;
		
		if($folioDebit->save(false) and $folioCredit->save(false)){
			//beging the recipt process and create a redirect to the recipt base on relationship etc
			$recipt->recipt_number = rand(100,189993) + $this->owner->booking_id;
			$recipt->amount = $this->owner->amount_paid;
			$recipt->receipt_issue_date = $dateNow;
			if($recipt->save(false)){
				$reciptRelationship->recipt_id = $recipt->recipt_id;
				$reciptRelationship->senarior_id = $this->owner->booking_id;
				$reciptRelationship->senarior_type = \yii\helpers\StringHelper::basename(get_class($this->owner));
				if($reciptRelationship->save(false)){
					//return Yii::$app->response->redirect(['room-booking/view', 'id' => $reciptRelationship->recipt_id]);
					$this->owner->reciptId = $reciptRelationship->recipt_id;
					Yii::$app->session->setFlash('amountDue', $folioDebit->amount); 
					
					
					//return \Yii::app()->getController()->redirect(['view', 'id' => $reciptRelationship->recipt_id]);
				}
				
			}
			
			
		}
		
		
		
	}
	
	

	
	
	
}
