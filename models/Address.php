<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pms_address".
 *
 * @property integer $address_id
 * @property string $address
 * @property integer $state_id
 * @property integer $country_id
 * @property integer $entity_id
 *
 * @property PmsEntity $entity
 * @property PmsCountries $country
 * @property PmsStates $state
 */
class Address extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pms_address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address', 'state_id', 'country_id'], 'required'],
            [['state_id', 'country_id', 'entity_id'], 'integer'],
            [['address'], 'string', 'max' => 255],
            [['entity_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entity::className(), 'targetAttribute' => ['entity_id' => 'entity_id']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Countries::className(), 'targetAttribute' => ['country_id' => 'country_id']],
            [['state_id'], 'exist', 'skipOnError' => true, 'targetClass' => States::className(), 'targetAttribute' => ['state_id' => 'state_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'address_id' => Yii::t('app', 'Address'),
            'address' => Yii::t('app', 'Address'),
            'state_id' => Yii::t('app', 'State'),
            'country_id' => Yii::t('app', 'Country'),
            'entity_id' => Yii::t('app', 'Entity ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntity()
    {
        return $this->hasOne(Entity::className(), ['entity_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Countries::className(), ['country_id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(States::className(), ['state_id' => 'state_id']);
    }
}
