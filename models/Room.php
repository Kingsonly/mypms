<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pms_room".
 *
 * @property integer $room_id
 * @property integer $room_number
 * @property integer $room_tariff_id
 * @property integer $room_type
 * @property integer $status
 *
 * @property PmsCheckInOut[] $pmsCheckInOuts
 * @property PmsRestaurantBooking[] $pmsRestaurantBookings
 * @property PmsStatusType $status0
 * @property PmsRoomType $roomType
 * @property PmsRoomTariff $roomTariff
 * @property PmsRoomBooking[] $pmsRoomBookings
 */
class   Room extends \yii\db\ActiveRecord 
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pms_room';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['room_number', 'room_tariff_id', 'room_type', 'status'], 'required'],
            [['room_number', 'room_tariff_id', 'room_type', 'status'], 'integer'],
            [['status'], 'exist', 'skipOnError' => true, 'targetClass' => StatusType::className(), 'targetAttribute' => ['status' => 'status_id']],
            [['room_type'], 'exist', 'skipOnError' => true, 'targetClass' => RoomType::className(), 'targetAttribute' => ['room_type' => 'room_type_id']],
            [['room_tariff_id'], 'exist', 'skipOnError' => true, 'targetClass' => RoomTariff::className(), 'targetAttribute' => ['room_tariff_id' => 'tariff_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'room_id' => 'Room',
            'room_number' => 'Room Number',
            'room_tariff_id' => 'Room Tariff ',
            'room_type' => 'Room Type',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckInOuts()
    {
        return $this->hasMany(CheckInOut::className(), ['room_number_id' => 'room_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurantBookings()
    {
        return $this->hasMany(RestaurantBooking::className(), ['room_number_id' => 'room_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus0()
    {
        return $this->hasOne(StatusType::className(), ['status_id' => 'status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoomType()
    {
        return $this->hasOne(RoomType::className(), ['room_type_id' => 'room_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoomTariff()
    {
        return $this->hasOne(RoomTariff::className(), ['tariff_id' => 'room_tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoomBookings()
    {
        return $this->hasMany(RoomBooking::className(), ['room_id' => 'room_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRooms()
    {
        return $this->hasOne(Rooms::className(), ['room_id' => 'room_id']);
    }

}