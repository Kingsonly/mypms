<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pms_recipt_relationship".
 *
 * @property integer $recipt_id
 * @property integer $senarior_id
 * @property string $senarior_type
 *
 * @property PmsRecipt $recipt
 */
class ReciptRelationship extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pms_recipt_relationship';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['recipt_id', 'senarior_id', 'senarior_type'], 'required'],
            [['recipt_id', 'senarior_id'], 'integer'],
            [['senarior_type'], 'string', 'max' => 50],
            [['recipt_id'], 'exist', 'skipOnError' => true, 'targetClass' => Recipt::className(), 'targetAttribute' => ['recipt_id' => 'recipt_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'recipt_id' => Yii::t('app', 'Recipt ID'),
            'senarior_id' => Yii::t('app', 'Senarior ID'),
            'senarior_type' => Yii::t('app', 'Senarior Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecipt()
    {
        return $this->hasOne(Recipt::className(), ['recipt_id' => 'recipt_id']);
    }
	public function getSenarior()
    {
        return $this->hasOne(RoomBooking::className(), ['booking_id' => 'senarior_id']);
    }
	
	
}
