<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pms_states".
 *
 * @property integer $state_id
 * @property string $state_name
 * @property integer $country_id
 * @property integer $status
 *
 * @property PmsAddress[] $pmsAddresses
 */
class States extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pms_states';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['state_name', 'country_id'], 'required'],
            [['country_id', 'status'], 'integer'],
            [['state_name'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'state_id' => Yii::t('app', 'State ID'),
            'state_name' => Yii::t('app', 'State Name'),
            'country_id' => Yii::t('app', 'Country ID'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(Address::className(), ['state_id' => 'state_id']);
    }
}
