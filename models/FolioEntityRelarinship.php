<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pms_folio_entity_relarinship".
 *
 * @property integer $relationship_folio_id
 * @property integer $relationship_entity_id
 *
 * @property PmsEntity $relationshipEntity
 * @property PmsFolio $relationshipFolio
 */
class FolioEntityRelarinship extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pms_folio_entity_relarinship';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['relationship_folio_id', 'relationship_entity_id'], 'required'],
            [['relationship_folio_id', 'relationship_entity_id'], 'integer'],
            [['relationship_entity_id'], 'exist', 'skipOnError' => true, 'targetClass' => PmsEntity::className(), 'targetAttribute' => ['relationship_entity_id' => 'entity_id']],
            [['relationship_folio_id'], 'exist', 'skipOnError' => true, 'targetClass' => PmsFolio::className(), 'targetAttribute' => ['relationship_folio_id' => 'folio_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'relationship_folio_id' => Yii::t('app', 'Relationship Folio ID'),
            'relationship_entity_id' => Yii::t('app', 'Relationship Entity ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelationshipEntity()
    {
        return $this->hasOne(PmsEntity::className(), ['entity_id' => 'relationship_entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelationshipFolio()
    {
        return $this->hasOne(PmsFolio::className(), ['folio_id' => 'relationship_folio_id']);
    }
}
