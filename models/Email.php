<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pms_email".
 *
 * @property integer $email_id
 * @property string $address
 * @property integer $entity_id
 *
 * @property PmsEntity $entity
 */
class Email extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pms_email';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address'], 'required'],
            [['entity_id'], 'integer'],
            [['address'], 'string', 'max' => 255],
            [['entity_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entity::className(), 'targetAttribute' => ['entity_id' => 'entity_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email_id' => Yii::t('app', 'Email ID'),
            'address' => Yii::t('app', 'Email Address'),
            'entity_id' => Yii::t('app', 'Entity ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntity()
    {
        return $this->hasOne(Entity::className(), ['entity_id' => 'entity_id']);
    }
}
