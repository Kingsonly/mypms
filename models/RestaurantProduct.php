<?php

namespace app\models;

use Yii;
use yz\shoppingcart\CartPositionInterface;

/**
 * This is the model class for table "pms_restaurant_product".
 *
 * @property integer $product_id
 * @property string $product_name
 * @property integer $price_id
 * @property integer $product_quantity
 * @property integer $product_status
 *
 * @property PmsRestaurantBooking[] $pmsRestaurantBookings
 * @property PmsStatusType $productStatus
 * @property PmsRestaurantPrice $price
 */
class RestaurantProduct extends \yii\db\ActiveRecord implements CartPositionInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pms_restaurant_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_name', 'price_id', 'product_quantity', 'product_status'], 'required'],
            [['price_id', 'product_quantity', 'product_status'], 'integer'],
            [['product_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => Yii::t('app', 'Product ID'),
            'product_name' => Yii::t('app', 'Product Name'),
            'price_id' => Yii::t('app', 'Price ID'),
            'product_quantity' => Yii::t('app', 'Product Quantity'),
            'product_status' => Yii::t('app', 'Product Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurantBookings()
    {
        return $this->hasMany(RestaurantBooking::className(), ['product_id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductStatus()
    {
        return $this->hasOne(StatusType::className(), ['status_id' => 'product_status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPricemain()
    {
        return $this->hasOne(RestaurantPrice::className(), ['price_id' => 'price_id']);
    }
	public function getPrice()
    {
        return $this->pricemain->product_price;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurantProducts()
    {
        return $this->hasMany(RestaurantProduct::className(), ['price_id' => 'price_id']);
    }
	
	    use \yz\shoppingcart\CartPositionTrait;

    

    public function getId()
    {
        return $this->product_id;
    }
}
