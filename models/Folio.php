<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pms_folio".
 *
 * @property integer $folio_id
 * @property string $amount
 * @property string $creation_date
 * @property integer $status
 * @property integer $user
 *
 * @property PmsFolioCredit[] $pmsFolioCredits
 * @property PmsFolioDebit[] $pmsFolioDebits
 */
class Folio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pms_folio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amount', 'creation_date', 'status', 'user'], 'required'],
            [['creation_date'], 'safe'],
            [['status', 'user'], 'integer'],
            [['amount'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'folio_id' => Yii::t('app', 'Folio ID'),
            'amount' => Yii::t('app', 'Amount'),
            'creation_date' => Yii::t('app', 'Creation Date'),
            'status' => Yii::t('app', 'Status'),
            'user' => Yii::t('app', 'User'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPmsFolioCredits()
    {
        return $this->hasMany(FolioCredit::className(), ['folio_id' => 'folio_id']);
    }
	
	public function getPmsFolioCreditsSum()
    {
        return $this->hasMany(FolioCredit::className(), ['folio_id' => 'folio_id'])->sum('amount');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPmsFolioDebits()
    {
        return $this->hasMany(FolioDebit::className(), ['folio_id' => 'folio_id']);
    }
}
