<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pms_telephone".
 *
 * @property integer $number_id
 * @property string $number
 * @property integer $entity_id
 *
 * @property PmsEntity $entity
 */
class Telephone extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pms_telephone';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number'], 'required'],
            [['entity_id'], 'integer'],
            [['number'], 'string', 'max' => 50],
            [['entity_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entity::className(), 'targetAttribute' => ['entity_id' => 'entity_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'number_id' => Yii::t('app', 'Number ID'),
            'number' => Yii::t('app', 'Number'),
            'entity_id' => Yii::t('app', 'Entity ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntity()
    {
        return $this->hasOne(Entity::className(), ['entity_id' => 'entity_id']);
    }
}
