<?php

namespace app\models;

use Yii;
use app\tenttacularclass\EntityBehaviour;
use app\tenttacularclass\FolioPosting;

/**
 * This is the model class for table "pms_room_booking".
 *
 * @property integer $booking_id
 * @property integer $room_id
 * @property integer $guest_id
 * @property integer $tariff_id
 * @property integer $amount_paid
 * @property integer $time_in
 * @property integer $time_out
 * @property integer $user_id
 * @property integer $status_id
 *
 * @property PmsGuest $guest
 * @property PmsUser $user
 * @property PmsRoom $room
 * @property PmsRoomTariff $tariff
 */
class RoomBooking extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	public $isNotNew;
	public $reciptId ;
	public $amountDue;
    public static function tableName()
    {
        return 'pms_room_booking';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
			[[  'amount_paid', 'status_id'], 'required'],
			[[  'isNotNew'], 'safe'],
            [['amount_paid'], 'integer'],
            [['room_id', 'entity_id', 'tariff_id', 'amount_paid', 'user_id', 'status_id'], 'integer'],
			[['time_in', 'time_out','number_of_nights'], 'safe'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Userdb::className(), 'targetAttribute' => ['user_id' => 'user_id']],
            [['room_id'], 'exist', 'skipOnError' => true, 'targetClass' => Room::className(), 'targetAttribute' => ['room_id' => 'room_id']],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'booking_id' => 'Booking ID',
            'room_id' => 'Room No.',
            'entity_id' => 'Entity ID',
            'tariff_id' => 'Tariff ID',
            'amount_paid' => 'Amount Paid',
            'time_in' => 'Check-in',
            'time_out' => 'Check-out',
            'user_id' => 'Admin',
            'status_id' => 'Guest Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuest()
    {
        return $this->hasOne(Guest::className(), ['entity_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Userdb::className(), ['user_id' => 'user_id']);
    }
	public function getperson()
    {
        return $this->guest->person;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoom()
    {
        return $this->hasOne(Room::className(), ['room_id' => 'room_id']);
    }
	public function getStatus()
    {
        return $this->hasOne(StatusType::className(), ['status_id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(RoomTariff::className(), ['tariff_id' => 'tariff_id']);
    }
	
	public function getEntity()
    {
        return $this->hasOne(Entity::className(), ['entity_id' => 'entity_id']);
    }
	
	public function behaviors()
	{
      return [
          EntityBehaviour::className(),
          FolioPosting::className(),
      ];
		
	}
	
}
