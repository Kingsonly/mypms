<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pms_client".
 *
 * @property integer $client_id
 * @property integer $entity_id
 * @property integer $account_number
 * @property integer $amount_paid
 *
 * @property PmsBookoutReturn[] $pmsBookoutReturns
 * @property PmsEntity $entity
 */
class Client extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pms_client';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entity_id', 'account_number', 'amount_paid'], 'required'],
            [['entity_id', 'account_number', 'amount_paid'], 'integer'],
            [['entity_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entity::className(), 'targetAttribute' => ['entity_id' => 'entity_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'client_id' => Yii::t('app', 'Client ID'),
            'entity_id' => Yii::t('app', 'Entity ID'),
            'account_number' => Yii::t('app', 'Account Number'),
            'amount_paid' => Yii::t('app', 'Amount Paid'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookoutReturns()
    {
        return $this->hasMany(BookoutReturn::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntity()
    {
        return $this->hasOne(Entity::className(), ['entity_id' => 'entity_id']);
    }
}
