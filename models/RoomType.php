<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pms_room_type".
 *
 * @property integer $room_type_id
 * @property string $type
 *
 * @property PmsRoom[] $pmsRooms
 */
class RoomType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pms_room_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'room_type_id' => 'Room Type ID',
            'type' => 'Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRooms()
    {
        return $this->hasMany(Room::className(), ['type' => 'room_type_id']);
    }
}
