<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pms_countries".
 *
 * @property integer $country_id
 * @property string $country_name
 * @property integer $status
 *
 * @property PmsAddress[] $pmsAddresses
 */
class Countries extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pms_countries';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_name'], 'required'],
            [['status'], 'integer'],
            [['country_name'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'country_id' => Yii::t('app', 'Country ID'),
            'country_name' => Yii::t('app', 'Country Name'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(Address::className(), ['country_id' => 'country_id']);
    }
}
