<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pms_recipt".
 *
 * @property integer $recipt_id
 * @property integer $recipt_number
 * @property string $amount
 * @property string $receipt_issue_date
 *
 * @property PmsReciptRelationship[] $pmsReciptRelationships
 */
class Recipt extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pms_recipt';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['recipt_number', 'amount', 'receipt_issue_date'], 'required'],
            [['recipt_number'], 'integer'],
            [['receipt_issue_date'], 'safe'],
            [['amount'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'recipt_id' => Yii::t('app', 'Recipt ID'),
            'recipt_number' => Yii::t('app', 'Recipt Number'),
            'amount' => Yii::t('app', 'Amount'),
            'receipt_issue_date' => Yii::t('app', 'Receipt Issue Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPmsReciptRelationships()
    {
        return $this->hasMany(PmsReciptRelationship::className(), ['recipt_id' => 'recipt_id']);
    }
}
