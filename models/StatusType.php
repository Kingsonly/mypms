<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pms_status_type".
 *
 * @property integer $status_id
 * @property string $status_category
 * @property string $status_title
 * @property integer $status_color
 *
 * @property PmsBookoutReturn[] $pmsBookoutReturns
 * @property PmsCheckInOut[] $pmsCheckInOuts
 * @property PmsGuest[] $pmsGuests
 * @property PmsRecipt[] $pmsRecipts
 * @property PmsRestaurantBooking[] $pmsRestaurantBookings
 * @property PmsRestaurantProduct[] $pmsRestaurantProducts
 * @property PmsRoom[] $pmsRooms
 * @property PmsColor $statusColor
 * @property PmsColor $statusColor0
 * @property PmsVendor[] $pmsVendors
 */
class StatusType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pms_status_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status_category', 'status_title', 'status_color'], 'required'],
            [['status_color'], 'integer'],
            [['status_category', 'status_title'], 'string', 'max' => 50],
            [['status_color'], 'exist', 'skipOnError' => true, 'targetClass' => Color::className(), 'targetAttribute' => ['status_color' => 'color_id']],
            [['status_color'], 'exist', 'skipOnError' => true, 'targetClass' => Color::className(), 'targetAttribute' => ['status_color' => 'color_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'status_id' => Yii::t('app', 'Status ID'),
            'status_category' => Yii::t('app', 'Status Category'),
            'status_title' => Yii::t('app', 'Status Title'),
            'status_color' => Yii::t('app', 'Status Color'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookoutReturns()
    {
        return $this->hasMany(BookoutReturn::className(), ['status' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckInOuts()
    {
        return $this->hasMany(CheckInOut::className(), ['status' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuests()
    {
        return $this->hasMany(Guest::className(), ['status_id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecipts()
    {
        return $this->hasMany(Recipt::className(), ['recipt_status' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurantBookings()
    {
        return $this->hasMany(RestaurantBooking::className(), ['status' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurantProducts()
    {
        return $this->hasMany(RestaurantProduct::className(), ['product_status' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRooms()
    {
        return $this->hasMany(Room::className(), ['status' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusColor()
    {
        return $this->hasOne(Color::className(), ['color_id' => 'status_color']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendors()
    {
        return $this->hasMany(Vendor::className(), ['status_id' => 'status_id']);
    }
}
