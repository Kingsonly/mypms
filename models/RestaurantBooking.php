<?php

namespace app\models;

use Yii;
use app\tenttacularclass\FolioPosting;
use app\tenttacularclass\EntityBehaviour;
/**
 * This is the model class for table "pms_restaurant_booking".
 *
 * @property integer $booking_id
 * @property integer $room_number_id
 * @property integer $guest_id
 * @property integer $product_id
 * @property string $amount_paid
 * @property string $time_of_order
 * @property integer $user_id
 * @property integer $status
 *
 * @property PmsGuest $guess
 * @property PmsRestaurantProduct $product
 * @property PmsStatusType $status0
 * @property PmsUser $user
 * @property PmsRoom $roomNumber
 */
class RestaurantBooking extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	public $reciptId ;
	public $amountDue;
	public $nonGuest;
    public static function tableName()
    {
        return 'pms_restaurant_booking';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_order'], 'required'],
            [['room_number_id', 'entity_id', 'user_id', 'status'], 'integer'],
            [['time_of_order','amountDue','entity_id','nonGuest','room_number_id'], 'safe'],
            [['amount_paid'], 'string', 'max' => 255],
            
            
            
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Userdb::className(), 'targetAttribute' => ['user_id' => 'user_id']],
            [['room_number_id'], 'exist', 'skipOnError' => true, 'targetClass' => Room::className(), 'targetAttribute' => ['room_number_id' => 'room_id']],
            
       
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'booking_id' => Yii::t('app', 'Booking ID'),
            'room_number_id' => Yii::t('app', 'Room Number ID'),
            'guest_id' => Yii::t('app', 'Guess ID'),
            'product_order' => Yii::t('app', 'product order'),
            'amount_paid' => Yii::t('app', 'Amount Paid'),
            'time_of_order' => Yii::t('app', 'Time Of Order'),
            'user_id' => Yii::t('app', 'User ID'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuest()
    {
        return $this->hasOne(Guest::className(), ['id' => 'entity_id']);
    }
	
	public function getperson()
    {
        return $this->guest->person;
    }
    /**
     * @return \yii\db\ActiveQuery
	 [['status'], 'exist', 'skipOnError' => true, 'targetClass' => StatusType::className(), 'targetAttribute' => ['status' => 'status_id']],
     */
    public function getProduct()
    {
        return $this->hasOne(RestaurantProduct::className(), ['product_id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus0()
    {
        return $this->hasOne(StatusType::className(), ['status_id' => 'status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoomNumber()
    {
        return $this->hasOne(Room::className(), ['room_id' => 'room_number_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     
    public function getRestaurantProducts()
    {
        return $this->hasMany(RestaurantProduct::className(), ['price_id' => 'price_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     
    public function getProductStatus()
    {
        return $this->hasOne(StatusType::className(), ['status_id' => 'product_status']);
    }
	*/

    /**
     * @return \yii\db\ActiveQuery
     
    public function getPrice()
    {
        return $this->hasOne(RestaurantPrice::className(), ['price_id' => 'price_id']);
    }
	
	*/
	
	public function behaviors()
	{
      return [
          FolioPosting::className(),
          EntityBehaviour::className(),
      ];
	}
		
}
