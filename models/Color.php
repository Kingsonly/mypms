<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pms_color".
 *
 * @property integer $color_id
 * @property string $color_class_name
 *
 * @property PmsStatusType[] $pmsStatusTypes
 * @property PmsStatusType[] $pmsStatusTypes0
 */
class Color extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pms_color';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['color_class_name'], 'required'],
            [['color_class_name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'color_id' => Yii::t('app', 'Color ID'),
            'color_class_name' => Yii::t('app', 'Color Class Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusTypes()
    {
        return $this->hasMany(StatusType::className(), ['status_color' => 'color_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusTypes0()
    {
        return $this->hasMany(StatusType::className(), ['status_color' => 'color_id']);
    }
}
