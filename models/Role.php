<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pms_role".
 *
 * @property integer $role_id
 * @property integer $role_number
 * @property string $role_description
 */
class Role extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pms_role';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role_number', 'role_description'], 'required'],
            [['role_number'], 'integer'],
            [['role_description'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'role_id' => Yii::t('app', 'Role ID'),
            'role_number' => Yii::t('app', 'Role Number'),
            'role_description' => Yii::t('app', 'Role Description'),
        ];
    }
}
