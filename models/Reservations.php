<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pms_room_booking".
 *
 * @property integer $booking_id
 * @property integer $room_id
 * @property integer $guest_id
 * @property integer $tariff_id
 * @property integer $amount_paid
 * @property string $time_in
 * @property string $time_out
 * @property integer $user_id
 * @property integer $status_id
 *
 * @property PmsGuest $guest
 * @property PmsUser $user
 * @property PmsRoom $room
 * @property PmsRoomTariff $tariff
 */
class Reservations extends RoomBooking
{
    /**
     * @inheritdoc
     */
    
}
