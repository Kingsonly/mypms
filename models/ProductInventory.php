<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pms_product_inventory".
 *
 * @property integer $product_id
 * @property string $product_name
 * @property integer $vendor_id
 * @property integer $product_quantity
 * @property string $purchase_date
 * @property integer $amount_purchased
 *
 * @property PmsBookoutReturn[] $pmsBookoutReturns
 * @property PmsVendor $vendor
 */
class ProductInventory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pms_product_inventory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_name', 'vendor_id', 'product_quantity', 'purchase_date', 'amount_purchased'], 'required'],
            [['vendor_id', 'product_quantity', 'amount_purchased'], 'integer'],
            [['purchase_date'], 'safe'],
            [['product_name'], 'string', 'max' => 255],
            [['vendor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Vendor::className(), 'targetAttribute' => ['vendor_id' => 'vendor_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => Yii::t('app', 'Product ID'),
            'product_name' => Yii::t('app', 'Product Name'),
            'vendor_id' => Yii::t('app', 'Vendor ID'),
            'product_quantity' => Yii::t('app', 'Product Quantity'),
            'purchase_date' => Yii::t('app', 'Purchase Date'),
            'amount_purchased' => Yii::t('app', 'Amount Purchased'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookoutReturns()
    {
        return $this->hasMany(BookoutReturn::className(), ['product_id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendor()
    {
        return $this->hasOne(Vendor::className(), ['vendor_id' => 'vendor_id']);
    }
}
