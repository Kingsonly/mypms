<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pms_user".
 *
 * @property integer $user_id
 * @property string $username
 * @property string $password
 * @property integer $entity_id
 * @property integer $access_level
 * @property integer $status_id
 *
 * @property PmsBookoutReturn[] $pmsBookoutReturns
 * @property PmsCheckInOut[] $pmsCheckInOuts
 * @property PmsGuest[] $pmsGuests
 * @property PmsRestaurantBooking[] $pmsRestaurantBookings
 * @property PmsRoomBooking[] $pmsRoomBookings
 * @property PmsEntity $entity
 */
class Userdb extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pms_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password','access_level', 'status_id'], 'required'],
            [['entity_id', 'access_level', 'status_id'], 'integer'],
            [['username', 'password'], 'string', 'max' => 50],
            [['entity_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entity::className(), 'targetAttribute' => ['entity_id' => 'entity_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'username' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
            'entity_id' => Yii::t('app', 'Entity ID'),
            'access_level' => Yii::t('app', 'Access Level'),
            'status_id' => Yii::t('app', 'Status ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookoutReturns()
    {
        return $this->hasMany(BookoutReturn::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckInOuts()
    {
        return $this->hasMany(CheckInOut::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuests()
    {
        return $this->hasMany(Guest::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurantBookings()
    {
        return $this->hasMany(RestaurantBooking::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoomBookings()
    {
        return $this->hasMany(RoomBooking::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntity()
    {
        return $this->hasOne(Entity::className(), ['entity_id' => 'entity_id']);
    }
	
	public function getRole()
    {
        return $this->hasOne(Role::className(), ['role_id' => 'access_level']);
    }
	
	public function getPerson(){
		return $this->hasOne(Person::className(), ['entity_id' => 'entity_id']);
	}
    
	public function getStatusType(){
		return $this->hasOne(StatusType::className(), ['status_id' => 'status_id']);
	}
	
	
	public static function findIdentity($id){
		return static::findOne($id);
	}

	public static function findIdentityByAccessToken($token, $type = null){
		throw new NotSupportedException();//I don't implement this method because I don't have any access token column in my database
	}

	public function getId(){
		return $this->user_id;
	}

    public function getAuthKey(){
		return $this->access_level;//Here I return a value of my authKey column
	}
 
	public function validateAuthKey($authKey){
		return $this->access_level === $authKey;
	}
	public static function findByUsername($username){
		return self::findOne(['username'=>$username]);
	}

	public function validatePassword($password){
		return $this->password === $password;
	}
}
