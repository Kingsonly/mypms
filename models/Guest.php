<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pms_guest".
 *
 * @property integer $id
 * @property integer $entity_id
 * @property string $registration_date
 * @property integer $user_id
 * @property integer $status_id
 *
 * @property PmsCheckInOut[] $pmsCheckInOuts
 * @property PmsEntity $entity
 * @property PmsStatusType $status
 * @property PmsUser $user
 * @property PmsRestaurantBooking[] $pmsRestaurantBookings
 * @property PmsRoomBooking[] $pmsRoomBookings
 */
class Guest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pms_guest';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status_id'], 'integer'],
            [['entity_id', 'user_id', 'status_id'], 'integer'],
            [['registration_date'], 'safe'],
            [['entity_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entity::className(), 'targetAttribute' => ['entity_id' => 'entity_id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => StatusType::className(), 'targetAttribute' => ['status_id' => 'status_id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'user_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'entity_id' => Yii::t('app', 'Entity ID'),
            'registration_date' => Yii::t('app', 'Registration Date'),
            'user_id' => Yii::t('app', 'User ID'),
            'status_id' => Yii::t('app', 'Guest Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckInOuts()
    {
        return $this->hasMany(CheckInOut::className(), ['guess_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntity()
    {
        return $this->hasOne(Entity::className(), ['entity_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(StatusType::className(), ['status_id' => 'status_id', 'status_category' => 'user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurantBookings()
    {
        return $this->hasMany(RestaurantBooking::className(), ['guess_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoomBookings()
    {
        return $this->hasMany(RoomBooking::className(), ['guest_id' => 'id']);
    }
	public function getPerson()
    {
        return $this->hasOne(Person::className(), ['entity_id' => 'entity_id']);
    }
	public function getPersonNameString()
    {
        return $this->person->surname .' '.$this->person->first_name ;
    }
	
	
	
}
