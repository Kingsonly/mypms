<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pms_room_tariff".
 *
 * @property integer $tariff_id
 * @property string $tariff
 *
 * @property PmsRoom[] $pmsRooms
 * @property PmsRoomBooking[] $pmsRoomBookings
 */
class RoomTariff extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pms_room_tariff';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tariff'], 'required'],
            [['tariff'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tariff_id' => 'Tariff ID',
            'tariff' => 'Tariff',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRooms()
    {
        return $this->hasMany(Room::className(), ['room_tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoomBookings()
    {
        return $this->hasMany(RoomBooking::className(), ['tariff_id' => 'tariff_id']);
    }
}
