<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pms_person".
 *
 * @property integer $person_id
 * @property string $surname
 * @property string $first_name
 * @property integer $entity_id
 *
 * @property PmsEntity $entity
 */
class Person extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pms_person';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['surname', 'first_name'], 'required'],
            [['entity_id'], 'integer'],
            [['surname', 'first_name'], 'string', 'max' => 50],
            [['entity_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entity::className(), 'targetAttribute' => ['entity_id' => 'entity_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'person_id' => Yii::t('app', 'Person ID'),
            'surname' => Yii::t('app', 'Surname'),
            'first_name' => Yii::t('app', 'First Name'),
            'entity_id' => Yii::t('app', 'Entity ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntity()
    {
        return $this->hasOne(Entity::className(), ['entity_id' => 'entity_id']);
    }
	public function getAddress()
    {
        return $this->hasOne(Address::className(), ['entity_id' => 'entity_id']);
    }
	public function getTelephones()
    {
        return $this->hasOne(Telephone::className(), ['entity_id' => 'entity_id']);
    }
	public function getPersonString()
    {
        return $this->surname . ' '. $this->first_name;
    }
	public function getEmails()
    {
        return $this->hasOne(Email::className(), ['entity_id' => 'entity_id']);
    }
}
