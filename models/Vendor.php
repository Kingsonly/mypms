<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pms_vendor".
 *
 * @property integer $vendor_id
 * @property integer $vendor_account_number
 * @property integer $entity_id
 * @property integer $status_id
 *
 * @property PmsProductInventory[] $pmsProductInventories
 * @property PmsEntity $entity
 * @property PmsStatusType $status
 */
class Vendor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pms_vendor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vendor_account_number', 'entity_id', 'status_id'], 'required'],
            [['vendor_account_number', 'entity_id', 'status_id'], 'integer'],
            [['entity_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entity::className(), 'targetAttribute' => ['entity_id' => 'entity_id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => StatusType::className(), 'targetAttribute' => ['status_id' => 'status_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'vendor_id' => Yii::t('app', 'Vendor ID'),
            'vendor_account_number' => Yii::t('app', 'Vendor Account Number'),
            'entity_id' => Yii::t('app', 'Entity ID'),
            'status_id' => Yii::t('app', 'Status ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductInventories()
    {
        return $this->hasMany(ProductInventory::className(), ['vendor_id' => 'vendor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntity()
    {
        return $this->hasOne(Entity::className(), ['entity_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(StatusType::className(), ['status_id' => 'status_id']);
    }
}
