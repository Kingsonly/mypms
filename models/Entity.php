<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pms_entity".
 *
 * @property integer $entity_id
 * @property string $type
 *
 * @property PmsAddress[] $pmsAddresses
 * @property PmsClient[] $pmsClients
 * @property PmsEmail[] $pmsEmails
 * @property PmsGuest[] $pmsGuests
 * @property PmsPerson[] $pmsPeople
 * @property PmsUser[] $pmsUsers
 * @property PmsVendor[] $pmsVendors
 */
class Entity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pms_entity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type','folio_id'], 'required'],
            [['type'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'entity_id' => Yii::t('app', 'Entity ID'),
            'type' => Yii::t('app', 'Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(Address::className(), ['entity_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients()
    {
        return $this->hasMany(Client::className(), ['entity_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
	
	public function getFolios(){
		return $this->folio;	
	}
	
    public function getEmail()
    {
        return $this->hasOne(Email::className(), ['entity_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuests()
    {
        return $this->hasOne(Guest::className(), ['entity_id' => 'entity_id']);
    }
	
	/**
     * @return \yii\db\ActiveQuery(relationship for folio to entity)
     */
	public function getFolio2()
    {
        return $this->hasOne(Folio::className(), ['folio_id' => 'folio_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeople()
    {
        return $this->hasOne(Person::className(), ['entity_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasOne(User::className(), ['entity_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendors()
    {
        return $this->hasMany(Vendor::className(), ['entity_id' => 'entity_id']);
    }
}
