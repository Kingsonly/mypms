<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pms_folio_credit".
 *
 * @property integer $folio_id
 * @property integer $guest_id
 * @property string $amount
 * @property integer $senario_id
 * @property string $senario_type
 * @property string $date_posted
 *
 * @property PmsFolio $folio
 */
class FolioCredit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pms_folio_credit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['folio_id', 'guest_id', 'amount', 'senario_id', 'senario_type', 'date_posted'], 'required'],
            [['folio_id', 'guest_id', 'senario_id'], 'integer'],
            [['date_posted'], 'safe'],
            [['amount', 'senario_type'], 'string', 'max' => 255],
            [['folio_id'], 'exist', 'skipOnError' => true, 'targetClass' => PmsFolio::className(), 'targetAttribute' => ['folio_id' => 'folio_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'folio_id' => Yii::t('app', 'Folio ID'),
            'guest_id' => Yii::t('app', 'Guest ID'),
            'amount' => Yii::t('app', 'Amount'),
            'senario_id' => Yii::t('app', 'Senario ID'),
            'senario_type' => Yii::t('app', 'Senario Type'),
            'date_posted' => Yii::t('app', 'Date Posted'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFolio()
    {
        return $this->hasOne(PmsFolio::className(), ['folio_id' => 'folio_id']);
    }
}
