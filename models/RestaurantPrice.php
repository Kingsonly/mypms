<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pms_restaurant_price".
 *
 * @property integer $price_id
 * @property integer $product_price
 *
 * @property PmsRestaurantProduct[] $pmsRestaurantProducts
 */
class RestaurantPrice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pms_restaurant_price';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_price'], 'required'],
            [['product_price'], 'integer'],
            [['price_id'], 'exist', 'skipOnError' => true, 'targetClass' => RestaurantPrice::className(), 'targetAttribute' => ['price_id' => 'price_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'price_id' => Yii::t('app', 'Price ID'),
            'product_price' => Yii::t('app', 'Product Price'),
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrice()
    {
        return $this->hasOne(RestaurantPrice::className(), ['price_id' => 'price_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurantProducts()
    {
        return $this->hasMany(RestaurantProduct::className(), ['price_id' => 'price_id']);
    }
}
