<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\restaurantBooking */
/* @var $form ActiveForm */
?>
<div class="restaurant-booking-cart">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'room_number_id') ?>
        <?= $form->field($model, 'guess_id') ?>
        <?= $form->field($model, 'product_id') ?>
        <?= $form->field($model, 'amount_paid') ?>
        <?= $form->field($model, 'time_of_order') ?>
        <?= $form->field($model, 'user_id') ?>
        <?= $form->field($model, 'status') ?>
        <?= $form->field($model, 'product_status') ?>
        <?= $form->field($model, 'price_id') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- restaurant-booking-cart -->
