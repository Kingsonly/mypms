<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RoomBooking */


?>
<div class="room-booking-view">

    
    

    <div class="">
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  
                  <div class="x_content">

                    <section class="content invoice">
                      <!-- title row -->
                      <div class="row">
                        <!-- /.col -->
                      </div>
                      <!-- info row -->
                      <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                          <address>
                                          <strong>DBI Guest House</strong>
                                          <strong>Iron Admin, Inc. </strong>
                                          <br>795 Freedom Ave, Suite 600
                                          <br>Lagos Nigeria
                                          <br>Phone: 1 (804) 123-9876
                                          <br>Email: dbiguestadmin.com
                                      </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          To
                          <address>
                                          <strong><?=$getRoomBookingDetails->person->surname.' '. $getRoomBookingDetails->person->first_name;?></strong>
                                          <br>Address: <?=$getRoomBookingDetails->person->address->address;?>
                                          <br>State/Country: <?='('.$getRoomBookingDetails->person->address->state->state_name.') '.$getRoomBookingDetails->person->address->country->country_name;?>
                                          <br>Phone: <?=$getRoomBookingDetails->person->telephones->number;?>
                                          <br>Email: <?=$getRoomBookingDetails->person->emails->address;?>
                                      </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          <b>Receipt  No #<?=$getReceiptDetails->recipt_number;?></b>
                          <br>
                          
                          <b>Payment Due:</b> <?=$getReceiptDetails->receipt_issue_date;?>
                          <br>
                          <b>Account:</b> 968-34567
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                      <!-- Table row -->
                      <div class="row">
                        <div class="col-xs-12 table">
                          <table class="table table-striped">
                            <thead>
                              <tr>
                                <th>Qty</th>
                                <th>Qty</th>
                                <th>Product</th>
                                <th>Serial #</th>
                                <th >Description</th>
                                <th>Subtotal</th>
                              </tr>
                            </thead>
                            <tbody>
                            
                              <tr>
                                <td>1</td>
                                <td><?=$model->senarior->room->room_number ;?></td>
                               
                                <td>booking of a room </td>
								  <td><?=$model->senarior->time_in ;?></td>
								 <td><?=$model->senarior->time_out ;?></td>
                                <td>N<?=$model->recipt->amount ;?></td>
								 
                              </tr>
                            </tbody>
                          </table>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                      <div class="row">
                        <!-- accepted payments column -->
                        <div class="col-xs-6">
                          
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-6">
                          <p class="lead">Amount Due <?=$amountDue;?></p>
                          <div class="table-responsive">
                            <table class="table">
                              <tbody>
                                <tr>
                                  <th style="width:50%">Subtotal:</th>
                                  <td><?=$model->recipt->amount ;?></td>
                                </tr>
                                <tr>
                                  <th>Service charge (10%)</th>
                                  <td>N<?=($tenper=$model->recipt->amount*10)/100 ;?></td>
                                </tr>
                                <tr>
                                  <th>VAT(5%):</th>
                                  <td>N<?=($tenper=$model->recipt->amount*5)/100 ;?></td>
                                </tr>
								  <tr>
                                  <th>consumption(5%):</th>
                                  <td>N<?=($tenper=$model->recipt->amount*5)/100 ;?></td>
                                </tr>
                                <tr>
                                  <th>Total:</th>
                                  <td>N<?=$model->recipt->amount*1.20 ;?></td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                      <!-- this row will not appear when printing -->
                      <div class="row no-print">
                        <div class="col-xs-12">
                          <button class="btn btn-primary pull-right" style="margin-right: 5px;" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
                          
                        </div>
                      </div>
                    </section>
                  </div>
                </div>
              </div>
            </div>
          </div>

</div>
