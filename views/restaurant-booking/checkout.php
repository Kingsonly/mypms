<?
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<div class="restaurant-booking-cart">
	<style>
		.right{
			float: right;
			width: 50%;
		}
	</style>
	
	
                     <div class="" role="tabpanel" data-example-id="togglable-tabs">
                      <ul id="checkouttabmenue" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#guesttab" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Guest</a>
                        </li>
                        <li role="presentation" class=""><a href="#nonguesttab" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Non Guest</a>
                        </li>
                        
                      </ul>
                      <div id="checkouttab" class="tab-content">
                    
                        <div role="tabpanel" class="tab-pane fade active in" id="guesttab" aria-labelledby="profile-tab">
                         
            <table id="checkoutcontent" class="table table-striped table-bordered">
		<tr>
				<td>product name</td>
				<td>Quantity</td>
				<td>amonth</td>
			</tr>
		<?php foreach(Yii::$app->cart->positions as $position){?>
			<tr>
				<td><?=$position->product_name; ?></td>
				<td><?=$position->Quantity; ?></td>
				<td><?=$position->price*$position->Quantity; ?></td>
			</tr>
		<?php }?>
		<tr>
				<td colspan="3">
					<div class="right">
					<table class="table table-striped table-bordered">
						<tr>
						<td>SubTotal</td>
						<td><?=\Yii::$app->cart->getCost()?></td>
						</tr>
						
						<tr>
						<td>Amount  due</td>
						<td><?=\Yii::$app->cart->getCost()*1.2?></td>
						</tr>
					</table>
					</div>
				</td>
				
			</tr>
	</table>
    <?php $form = ActiveForm::begin(['action'=>['restaurant-booking/pos-checkout']]); ?>

        <?= Html::label('Guest Phonenumber', 'guest phonenumber','',['class' => 'form-label']); ?>
        <?= Html::input('phone', 'phone_number','',['class' => 'searchphone form-control']); ?>
        <?= $form->field($model, 'room_number_id')->hiddenInput(['class'=>'roomId'])->label(false); ?>
        <?= $form->field($model, 'entity_id')->hiddenInput(['class'=>'guestId'])->label(false); ?>
        <?= $form->field($model, 'product_order')->hiddenInput(['value'=>\Yii::$app->cart->getSerial()])->label(false) ?>
        <?= $form->field($model, 'amountDue')->hiddenInput(['value'=>\Yii::$app->cart->getCost()])->label(false)?>
        <?= $form->field($model, 'amount_paid') ?>
        <?//= $form->field($model, 'status') ?>
       
     
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="nonguesttab" aria-labelledby="profile-tab">
                         
                          <table id="checkoutcontent" class="table table-striped table-bordered">
		<tr>
				<td>product name</td>
				<td>Quantity</td>
				<td>amonth</td>
			</tr>
		<?php foreach(Yii::$app->cart->positions as $position){?>
			<tr>
				<td><?=$position->product_name; ?></td>
				<td><?=$position->Quantity; ?></td>
				<td><?=$position->price*$position->Quantity; ?></td>
			</tr>
		<?php }?>
		<tr>
				<td colspan="3">
					<div class="right">
					<table class="table table-striped table-bordered">
						<tr>
						<td>SubTotal</td>
						<td><?=\Yii::$app->cart->getCost()?></td>
						</tr>
						
						<tr>
						<td>Amount  due</td>
						<td><?=\Yii::$app->cart->getCost()*1.2?></td>
						</tr>
					</table>
					</div>
				</td>
				
			</tr>
	</table>
    <?php $form = ActiveForm::begin(['action'=>['restaurant-booking/pos-checkout-non-guest']]); ?>

        <?= $form->field($model, 'product_order')->hiddenInput(['value'=>\Yii::$app->cart->getSerial()])->label(false) ?>
        <?= $form->field($model, 'amountDue')->hiddenInput(['value'=>\Yii::$app->cart->getCost()])->label(false)?>
        <?= $form->field($model, 'amount_paid') ?>
        <?//= $form->field($model, 'status') ?>
       
     
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>
            
                        </div>
                       
                      </div>
                    </div>

                  
	
</div>


 




