<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Point Of Sales';
$this->params['breadcrumbs'][] = $this->title;
?>
<style type="text/css">
  .small-badge{
    font-size: 9px !important;
    color: #fff !important;
  }
</style>
<div class="">
    
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
					<?php Pjax::begin(['id' => 'cart']); ?>
                  	<div class="x_title">
                    <h2><i class="fa fa-glass"></i> POINT OF SALES <small></small></h2>
                    <div class="pull-right">Total: <font size="+2">
						N<?=\Yii::$app->cart->getCost()?>
						
						<ul class="nav navbar-nav navbar-right">
              
                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-shopping-cart"></i>
                    <span class="badge bg-green" style="float: right; color: #fff;"><?= \Yii::$app->cart->getCount();?></span>
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                    
					 <?php foreach(Yii::$app->cart->positions as $position){?>
          				<li>
                      <a>
                          <span class="message">  
							 Product name: <? $position->product_name ?>
						</span>
                          <span class="message">Product Price:<?= $position->price ?> </span>
                        </span>
                        <span class="message">
                          Quantity: <?= $position->quantity ?>
                        </span>
							<a class="delete"  data-url="<?=Url::to(['restaurant-booking/removefromcart','id'=>$position->product_id] )?>">
								<span class=" " >
									<i class="fa fa-delete"></i>
									abciygih
								</span>
							</a>
                      </a>
                    </li>
        			<?php }?>
                   <li>
                      <div class="text-center">
                        <a id="clearCart"  data-url="<?=Url::to(['restaurant-booking/clearcart'])?>">
                          <strong>Clear Cart</strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
						  
						  <a id="checkOut" data-url="<?=Url::to(['pos-checkout']);?>" style="right:0px; position: absolute;right: 20px; ">
                          <strong>Check Out</strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
						  
                      </div>
                    </li> 
                  </ul>
                </li>
              </ul>
						</font>
                      </div>
                    <div class="clearfix"></div>
                  </div>
					<?php Pjax::end(); ?>
                  <div class="x_content">
                     <div class="" role="tabpanel" data-example-id="togglable-tabs">
                      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class=""><a href="#tab_content1" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Food Items</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Order History</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Add Food Price</a>
                        </li>
                      </ul>
                      <div id="myTabContent" class="tab-content">
                    
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="profile-tab">
                         
            <div class="row top_tiles">
                            <?php echo $this->render('/restaurant-product/index',['restaurantProductModel' => $restaurantProductModel]);?>
                        </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                         
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Booking ID</th>
                          <th>Room Number(optional)</th>
                          <th>Guest</th>
                          <th>Product</th>
                          <th>Amount Paid</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
              <?php foreach($viewModel as $k => $v){?>

                        <tr>
                          <td><?php echo  $v['booking_id'];?></td>
							<td><?php echo  $v['room_number_id'];?></td>
                          <td>
							 abc 
							</td>
                          <td>
							  <table class="table">
								  <thead>
								      <th>Product Name</th>
								      <th>Product Unit Price</th>
								      <th>Product Quantity</th>
								      <th>Amount Due Per Product</th>
								  </thead>
							  <?php
									$data=preg_replace('!s:(\d+):"(.*?)";!e', "'s:'.strlen('$2').':\"$2\";'", $v['product_order']); $unserialisedProduct = unserialize($data);
													//var_dump($unserialisedProduct);
									foreach($unserialisedProduct as $unk => $unv){
										
							  ?>
							  
							  	  <tr>
									  <td>
										  
										  <?= $unv['productName'];?>
										  
										  
									  </td>
									  <td>
									      N<?= $unv['productPrice'];?>
									  </td>
									  <td>
									  	  <?= $unv['productQuantity'];?>
									  </td>
									  <td>
									  	 N <?= $unv['productQuantity']*$unv['productPrice'];?>
									  </td>
									  
								  
								  </tr>
							  
							  <?}?>
								  </table>
							  </td>
						<td><?php echo  $v['amount_paid'];?></td>
                          <td><i class="fa fa-eye hvr-bounce-in"></i>&nbsp;&nbsp;<i class="fa fa-pencil hvr-bounce-in"></i></td>
							
                        </tr>
                            <?php }?>
                            </tbody>
                            </table>
            <div class="row top_tiles">
                        </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                         
            <div class="row top_tiles">
                            <?php echo $this->render('/restaurant-price/index',['restaurantPriceModel' => $restaurantPriceModel]);?>
                            </div>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
              </div>
<?php
	Modal::begin([
				'header' =>'<h1 id="checkOutHeader">Check Out</h1>',
				'id' => 'checkOutModal',
				'size' => 'modal-md',  
			]);
?>

        <div id='checkOutContent'>
			<div id="existingGuest"></div>
			<div id="listOfProducts"></div>
			<div id="total"></div>
			<div id="sendbutton"></div>
		</div>
<?
	Modal::end();
?>





<?php
$url = Url::to(['restaurant-booking/search-guest-number']);
$paymentJss = <<<JS



$(document).on('change','.searchphone',function(){
var number = $(this).val();
  
  var jqxhr = $.ajax('$url&number='+number)
  .done(function(data) {
	  roomId = data.roomId;
	  entityId =data.entityId;
	  $(document).find('.roomId').val(roomId);
	  $(document).find('.guestId').val(entityId);
  })
  .fail(function() {
    alert( "error" );
  })
  
})

JS;
 
$this->registerJs($paymentJss);
?>