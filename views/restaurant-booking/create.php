<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RestaurantBooking */

$this->title = 'Create Restaurant Booking';
$this->params['breadcrumbs'][] = ['label' => 'Restaurant Bookings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="restaurant-booking-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        
    ]) ?>

</div>
