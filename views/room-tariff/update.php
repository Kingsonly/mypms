<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RoomTariff */

$this->title = 'Update Room Tariff: ' . $model->tariff_id;
$this->params['breadcrumbs'][] = ['label' => 'Room Tariffs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tariff_id, 'url' => ['view', 'id' => $model->tariff_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="room-tariff-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
