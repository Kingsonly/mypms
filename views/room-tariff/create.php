<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RoomTariff */

$this->title = 'Create Room Tariff';
$this->params['breadcrumbs'][] = ['label' => 'Room Tariffs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="room-tariff-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
