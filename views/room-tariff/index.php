<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Button ;
use  yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="room-tariff-index">
    <p>
                        <button class="btn btn-danger booking pull-right" data-url="<?php echo Url::to(['/room-tariff/create']); ?>" >Create Room Tariff</button>
    </p>
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>SN</th>
                          <th>Tariff</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
              <?php foreach($roomTariffModel as $k => $v){?>

                        <tr>
                          <td><?php echo  $v['tariff_id'];?></td>
                          <td>N<?php echo  $v['tariff'];?></td>
                          <td>
                        <a class="booking" data-url="<?php echo Url::to(['/room-tariff/view', 'id' => $v['tariff_id']]); ?>"><i class="fa fa-eye hvr-bounce-in"></i></a>
                         &nbsp;&nbsp;
                        <a class="booking" data-url="<?php echo Url::to(['/room-tariff/update', 'id' => $v['tariff_id']]); ?>"><i class="fa fa-pencil hvr-bounce-in"></i></a>
                        </tr>
                            <?php }?>
                            </tbody>
                            </table>
</div>