<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\WebSiteAsset;

WebSiteAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html class="no-js" lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Megatron - Multipurpose Responsive HTML5 Template</title>
    <meta name="description" content="Megatron - Multipurpose Responsive HTML5 Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="favicon/favicon-194x194.png" sizes="194x194">
    <link rel="icon" type="image/png" href="favicon/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="favicon/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="favicon/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="favicon/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="vendors/animate.css/animate.min.css">
    <link href="//fonts.googleapis.com/css?family=Montserrat:700,400" rel="stylesheet" type="text/css">
    <link href="//fonts.googleapis.com/css?family=Playfair+Display:400,700,900,400italic,700italic,900italic&amp;subset=latin,latin-ext,cyrillic" rel="stylesheet" type="text/css">
    <link href="//fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/fonts/megatron/styles.css">
    <!--Main stylesheet-->
    <link rel="stylesheet" href="assets/css/main.css">
    <!--Modernizr js-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
    <div class="page-content">
     
      <!-- Page Navigation-->
      <div class="main-nav nav-absolute">
        <div class="__header bgc-light">
          <div class="container">
            <div class="nav-content-wrapper">
              <div class="pull-left">
                <div class="nav-info nav-item">
                  <div class="__info select-language dropdown"><a href="#" data-target="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-globe color-primary"></i>Language: English</a>
                    <div class="dropdown-menu menu menu-link-block">
                      <ul>
                        <li><a href="#"><img src="assets/images/flag/united_kingdom.png" alt="flag" class="flag"/>ENGLISH</a></li>
                        <li><a href="#"><img src="assets/images/flag/france.png" alt="flag" class="flag"/>FRANCE</a></li>
                        <li><a href="#"><img src="assets/images/flag/china.png" alt="flag" class="flag"/>CHINESE</a></li>
                        <li><a href="#"><img src="assets/images/flag/japan.png" alt="flag" class="flag"/>JAPANESE</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="__info"><i class="icon icon-phone-1 color-primary"></i>(123) 45678910</div>
                  <div class="__info"><i class="fa fa-clock-o color-primary"></i>Mon - Sat: 9:00 - 18:00</div>
                </div>
              </div>
              <div class="pull-right">
                <div class="nav-info nav-item mr-20"><span class="fz-6-s"><i class="fa fa-send color-primary"></i>info@osthemes.com</span></div>
                <ul class="social nav-item">
                  <li><a href="#"><i class="icon icon-facebook-1"></i></a></li>
                  <li><a href="#"><i class="icon icon-pinterest"></i></a></li>
                  <li><a href="#"><i class="icon icon-twitter-1"></i></a></li>
                  <li><a href="#"><i class="icon icon-linkedin-1"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="__middle bgc-dark-o-5 ">
          <div class="container">
            <div class="nav-content-wrapper">
              <div class="pull-left">
                <div class="megatron inline">
                  <div class="cell-vertical-wrapper">
                    <div class="cell-middle"><a href="index.html">
                        <div class="logo">
                        </div>
                        <div class="brand">MEGATRON</div></a></div>
                  </div>
                </div>
              </div>
              <div class="pull-right">
                <nav class="os-menu main-menu text-center">
                  <ul>
                     <li><?= Html::a('HOME', ['site/index'], ['class' => 'active']) ?></li>
                    <li><?= Html::a('ABOUT', ['site/about'], ['class' => 'active']) ?></li>
                    <li><?= Html::a('PRICING', ['site/price'], ['class' => 'active']) ?></li>
                    <li><?= Html::a('GALLERY', ['site/gallery'], ['class' => 'active']) ?></li>
                    <li><?= Html::a('CONTACT US', ['site/contact_us'], ['class' => 'active']) ?></li>
                  </ul>
                </nav>
                <div class="nav-function nav-item">
                  <div class="__search"><a href="__modal-search.html" data-modal-open="data-modal-open" class="icon-search-icon"></a>
                  </div>
                  <div class="__offcanvas-button"><a href="#" class="offcanvas-nav-toggle icon-menu27"></a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="main-nav small-nav bgc-light fixed-tranformed-bg-light border-bottom">
        <div class="container">
          <div class="nav-content-wrapper">
            <div class="pull-left">
              <div class="megatron inline logo-dark">
                <div class="cell-vertical-wrapper">
                  <div class="cell-middle"><a href="index.html">
                      <div class="logo">
                      </div>
                      <div class="brand">MEGATRON</div></a></div>
                </div>
              </div>
            </div>
            <div class="pull-right visible-lg">
              <nav class="os-menu main-menu">
                <ul>
                      <li><?= Html::a('HOME', ['site/index'], ['class' => 'active']) ?></li>
                    <li><?= Html::a('ABOUT', ['site/about'], ['class' => 'active']) ?></li>
                    <li><?= Html::a('PRICING', ['site/price'], ['class' => 'active']) ?></li>
                    <li><?= Html::a('GALLERY', ['site/gallery'], ['class' => 'active']) ?></li>
                    <li><?= Html::a('CONTACT US', ['site/contact_us'], ['class' => 'active']) ?></li>
                </ul>
              </nav>
              <div class="nav-function nav-item">
                <div class="__search"><a href="__modal-search.html" data-modal-open="data-modal-open" class="icon-search-icon"></a>
                </div>
                <div class="__offcanvas-button"><a href="#" class="offcanvas-nav-toggle icon-menu27"></a></div>
              </div>
            </div>
            <div class="pull-right nav-item hidden-lg"><a href="#" class="mobile-nav-toggle nav-hamburger"><span></span></a></div>
          </div>
        </div>
      </div>
      <nav class="offcanvas-nav bgc-gray-darkest"><a href="#" class="offcanvas-nav-toggle"><i class="icon-wrong6"></i></a>
        <h6 class="size-s smb">CUSTOM PAGES</h6>
        <nav class="nav-single nav-item">
          <ul>
            <li><a href="#">About</a></li>
            <li><a href="#">Services</a></li>
            <li><a href="#">Blog</a></li>
            <li><a href="#">Portfolio</a></li>
            <li><a href="#">Contact</a></li>
          </ul>
        </nav>
        <h6 class="size-s smb">ADDTIONAL LINKS</h6>
        <nav class="nav-single nav-item">
          <ul>
            <li><a href="#">Retina Homepage</a></li>
            <li><a href="#">New Page Examples</a></li>
            <li><a href="#">Parallax Sections</a></li>
            <li><a href="#">Shortcode Central</a></li>
            <li><a href="#">Ultimate Font</a></li>
            <li><a href="#">Collection</a></li>
          </ul>
        </nav>
        <h6 class="size-s">GALLERY</h6>
        <div class="widget widget-gallery">
          <ul class="__widget-content magnific-gallery">
            <li><a href="assets/images/vintage/vintage-1.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-1-thumbnail.jpg" alt="instagram image"/></a></li>
            <li><a href="assets/images/vintage/vintage-2.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-2-thumbnail.jpg" alt="instagram image"/></a></li>
            <li><a href="assets/images/vintage/vintage-3.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-3-thumbnail.jpg" alt="instagram image"/></a></li>
            <li><a href="assets/images/vintage/vintage-4.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-4-thumbnail.jpg" alt="instagram image"/></a></li>
            <li><a href="assets/images/vintage/vintage-5.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-5-thumbnail.jpg" alt="instagram image"/></a></li>
            <li><a href="assets/images/vintage/vintage-6.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-6-thumbnail.jpg" alt="instagram image"/></a></li>
            <li><a href="assets/images/vintage/vintage-7.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-7-thumbnail.jpg" alt="instagram image"/></a></li>
            <li><a href="assets/images/vintage/vintage-8.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-8-thumbnail.jpg" alt="instagram image"/></a></li>
            <li><a href="assets/images/vintage/vintage-9.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-9-thumbnail.jpg" alt="instagram image"/></a></li>
          </ul>
        </div>
        <ul class="social circle gray">
          <li><a href="#"><i class="icon icon-facebook-1"></i></a></li>
          <li><a href="#"><i class="icon icon-pinterest"></i></a></li>
          <li><a href="#"><i class="icon icon-twitter-1"></i></a></li>
          <li><a href="#"><i class="icon icon-linkedin-1"></i></a></li>
        </ul>
      </nav>
      <div class="mobile-nav"><a href="#" class="mobile-nav-toggle"><i class="icon-close47"></i></a><a href="index.html" class="megatron">
          <div class="logo">
          </div>
          <div class="brand">MEGATRON</div></a>
        <nav class="os-menu mobile-menu">
          <ul>
            <li><?= Html::a('HOME', ['site/index'], ['class' => 'active']) ?></li>
                    <li><?= Html::a('ABOUT', ['site/about'], ['class' => 'active']) ?></li>
                    <li><?= Html::a('PRICING', ['site/price'], ['class' => 'active']) ?></li>
                    <li><?= Html::a('GALLERY', ['site/gallery'], ['class' => 'active']) ?></li>
                    <li><?= Html::a('CONTACT US', ['site/contact_us'], ['class' => 'active']) ?></li>
          </ul>
        </nav>
        <div class="search-area">
          <form method="post" class="search-box">
            <input type="search" placeholder="Search..."/>
            <button type="submit"><i class="icon-search-icon"></i></button>
          </form>
        </div>
        <div class="social-area">
          <ul class="social circle gray">
            <li><a href="#"><i class="icon icon-facebook-1"></i></a></li>
            <li><a href="#"><i class="icon icon-pinterest"></i></a></li>
            <li><a href="#"><i class="icon icon-twitter-1"></i></a></li>
            <li><a href="#"><i class="icon icon-linkedin-1"></i></a></li>
          </ul>
        </div>
      </div>
      <!-- Page Navigation-->
		<div id="page-body" class="page-body">
			<?=$content;?>
		</div>
		  <!--End Page Header-->
  <!--Page Footer-->
      <footer id="footer" class="page-footer footer-preset-6">
        <div class="footer-body bgc-gray-darker">
          <div class="container">
            <div class="row">
              <div class="col-lg-3 col-sm-4 col-xs-12">
                <div class="footer-widget-about">
                  <h4>ABOUT US</h4>
                  <p>We are a creative company that specializes in strategy &amp; design. We like to create things with like - minded people who are serious about their passions. </p>
                  <ul class="social circle">
                    <li><a href="#"><i class="icon icon-facebook-1"></i></a></li>
                    <li><a href="#"><i class="icon icon-twitter-1"></i></a></li>
                    <li><a href="#"><i class="icon icon-flickr"></i></a></li>
                    <li><a href="#"><i class="icon icon-linkedin-1"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="col-lg-3 col-sm-4 col-xs-12">
                <div class="footer-widget-tag">
                  <h4>ITEM TAGS</h4>
                  <div class="__content"><a href="#">awesome</a><a href="#">beautiful</a><a href="#">mass</a><a href="#">ios</a><a href="#">themeforest</a><a href="#">osthemes</a><a href="#">flat design</a><a href="#">multi - purposes</a></div>
                </div>
              </div>
              <div class="col-lg-3 col-sm-4 col-xs-12">
                <div class="footer-widget-info">
                  <h4>INFORMATION</h4>
                  <div class="__content font-heading fz-6-ss">
                    <div><span><a href="#">HISTORY OF BROOKLYN</a></span><span><a href="#">PENANG STREET FOOD</a></span><span><a href="#">THE MIST OF MADAGASCAR</a></span><span><a href="#">J.R.R. TOLKIEN</a></span><span><a href="#">MOROCCO ROAD TRIP</a></span><span><a href="#">THE NOISE - ELIXIR</a></span></div>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-xs-12">
                <div class="footer-widget-newsletter">
                  <h4>NEWS LETTER</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                  <form>
                    <div class="__inputs">
                      <input type="text" name="name" placeholder="Name"/>
                      <input type="email" name="email" placeholder="hello@domain.com"/>
                    </div>
                    <div class="__button"><a href="#" class="btn btn-primary fullwidth">SEND MESSAGE</a></div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="footer-foot-3 bgc-gray-darkest">
          <div class="container">
            <div class="row">
              <div class="__content-left">
                <p class="font-heading fz-6-s">MEGATRON &copy; 2015 OSTHEMES - ENVATO PTY LTD</p>
              </div>
              <div class="__content-right">
                <nav class="footer-nav">
                  <ul class="font-heading">
                    <li><a href="#">HOME</a></li>
                    <li><a href="#">FEATURES</a></li>
                    <li><a href="#">PORTFOLIO</a></li>
                    <li><a href="#">BLOG</a></li>
                    <li><a href="#">SHOP</a></li>
                    <li><a href="#">SHORTCODE</a></li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </footer>
      <!--End Page Footer-->
    </div>
    <!--End Page content-->
<?php $this->endBody() ?>
    <!-- Google analytics-->
    <script type="text/javascript">(function(b,o,i,l,e,r){b.GoogleAsnalyticsObject=l;b[l]||(b[l]=function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;e=o.createElement(i);r=o.getElementsByTagName(i)[0];e.src='//www.google-analytics.com/analytics.js';r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));ga('create','UA-57387972-3');ga('send','pageview');</script>
    <!--End Javascript Library-->
  </body>
</html>
<?php $this->endPage() ?>
