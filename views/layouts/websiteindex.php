<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\WebSiteIndexAsset;

WebSiteIndexAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>DBI Guest House</title>
    <meta name="description" content="Megatron - Multipurpose Responsive HTML5 Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="favicon/favicon-194x194.png" sizes="194x194">
    <link rel="icon" type="image/png" href="favicon/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="favicon/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="favicon/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="favicon/mstile-144x144.png">
    <link href="//fonts.googleapis.com/css?family=Montserrat:700,400" rel="stylesheet" type="text/css">
    <link href="//fonts.googleapis.com/css?family=Playfair+Display:400,700,900,400italic,700italic,900italic&amp;subset=latin,latin-ext,cyrillic" rel="stylesheet" type="text/css">
    <link href="//fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <meta name="theme-color" content="#ffffff">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
 <div class="page-content">
	
	  <div class="main-nav main-nav-portfolio nav-absolute">
        <div class="__middle bgc-transparent">
          <div class="container">
            <div class="nav-content-wrapper">
              <div class="pull-left">
                <div class="megatron inline logo-light">
                  <div class="cell-vertical-wrapper">
                    <div class="cell-middle"><?= Html::a('<div class="logo">
                        </div>
                        <div class="brand">DBI GUEST HOUSE</div>', ['site/index']) ?></div>
                  </div>
                </div>
              </div>
              <div class="pull-right">
                <nav class="main-menu">
                  <ul>
                    <li><?= Html::a('HOME', ['site/index'], ['class' => 'active']) ?></li>
                    <li><?= Html::a('ABOUT', ['site/about'], ['class' => 'active']) ?></li>
                    <li><?= Html::a('PRICING', ['site/price'], ['class' => 'active']) ?></li>
                    <li><?= Html::a('GALLERY', ['site/gallery'], ['class' => 'active']) ?></li>
                    <li><?= Html::a('CONTACT US', ['site/contact_us'], ['class' => 'active']) ?></li>
                  </ul>
                </nav>
                <div class="nav-function nav-item">
                  <div class="__search"><a href="__modal-search.html" data-modal-open="data-modal-open" class="icon-search-icon"></a>
                  </div>
                  <div class="__mobile-button"><a href="#" class="mobile-nav-toggle icon-menu27"></a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
	 
	 
	 <div class="main-nav left-menu-top-nav hidden-lg nav-relative ">
        <div class="__middle">
          <div class="container">
            <div class="nav-content-wrapper">
              <div class="pull-left">
                <div class="megatron inline">
                  <div class="cell-vertical-wrapper">
                    <div class="cell-middle"><a href="index.html">
                        <div class="logo">
                        </div>
                        <div class="brand">DBI GUEST HOUSE</div></a></div>
                  </div>
                </div>
              </div>
              <div class="pull-right nav-item"><a href="#" class="mobile-nav-toggle nav-hamburger"><span></span></a></div>
            </div>
          </div>
        </div>
      </div>
      <nav class="offcanvas-nav bgc-gray-darkest"><a href="#" class="offcanvas-nav-toggle"><i class="icon-wrong6"></i></a>
        <h6 class="size-s smb">CUSTOM PAGES</h6>
        <nav class="nav-single nav-item">
           <li><?= Html::a('HOME', ['site/index'], ['class' => 'active']) ?></li>
                    <li><?= Html::a('ABOUT', ['site/about'], ['class' => 'active']) ?></li>
                    <li><?= Html::a('PRICING', ['site/price'], ['class' => 'active']) ?></li>
                    <li><?= Html::a('GALLERY', ['site/gallery'], ['class' => 'active']) ?></li>
                    <li><?= Html::a('CONTACT US', ['site/contact_us'], ['class' => 'active']) ?></li>
          </ul>
        </nav>
        <ul class="social circle gray">
          <li><a href="#"><i class="icon icon-facebook-1"></i></a></li>
          <li><a href="#"><i class="icon icon-pinterest"></i></a></li>
          <li><a href="#"><i class="icon icon-twitter-1"></i></a></li>
          <li><a href="#"><i class="icon icon-linkedin-1"></i></a></li>
        </ul>
      </nav>
      <div class="mobile-nav"><a href="#" class="mobile-nav-toggle"><i class="icon-close47"></i></a><a href="index.html" class="megatron">
          <div class="logo">
          </div>
          <div class="brand">DBI GUEST HOUSE</div></a>
        <nav class="os-menu mobile-menu">
          <ul>
  <li><?= Html::a('HOME', ['site/index'], ['class' => 'active']) ?></li>
                    <li><?= Html::a('ABOUT', ['site/about'], ['class' => 'active']) ?></li>
                    <li><?= Html::a('PRICING', ['site/price'], ['class' => 'active']) ?></li>
                    <li><?= Html::a('GALLERY', ['site/gallery'], ['class' => 'active']) ?></li>
                    <li><?= Html::a('CONTACT US', ['site/contact_us'], ['class' => 'active']) ?></li>
          </ul>
        </nav>
        <div class="search-area">
          <form method="post" class="search-box">
            <input type="search" placeholder="Search..."/>
            <button type="submit"><i class="icon-search-icon"></i></button>
          </form>
        </div>
        <div class="social-area">
          <ul class="social circle gray">
            <li><a href="#"><i class="icon icon-facebook-1"></i></a></li>
            <li><a href="#"><i class="icon icon-pinterest"></i></a></li>
            <li><a href="#"><i class="icon icon-twitter-1"></i></a></li>
            <li><a href="#"><i class="icon icon-linkedin-1"></i></a></li>
          </ul>
        </div>
      </div>
		<!-- make this a block Quote -->
		<header class="page-header home home-slider-1">
        <div class="slider caption-slider control-nav direction-nav">
          <div class="block-caption-slider overlay-container">
            <div style="background-image: url(assets/images/interior/interior_bg-1.jpg); background-position: undefined" class="__img-background"></div>
            <div class="overlay bgc-dark-o-4">
              <div class="cell-vertical-wrapper">
                <div class="cell-middle">
                  <div class="caption-preset-simple-5 text-center">
                    <div class="container">
                      <div class="caption-wrapper">
                        <h1 class="text-responsive size-ll caption">HOME AWAY FROM HOME</h1>
                        <p class="font-serif-italic fz-3 caption">Get ready to change your life and truly feel your absolute best!</p>
                        <div class="__buttons caption"><a href="index.html" class="btn btn-gray-base fullwidth">FIND ROOM</a><br/><a href="http://goo.gl/fi9Eas" target="_blank" class="btn-border btn-light fullwidth">GALLERY</a></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="block-caption-slider overlay-container">
            <div style="background-image: url(assets/images/interior/interior_bg-2.jpg); background-position: undefined" class="__img-background"></div>
            <div class="overlay bgc-dark-o-4">
              <div class="cell-vertical-wrapper">
                <div class="cell-middle">
                  <div class="caption-preset-simple-5 text-center-sm-max">
                    <div class="container">
                      <div class="caption-wrapper">
                        <h1 class="text-responsive size-ll caption">150+ REUSABLE <br/> ELEMENTS</h1>
                        <p class="font-serif-italic fz-3 caption">A hight quality Multi-layout, Multipurpose premium HTML5 theme</p>
                        <div class="__buttons caption"><a href="index.html" class="btn btn-gray-base fullwidth">SELECT DEMO</a><br/><a href="http://goo.gl/fi9Eas" target="_blank" class="btn-border btn-light fullwidth">PURCHASE NOW</a></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="block-caption-slider overlay-container">
            <div style="background-image: url(assets/images/interior/interior_bg-3.jpg); background-position: undefined" class="__img-background"></div>
            <div class="overlay bgc-dark-o-4">
              <div class="cell-vertical-wrapper">
                <div class="cell-middle">
                  <div class="caption-preset-simple-5 text-center-sm-max">
                    <div class="container">
                      <div class="caption-wrapper">
                        <h1 class="text-responsive size-ll caption">MULTI-PURPOSE AND<br/>CREATIVE THEME</h1>
                        <p class="font-serif-italic fz-3 caption">It is ideal for professional teams to introduce their services</p>
                        <div class="__buttons caption"><a href="index.html" class="btn btn-gray-base fullwidth">SELECT DEMO</a><br/><a href="http://goo.gl/fi9Eas" target="_blank" class="btn-border btn-light fullwidth">PURCHASE NOW</a></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>

		<div id="page-body" class="page-body">
			<?=$content;?>
		</div>
		  <!--End Page Header-->
<!--Page Footer-->
      <footer id="footer" class="page-footer footer-preset-6">
        <div class="footer-body bgc-gray-darker">
          <div class="container">
            <div class="row">
              <div class="col-lg-3 col-sm-4 col-xs-12">
                <div class="footer-widget-about">
                  <h4>ABOUT US</h4>
                  <p>We are a creative company that specializes in strategy &amp; design. We like to create things with like - minded people who are serious about their passions. </p>
                  <ul class="social circle">
                    <li><a href="#"><i class="icon icon-facebook-1"></i></a></li>
                    <li><a href="#"><i class="icon icon-twitter-1"></i></a></li>
                    <li><a href="#"><i class="icon icon-flickr"></i></a></li>
                    <li><a href="#"><i class="icon icon-linkedin-1"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="col-lg-3 col-sm-4 col-xs-12">
                <div class="footer-widget-tag">
                  <h4>ITEM TAGS</h4>
                  <div class="__content"><a href="#">awesome</a><a href="#">beautiful</a><a href="#">mass</a><a href="#">ios</a><a href="#">themeforest</a><a href="#">osthemes</a><a href="#">flat design</a><a href="#">multi - purposes</a></div>
                </div>
              </div>
              <div class="col-lg-3 col-sm-4 col-xs-12">
                <div class="footer-widget-info">
                  <h4>INFORMATION</h4>
                  <div class="__content font-heading fz-6-ss">
                    <div><span><a href="#">HISTORY OF BROOKLYN</a></span><span><a href="#">PENANG STREET FOOD</a></span><span><a href="#">THE MIST OF MADAGASCAR</a></span><span><a href="#">J.R.R. TOLKIEN</a></span><span><a href="#">MOROCCO ROAD TRIP</a></span><span><a href="#">THE NOISE - ELIXIR</a></span></div>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-xs-12">
                <div class="footer-widget-newsletter">
                  <h4>NEWS LETTER</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                  <form>
                    <div class="__inputs">
                      <input type="text" name="name" placeholder="Name"/>
                      <input type="email" name="email" placeholder="hello@domain.com"/>
                    </div>
                    <div class="__button"><a href="#" class="btn btn-primary fullwidth">SEND MESSAGE</a></div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="footer-foot-3 bgc-gray-darkest">
          <div class="container">
            <div class="row">
              <div class="__content-left">
                <p class="font-heading fz-6-s">MEGATRON &copy; 2015 OSTHEMES - ENVATO PTY LTD</p>
              </div>
              <div class="__content-right">
                <nav class="footer-nav">
                  <ul class="font-heading">
                    <li><a href="#">HOME</a></li>
                    <li><a href="#">FEATURES</a></li>
                    <li><a href="#">PORTFOLIO</a></li>
                    <li><a href="#">BLOG</a></li>
                    <li><a href="#">SHOP</a></li>
                    <li><a href="#">SHORTCODE</a></li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </footer>
      <!--End Page Footer-->
<?php $this->endBody() ?>
 <!--Javascript Library-->
    <button id="back-to-top-btn"><i class="icon-up-open-big"></i></button>
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="vendors/waypoints/lib/jquery.waypoints.min.js"></script>
    <script src="vendors/waypoints/lib/shortcuts/sticky.min.js"></script>
    <script src="vendors/smoothscroll/SmoothScroll.js"></script>
    <script src="vendors/wow/dist/wow.min.js"></script>
    <script type="text/javascript" src="vendors/slick-carousel/slick/slick.js"></script>
    <script type="text/javascript" src="vendors/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="vendors/jquery-modal/jquery.modal.min.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>
    <!-- Google analytics-->
    <script type="text/javascript">(function(b,o,i,l,e,r){b.GoogleAsnalyticsObject=l;b[l]||(b[l]=function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;e=o.createElement(i);r=o.getElementsByTagName(i)[0];e.src='//www.google-analytics.com/analytics.js';r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));ga('create','UA-57387972-3');ga('send','pageview');</script>
    <!--End Javascript Library-->
    </div>
  </body>
</html>

<?php $this->endPage() ?>