<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\components\controllers\Menu;
$session = Yii::$app->session;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <style type="text/css">      
     @font-face {
          font-family: 'Raleway';
          src: url('../web/admin/Raleway-Medium.ttf') format('truetype');
      }
      .activex
      {
        border-right: 5px solid #1ABB9C;
        background-color: rgba(3, 88, 106, 0.70);
      }
      .fade-scale {
  transform: scale(0) !important;
  opacity: 0 !important;
  -webkit-transition: all .25s linear !important;
  -o-transition: all .25s linear !important;
  transition: all .25s linear !important;
}

.fade-scale.in {
  opacity: 1 !important;
  transform: scale(1) !important;
}
.chart{
  width:800px;
}
    </style>
</head>
<body class="nav-md" style="font-family: Raleway;">
<?php $this->beginBody() ?>
<div class="container body">
	<div class="main_container">
		 <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
            <?= Html::a('<i class="fa fa-home"></i><span>DBI GUEST HOUSE</span>', ['/admin'], ['class' => 'site_title']) ?>
           
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="../web/admin/images/img.jpg" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?= Yii::$app->user->identity->person->surname.' '.Yii::$app->user->identity->person->first_name ?></h2>
                
              </div>
            </div>
            <!-- /menu profile quick info -->

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <ul class="nav side-menu">
                  <li class="<?php if (Html::encode($this->title) == "Admin Dashboard") echo "activex"; ?>"><?= Html::a('<i class="fa fa-dashboard"></i> Dashboard ', ['/admin']) ?>
                  </li>
					
					<?=Menu::widget();?>
                  <?php /*<li class="<?php if (Html::encode($this->title) == "Rooms") echo "activex"; ?>"><?= Html::a('<i class="fa fa-building"></i> Rooms ', ['room/index']) ?>
                  </li>
                 <!--  <li><a><i class="fa fa-envelope"></i> Messages </a>
                  </li> -->
                  <li class="<?php if (Html::encode($this->title) == "Admin Reports") echo "activex"; ?>"><?= Html::a('<i class="fa fa-bar-chart"></i> Reports ', ['admin/reports']) ?>
                  </li>
                  <li class="<?php if (Html::encode($this->title) == "Admin Records") echo "activex"; ?>"><?= Html::a('<i class="fa fa-book"></i> Records ', ['admin/records']) ?>
                  </li>      
                  <li class="<?php if (Html::encode($this->title) == "Admin Ledgers") echo "activex"; ?>"><?= Html::a('<i class="fa fa-folder-open"></i> Ledgers ', ['admin/ledgers']) ?>
                  </li>
                  <li class="<?php if (Html::encode($this->title) == "Reservations") echo "activex"; ?>"><?= Html::a('<i class="fa fa-calendar"></i> Reservations ', ['room-booking/index'], ['class' => 'active']) ?>
                  </li>  
                  <li class="<?php if (Html::encode($this->title) == "Point Of Sales") echo "activex"; ?>"><?= Html::a('<i class="fa fa-glass"></i> Point Of Sales ', ['restaurant-booking/index'], ['class' => 'active']) ?>
                  </li> 
                  <li class="<?php if (Html::encode($this->title) == "Guests") echo "activex"; ?>"><?= Html::a('<i class="fa fa-users"></i> Guests ', ['guest/index'], ['class' => 'active']) ?>
                  </li> 
					
				<li class="<?php if (Html::encode($this->title) == "Users") echo "activex"; ?>"><?= Html::a('<i class="fa fa-user"></i> Users ', ['user/index']) ?> </li>*/
				?>
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
            <center><p style="font-size: 10px;"><?php echo date('Y'); ?>&nbsp; &copy; Tentacular Technologies</a><br>All Rights Reserved</p></center>
           
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>
		 <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
					  <?= Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    '<i class="fa fa-sign-out"></i><span>LOGOUT</span>',
                    ['class' => 'btn']
                )
                . Html::endForm(); ?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
		
		<div class="right_col" role="main">
          <div class="">
			  <?=$content; ?>
			</div>
		</div>
		
		
		
		<footer>
          <div class="pull-right">
            SUPERIOR OPTIONS LIMITED
          </div>
          <div class="clearfix"></div>
        </footer>
	</div>
	</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
