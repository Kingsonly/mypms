<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;


?>
<!DOCTYPE html>
<html class="no-js">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Megatron - Multipurpose Responsive HTML5 Template</title>
    <meta name="description" content="Megatron - Multipurpose Responsive HTML5 Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="favicon/favicon-194x194.png" sizes="194x194">
    <link rel="icon" type="image/png" href="favicon/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="favicon/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="favicon/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="favicon/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="vendors/animate.css/animate.min.css">
    <link href="//fonts.googleapis.com/css?family=Montserrat:700,400" rel="stylesheet" type="text/css">
    <link href="//fonts.googleapis.com/css?family=Playfair+Display:400,700,900,400italic,700italic,900italic&amp;subset=latin,latin-ext,cyrillic" rel="stylesheet" type="text/css">
    <link href="//fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/fonts/megatron/styles.css">
    <!--Main stylesheet-->
    <link rel="stylesheet" href="assets/css/main-interior.css">
    <!--Modernizr js-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
  </head>
  <body>
    <!--[if lt IE 8]>
    <p class="browsenrupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <!--Page Content-->
    <div class="page-content">
      <!-- Page Navigation-->
      <div class="main-nav main-nav-portfolio nav-absolute">
        <div class="__middle bgc-transparent">
          <div class="container">
            <div class="nav-content-wrapper">
              <div class="pull-left">
                <div class="megatron inline logo-light">
                  <div class="cell-vertical-wrapper">
                    <div class="cell-middle"><a href="index.html">
                        <div class="logo"><svg viewBox="0 0 100 100"> <g> <path d="M64.264,31.886v56.803l-14.319,8.259l-14.319-8.259v-74.3L14.157,26.771v41.271l7.156,4.128V30.905   l13.467-7.768v8.26l-6.311,3.64V84.56L7,72.178v-49.54L42.781,2v82.553l7.162,4.132l7.163-4.132V27.758L64.264,31.886z    M43.626,26.294l6.317-3.644L71.42,35.038V84.56l21.469-12.382v-49.54L57.106,2v15.546l7.157,4.128v-7.285l21.469,12.382v41.271   l-7.156,4.128V30.905L49.943,14.391l-6.317,3.644V26.294z"/> </g> </svg>
                        </div>
                        <div class="brand">MEGATRON</div></a></div>
                  </div>
                </div>
              </div>
              <div class="pull-right">
                <nav class="main-menu">
                  <ul>
                    <li><a href="demo-interior-homepage.html">HOME</a></li>
                    <li><a href="demo-interior-about.html">ABOUT</a></li>
                    <li><a href="demo-interior-price.html">PRICE</a></li>
                    <li><a href="demo-interior-blog.html">BLOG</a></li>
                    <li><a href="demo-interior-gallery.html">GALLERY</a></li>
                    <li><a href="demo-interior-contact.html">CONTACT</a></li>
                  </ul>
                </nav>
                <div class="nav-function nav-item">
                  <div class="__search"><a href="__modal-search.html" data-modal-open="data-modal-open" class="icon-search-icon"></a>
                  </div>
                  <div class="__mobile-button"><a href="#" class="mobile-nav-toggle icon-menu27"></a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="main-nav left-menu-top-nav hidden-lg nav-relative ">
        <div class="__middle">
          <div class="container">
            <div class="nav-content-wrapper">
              <div class="pull-left">
                <div class="megatron inline">
                  <div class="cell-vertical-wrapper">
                    <div class="cell-middle"><a href="index.html">
                        <div class="logo"><svg viewBox="0 0 100 100"> <g> <path d="M64.264,31.886v56.803l-14.319,8.259l-14.319-8.259v-74.3L14.157,26.771v41.271l7.156,4.128V30.905   l13.467-7.768v8.26l-6.311,3.64V84.56L7,72.178v-49.54L42.781,2v82.553l7.162,4.132l7.163-4.132V27.758L64.264,31.886z    M43.626,26.294l6.317-3.644L71.42,35.038V84.56l21.469-12.382v-49.54L57.106,2v15.546l7.157,4.128v-7.285l21.469,12.382v41.271   l-7.156,4.128V30.905L49.943,14.391l-6.317,3.644V26.294z"/> </g> </svg>
                        </div>
                        <div class="brand">MEGATRON</div></a></div>
                  </div>
                </div>
              </div>
              <div class="pull-right nav-item"><a href="#" class="mobile-nav-toggle nav-hamburger"><span></span></a></div>
            </div>
          </div>
        </div>
      </div>
      <nav class="offcanvas-nav bgc-gray-darkest"><a href="#" class="offcanvas-nav-toggle"><i class="icon-wrong6"></i></a>
        <h6 class="size-s smb">CUSTOM PAGES</h6>
        <nav class="nav-single nav-item">
          <ul>
            <li><a href="#">About</a></li>
            <li><a href="#">Services</a></li>
            <li><a href="#">Blog</a></li>
            <li><a href="#">Portfolio</a></li>
            <li><a href="#">Contact</a></li>
          </ul>
        </nav>
        <h6 class="size-s smb">ADDTIONAL LINKS</h6>
        <nav class="nav-single nav-item">
          <ul>
            <li><a href="#">Retina Homepage</a></li>
            <li><a href="#">New Page Examples</a></li>
            <li><a href="#">Parallax Sections</a></li>
            <li><a href="#">Shortcode Central</a></li>
            <li><a href="#">Ultimate Font</a></li>
            <li><a href="#">Collection</a></li>
          </ul>
        </nav>
        <h6 class="size-s">GALLERY</h6>
        <div class="widget widget-gallery">
          <ul class="__widget-content magnific-gallery">
            <li><a href="assets/images/vintage/vintage-1.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-1-thumbnail.jpg" alt="instagram image"/></a></li>
            <li><a href="assets/images/vintage/vintage-2.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-2-thumbnail.jpg" alt="instagram image"/></a></li>
            <li><a href="assets/images/vintage/vintage-3.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-3-thumbnail.jpg" alt="instagram image"/></a></li>
            <li><a href="assets/images/vintage/vintage-4.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-4-thumbnail.jpg" alt="instagram image"/></a></li>
            <li><a href="assets/images/vintage/vintage-5.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-5-thumbnail.jpg" alt="instagram image"/></a></li>
            <li><a href="assets/images/vintage/vintage-6.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-6-thumbnail.jpg" alt="instagram image"/></a></li>
            <li><a href="assets/images/vintage/vintage-7.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-7-thumbnail.jpg" alt="instagram image"/></a></li>
            <li><a href="assets/images/vintage/vintage-8.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-8-thumbnail.jpg" alt="instagram image"/></a></li>
            <li><a href="assets/images/vintage/vintage-9.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-9-thumbnail.jpg" alt="instagram image"/></a></li>
          </ul>
        </div>
        <ul class="social circle gray">
          <li><a href="#"><i class="icon icon-facebook-1"></i></a></li>
          <li><a href="#"><i class="icon icon-pinterest"></i></a></li>
          <li><a href="#"><i class="icon icon-twitter-1"></i></a></li>
          <li><a href="#"><i class="icon icon-linkedin-1"></i></a></li>
        </ul>
      </nav>
      <div class="mobile-nav"><a href="#" class="mobile-nav-toggle"><i class="icon-close47"></i></a><a href="index.html" class="megatron">
          <div class="logo"><svg viewBox="0 0 100 100"> <g> <path d="M64.264,31.886v56.803l-14.319,8.259l-14.319-8.259v-74.3L14.157,26.771v41.271l7.156,4.128V30.905   l13.467-7.768v8.26l-6.311,3.64V84.56L7,72.178v-49.54L42.781,2v82.553l7.162,4.132l7.163-4.132V27.758L64.264,31.886z    M43.626,26.294l6.317-3.644L71.42,35.038V84.56l21.469-12.382v-49.54L57.106,2v15.546l7.157,4.128v-7.285l21.469,12.382v41.271   l-7.156,4.128V30.905L49.943,14.391l-6.317,3.644V26.294z"/> </g> </svg>
          </div>
          <div class="brand">MEGATRON</div></a>
        <nav class="os-menu mobile-menu">
          <ul>
            <li><a href="#">HOME</a>
              <ul>
                <li><a href="homepage-landing-page.html">LANDING PAGE</a>
                </li>
                <li><a href="#">DEMOS</a>
                  <ul>
                    <li><a href="demo-construction-homepage.html">DEMO CONSTRUCTION</a>
                    </li>
                    <li><a href="demo-hospital-homepage.html">DEMO HOSPITAL</a>
                    </li>
                    <li><a href="demo-handyman-homepage.html">DEMO HANDYMAN</a>
                    </li>
                    <li><a href="demo-cleaning-homepage.html">DEMO CLEANING</a>
                    </li>
                    <li><a href="demo-fitness-homepage.html">DEMO FITNESS</a>
                    </li>
                    <li><a href="demo-interior-homepage.html">DEMO INTERIOR</a>
                    </li>
                    <li><a href="demo-lawyer-homepage.html">DEMO LAWYER</a>
                    </li>
                    <li><a href="demo-logistics-homepage.html">DEMO LOGISTICS</a>
                    </li>
                    <li><a href="#">UPDATING...</a>
                    </li>
                  </ul>
                </li>
                <li><a href="#">CREATIVE</a>
                  <ul>
                    <li><a href="homepage-creative-1.html">CREATIVE 01</a>
                    </li>
                    <li><a href="homepage-creative-2.html">CREATIVE 02</a>
                    </li>
                    <li><a href="homepage-creative-3.html">CREATIVE 03</a>
                    </li>
                  </ul>
                </li>
                <li><a href="#">BUSINESS</a>
                  <ul>
                    <li><a href="homepage-business-1.html">BUSINESS 01</a>
                    </li>
                    <li><a href="homepage-business-2.html">BUSINESS 02</a>
                    </li>
                    <li><a href="homepage-business-3.html">BUSINESS 03</a>
                    </li>
                  </ul>
                </li>
                <li><a href="#">ONEPAGE</a>
                  <ul>
                    <li><a href="homepage-onepage-1.html">ONEPAGE 01</a>
                    </li>
                    <li><a href="homepage-onepage-2.html">ONEPAGE 02</a>
                    </li>
                    <li><a href="homepage-onepage-3.html">ONEPAGE 03</a>
                    </li>
                  </ul>
                </li>
                <li><a href="#">AGENCY</a>
                  <ul>
                    <li><a href="homepage-agency-1.html">AGENCY 01</a>
                    </li>
                    <li><a href="homepage-agency-2.html">AGENCY 02</a>
                    </li>
                    <li><a href="homepage-agency-3.html">AGENCY 03</a>
                    </li>
                  </ul>
                </li>
                <li><a href="#">VIDEO HEADER</a>
                  <ul>
                    <li><a href="homepage-video-1.html">VIDEO HEADER 01</a>
                    </li>
                    <li><a href="homepage-video-2.html">VIDEO HEADER 02</a>
                    </li>
                    <li><a href="homepage-video-3.html">VIDEO HEADER 03</a>
                    </li>
                  </ul>
                </li>
                <li><a href="#">ALTERNATIVE</a>
                  <ul>
                    <li><a href="homepage-alternative-1.html">ALTERNATIVE 01</a>
                    </li>
                    <li><a href="homepage-alternative-2.html">ALTERNATIVE 02</a>
                    </li>
                    <li><a href="homepage-alternative-3.html">ALTERNATIVE 03</a>
                    </li>
                    <li><a href="homepage-alternative-4.html">ALTERNATIVE 04</a>
                    </li>
                  </ul>
                </li>
                <li><a href="homepage-christmas-1.html">CHRISTMAS</a>
                </li>
                <li><a href="#">PORTFOLIO</a>
                  <ul>
                    <li><a href="homepage-portfolio-1.html">PORTFOLIO 01</a>
                    </li>
                    <li><a href="homepage-portfolio-2.html">PORTFOLIO 02</a>
                    </li>
                    <li><a href="homepage-portfolio-3.html">PORTFOLIO 03</a>
                    </li>
                    <li><a href="homepage-portfolio-4.html">PORTFOLIO 04</a>
                    </li>
                  </ul>
                </li>
                <li><a href="homepage-leftmenu.html">LEFTMENU</a>
                </li>
                <li><a href="#">STORE</a>
                  <ul>
                    <li><a href="homepage-store-1.html">HOMEPAGE SHOP 01</a>
                    </li>
                    <li><a href="homepage-store-2.html">HOMEPAGE SHOP 02</a>
                    </li>
                    <li><a href="homepage-store-3.html">HOMEPAGE SHOP 03</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li><a href="#">PAGES</a>
              <ul>
                <li><a href="#">GROUP 01</a>
                  <ul>
                    <li><a href="page-about-us-1.html">ABOUT US</a>
                    </li>
                    <li><a href="page-about-us-2.html">ABOUT US 2</a>
                    </li>
                    <li><a href="page-about-us-3.html">ABOUT US 3</a>
                    </li>
                    <li><a href="page-about-us-4.html">ABOUT US 4</a>
                    </li>
                    <li><a href="page-about-me.html">ABOUT ME</a>
                    </li>
                    <li><a href="page-services.html">SERVICES</a>
                    </li>
                    <li><a href="page-faq.html">FAQ</a>
                    </li>
                  </ul>
                </li>
                <li><a href="#">GROUP 02</a>
                  <ul>
                    <li><a href="page-contact-us.html">CONTACT US</a>
                    </li>
                    <li><a href="page-contact-us-2.html">CONTACT US 2</a>
                    </li>
                    <li><a href="page-contact-us-3.html">CONTACT US 3</a>
                    </li>
                    <li><a href="page-fullwidth.html">PAGE FULLWIDTH</a>
                    </li>
                    <li><a href="page-sidebar-left.html">PAGE SIDEBAR LEFT</a>
                    </li>
                    <li><a href="page-sidebar-right.html">PAGE SIDEBAR RIGHT</a>
                    </li>
                    <li><a href="page-policy.html">POLICY</a>
                    </li>
                  </ul>
                </li>
                <li><a href="#">GROUP 03</a>
                  <ul>
                    <li><a href="404.html">404 ERROR</a>
                    </li>
                    <li><a href="page-underconstruction.html">UNDERCONSTRUCTION</a>
                    </li>
                    <li><a href="page-search-result.html">SEARCH RESULT</a>
                    </li>
                    <li><a href="page-portfolio.html">PAGE PORTFOLIO</a>
                    </li>
                    <li><a href="page-portfolio-blog.html">PAGE PORTFOLIO BLOG</a>
                    </li>
                    <li><a href="page-login-vs-register.html">LOGIN VS REGISTER</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li><a href="#">FEATURES</a>
              <ul>
                <li><a href="#">SHORTCODES 01</a>
                  <ul>
                    <li><a href="shortcode-accordion.html"><i class="__icon fa fa-bars"></i>ACCORDION</a>
                    </li>
                    <li><a href="shortcode-alert-message.html"><i class="__icon fa fa-exclamation-triangle"></i>ALERT MESSAGE<span class="__flag border-danger color-danger">HOT</span></a>
                    </li>
                    <li><a href="shortcode-blockquotes.html"><i class="__icon fa fa-quote-right"></i>BLOCKQUOTES</a>
                    </li>
                    <li><a href="shortcode-blogpost.html"><i class="__icon fa fa-pencil-square-o"></i>BLOGPOST</a>
                    </li>
                    <li><a href="shortcode-button.html"><i class="__icon fa fa-anchor"></i>BUTTON</a>
                    </li>
                    <li><a href="shortcode-call-to-action.html"><i class="__icon fa fa-building"></i>CALL TO ACTION</a>
                    </li>
                    <li><a href="shortcode-clients.html"><i class="__icon fa fa-user-secret"></i>CLIENTS</a>
                    </li>
                    <li><a href="shortcode-columns.html"><i class="__icon fa fa-columns"></i>COLUMNS</a>
                    </li>
                    <li><a href="shortcode-contact-form.html"><i class="__icon fa fa-keyboard-o"></i>CONTACT FORM</a>
                    </li>
                    <li><a href="shortcode-counter.html"><i class="__icon fa fa-money"></i>COUNTER</a>
                    </li>
                    <li><a href="shortcode-coverbox.html"><i class="__icon fa fa-bicycle"></i>COVERBOX</a>
                    </li>
                  </ul>
                </li>
                <li><a href="#">SHORTCODES 02</a>
                  <ul>
                    <li><a href="shortcode-dropcaps.html"><i class="__icon fa fa-align-right"></i>DROPCAPS</a>
                    </li>
                    <li><a href="shortcode-expendable-section.html"><i class="__icon fa fa-expand"></i>EXPENDABLE SECTION</a>
                    </li>
                    <li><a href="shortcode-google-map.html"><i class="__icon fa fa-google"></i>GOOGLE MAP</a>
                    </li>
                    <li><a href="shortcode-heading.html"><i class="__icon fa fa-header"></i>HEADING</a>
                    </li>
                    <li><a href="shortcode-hightlight.html"><i class="__icon fa fa-bookmark-o"></i>HIGHTLIGHT</a>
                    </li>
                    <li><a href="shortcode-iconboxes.html"><i class="__icon fa fa-asterisk"></i>ICONBOXES</a>
                    </li>
                    <li><a href="shortcode-iconboxes-carousel.html"><i class="__icon fa fa-arrows-h"></i>ICONBOXES CAROUSEL</a>
                    </li>
                    <li><a href="shortcode-iconlists.html"><i class="__icon fa fa-list"></i>ICONLISTS</a>
                    </li>
                    <li><a href="shortcode-icons.html"><i class="__icon fa fa-briefcase"></i>ICONS</a>
                    </li>
                    <li><a href="shortcode-interactive-banner.html"><i class="__icon fa fa-diamond"></i>INTERACTIVE BANNER</a>
                    </li>
                    <li><a href="shortcode-lists.html"><i class="__icon fa fa-list-ol"></i>LISTS</a>
                    </li>
                  </ul>
                </li>
                <li><a href="#">SHORTCODES 03</a>
                  <ul>
                    <li><a href="shortcode-pagination.html"><i class="__icon fa fa-pagelines"></i>PAGINATION</a>
                    </li>
                    <li><a href="shortcode-parallax.html"><i class="__icon fa fa-file-image-o"></i>PARALLAX</a>
                    </li>
                    <li><a href="shortcode-piechart.html"><i class="__icon fa fa-pie-chart"></i>PIECHART</a>
                    </li>
                    <li><a href="shortcode-piechart2.html"><i class="__icon fa fa-pie-chart"></i>PIECHART 02<span class="__flag border-primary color-primary">NEW</span></a>
                    </li>
                    <li><a href="shortcode-piechart3.html"><i class="__icon fa fa-pie-chart"></i>PIECHART 03<span class="__flag border-primary color-primary">NEW</span></a>
                    </li>
                    <li><a href="shortcode-pricing-table.html"><i class="__icon fa fa-list-alt"></i>PRICING TABLE</a>
                    </li>
                    <li><a href="shortcode-process.html"><i class="__icon fa fa-check-square"></i>PROCESS</a>
                    </li>
                    <li><a href="shortcode-progressbar.html"><i class="__icon fa fa-server"></i>PROGRESSBAR</a>
                    </li>
                    <li><a href="shortcode-qrcode.html"><i class="__icon fa fa-qrcode"></i>QRCODE</a>
                    </li>
                    <li><a href="shortcode-separator.html"><i class="__icon fa fa-minus"></i>SEPARATOR</a>
                    </li>
                    <li><a href="shortcode-service-table.html"><i class="__icon fa fa-th"></i>SERVICE TABLE</a>
                    </li>
                  </ul>
                </li>
                <li><a href="#">SHORTCODES 04</a>
                  <ul>
                    <li><a href="shortcode-tab.html"><i class="__icon fa fa-th-large"></i>TAB</a>
                    </li>
                    <li><a href="shortcode-table.html"><i class="__icon fa fa-table"></i>TABLE</a>
                    </li>
                    <li><a href="shortcode-team.html"><i class="__icon fa fa-users"></i>TEAM</a>
                    </li>
                    <li><a href="shortcode-testimonials.html"><i class="__icon fa fa-comments"></i>TESTIMONIALS</a>
                    </li>
                    <li><a href="shortcode-typography.html"><i class="__icon fa fa-text-height"></i>TYPOGRAPHY</a>
                    </li>
                    <li><a href="shortcode-vertical-progressbar.html"><i class="__icon fa fa-bar-chart"></i>VERTICAL PROGRESSBAR</a>
                    </li>
                    <li><a href="shortcode-widget.html"><i class="__icon fa fa-cube"></i>WIDGET</a>
                    </li>
                    <li><a href="shortcode-image-gallery.html"><i class="__icon fa fa-file-image-o"></i>IMAGE GALLERY</a>
                    </li>
                    <li><a href="shortcode-image-slider.html"><i class="__icon fa fa-picture-o"></i>IMAGE SLIDER</a>
                    </li>
                    <li><a href="shortcode-share-module.html"><i class="__icon fa fa-paper-plane-o"></i>SHARE MODULE</a>
                    </li>
                  </ul>
                </li>
                <li><a href="#">MANY FEATURES</a>
                  <ul>
                    <li><a href="#">BOOTSTRAP 3 GRID SYSTEM</a>
                    </li>
                    <li><a href="#">HTML5 &amp; CSS3 ANIMATION</a>
                    </li>
                    <li><a href="#">AUTOMATE YOUR WORKFLOW</a>
                    </li>
                    <li><a href="#">NODE TEMPLATE ENGINE</a>
                    </li>
                    <li><a href="#">LESS PRE-PROCESSOR</a>
                    </li>
                    <li><a href="#">FONT AWESOME INCLUDED</a>
                    </li>
                    <li><a href="#">+1000 ICON FONTS </a>
                    </li>
                    <li><a href="#">FULLY RESPONSIVE</a>
                    </li>
                    <li><a href="#">FREE GOOGLE FONTS</a>
                    </li>
                    <li><a href="#">PARALLAX BACKGROUND</a>
                    </li>
                    <li><a href="#">WIDE AND BOXED LAYOUT</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li><a href="#">OPTIONS</a>
              <ul>
                <li><a href="#"><i class="__icon fa fa-files-o"></i>HEADER OPTIONS</a>
                  <ul>
                    <li><a href="header-option-1.html">HEADER STYLE 01</a>
                    </li>
                    <li><a href="header-option-2.html">HEADER STYLE 02</a>
                    </li>
                    <li><a href="header-option-3.html">HEADER STYLE 03</a>
                    </li>
                    <li><a href="header-option-4.html">HEADER STYLE 04</a>
                    </li>
                    <li><a href="header-option-5.html">HEADER STYLE 05</a>
                    </li>
                    <li><a href="header-option-6.html">HEADER STYLE 06</a>
                    </li>
                    <li><a href="header-option-7.html">HEADER STYLE 07</a>
                    </li>
                    <li><a href="header-option-8.html">HEADER STYLE 08</a>
                    </li>
                    <li><a href="header-option-12.html">HEADER STYLE 09</a>
                    </li>
                    <li><a href="header-option-16.html">HEADER STYLE 10</a>
                    </li>
                    <li><a href="header-option-20.html">HEADER STYLE 11</a>
                    </li>
                    <li><a href="header-option-17.html">HEADER STYLE 12</a>
                    </li>
                    <li><a href="#">AND MUCH MORE</a>
                    </li>
                  </ul>
                </li>
                <li><a href="#"><i class="__icon fa fa-magic"></i>FOOTER OPTIONS</a>
                  <ul>
                    <li><a href="footer-option-1.html#footer">FOOTER STYLE 01</a>
                    </li>
                    <li><a href="footer-option-2.html#footer">FOOTER STYLE 02</a>
                    </li>
                    <li><a href="footer-option-3.html#footer">FOOTER STYLE 03</a>
                    </li>
                    <li><a href="footer-option-4.html#footer">FOOTER STYLE 04</a>
                    </li>
                    <li><a href="footer-option-5.html#footer">FOOTER STYLE 05</a>
                    </li>
                    <li><a href="footer-option-6.html#footer">FOOTER STYLE 06</a>
                    </li>
                    <li><a href="footer-option-7.html#footer">FOOTER STYLE 07</a>
                    </li>
                    <li><a href="footer-option-8.html#footer">FOOTER STYLE 08</a>
                    </li>
                    <li><a href="footer-option-9.html#footer">FOOTER STYLE 09</a>
                    </li>
                    <li><a href="footer-option-10.html#footer">FOOTER STYLE 10</a>
                    </li>
                    <li><a href="#">AND MUCH MORE</a>
                    </li>
                  </ul>
                </li>
                <li><a href="#"><i class="__icon fa fa-youtube-play"></i>VIDEO OPTIONS</a>
                  <ul>
                    <li><a href="homepage-video-1.html">YOUTUBE VIDEO</a>
                    </li>
                    <li><a href="homepage-video-2.html">HTML5 VIDEO</a>
                    </li>
                    <li><a href="homepage-video-3.html">SLIDER &amp; VIDEO</a>
                    </li>
                  </ul>
                </li>
                <li><a href="#"><i class="__icon fa fa-diamond"></i>WIDGET OPTIONS</a>
                  <ul>
                    <li><a href="shortcode-widget.html">CATEGORIES</a>
                    </li>
                    <li><a href="shortcode-widget.html">RECENT POSTS</a>
                    </li>
                    <li><a href="shortcode-widget.html">IMAGES GALLERY</a>
                    </li>
                    <li><a href="shortcode-widget.html">RECENT TWEETS</a>
                    </li>
                    <li><a href="shortcode-widget.html">POPULAR TAGS</a>
                    </li>
                    <li><a href="shortcode-widget.html">SEARCH BOX</a>
                    </li>
                    <li><a href="shortcode-widget.html">FILTER BY PRICE</a>
                    </li>
                    <li><a href="shortcode-widget.html">ACCORDION WIDGET</a>
                    </li>
                    <li><a href="shortcode-widget.html">RECENT PRODUCTS</a>
                    </li>
                    <li><a href="shortcode-widget.html">FREE TEXT WIDGET</a>
                    </li>
                    <li><a href="shortcode-widget.html">SLIDER IMAGES</a>
                    </li>
                    <li><a href="shortcode-widget.html">SOCIAL ICONS</a>
                    </li>
                  </ul>
                </li>
                <li><a href="#"><i class="__icon fa fa-cubes"></i>MENU OPTIONS</a>
                  <ul>
                    <li><a href="#">DROPDOWN MENU</a>
                    </li>
                    <li><a href="#">MEGAMENU 2 COLUMNS</a>
                    </li>
                    <li><a href="#">MEGAMENU 3 COLUMNS</a>
                    </li>
                    <li><a href="#">MEGAMENU 4 COLUMNS</a>
                    </li>
                    <li><a href="#">MEGAMENU 5 COLUMNS</a>
                    </li>
                    <li><a href="#">MEGAMENU WITH HEADING</a>
                    </li>
                    <li><a href="#">OFF CANVAS MENU</a>
                    </li>
                    <li><a href="#">STICKY MENU</a>
                    </li>
                    <li><a href="#">MENU WITH ICON</a>
                    </li>
                    <li><a href="#">MENU WITH FLAG</a>
                    </li>
                  </ul>
                </li>
                <li><a href="#"><i class="__icon fa fa-spinner"></i>UPDATING...</a>
                </li>
              </ul>
            </li>
            <li><a href="#">BLOG</a>
              <ul>
                <li><a href="#">BLOG LARGE IMAGE</a>
                  <ul>
                    <li><a href="blog-fullwidth.html">LARGE IMAGE FULLWIDTH</a>
                    </li>
                    <li><a href="blog-sidebar-left.html">LARGE IMAGE SIDEBAR LEFT</a>
                    </li>
                    <li><a href="blog-sidebar-right.html">LARGE IMAGE SIDEBAR RIGHT</a>
                    </li>
                  </ul>
                </li>
                <li><a href="#">BLOG LEFT IMAGE</a>
                  <ul>
                    <li><a href="blog-left-image-fullwidth.html">LEFT IMAGES IMAGE FULLWIDTH</a>
                    </li>
                    <li><a href="blog-left-image-sidebar-left.html">LEFT IMAGES IMAGE SIDEBAR LEFT</a>
                    </li>
                    <li><a href="blog-left-image-sidebar-right.html">LEFT IMAGES IMAGE SIDEBAR RIGHT</a>
                    </li>
                  </ul>
                </li>
                <li><a href="#">BLOG TIME LINE</a>
                  <ul>
                    <li><a href="blog-timeline-fullwidth.html">TIME LINE FULLWIDTH</a>
                    </li>
                    <li><a href="blog-timeline-sidebar-left.html">TIME LINE SIDEBAR LEFT</a>
                    </li>
                    <li><a href="blog-timeline-sidebar-right.html">TIME LINE SIDEBAR RIGHT</a>
                    </li>
                  </ul>
                </li>
                <li><a href="#">BLOG MASONRY</a>
                  <ul>
                    <li><a href="blog-masonry-wide.html">MASONRY WIDE</a>
                    </li>
                    <li><a href="blog-masonry-fullwidth.html">MASONRY FULLWIDTH</a>
                    </li>
                    <li><a href="blog-masonry-sidebar-left.html">MASONRY SIDEBAR LEFT</a>
                    </li>
                    <li><a href="blog-masonry-sidebar-right.html">MASONRY SIDEBAR RIGHT</a>
                    </li>
                  </ul>
                </li>
                <li><a href="#">BLOG SINGLE</a>
                  <ul>
                    <li><a href="blog-single-sidebar-left.html">SINGLE SIDEBAR LEFT</a>
                    </li>
                    <li><a href="blog-single-fullwidth.html">SINGLE FULLWIDTH</a>
                    </li>
                    <li><a href="blog-single-standard.html">SINGLE STANDARD</a>
                    </li>
                    <li><a href="blog-single-audio.html">SINGLE AUDIO</a>
                    </li>
                    <li><a href="blog-single-gallery.html">SINGLE GALLERY</a>
                    </li>
                    <li><a href="blog-single-quote.html">SINGLE QUOTE</a>
                    </li>
                    <li><a href="blog-single-video.html">SINGLE VIDEO</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li><a href="#">WORKS</a>
              <ul>
                <li><a href="#">3 COLUMNS</a>
                  <ul>
                    <li><a href="portfolio-3-columns-hover-style-1.html">3 COLUMNS STYLE 01</a>
                    </li>
                    <li><a href="portfolio-3-columns-hover-style-2.html">3 COLUMNS STYLE 02</a>
                    </li>
                    <li><a href="portfolio-3-columns-hover-style-3.html">3 COLUMNS STYLE 03</a>
                    </li>
                    <li><a href="portfolio-3-columns-hover-style-4.html">3 COLUMNS STYLE 04</a>
                    </li>
                    <li><a href="portfolio-3-columns-hover-style-5.html">3 COLUMNS STYLE 05</a>
                    </li>
                    <li><a href="portfolio-3-columns-hover-style-6.html">3 COLUMNS STYLE 06</a>
                    </li>
                    <li><a href="portfolio-3-columns-no-space.html">NO SPACE</a>
                    </li>
                    <li><a href="portfolio-3-columns-wide.html">WIDE SCREEN</a>
                    </li>
                    <li><a href="portfolio-3-columns-wide-no-space.html">WIDE &amp; NO SPACE</a>
                    </li>
                  </ul>
                </li>
                <li><a href="#">4 COLUMNS</a>
                  <ul>
                    <li><a href="portfolio-4-columns-hover-style-1.html">4 COLUMNS STYLE 01</a>
                    </li>
                    <li><a href="portfolio-4-columns-hover-style-2.html">4 COLUMNS STYLE 02</a>
                    </li>
                    <li><a href="portfolio-4-columns-hover-style-3.html">4 COLUMNS STYLE 03</a>
                    </li>
                    <li><a href="portfolio-4-columns-hover-style-4.html">4 COLUMNS STYLE 04</a>
                    </li>
                    <li><a href="portfolio-4-columns-hover-style-5.html">4 COLUMNS STYLE 05</a>
                    </li>
                    <li><a href="portfolio-4-columns-hover-style-6.html">4 COLUMNS STYLE 06</a>
                    </li>
                    <li><a href="portfolio-4-columns-no-space.html">NO SPACE</a>
                    </li>
                    <li><a href="portfolio-4-columns-wide.html">WIDE SCREEN</a>
                    </li>
                    <li><a href="portfolio-4-columns-wide-no-space.html">WIDE &amp; NO SPACE</a>
                    </li>
                  </ul>
                </li>
                <li><a href="#">5 COLUMNS</a>
                  <ul>
                    <li><a href="portfolio-5-columns-hover-style-1.html">5 COLUMNS STYLE 01</a>
                    </li>
                    <li><a href="portfolio-5-columns-hover-style-2.html">5 COLUMNS STYLE 02</a>
                    </li>
                    <li><a href="portfolio-5-columns-hover-style-3.html">5 COLUMNS STYLE 03</a>
                    </li>
                    <li><a href="portfolio-5-columns-hover-style-4.html">5 COLUMNS STYLE 04</a>
                    </li>
                    <li><a href="portfolio-5-columns-hover-style-5.html">5 COLUMNS STYLE 05</a>
                    </li>
                    <li><a href="portfolio-5-columns-hover-style-6.html">5 COLUMNS STYLE 06</a>
                    </li>
                    <li><a href="portfolio-5-columns-no-space.html">NO SPACE</a>
                    </li>
                    <li> <a href="portfolio-5-columns-wide.html">WIDE SCREEN</a>
                    </li>
                    <li><a href="portfolio-5-columns-wide-no-space.html">WIDE &amp; NO SPACE</a>
                    </li>
                  </ul>
                </li>
                <li><a href="#">SPECIAL STYLE</a>
                  <ul>
                    <li><a href="portfolio-carousel.html">CAROUSEL</a>
                    </li>
                    <li><a href="portfolio-hover-effect.html">HOVER EFFECT<span class="__flag border-danger color-danger">HOT</span></a>
                    </li>
                    <li><a href="portfolio-masonry.html">MASONRY</a>
                    </li>
                    <li><a href="portfolio-masonry-wide.html">MASONRY WIDE</a>
                    </li>
                    <li><a href="portfolio-shortcode-masonry.html">METRO STYLE 01</a>
                    </li>
                    <li><a href="portfolio-shortcode-masonry-2.html">METRO STYLE 02</a>
                    </li>
                    <li><a href="portfolio-2-columns-hover-style-1.html">2 COLUMNS STYLE 01</a>
                    </li>
                    <li><a href="portfolio-2-columns-hover-style-2.html">2 COLUMNS STYLE 02</a>
                    </li>
                    <li><a href="portfolio-2-columns-hover-style-3.html">2 COLUMNS STYLE 03</a>
                    </li>
                  </ul>
                </li>
                <li><a href="#">SINGLE PORTFOLIO</a>
                  <ul>
                    <li><a href="portfolio-single-1.html">PORTFOLIO SINGLE 01</a>
                    </li>
                    <li><a href="portfolio-single-2.html">PORTFOLIO SINGLE 02</a>
                    </li>
                    <li><a href="portfolio-single-3.html">PORTFOLIO SINGLE 03</a>
                    </li>
                    <li><a href="portfolio-single-4.html">PORTFOLIO SINGLE 04</a>
                    </li>
                    <li><a href="portfolio-single-5.html">PORTFOLIO SINGLE 05</a>
                    </li>
                    <li><a href="portfolio-single-6.html">PORTFOLIO SINGLE 06</a>
                    </li>
                    <li><a href="portfolio-single-7.html">PORTFOLIO SINGLE 07</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li><a href="#">SHOP</a>
              <ul>
                <li><a href="#">SHOP CATEGORY</a>
                  <ul>
                    <li><a href="shop-category-fullwidth.html">CATEGORY FULLWIDTH</a>
                    </li>
                    <li><a href="shop-category-sidebar-left.html">CATEGORY SIDE BAR LEFT</a>
                    </li>
                    <li><a href="shop-category-sidebar-right.html">CATEGORY SIDE BAR RIGHT</a>
                    </li>
                    <li><a href="shop-category-top-slider.html">CATEGORY TOP SLIDER</a>
                    </li>
                  </ul>
                </li>
                <li><a href="#">SINGLE SHOP</a>
                  <ul>
                    <li><a href="shop-single-fullwidth.html">SINGLE FULLWIDTH</a>
                    </li>
                    <li><a href="shop-single-sidebar-left.html">SINGLE SIDE BAR LEFT</a>
                    </li>
                    <li><a href="shop-single-sidebar-right.html">SINGLE SIDE BAR RIGHT</a>
                    </li>
                    <li><a href="shop-single-top-slider.html">SINGLE TOP SLIDER</a>
                    </li>
                    <li><a href="shop-single-variable.html">SINGLE VARIABLE</a>
                    </li>
                  </ul>
                </li>
                <li><a href="#">OTHER PAGE</a>
                  <ul>
                    <li><a href="shop-account.html">MY ACCOUNT</a>
                    </li>
                    <li><a href="shop-cart.html">CART</a>
                    </li>
                    <li><a href="shop-checkout.html">CHECK OUT</a>
                    </li>
                    <li><a href="shop-order-tracking.html">ORDER TRACKING</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
          </ul>
        </nav>
        <div class="search-area">
          <form method="post" class="search-box">
            <input type="search" placeholder="Search..."/>
            <button type="submit"><i class="icon-search-icon"></i></button>
          </form>
        </div>
        <div class="social-area">
          <ul class="social circle gray">
            <li><a href="#"><i class="icon icon-facebook-1"></i></a></li>
            <li><a href="#"><i class="icon icon-pinterest"></i></a></li>
            <li><a href="#"><i class="icon icon-twitter-1"></i></a></li>
            <li><a href="#"><i class="icon icon-linkedin-1"></i></a></li>
          </ul>
        </div>
      </div>
      <!-- Page Navigation-->
      <!--Page Header-->
      <header class="page-header home home-slider-1">
        <div class="slider caption-slider control-nav direction-nav">
          <div class="block-caption-slider overlay-container">
            <div style="background-image: url(assets/images/interior/interior_bg-1.jpg); background-position: undefined" class="__img-background"></div>
            <div class="overlay bgc-dark-o-4">
              <div class="cell-vertical-wrapper">
                <div class="cell-middle">
                  <div class="caption-preset-simple-5 text-center">
                    <div class="container">
                      <div class="caption-wrapper">
                        <h1 class="text-responsive size-ll caption">DEMO INTERIOR DESIGN</h1>
                        <p class="font-serif-italic fz-3 caption">Get ready to change your life and truly feel your absolute best!</p>
                        <div class="__buttons caption"><a href="index.html" class="btn btn-gray-base fullwidth">SELECT DEMO</a><br/><a href="http://goo.gl/fi9Eas" target="_blank" class="btn-border btn-light fullwidth">PURCHASE NOW</a></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="block-caption-slider overlay-container">
            <div style="background-image: url(assets/images/interior/interior_bg-2.jpg); background-position: undefined" class="__img-background"></div>
            <div class="overlay bgc-dark-o-4">
              <div class="cell-vertical-wrapper">
                <div class="cell-middle">
                  <div class="caption-preset-simple-5 text-center-sm-max">
                    <div class="container">
                      <div class="caption-wrapper">
                        <h1 class="text-responsive size-ll caption">150+ REUSABLE <br/> ELEMENTS</h1>
                        <p class="font-serif-italic fz-3 caption">A hight quality Multi-layout, Multipurpose premium HTML5 theme</p>
                        <div class="__buttons caption"><a href="index.html" class="btn btn-gray-base fullwidth">SELECT DEMO</a><br/><a href="http://goo.gl/fi9Eas" target="_blank" class="btn-border btn-light fullwidth">PURCHASE NOW</a></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="block-caption-slider overlay-container">
            <div style="background-image: url(assets/images/interior/interior_bg-3.jpg); background-position: undefined" class="__img-background"></div>
            <div class="overlay bgc-dark-o-4">
              <div class="cell-vertical-wrapper">
                <div class="cell-middle">
                  <div class="caption-preset-simple-5 text-center-sm-max">
                    <div class="container">
                      <div class="caption-wrapper">
                        <h1 class="text-responsive size-ll caption">MULTI-PURPOSE AND<br/>CREATIVE THEME</h1>
                        <p class="font-serif-italic fz-3 caption">It is ideal for professional teams to introduce their services</p>
                        <div class="__buttons caption"><a href="index.html" class="btn btn-gray-base fullwidth">SELECT DEMO</a><br/><a href="http://goo.gl/fi9Eas" target="_blank" class="btn-border btn-light fullwidth">PURCHASE NOW</a></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
      <!--End Page Header-->
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
