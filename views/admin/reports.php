<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchGuest */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reports';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="">
              <div class="col-md-12 col-sm-12 col-xs-12">
            
                <div class="x_panel">
                  <div class="x_title">
                  <h3><i class="fa fa-bar-chart"></i> 
    <?= Html::encode($this->title) ?></h3>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
<div class="col-md-12">
<div class="col-md-4"><h3>Front Office</h3>
<li><?php echo Html::a(' Arrival Departure & Stay Over Report', ['/room/arrival-departure-report']) ?></li>
<li><?php echo Html::a(' Guest Inhouse Report', ['/room/guest-inhouse-report']) ?></li>
<li><?php echo Html::a(' Guest Ledger Report', ['/room/guest-ledger-report']) ?></li>
<li><?php echo Html::a(' Room Availibility Report', ['/room/room-availability-report']) ?></li>
<li><?php echo Html::a(' Room Chart', ['/room/room-chart']) ?></li>
</div>
<div class="col-md-4"><h3>Reservations</h3>
                        <li><?php echo Html::a(' Arrival List Report', ['/reservations/arrival-list-report']) ?></li>
                        <li><?php echo Html::a(' Availability Chart', ['/reservations/availability-chart']) ?></li>
                        <li><?php echo Html::a(' Monthly Inventory Report', ['/reservations/monthly-inventory-report']) ?></li>
                        <li><?php echo Html::a(' Reservation Report', ['/reservations/reservation-report']) ?></li>
</div>
<div class="col-md-4"><h3>Rooms</h3>
                        <li><?php echo Html::a(' Room Status Report', ['/room/room-status-report']) ?></li>
                        <li><?php echo Html::a(' Company Wise Room Report', ['/room/company-wise-room-report']) ?></li>
                        <li><?php echo Html::a(' Guest Wise Room Report', ['/room/guest-wise-room-report']) ?></li>
                        <li><?php echo Html::a(' Room History Report', ['/room/room-history-report']) ?></li>
                        <li><?php echo Html::a(' Room Statistics', ['/room/room-statistics-report']) ?></li>
                        <br>
</div>
</div>

<div class="col-md-12">
<div class="col-md-4"><h3>Night Audit</h3>
<li><?php echo Html::a(' Daily Flash Report', ['/reservations/availability-chart']) ?></li>
<li><?php echo Html::a(' Daily Summary Report', ['/reservations/availability-chart']) ?></li>
<li><?php echo Html::a(' Night Audit Report', ['/reservations/availability-chart']) ?></li>
<li><?php echo Html::a(' Room Rent Variation Report', ['/reservations/availability-chart']) ?></li>
</div>
<div class="col-md-4"><h3>Direct Billing & Company</h3>
<li><?php echo Html::a(' City Ledger Report', ['/reservations/availability-chart']) ?></li>
<li><?php echo Html::a(' Direct Bill Register Report</li>', ['/reservations/availability-chart']) ?></div>
<div class="col-md-4"><h3>Group Booking</h3>
<li><?php echo Html::a(' Assigned Room Report', ['/reservations/availability-chart']) ?></li>
<li><?php echo Html::a(' Group Arrival List', ['/reservations/availability-chart']) ?></li>
<li><?php echo Html::a(' Group In-house Report', ['/reservations/availability-chart']) ?></li>
<li><?php echo Html::a(' Group Outstanding Report', ['/reservations/availability-chart']) ?></li>
<br>
</div>
</div>
</div>
</div>
</div>
</div>