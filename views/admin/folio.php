<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchGuest */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Folio';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="">
              <div class="col-md-12 col-sm-12 col-xs-12">
            
                <div class="x_panel">
                  <div class="x_title">
                  <h3><i class="fa fa-folder-open"></i> 
    <?= Html::encode($this->title) ?></h3>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
<div class="guest-index">
<h3>Credit</h3>
<div class="table-responsive">
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Entity ID</th>
                          <th>Registration Date</th>
                          <th>User ID</th>
                          <th>Status ID</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                            </tbody>
                            </table>
                            </div>

<h3>Debit</h3>
<div class="table-responsive">
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Entity ID</th>
                          <th>Registration Date</th>
                          <th>User ID</th>
                          <th>Status ID</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                            </tbody>
                            </table>
                            </div>
</div>
</div>
</div>
</div>
</div>