<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RestaurantProduct */

$this->title = 'Update Restaurant Product: ' . $model->product_id;
$this->params['breadcrumbs'][] = ['label' => 'Restaurant Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->product_id, 'url' => ['view', 'id' => $model->product_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="restaurant-product-update">

        <?= Html::a('Back', ['restaurant-booking/index'], ['class' => 'btn btn-danger pull-right']) ?>
    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
