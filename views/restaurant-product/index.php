<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widget\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Point Of Sales';
$this->params['breadcrumbs'][] = $this->title;
?>
<style type="text/css">
  .count{
    font-size: 15px !important;
  }
</style>
<div class="restaurant-product-index">
        <?php echo Html::a('Register New Product', ['/restaurant-product/create'], ['class' => 'btn btn-danger'], ['data-toggle' => 'modal'], ['data-target' => '.bs-example-modal-lg']) ?>
                    
              <?php foreach($restaurantProductModel as $k => $v){?>

              <div class="animated flipInY col-lg-2 col-md-2 col-sm-4 col-xs-12">
     
                <div class="tile-stats outer-box" style="padding-right: 10px !important;">
                <br>
                  <div class="icon"><i class="fa <?php if($v['product_status'] == 8){echo 'fa-cutlery';} else {echo 'fa-glass';}?>"></i></div>
                  <div class="count"><?php echo  $v['product_name'];?> <br>Status: <?php echo  $v['productStatus']['status_title'];?></div>
                  <div class="count">N<?php echo  $v['price']['product_price'];?></div>
                  <br><center>
          <?= Html::input('number','','',['class'=>'quantity', 'style'=>'width:80px;'])?></center><br><button class="hvr-bounce-in pull-right btn btn-danger btn-round addToCart" data-url="<?= Url::to(['restaurant-booking/addtocart','id'=>$v['product_id'],'quantity'=>4])?>">ADD ITEM <i class="fa fa-plus"></i></button><br>
                </div>

                

              </div>
                       
                            <?php }?>

</div>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                        </div>
                        <div class="modal-body">
                           
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="button" class="btn btn-primary">Save changes</button>
                        </div>

                      </div>
                    </div>
                  </div>



<?php
$paymentJs = <<<JS
$(document).ready(function(){


$(document).on('click','.addToCart',function(){
var quantity = $(this).parent().find('.quantity').val();
var url = $(this).data('url');

var jqxhr = $.post(url+'&quantity='+quantity)
  .done(function() {
    alert( 'sent');
	$.pjax.reload({container:"#cart",async: false
}); 
  })
  .fail(function() {
    alert( "error" );
  })
  

})


$(document).on('click','#clearCart',function(){
var url = $(this).data('url');

var jqxhr = $.ajax(url)
  .done(function() {
    alert( 'sent');
	$.pjax.reload({container:"#cart",async: false
}); 
  })
  .fail(function() {
    alert( "error" );
  })
  

})

$(document).on('click','.delete',function(){
var url = $(this).data('url');

var jqxhr = $.ajax(url)
  .done(function() {
    alert( 'sent');
	$.pjax.reload({container:"#cart",async: false
}); 
  })
  .fail(function() {
    alert( "error" );
  })
  

})


})


JS;
 
$this->registerJs($paymentJs);
?>



