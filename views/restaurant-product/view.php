<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RestaurantProduct */

$this->title = $model->product_id;
$this->params['breadcrumbs'][] = ['label' => 'Restaurant Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="restaurant-product-view">

    <h3>Product Details: <?= Html::encode($this->title) ?></h3>
        <?= Html::a('Back', ['restaurant-booking/index'], ['class' => 'btn btn-danger pull-right']) ?>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->product_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->product_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'product_id',
            'product_name',
            'price_id',
            'product_quantity',
            'product_status',
        ],
    ]) ?>

</div>
