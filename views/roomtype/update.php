<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RoomType */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Room Type',
]) . $model->room_type_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Room Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->room_type_id, 'url' => ['view', 'id' => $model->room_type_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="room-type-update">
    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
