<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Button ;
use  yii\bootstrap\Modal;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="room-type-index">

                        <button class="btn btn-danger booking pull-right" data-url="<?php echo Url::to(['/roomtype/create']); ?>" >Create Room Type</button>
                    <table id="datatable-fixed-header" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>SN</th>
                          <th>Type</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
              <?php foreach($roomTypeModel as $k => $v){?>

                        <tr>
                          <td><?php echo  $v['room_type_id'];?></td>
                          <td><?php echo  $v['type'];?></td>
                          <td>
                        <a class="booking" data-url="<?php echo Url::to(['/roomtype/view', 'id' => $v['room_type_id']]); ?>" ><i class="fa fa-eye hvr-bounce-in"></i></a>
                         &nbsp;&nbsp;
                        <a class="booking" data-url="<?php echo Url::to(['/roomtype/update', 'id' => $v['room_type_id']]); ?>" ><i class="fa fa-pencil hvr-bounce-in"></i></a>
                        </td>
                        </tr>
                            <?php }?>
                            </tbody>
                            </table>

</div>
