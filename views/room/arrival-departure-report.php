<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Reservations */

$this->title = 'Arrival, Departure & Stay over Report';
?>
<div class="">
     
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel" style="border-width:10px; border-style:ridge">
                  <div class="x_content">
<br>
<div class="col-md-12">
    <div class="col-md-4">
    	<div class="pull-left">
    		Printed By: <?= Yii::$app->user->identity->person->surname.' '.Yii::$app->user->identity->person->first_name ?><br>
    		Printed Date: <?= date('Y-m-d H:i:s') ?>
    	</div>
    </div>
    <div class="col-md-4"><center>DBI GUEST HOUSE<br>ARRIVAL DEPARTURE & STAY OVER REPORT</center></div>
    <div class="col-md-4">
    	<div class="pull-right">Page 1</div></div>
    	</div>
    	<br>
    	<br>
    	<hr>
    	Month As On <?= date('M Y'); ?>
<hr>
<table class="table table-hover">
                      <thead>
                        <tr>
                          <th>Arvl.</th>
                          <th>Dept.</th>
                          <th>Nights</th>
                          <th>Doc#</th>
                          <th>Guest</th>
                          <th>Rm No.</th>
                          <th>Tariff</th>
                          <th>Source</th>
                          <th>Booking Type</th>
                        </tr>
                      </thead>
                      <tbody>
                      <td>&nbsp;&nbsp;<strong>Checked In</strong></td>
              <?php foreach($arrivalReport as $k => $v){?>
                        <tr>
                          <td><?php echo  $v['time_in'];?></td>
                          <td><?php echo  $v['time_out'];?></td>
                          <td><?php $date1 = strtotime($v['time_out']); $date2 = strtotime($v['time_in']);
                          echo floor(($date1-$date2)/(60 * 60 * 24)); ?></td> 
                          <td></td>
              <td><?php echo  $v['person']['surname'].' '. $v['person']['first_name'];?></td>
                           <td><?php echo  $v['room']['room_number'];?></td>
                          <td><?php echo  $v['tariff']['tariff'];?></td>
                          <td></td>
              <td><?php echo  $v['status']['status_title'];?></td>
                        </tr>
                            <?php }?>
                      <tr><td>Grand Total: <?php echo $arrivalReportTotal; ?></td></tr>
                      <tr><td></td></tr>
                     <td>&nbsp;&nbsp;<strong>Checked Out</strong></td>
              <?php foreach($departureReport as $k => $v){?>
                        <tr>
                          <td><?php echo  $v['time_in'];?></td>
                          <td><?php echo  $v['time_out'];?></td>
                          <td><?php $date1 = strtotime($v['time_out']); $date2 = strtotime($v['time_in']);
                          echo floor(($date1-$date2)/(60 * 60 * 24)); ?></td> 
                          <td></td>
              <td><?php echo  $v['person']['surname'].' '. $v['person']['first_name'];?></td>
                           <td><?php echo  $v['room']['room_number'];?></td>
                          <td><?php echo  $v['tariff']['tariff'];?></td>
                          <td></td>
              <td><?php echo  $v['status']['status_title'];?></td>
                        </tr>
                            <?php }?>

                      <tr><td>Grand Total: <?php echo $departureReportTotal; ?></td></tr>
                      <tr><td></td></tr>
                      <td>&nbsp;&nbsp;<strong>Stay Over</strong></td>
              <?php foreach($stayOverReport as $k => $v){?>
                        <tr>
                          <td><?php echo  $v['time_in'];?></td>
                          <td><?php echo  $v['time_out'];?></td>
                          <td><?php $date1 = strtotime($v['time_out']); $date2 = strtotime($v['time_in']);
                          echo floor(($date1-$date2)/(60 * 60 * 24)); ?></td> 
                          <td></td>
              <td><?php echo  $v['person']['surname'].' '. $v['person']['first_name'];?></td>
                           <td><?php echo  $v['room']['room_number'];?></td>
                          <td><?php echo  $v['tariff']['tariff'];?></td>
                          <td></td>
              <td><?php echo  $v['status']['status_title'];?></td>
                        </tr>
                            <?php }?>
                      <tr><td>Grand Total: <?php echo $stayOverReportTotal; ?></td></tr>
                      </tbody>
                    </table>

</div>
</div>
</div>
</div>