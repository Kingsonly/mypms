<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Reservations */

$this->title = 'Room Chart';
?>
<div class="">

  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel" style="border-width:10px; border-style:ridge">
      <div class="x_content">
        <br>
        <div class="col-md-12">
          <div class="col-md-4">
            <div class="pull-left">
              Printed By: <?= Yii::$app->user->identity->person->surname.' '.Yii::$app->user->identity->person->first_name ?><br>
              Printed Date: <?= date('Y-m-d H:i:s') ?>
            </div>
          </div>
          <div class="col-md-4"><center>DBI GUEST HOUSE<br>WEEKLY ROOM CHART</center></div>
          <div class="col-md-4">
            <div class="pull-right">Page 1</div></div>
          </div>
          <br>
          <br>
          <hr>
          Month As On <?= date('M Y'); ?>
          <hr>
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Room</th>
                <?php
                $day = date('Y-m-d');
                $todaysdate = strtotime($day);
                $today = date("D", $todaysdate);
                if ($today == 'Sun') {
                $day1 = $day;
                $day2 = DATE($day) +1;
                $day3 = DATE($day) +2;
                $day4 = DATE($day) +3;
                $day5 = DATE($day) +4;
                $day6 = DATE($day) +5;
                $day7 = DATE($day) +6;
                } elseif ($today == 'Mon') {
                $day1 = date('Y-m-d', strtotime(' -1 day')); 
                $day2 = $day;
                $day3 = date('Y-m-d', strtotime(' +1 day')); 
                $day4 = date('Y-m-d', strtotime(' +2 day')); 
                $day5 = date('Y-m-d', strtotime(' +3 day')); 
                $day6 = date('Y-m-d', strtotime(' +4 day'));
                $day7 = date('Y-m-d', strtotime(' +5 day'));      
                }elseif ($today == 'Tue') {
                $day1 = date('Y-m-d', strtotime(' -2 day')); 
                $day2 = date('Y-m-d', strtotime(' -1 day')); 
                $day3 = $day;
                $day4 = date('Y-m-d', strtotime(' +1 day')); 
                $day5 = date('Y-m-d', strtotime(' +2 day')); 
                $day6 = date('Y-m-d', strtotime(' +3 day')); 
                $day7 = date('Y-m-d', strtotime(' +4 day'));                  
                }elseif ($today == 'Wed') {
                $day1 = date('Y-m-d', strtotime(' -3 day'));
                $day2 = date('Y-m-d', strtotime(' -2 day')); 
                $day3 = date('Y-m-d', strtotime(' -1 day')); 
                $day4 = $day;
                $day5 = date('Y-m-d', strtotime(' +1 day')); 
                $day6 = date('Y-m-d', strtotime(' +2 day')); 
                $day7 = date('Y-m-d', strtotime(' +3 day'));                 
                }elseif ($today == 'Thu') {
                $day1 = date('Y-m-d', strtotime(' -4 day'));
                $day2 = date('Y-m-d', strtotime(' -3 day'));
                $day3 = date('Y-m-d', strtotime(' -2 day')); 
                $day4 = date('Y-m-d', strtotime(' -1 day')); 
                $day5 = $day;
                $day6 = date('Y-m-d', strtotime(' +1 day')); 
                $day7 = date('Y-m-d', strtotime(' +2 day'));                  
                }elseif ($today == 'Fri') {
                $day1 = date('Y-m-d', strtotime(' -5 day'));
                $day2 = date('Y-m-d', strtotime(' -4 day'));
                $day3 = date('Y-m-d', strtotime(' -3 day'));
                $day4 = date('Y-m-d', strtotime(' -2 day')); 
                $day5 = date('Y-m-d', strtotime(' -1 day')); 
                $day6 = $day; 
                $day7 = date('Y-m-d', strtotime(' +1 day'));                  
                }elseif ($today == 'Sat') {
                $day1 = date('Y-m-d', strtotime(' -6 day'));
                $day2 = date('Y-m-d', strtotime(' -5 day'));
                $day3 = date('Y-m-d', strtotime(' -4 day'));
                $day4 = date('Y-m-d', strtotime(' -3 day'));
                $day5 = date('Y-m-d', strtotime(' -2 day')); 
                $day6 = date('Y-m-d', strtotime(' -1 day')); 
                $day7 = $day;                
                }
                echo '<th>Sunday<br>'.$day1.'</th>
                <th>Monday<br>'.$day2.'</th>
                <th>Tuesday<br>'.$day3.'</th>
                <th>Wednesday<br>'.$day4.'</th>
                <th>Thursday<br>'.$day5.'</th>
                <th>Friday<br>'.$day6.'</th>
                <th>Saturday<br>'.$day7.'</th>';
                ?>
              </tr>
            </thead>
            <tbody>
              <?php foreach($roomList as $k => $v){?>
              <tr>
               
              <td><?php echo  $v['room_number'];?></td>

              <?php foreach($sunday as $k => $v){?>
              <td><?php echo  $v['person']['surname'].' '. $v['person']['first_name'];?></td>
              <?php }?>

              <?php foreach($monday as $k => $v){?>
              <td><?php echo  $v['person']['surname'].' '. $v['person']['first_name'];?></td>
              <?php }?>

              <?php foreach($tuesday as $k => $v){?>
              <td><?php echo  $v['person']['surname'].' '. $v['person']['first_name'];?></td>
              <?php }?>

              <?php foreach($wednesday as $k => $v){?>
              <td><?php echo  $v['person']['surname'].' '. $v['person']['first_name'];?></td>
              <?php }?>

              <?php foreach($thursday as $k => $v){?>
              <td><?php echo  $v['person']['surname'].' '. $v['person']['first_name'];?></td>
              <?php }?>

              <?php foreach($friday as $k => $v){?>
              <td><?php echo  $v['person']['surname'].' '. $v['person']['first_name'];?></td>
              <?php }?>

              <?php foreach($saturday as $k => $v){?>
              <td><?php echo  $v['person']['surname'].' '. $v['person']['first_name'];?></td>
              <?php }?>
                </tr>
              <?php }?>
                
              </tbody>
            </table>

          </div>
        </div>
      </div>
    </div>