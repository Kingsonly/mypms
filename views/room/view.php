<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\room */


?>
<div class="room-view">

     <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          	<th>SN</th>
							<th>guest</th>
							<th>Amount Paid</th>
              <th>Check-in</th>
							<th>Check-out</th>
							<th>User</th>
                        </tr>
                      </thead>
                      <tbody>
              <?php $basket = \Yii::$app->ecom->basket; $i=1; foreach($model as $k => $v){?>

							<div>
								<td><?php echo  $i;?></td>
                          		<td><?php echo  $v['person']['surname'].' '.$v['person']['first_name'] ;?></td>
                          		<td><?=$v['amount_paid'];?></td>
                          		<td><?=$v['time_in'];?></td>
                          		<td><?=$v['time_out'];?></td>
								<td>
									<?=$v['user']['person']['surname'].' '.$v['user']['person']['first_name'];?>
								</td>
            
							</div>
							<td></td>
						</tr>
                          
                            <?php $i++; }?>
                            </tbody>
                            </table>

</div>
