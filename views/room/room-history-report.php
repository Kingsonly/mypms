<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Reservations */

$this->title = 'Room History Report';
?>
<div class="">
     
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel" style="border-width:10px; border-style:ridge">
                  <div class="x_content">
<br>
<div class="col-md-12">
    <div class="col-md-4">
    	<div class="pull-left">
    		Printed By: <?= Yii::$app->user->identity->person->surname.' '.Yii::$app->user->identity->person->first_name ?><br>
    		Printed Date: <?= date('Y-m-d H:i:s') ?>
    	</div>
    </div>
    <div class="col-md-4"><center>DBI GUEST HOUSE<br>ROOM HISTORY REPORT</center></div>
    <div class="col-md-4">
    	<div class="pull-right">Page 1</div></div>
    	</div>
    	<br>
    	<br>
    	<hr>
    	Report For <?= date('d M Y'); ?>
<hr>
<table class="table table-hover">
                      <thead>
                        <tr>
                            <th>Guest Name</th>
              <th>Rm No.</th>
              <th>Folio No.</th>
                            <th></th>
                            <th>Rate</th>
                            <th></th>
                            <th>Srvc Chrg.</th>
                            <th></th>
                            <th>VAT</th>
                            <th></th>
                            <th>Consumption</th>
                            <th></th>
                            <th>Total</th>
                            <th></th>
                            <th>Amt Paid</th>
                            <th></th>
                            <th>Bal</th>
                        </tr>
                      </thead>
                      <tbody>
              <?php foreach($roomHistoryReport as $k => $v){?>
                          <tr>
              <td><?php echo  $v['person']['surname'].' '. $v['person']['first_name'];?></td>
              <td><?php echo  $v['room']['room_number'];?></td>
              <td></td>
              <td></td>
              <td style="border-bottom-width:2px; border-bottom-style:dashed;"><?php echo  $v['tariff']['tariff'];?></td>
              <td></td>
              <td style="border-bottom-width:2px; border-bottom-style:dashed;"><?php echo  (10/100) * ($v['tariff']['tariff']);?></td>
              <td></td>
              <td style="border-bottom-width:2px; border-bottom-style:dashed;"><?php echo  (5/100) * ($v['tariff']['tariff']);?></td>
              <td></td>
              <td style="border-bottom-width:2px; border-bottom-style:dashed;"><?php echo  (5/100) * ($v['tariff']['tariff']);?></td>
              <td></td>
              <td style="border-bottom-width:2px; border-bottom-style:dashed;"><?php echo  $v['tariff']['tariff'] + ((10/100) * ($v['tariff']['tariff'])) + ((5/100) * ($v['tariff']['tariff'])) + ((5/100) * ($v['tariff']['tariff']));?></td>
              <td></td>
                          <td style="border-bottom-width:2px; border-bottom-style:dashed;"><?php echo  ($v['amount_paid']);?></td>
              <td></td>
                          <td style="border-bottom-width:2px; border-bottom-style:dashed;"><?php echo ($v['tariff']['tariff'] + ((10/100) * ($v['tariff']['tariff'])) + ((5/100) * ($v['tariff']['tariff'])) + ((5/100) * ($v['tariff']['tariff']))) - $v['amount_paid'];?></td>
                        </tr>
                            <?php }?>
                      </tbody>
                    </table>

</div>
</div>
</div>
</div>