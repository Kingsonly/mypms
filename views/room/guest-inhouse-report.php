<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Reservations */

$this->title = 'Guest In-house Report';
?>
<div class="">
     
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel" style="border-width:10px; border-style:ridge">
                  <div class="x_content">
<br>
<div class="col-md-12">
    <div class="col-md-4">
        <div class="pull-left">
            Printed By: <?= Yii::$app->user->identity->person->surname.' '.Yii::$app->user->identity->person->first_name ?><br>
            Printed Date: <?= date('Y-m-d H:i:s') ?>
        </div>
    </div>
    <div class="col-md-4"><center>DBI GUEST HOUSE<br>GUEST IN-HOUSE REPORT</center></div>
    <div class="col-md-4">
        <div class="pull-right">Page 1</div></div>
        </div>
        <br>
        <br>
        <hr>
        Month As On <?= date('M Y'); ?>
<hr>
<table class="table table-hover">
                      <thead>
                        <tr>
                          <th>S/n</th>
                          <th>Rm No.</th>
                          <th>Adult</th>
                          <th>Child</th>
                          <th>Guest</th>
                          <th>Source</th>
                          <th>Tariff</th>
                          <th>Balance</th>
                          <th>Arvl</th>
                          <th>Dept.</th>
                          <th>Remark</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php for ($i=1; $i <= $guestInhouseReportTotal ; $i++) { ?>
              <?php foreach($guestInhouseReport as $k => $v){?>
                        <tr>
                        <td><?= $i; ?></td>
                           <td><?php echo  $v['room']['room_number'];?></td>
                           <td></td>
                           <td></td>
              <td><?php echo  $v['person']['surname'].' '. $v['person']['first_name'];?></td>
              <td></td>
                          <td><?php echo $v['tariff']['tariff'];?></td>
                          <td><?php echo $v['tariff']['tariff'] - $v['amount_paid']; ?></td>
                          <td><?php echo  $v['time_in'];?></td>
                          <td><?php echo  $v['time_out'];?></td>
              <td><?php echo  $v['status']['status_title'];?></td>
                        </tr>
                            <?php }}?>
                     
                      </tbody>
                    </table>

</div>
</div>
</div>
</div>