<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\RoomTariff;
use app\models\RoomType;
use app\models\StatusType;


/* @var $this yii\web\View */
/* @var $model app\models\room */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="room-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'room_number')->hiddenInput()->label(false); ?>

    <?=$form->field($model, 'room_tariff_id')->label(false)->dropdownList(ArrayHelper::map(RoomTariff::find()->all(), 'tariff_id', 'tariff'), array('style' => 'display:none'));?>
	
 	<?=$form->field($model, 'room_type')->label(false)->dropdownList(ArrayHelper::map(RoomType::find()->all(), 'room_type_id', 'type'), array('style' => 'display:none'));?>


<!--     <?= $form->field($model, 'status')->textInput() ?> -->
	
	<?= $form->field($model, 'status')->dropdownList(ArrayHelper::map(StatusType::find()->all(), 'status_id', 'status_title'));?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update Status', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

    