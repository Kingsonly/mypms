<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Reservations */

$this->title = 'Room Status Report';
?>
<div class="">
     
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel" style="border-width:10px; border-style:ridge">
                  <div class="x_content">
<br>
<div class="col-md-12">
    <div class="col-md-4">
    	<div class="pull-left">
    		Printed By: <?= Yii::$app->user->identity->person->surname.' '.Yii::$app->user->identity->person->first_name ?><br>
    		Printed Date: <?= date('Y-m-d H:i:s') ?>
    	</div>
    </div>
    <div class="col-md-4"><center>DBI GUEST HOUSE<br>ROOM STATUS REPORT</center></div>
    <div class="col-md-4">
    	<div class="pull-right">Page 1</div></div>
    	</div>
    	<br>
    	<br>
    	<hr>
    	Month As On <?= date('M Y'); ?>
<hr>
<table class="table table-striped">
                      <thead>
                        <tr>
                          <th>Rm. No.</th>
                          <th>Rm. Type</th>
                          <th>Tarriff</th>
                          <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
              <?php foreach($roomStatusReport as $k => $v){?>
                        <tr>
                          <th scope="row"><?php echo  $v['room_number'];?></th>
                          <td><?php echo  $v['roomType']['type'];?></td>
                          <td><?php echo  $v['roomTariff']['tariff'];?></td>
                          <td><?php echo  $v['status0']['status_title'];?></td>
                        </tr>
                            <?php }?>
                      </tbody>
                    </table>

</div>
</div>
</div>
</div>