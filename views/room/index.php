<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use  yii\bootstrap\Modal;
use yii\bootstrap\Button;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rooms';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
 /* line 4, ../sass/style.scss */
.outer-box {
  height: 121px;
  border-radius: 5px;
  display: block;
  position: relative;
}
/* line 10, ../sass/style.scss */
.outer-box img {
  height: auto;
}
/* line 15, ../sass/style.scss */
.outer-box .inner-box {
  height: 100%;
  border-radius: 5px;
  width: 100%;
  opacity: 0;
  top: 0;
  left: 0;
  position: absolute;
  padding: 0;
  transition: opacity .9s;
}
/* line 26, ../sass/style.scss */
.outer-box .inner-box p {
  color: #fff;
  line-height: 100px;
  text-align: center;
  font-size: 25px;
}
.outer-box:hover .hover-hide{
  opacity: 0;
}
/* line 36, ../sass/style.scss */
.outer-box:hover .inner-box {
  opacity: 1;
  transition: opacity .5s;
}


</style>
    <div class="">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                  <button class="pull-right btn btn-danger booking" data-url="<?php echo Url::to(['create']); ?>" >Create</button>
                    <h2><i class="fa fa-building"></i> ROOMS</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
						
                      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#rooms" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Status</a>
                        </li>
                        <li role="presentation" class=""><a href="#room_tariff" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Tariff</a>
                        </li>
                        <li role="presentation" class=""><a href="#room_type" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Types</a>
                        </li>
                        <li role="presentation" class=""><a href="#create_room" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Reports</a>
                        </li>
                      </ul>
                      <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="rooms" aria-labelledby="home-tab">
                       
<div class="room-index">
            <div class="row top_tiles">
              <?php foreach($viewModel as $k => $v){?>

				
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
     
                <div class="tile-stats outer-box">
                <div class="hover-hide">
                  <div class="icon">
						<i style="color: 
						<?php echo  $v['status0']['statusColor']['color_class_name'];?> !important;" class="fa <?php if($v['status0']['statusColor']['color_class_name'] == 'green'){echo 'fa-lock';} else {echo 'fa-unlock';}?>"></i>
						</div>

						<div class="count">
						<?php echo Html::a('00'.$v["room_number"], ['room-booking/create','id'=>1]); ?>
						</div>

						<h3><?php echo $v['roomType']['type']; ?> </h3>
            
						<h3>N<?php echo $v['roomTariff']['tariff']; ?></h3>
                  </div>
					
                  <div class="inner-box" style="background-color: <?php echo  $v['status0']['statusColor']['color_class_name'];?> !important;">

                  <p>
					  <?php if($v['status0']['statusColor']['color_class_name'] == 'green'){ ?> 
					  <i style="color:#fff;" class="hvr-bounce-in fa fa-unlock booking" data-url="<?=Url::to(['status','id'=>$v['room_id']])?>"></i>
					  
					  <?php } else { ?>
					  
           				<i style="color:#fff;" class="hvr-bounce-in fa fa-lock booking" data-url="<?=Url::to(['room-booking/create','id'=>$v['room_id']])?>"></i>
					  <?php } ?>
					  
					   <i style="color:#fff;" class="fa fa-eye hvr-bounce-in booking" data-url="<?= Url::to(['view','id'=>$v['room_id']])?>"></i>
					  
					 <!-- <i style="color:#fff;" class="fa fa-pencil hvr-bounce-in booking" data-url="<?= Url::to(['room/update','id'=>$v['room_id']])?>"></i>  -->
					  <!-- <?php echo Html::a(' <i style="color:#fff;" class="fa fa-eye hvr-bounce-in"></i>', ['/room-booking/view', 'id' => $v['room_id']]) ?>&nbsp;&nbsp; -->
					  
					  <!-- <?php echo Html::a(' <i style="color:#fff;" class="fa fa-pencil hvr-bounce-in"></i>', ['update', 'id' => $v['room_id']]) ?></p> -->
					  </p>
               </div>
                </div>

                

              </div>
							
							
							<?php }?>
							
            </div>
                     
</div>

                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="room_tariff" aria-labelledby="profile-tab">
                        
							<?php echo $this->render('/room-tariff/index',['roomTariffModel' => $roomTariffModel]);?>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="room_type" aria-labelledby="profile-tab">
              <?php echo $this->render('/roomtype/index',['roomTypeModel' => $roomTypeModel]);?>                       
                        </div>
                     <div role="tabpanel" class="tab-pane fade" id="create_room" aria-labelledby="profile-tab"> 
                     <div class="col-md-12">   
                        <font size="+1">
                        <li><?php echo Html::a(' Room Status Report', ['room-status-report']) ?></li>
                        <li><?php echo Html::a(' Company Wise Room Report', ['company-wise-room-report']) ?></li>
                        <li><?php echo Html::a(' Guest Wise Room Report', ['guest-wise-room-report']) ?></li>
                        <li><?php echo Html::a(' Room History Report', ['room-history-report']) ?></li>
                        <li><?php echo Html::a(' Room Statistics', ['room-statistics-report']) ?></li>
                        </font>
                        </div>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>


              

            </div>
            <script>
      /**
       * Used to demonstrate Hover.css only. Not required when adding
       * Hover.css to your own pages. Prevents a link from being
       * navigated and gaining focus.
       */
      var effects = document.querySelectorAll('.effects')[0];

      effects.addEventListener('click', function(e) {

        if (e.target.className.indexOf('hvr') > -1) {
          e.preventDefault();
          e.target.blur();

        }
      });
    </script>

                    
<?php
//this should be put directly inside the user index page (best practice should be the last thing in the page 
Modal::begin([
            'header' =>'<h1 id="modalheader"></h1>',
            'id' => 'roommodal',
            'size' => 'modal-lg',  
        ]);
echo "
<div id='wait'><center><img src='../web/admin/images/ajax-loader.gif' /></center></div>";

        echo "<div id='roommodalcontent'></div>";
        Modal::end();
    ?>