<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Reservations */

$this->title = 'Room Status Report';
?>
<div class="">
     
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel" style="border-width:10px; border-style:ridge">
                  <div class="x_content">
<br>
<div class="col-md-12">
    <div class="col-md-4">
    	<div class="pull-left">
    		Printed By: <?= Yii::$app->user->identity->person->surname.' '.Yii::$app->user->identity->person->first_name ?><br>
    		Printed Date: <?= date('Y-m-d H:i:s') ?>
    	</div>
    </div>
    <div class="col-md-4"><center>DBI GUEST HOUSE<br>COMPANY WISE ROOM REPORT</center></div>
    <div class="col-md-4">
    	<div class="pull-right">Page 1</div></div>
    	</div>
    	<br>
    	<br>
    	<hr>
    	Month As On <?= date('M Y'); ?>
<hr>
<table class="table table-striped">
                      <thead>
                        <tr>
                          <th>Guest</th>
                          <th>Arvl.</th>
                          <th>Dept.</th>
                          <th>Nights</th>
                          <th>Rm No.</th>
                          <th>Rm Type</th>
                          <th>Tariff</th>
                          <th>Total</th>
                          <th>Ref. No</th>
                        </tr>
                      </thead>
                      <tbody>
              <?php foreach($companyWiseRoomReport as $k => $v){?>
                        <tr>
              <td><?php echo  $v['person']['surname'].' '. $v['person']['first_name'];?></td>
                          <td><?php echo  $v['time_in'];?></td>
                          <td><?php echo  $v['time_out'];?></td>
                          <td><?php $date1 = strtotime($v['time_out']); $date2 = strtotime($v['time_in']);
                          echo floor(($date1-$date2)/(60 * 60 * 24));
// $timeDiff = abs($date1 - $date2);
// echo ceil($timeDiff / (1000 * 3600 * 24));  ?></td> 
                           <td><?php echo  $v['room']['room_number'];?></td>
                           <td><?php echo  $v['room_id']['room_type']['room_type_id'];?></td>
                          <td><?php echo  $v['tariff_id'];?></td>
                          <td><?php echo  'N'.$v['amount_paid'];?></td>
              <td><?php echo  $v['status']['status_title'];?></td>
                        </tr>
                            <?php }?>
                      </tbody>
                    </table>

</div>
</div>
</div>
</div>