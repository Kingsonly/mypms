<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Reservations */

$this->title = 'Room Availability Report';
?>
<div class="">
     
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel" style="border-width:10px; border-style:ridge">
                  <div class="x_content">
<br>
<div class="col-md-12">
    <div class="col-md-4">
    	<div class="pull-left">
    		Printed By: <?= Yii::$app->user->identity->person->surname.' '.Yii::$app->user->identity->person->first_name ?><br>
    		Printed Date: <?= date('Y-m-d H:i:s') ?>
    	</div>
    </div>
    <div class="col-md-4"><center>DBI GUEST HOUSE<br>ROOM AVAILABILITY REPORT: <br>Room Status Report</center></div>
    <div class="col-md-4">
    	<div class="pull-right">Page 1</div></div>
    	</div>
    	<br>
    	<br>
    	<hr>
    	Date: <?= date('Y-m-d'); ?>
<hr>
<br>
                      <h4><strong>Occupied Rooms</strong></h4>
                      
              <?php foreach($occupied as $k => $v){?>
              <div class="col-md-1">
              <?php echo  $v['room_number'];?></div>
                            <?php }?>
<br>
<hr><hr>
                    <h4><strong>Out Of Order Rooms</strong></h4>

              <?php foreach($outOfOrder as $k => $v){?>
              <div class="col-md-1">
              <?php echo  $v['room_number'];?></div>
                            <?php }?>

                      <br>
<hr><hr>
                   <h4><strong>Vacant But Dirty Rooms</strong></h4>
              <?php foreach($vacantButDirty as $k => $v){?>
              <div class="col-md-1">
              <?php echo  $v['room_number'];?></div>
                            <?php }?>

                      <br>
<hr><hr>
                    <h4><strong>Unoccupied Rooms</strong></h4>
              <?php foreach($unoccupied as $k => $v){?>
              <div class="col-md-1">
              <?php echo  $v['room_number'];?></div>
                            <?php }?>
<br>
<hr>
<hr><h4><strong>Out Of Service Rooms</strong>
</h4>
              <?php foreach($outOfService as $k => $v){?>
              <div class="col-md-1">
              <?php echo  $v['room_number'];?></div>
                            <?php }?>

</div>
</div>
</div>
</div>