<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RoomBooking */

$this->title = 'Unbook Room: ' . $model->booking_id;
?>
<div class="room-booking-unbook">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
