<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RoomBooking */

$this->title = 'Create Room Booking';
$this->params['breadcrumbs'][] = ['label' => 'Room Bookings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="room-booking-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'personModel' => $personModel,
        'emailModel' => $emailModel,
        'telephoneModel' => $telephoneModel,
        'addressModel' => $addressModel,
        'statesModel' => $statesModel,
        'countriesModel' => $countriesModel,
        'statusTypeModel' => $statusTypeModel,
        'guestModel' => $guestModel,
    ]) ?>

</div>
