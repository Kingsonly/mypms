<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RoomBooking */

$this->title = 'Update Room Booking: ' . $model->booking_id;
$this->params['breadcrumbs'][] = ['label' => 'Room Bookings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->booking_id, 'url' => ['view', 'id' => $model->booking_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="room-booking-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
