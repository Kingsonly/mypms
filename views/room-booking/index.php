<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\StatusType;
use kartik\datetime\DateTimePicker;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Room Booking';
$this->params['breadcrumbs'][] = $this->title;
?>
  <div class="">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
    <h3><i class="fa fa-calendar"></i> <?= Html::encode($this->title) ?></h3>
                    <div class="clearfix"></div>
    </div>
                  <div class="x_content">
<div class="room-booking-index">

  <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>

							<th>SN</th>
                          	<th>Guest Name</th>
							<th>Room</th>
                          	<th>Amount Paid</th>
                          	<th>Check-in</th>
                          	<th>Check-out</th>
                          	<th>Status</th>
	  						<th>Action</th>

                        </tr>
                      </thead>
                      <tbody>
              <?php $i=1; foreach($model as $k => $v){?>

                        <tr>

                          </td>

                          	<td><?php echo  $i;?></td>
							<td><?php echo  $v['person']['surname'].' '. $v['person']['first_name'];?></td>
							<td><?php echo  $v['room']['room_number'];?></td>
							<td><?php echo  $v['amount_paid'];?></td>
							<td><?php echo  $v['time_in'];?></td>
							<td><?php echo  $v['time_out'];?></td>
							<td><?php echo  $v['status']['status_title'];?></td>
	  						<td><i class="fa fa-eye hvr-bounce-in"></i>&nbsp;&nbsp;<i class="fa fa-pencil hvr-bounce-in"></i>
                        </tr>
                            <?php $i++; }?>
                            </tbody>
                            </table>
</div>
</div>
</div>
</div>
</div>
