<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\StatusType;
use kartik\depdrop\DepDrop;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\RoomBooking */
/* @var $form yii\widgets\ActiveForm */

$id = $_GET['id'];
?>

<div class="room-booking-form">

    <?php $form = ActiveForm::begin(); ?>
	

    <?= $form->field($personModel, 'surname')->textInput(['maxlength' => true]) ?>
	
	<?= $form->field($personModel, 'first_name')->textInput(['maxlength' => true]) ?>

	
	<?= $form->field($emailModel, 'address')->textInput(['maxlength' => true]) ?>
	
	<?= $form->field($telephoneModel, 'number')->textInput(['maxlength' => true]) ?>
	
	<?= $form->field($addressModel, 'address')->textInput(['maxlength' => true]) ?>
	
	<?= $form->field($addressModel, 'country_id')->dropDownList(ArrayHelper::map($countriesModel->find()->all(), 'country_id', 'country_name'), ['id'=>'countryId']);?>


	<?= $form->field($addressModel, 'state_id')->widget(DepDrop::classname(), [
		 'options' => ['id'=>'stateId'],
		 'pluginOptions'=>[
			 'depends'=>['countryId'],
			 'placeholder' => 'Select...',
			 'url' => Url::to(['/site/state'])
		 ]
	 ]);
	?>
	

    <?= $form->field($model, 'room_id')->hiddenInput(['value'=>$id])->label(false) ?>

	
    <?= $form->field($model, 'amount_paid')->textInput(['type' => 'number']) ?>
	
	<?= $form->field($model, 'time_in')->widget(DatePicker::classname(), [
							'options' => ['placeholder' => 'Check-in','id' => 'timein'],
							 'pluginOptions' => [
								 'format' => 'yyyy-mm-dd',
								 'todayHighlight' => true
									],
								
								]) 
		?>
	<?= $form->field($model, 'time_out')->widget(DatePicker::classname(), [
							'options' => ['placeholder' => 'Check-out','id' => 'timeout'],
							 'pluginOptions' => [
								'format' => 'yyyy-mm-dd',
								 'todayHighlight' => true
								 
									],
								
								]) 
		?>
	
	

    <?= $form->field($guestModel, 'status_id')->dropdownList(ArrayHelper::map($statusTypeModel->find()->all(), 'status_id', 'status_title'), array('style' => 'display:none'));?>

    <?=$form->field($model, 'status_id')->label(false)->dropdownList(ArrayHelper::map($statusTypeModel->find()->all(), 'status_id', 'status_title'));?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-danger' : 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
