<?php

/* @var $this yii\web\View */

$this->title = 'Index';
?>
 <!--Page Body-->
      <div id="page-body" class="page-body">
          <!-- Services-->
        <section id="service" class="page-section one-child">
          <div class="container">
            <div class="group-icon-box-border-container">
              <div class="__container-inner clearfix">
                <div class="__border-item">
                  <div data-wow-delay="0.3s" class="block-icon-box-vertical wow fadeInUp">
                    <div class="__icon simple-icon icon-genius"></div>
                    <h4 class="__heading smb">BOOTSTRAP 3 GRID SYSTEM</h4>
                    <p class="__caption font-serif italic">Lorem ipsum dolor sit amet</p>
                    <p class="__content">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam</p>
                  </div>
                </div>
                <div class="__border-item">
                  <div data-wow-delay="0.3s" class="block-icon-box-vertical wow fadeInUp">
                    <div class="__icon simple-icon icon-trophy"></div>
                    <h4 class="__heading smb">HTML5 &amp; CSS3 ANIMATION</h4>
                    <p class="__caption font-serif italic">Lorem ipsum dolor sit amet</p>
                    <p class="__content">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam</p>
                  </div>
                </div>
                <div class="__border-item">
                  <div data-wow-delay="0.3s" class="block-icon-box-vertical wow fadeInUp">
                    <div class="__icon simple-icon icon-layers"></div>
                    <h4 class="__heading smb">AUTOMATE YOUR WORKFLOW</h4>
                    <p class="__caption font-serif italic">Lorem ipsum dolor sit amet</p>
                    <p class="__content">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam</p>
                  </div>
                </div>
                <div class="__border-item">
                  <div data-wow-delay="0.3s" class="block-icon-box-vertical wow fadeInUp">
                    <div class="__icon simple-icon icon-edit"></div>
                    <h4 class="__heading smb">NODE TEMPLATE ENGINE</h4>
                    <p class="__caption font-serif italic">Lorem ipsum dolor sit amet</p>
                    <p class="__content">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam</p>
                  </div>
                </div>
                <div class="__border-item">
                  <div data-wow-delay="0.3s" class="block-icon-box-vertical wow fadeInUp">
                    <div class="__icon simple-icon icon-traget"></div>
                    <h4 class="__heading smb">LESS PRE-PROCESSOR</h4>
                    <p class="__caption font-serif italic">Lorem ipsum dolor sit amet</p>
                    <p class="__content">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam</p>
                  </div>
                </div>
                <div class="__border-item">
                  <div data-wow-delay="0.3s" class="block-icon-box-vertical wow fadeInUp">
                    <div class="__icon simple-icon icon-mobile"></div>
                    <h4 class="__heading smb">FULLY RESPONSIVE</h4>
                    <p class="__caption font-serif italic">Lorem ipsum dolor sit amet</p>
                    <p class="__content">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- End Services-->
        <!-- Call To Action-->
        <section class="page-section no-padding">
          <div class="call-to-action-parallax parallax-wrapper text-center bgc-dark-o-6">
            <div data-parallax="scroll" data-position="center -550px" data-image-src="assets/images/background/background-13.jpg" data-speed="0.3" class="parallax-background"></div>
            <div class="container">
              <h1 class="size-ll text-responsive color-light mb-15">PRICE IS WHAT YOU PAY <br/> VALUE IS WHAT YOU GET</h1>
              <p class="font-serif-italic fz-3 color-light mb-55">We will work with you to fully understand your business and your target</p>
              <div class="__button"><a href="index.html" class="btn btn-primary fullwidth wow fadeInLeft">SELECT DEMO</a><br class="visible-xs-block"/><a href="http://goo.gl/fi9Eas" target="_blank" class="btn btn-secondary fullwidth wow fadeInRight">PURCHASE NOW</a></div>
            </div>
          </div>
        </section>
        <!-- End Call To Action--> 
          <!-- Shop-->
        <section id="shop" class="page-section bgc-light">
          <div class="container">
            <div class="row">
              <div class="col-xs-12 text-center">
                <header class="hr-header section-header">
                  <h2 class="smb">SHOPPING</h2>
                  <p class="common-serif __caption">Ecommerce ready with Megatron</p>
                  <div class="separator-2-color"></div>
                  <p class="__content">Create a online store in a matter of minutes & start selling with Megatron today! The theme also includes a few custom Modules, custom sidebars for your store and product pages, a capability for adding a slider above your store.</p>
                </header>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3 col-sm-6 col-xs-12 shop-product-wrapper">
                <div class="block-shop-product font-heading">
                  <div class="__image overlay-container"><img src="assets/images/shop/product-img-1-m.jpg" alt="Shop Product"/>
                    <div class="overlay text-center">
                      <div class="__layer bgc-dark-o-3"></div>
                      <ul>
                        <li class="clearfix"><a href="#" class="compare"><i class="icon-185061-photo-pictures-streamline"></i></a><a href="#shop-quickview" data-modal-open="data-modal-open" class="quick-view"><i class="icon-search-icon"></i></a><a href="shop-cart.html" class="add-to-cart"><i class="icon-shopping111"></i></a><a href="#" class="wish-list"><i class="icon-185083-like-love-streamline"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="__info text-center"><a href="#">FITCH ZIP THROUGH HOODIE</a>
                    <div class="__price"><span>$200</span>
                    </div>
                    <div data-rating="4" class="star-ratings"><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span></div>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12 shop-product-wrapper">
                <div class="block-shop-product font-heading">
                  <div class="__image overlay-container"><span class="status sale-off">SALE</span><img src="assets/images/shop/product-img-2-m.jpg" alt="Shop Product"/>
                    <div class="overlay text-center">
                      <div class="__layer bgc-dark-o-3"></div>
                      <ul>
                        <li class="clearfix"><a href="#" class="compare"><i class="icon-185061-photo-pictures-streamline"></i></a><a href="#shop-quickview" data-modal-open="data-modal-open" class="quick-view"><i class="icon-search-icon"></i></a><a href="shop-cart.html" class="add-to-cart"><i class="icon-shopping111"></i></a><a href="#" class="wish-list"><i class="icon-185083-like-love-streamline"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="__info text-center"><a href="#">FITCH ZIP THROUGH HOODIE</a>
                    <div class="__price">
                      <del>$250</del><span>$230</span>
                    </div>
                    <div data-rating="4" class="star-ratings"><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span></div>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12 shop-product-wrapper">
                <div class="block-shop-product font-heading">
                  <div class="__image overlay-container"><span class="status new">NEW</span><img src="assets/images/shop/product-img-3-m.jpg" alt="Shop Product"/>
                    <div class="overlay text-center">
                      <div class="__layer bgc-dark-o-3"></div>
                      <ul>
                        <li class="clearfix"><a href="#" class="compare"><i class="icon-185061-photo-pictures-streamline"></i></a><a href="#shop-quickview" data-modal-open="data-modal-open" class="quick-view"><i class="icon-search-icon"></i></a><a href="shop-cart.html" class="add-to-cart"><i class="icon-shopping111"></i></a><a href="#" class="wish-list"><i class="icon-185083-like-love-streamline"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="__info text-center"><a href="#">FITCH ZIP THROUGH HOODIE</a>
                    <div class="__price"><span>$200</span>
                    </div>
                    <div data-rating="4" class="star-ratings"><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span></div>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12 shop-product-wrapper">
                <div class="block-shop-product font-heading">
                  <div class="__image overlay-container"><span class="status sale-off">SALE</span><img src="assets/images/shop/product-img-4-m.jpg" alt="Shop Product"/>
                    <div class="overlay text-center">
                      <div class="__layer bgc-dark-o-3"></div>
                      <ul>
                        <li class="clearfix"><a href="#" class="compare"><i class="icon-185061-photo-pictures-streamline"></i></a><a href="#shop-quickview" data-modal-open="data-modal-open" class="quick-view"><i class="icon-search-icon"></i></a><a href="shop-cart.html" class="add-to-cart"><i class="icon-shopping111"></i></a><a href="#" class="wish-list"><i class="icon-185083-like-love-streamline"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="__info text-center"><a href="#">FITCH ZIP THROUGH HOODIE</a>
                    <div class="__price">
                      <del>$240</del><span>$210</span>
                    </div>
                    <div data-rating="4" class="star-ratings"><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- End Shop-->
           <!-- Portfolio-->
       
        <section class="page-section bgc-light one-child">
          <div class="group-portfolio isotope-container">
            <div class="isotope-grid remove-gutter container">
              <div class="grid-sizer col-md-4 col-sm-6"></div>
              <div class="grid-item col-md-4 col-sm-6 content-marketing graphic-design seo-optimization">
                <div class="block-portfolio overlay-container basic"><img src="assets/images/portfolio/portfolio-6.jpg" alt="BY THE SEA " class="__image"/>
                  <div class="overlay bgc-dark-o-7">
                    <div class="cell-vertical-wrapper">
                      <div class="cell-middle">
                        <h4 class="__title"><a href="#">BY THE SEA </a></h4>
                        <div class="__caption font-serif-italic">Lorem ipsum dolor sit amet</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="grid-item col-md-8 col-sm-12 graphic-design social-marketing web-design">
                <div class="block-portfolio overlay-container basic"><img src="assets/images/portfolio/portfolio-h-2.jpg" alt="BY THE SEA " class="__image"/>
                  <div class="overlay bgc-dark-o-7">
                    <div class="cell-vertical-wrapper">
                      <div class="cell-middle">
                        <h4 class="__title"><a href="#">BY THE SEA </a></h4>
                        <div class="__caption font-serif-italic">Lorem ipsum dolor sit amet</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="grid-item col-md-4 col-sm-6 web-design branding content-marketing">
                <div class="block-portfolio overlay-container basic"><img src="assets/images/portfolio/portfolio-v-1.jpg" alt="BY THE SEA " class="__image"/>
                  <div class="overlay bgc-dark-o-7">
                    <div class="cell-vertical-wrapper">
                      <div class="cell-middle">
                        <h4 class="__title"><a href="#">BY THE SEA </a></h4>
                        <div class="__caption font-serif-italic">Lorem ipsum dolor sit amet</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="grid-item col-md-4 col-sm-6 branding seo-optimization graphic-design">
                <div class="block-portfolio overlay-container basic"><img src="assets/images/portfolio/portfolio-3.jpg" alt="BY THE SEA " class="__image"/>
                  <div class="overlay bgc-dark-o-7">
                    <div class="cell-vertical-wrapper">
                      <div class="cell-middle">
                        <h4 class="__title"><a href="#">BY THE SEA </a></h4>
                        <div class="__caption font-serif-italic">Lorem ipsum dolor sit amet</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="grid-item col-md-4 col-sm-6 seo-optimization web-design social-marketing">
                <div class="block-portfolio overlay-container basic"><img src="assets/images/portfolio/portfolio-5.jpg" alt="BY THE SEA " class="__image"/>
                  <div class="overlay bgc-dark-o-7">
                    <div class="cell-vertical-wrapper">
                      <div class="cell-middle">
                        <h4 class="__title"><a href="#">BY THE SEA </a></h4>
                        <div class="__caption font-serif-italic">Lorem ipsum dolor sit amet</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="grid-item col-md-8 col-sm-12 web-design branding content-marketing">
                <div class="block-portfolio overlay-container basic"><img src="assets/images/portfolio/portfolio-h-1.jpg" alt="BY THE SEA " class="__image"/>
                  <div class="overlay bgc-dark-o-7">
                    <div class="cell-vertical-wrapper">
                      <div class="cell-middle">
                        <h4 class="__title"><a href="#">BY THE SEA </a></h4>
                        <div class="__caption font-serif-italic">Lorem ipsum dolor sit amet</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- End Portfolio-->
      </div>