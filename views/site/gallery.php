      <?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
$this->title = 'Gallery';
?>
  <!--Page Header-->
      <header class="page-header common-header bgc-dark-o-5">
        <div data-parallax="scroll" data-image-src="assets/images/lawyer/lawyer1_bg.jpg" data-speed="0.3" class="parallax-background"></div>
        <div class="container text-center cell-vertical-wrapper">
          <div class="cell-middle">
            <h1 class="text-responsive size-l nmb">OUR GALLERY</h1>
            <p class="nmb common-serif text-responsive">Beautifully suited for all your web-based needs</p>
          </div>
        </div>
        <div class="ab-bottom col-xs-12">
          <div class="container">
            <div class="breadcrumb bgc-dark-o-6"><span class="__title font-heading fz-6-s">YOU ARE HERE :</span><span class="__content italic font-serif fz-6-ss"><span><?= Html::a('Home', ['site/index'], ['class' => 'active']) ?></span><span><a>Our Gallery</a></span></span></div>
          </div>
        </div>
      </header>
      <!--End Page Header-->
      <!--Page Body-->
      <div id="page-body" class="page-body">
        <!-- Gallery-->
        <section class="page-section">
          <div class="container">
            <div class="row magnific-gallery">
              <div class="col-md-4 col-sm-6 col-xs-12 pb-30">
                <div data-mfp-src="assets/images/lawyer/portfolio/portfolio-1.jpg" class="block-image-zoom overlay-container img-wrapper zoom-button"><img src="assets/images/lawyer/portfolio/portfolio-1.jpg" alt="image"/>
                  <div class="overlay-hover bgc-dark-o-6">
                    <div class="__icon"><i class="icon-resize-full"></i></div>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12 pb-30">
                <div data-mfp-src="assets/images/lawyer/portfolio/portfolio-2.jpg" class="block-image-zoom overlay-container img-wrapper zoom-button"><img src="assets/images/lawyer/portfolio/portfolio-2.jpg" alt="image"/>
                  <div class="overlay-hover bgc-dark-o-6">
                    <div class="__icon"><i class="icon-resize-full"></i></div>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12 pb-30">
                <div data-mfp-src="assets/images/lawyer/portfolio/portfolio-3.jpg" class="block-image-zoom overlay-container img-wrapper zoom-button"><img src="assets/images/lawyer/portfolio/portfolio-3.jpg" alt="image"/>
                  <div class="overlay-hover bgc-dark-o-6">
                    <div class="__icon"><i class="icon-resize-full"></i></div>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12 pb-30">
                <div data-mfp-src="assets/images/lawyer/portfolio/portfolio-4.jpg" class="block-image-zoom overlay-container img-wrapper zoom-button"><img src="assets/images/lawyer/portfolio/portfolio-4.jpg" alt="image"/>
                  <div class="overlay-hover bgc-dark-o-6">
                    <div class="__icon"><i class="icon-resize-full"></i></div>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12 pb-30">
                <div data-mfp-src="assets/images/lawyer/portfolio/portfolio-5.jpg" class="block-image-zoom overlay-container img-wrapper zoom-button"><img src="assets/images/lawyer/portfolio/portfolio-5.jpg" alt="image"/>
                  <div class="overlay-hover bgc-dark-o-6">
                    <div class="__icon"><i class="icon-resize-full"></i></div>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12 pb-30">
                <div data-mfp-src="assets/images/lawyer/portfolio/portfolio-6.jpg" class="block-image-zoom overlay-container img-wrapper zoom-button"><img src="assets/images/lawyer/portfolio/portfolio-6.jpg" alt="image"/>
                  <div class="overlay-hover bgc-dark-o-6">
                    <div class="__icon"><i class="icon-resize-full"></i></div>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12 pb-30">
                <div data-mfp-src="assets/images/lawyer/portfolio/portfolio-7.jpg" class="block-image-zoom overlay-container img-wrapper zoom-button"><img src="assets/images/lawyer/portfolio/portfolio-7.jpg" alt="image"/>
                  <div class="overlay-hover bgc-dark-o-6">
                    <div class="__icon"><i class="icon-resize-full"></i></div>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12 pb-30">
                <div data-mfp-src="assets/images/lawyer/portfolio/portfolio-8.jpg" class="block-image-zoom overlay-container img-wrapper zoom-button"><img src="assets/images/lawyer/portfolio/portfolio-8.jpg" alt="image"/>
                  <div class="overlay-hover bgc-dark-o-6">
                    <div class="__icon"><i class="icon-resize-full"></i></div>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-md-offset-0 col-sm-offset-3 col-xs-12 section-block">
                <div data-mfp-src="assets/images/lawyer/portfolio/portfolio-9.jpg" class="block-image-zoom overlay-container img-wrapper zoom-button"><img src="assets/images/lawyer/portfolio/portfolio-9.jpg" alt="image"/>
                  <div class="overlay-hover bgc-dark-o-6">
                    <div class="__icon"><i class="icon-resize-full"></i></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- End Gallery-->
        <!-- Call To Action-->
        <section class="page-section no-padding">
          <div class="call-to-action-common bgc-secondary">
            <div class="container">
              <div class="__content-wrapper row">
                <div class="col-lg-10 col-md-9 col-xs-12 __content-left">
                  <div class="cell-vertical-wrapper">
                    <div class="cell-middle">
                      <p class="font-serif-italic mb-0 fz-3 color-light">Like what you see? Start now with this demo or check out our other demos to find what you need.</p>
                    </div>
                  </div>
                </div>
                <div data-wow-delay="0.3s" class="col-lg-2 col-md-3 col-xs-12 __content-right wow fadeInRight">
                  <div class="cell-vertical-wrapper">
                    <div class="cell-middle"><a href="index.html" class="fullwidth btn-border btn-light">CHECK IT OUT</a></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- End Call To Action-->
      </div>
      <!--End Page Body-->