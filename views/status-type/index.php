<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Status Types';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="status-type-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Status Type', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'status_id',
            'status_category',
            'status_title',
            'status_color',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
