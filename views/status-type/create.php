<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\StatusType */

$this->title = 'Create Status Type';
$this->params['breadcrumbs'][] = ['label' => 'Status Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="status-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
