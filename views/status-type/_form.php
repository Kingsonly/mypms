<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\StatusType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="status-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'status_category')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status_color')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
