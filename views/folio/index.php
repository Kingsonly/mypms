<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use  yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchGuest */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Folio';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="">
              <div class="col-md-12 col-sm-12 col-xs-12">
            
                <div class="x_panel">
                  <div class="x_title">
                  <h3><i class="fa fa-users"></i> 
    <?= Html::encode($this->title) ?></h3>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        
                      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#bookings" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Folio</a>
                        </li>
                        <li role="presentation" class=""><a href="#report" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Reports</a>
                        </li>
                      </ul>
                      <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="bookings" aria-labelledby="home-tab">
                      
<div class="guest-index">

                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>

							<th>SN</th>
                          	<th>Guest Name</th>
							<th>Room</th>
                          	<th>Amount Paid</th>
                          	
                        </tr>
                      </thead>
                      <tbody>
						  <? //var_dump($folio); return false;?>
						  <?php if(!empty($folio)){ ?>
           <?php $i=1; foreach($folio as $k => $v){?>

                        <tr>

                          	<td><?php echo  $i;?></td>
							<td><?php echo !empty($v['pmsFolioCredits'])?'#'.$v['pmsFolioCreditsSum']:0;?></td>
							<td><?php echo  !empty($v['pmsFolioDebits']['amount'])?count($v['pmsFolioDredits']['amount']):0;?></td>
							
							<td>
								
							</td>

                        </tr>
                            <?php $i++; }?>
						  <?php } ?>
						  
                            </tbody>
                            </table>
</div>
</div>

                        <div role="tabpanel" class="tab-pane fade" id="report" aria-labelledby="profile-tab">
                
                        <font size="+1">
<li><?php echo Html::a(' Arrival Departure & Stay Over Report', ['/room/arrival-departure-report']) ?></li>
<li><?php echo Html::a(' Guest Inhouse Report', ['/room/guest-inhouse-report']) ?></li>
<li><?php echo Html::a(' Guest Ledger Report', ['/room/guest-ledger-report']) ?></li>
                        <li><?php echo Html::a(' Guest Wise Room Report', ['/room/guest-wise-room-report']) ?></li>
<li><?php echo Html::a(' Assigned Room Report', ['/reservations/availability-chart']) ?></li>
<li><?php echo Html::a(' Group Arrival List', ['/reservations/availability-chart']) ?></li>
<li><?php echo Html::a(' Group In-house Report', ['/reservations/availability-chart']) ?></li>
<li><?php echo Html::a(' Group Outstanding Report', ['/reservations/availability-chart']) ?></li>
                        </font>
                        </div>
</div>
</div>
</div>
</div>
</div>
</div>
<?php
//this should be put directly inside the user index page (best practice should be the last thing in the page 
Modal::begin([
            'header' =>'<h1 id="modalheader"></h1>',
            'id' => 'guestmodal',
            'size' => 'modal-lg',  
        ]);
echo "
<div id='wait'><center><img src='../web/admin/images/ajax-loader.gif' /></center></div>";
        echo "<div id='guestmodalcontent'></div>";
        Modal::end();
    ?>