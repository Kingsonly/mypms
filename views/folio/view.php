<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;


?>
<div class="color-view">
<div>
	<?=$entity->people->personString; ?>
	<?=$entity->email->address; ?>
	
	<button class="pull-right btn btn-danger booking" data-url="<?php echo Url::to(['create']); ?>" >Credit Account</button>
	<button class="pull-right btn btn-danger booking" data-url="<?php echo Url::to(['create']); ?>" >Close Account</button>
	<button class="pull-right btn btn-danger booking" data-url="<?php echo Url::to(['create']); ?>" >Send to back office</button>

	</div>
    
    <table class="table table-bordered table-condensed" >
		
		
		<tbody>
			<tr>
		
			<td>
				<table class="table table-striped">
					<thead>

						<th>Date</th>
						<th>Text</th>
						<th>Amount</th>
					</thead>
				<? foreach($debit as $k => $v){ ?>
					<tr>
						<td>
							<?=$v['date_posted'];?>
						</td>
						
						<td>
							<?=$v['senario_type'];?>
						</td>
						
						<td>
							<?=$v['amount'].'('.$v['senario_id'].')' ;?>
						</td>
					</tr>
				<? } ?>
				</table>
			</td>
			<td>
				<table class="table table-striped">
					<thead>

						<th>Date</th>
						<th>Text</th>
						<th>Amount</th>
					</thead>
		
				<? foreach($credit as $k => $v){ ?>
					<tr>
						<td>
							<?=$v['date_posted'];?>
						</td>
						
						<td>
							<?=$v['senario_type'];?>
						</td>
						
						<td>
							<?=$v['amount'].'('.$v['senario_id'].')' ;?>
						</td>
					</tr>
				<? } ?>
				</table>
			</td>
		   </tr>
			<tr>
				<td>Total: <?=$debitSum; ?></td>
				<td>Total: <?=($creditSum != $debitSum and $creditSum < $debitSum )?'('.$creditSum.')':$creditSum; ?></td>
			</tr>
		</tbody>
	</table>

	 
</div>
