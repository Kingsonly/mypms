<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RestaurantPrice */

$this->title = 'Create Restaurant Price';
$this->params['breadcrumbs'][] = ['label' => 'Restaurant Prices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="restaurant-price-create">
        <?= Html::a('Back', ['/restaurant-booking'], ['class' => 'btn btn-danger pull-right']) ?>

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
