<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RestaurantPrice */

$this->title = $model->price_id;
$this->params['breadcrumbs'][] = ['label' => 'Restaurant Prices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="restaurant-price-view">

        <?= Html::a('Back', ['restaurant-booking/index'], ['class' => 'btn btn-danger pull-right']) ?>
    <h3>Product Price: <?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->price_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->price_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'price_id',
            'product_price',
        ],
    ]) ?>

</div>
