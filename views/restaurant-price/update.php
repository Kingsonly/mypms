<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RestaurantPrice */

$this->title = 'Update Restaurant Price: ' . $model->price_id;
$this->params['breadcrumbs'][] = ['label' => 'Restaurant Prices', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->price_id, 'url' => ['view', 'id' => $model->price_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="restaurant-price-update">

        <?= Html::a('Back', ['restaurant-booking/index'], ['class' => 'btn btn-danger pull-right']) ?>
    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
