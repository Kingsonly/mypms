<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use  yii\bootstrap\Modal;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Point Of Sales';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="restaurant-price-index">

    <p>
        <?php echo Html::a('Add New Price', ['/restaurant-price/create'], ['class' => 'btn btn-danger']) ?>
    </p>
                    <table id="datatable-fixed-header" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Price ID</th>
                          <th>Product Price</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
              <?php foreach($restaurantPriceModel as $k => $v){?>

                        <tr>
                          <td><?php echo  $v['price_id'];?></td>
                          <td><?php echo  $v['product_price'];?></td>
                          <td><?php echo Html::a(' <i class="fa fa-eye hvr-bounce-in"></i>', ['/restaurant-price/view', 'id' => $v['price_id']]) ?>
                         &nbsp;&nbsp;<?php echo Html::a(' <i class="fa fa-pencil hvr-bounce-in"></i>', ['/restaurant-price/update', 'id' => $v['price_id']]) ?></td>
                        </tr>
                            <?php }?>
                            </tbody>
                            </table>
</div>
<?php
//this should be put directly inside the user index page (best practice should be the last thing in the page 
Modal::begin([
            'header' =>'<h1 id="modalheader"></h1>',
            'id' => 'roommodal',
            'size' => 'modal-lg',  
        ]);
echo "
<div id='wait'><center><img src='../web/admin/images/ajax-loader.gif' /></center></div>";

        echo "<div id='roommodalcontent'></div>";
        Modal::end();
    ?>
