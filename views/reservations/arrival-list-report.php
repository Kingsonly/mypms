<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Reservations */

$this->title = 'Arrival List Report';
?>
<div class="">
     
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel" style="border-width:10px; border-style:ridge">
                  <div class="x_content">
<br>
<div class="col-md-12">
    <div class="col-md-4">
    	<div class="pull-left">
    		Printed By: <?= Yii::$app->user->identity->person->surname.' '.Yii::$app->user->identity->person->first_name ?><br>
    		Printed Date: <?= date('Y-m-d H:i:s') ?>
    	</div>
    </div>
    <div class="col-md-4"><center>DBI GUEST HOUSE<br>ARRIVAL LIST REPORT</center></div>
    <div class="col-md-4">
    	<div class="pull-right">Page 1</div></div>
    	</div>
    	<br>
    	<br>
    	<hr>
    	Date From <!-- <?= $_GET['startDate'] ?> --> To <!-- <?= $_GET['endDate'] ?> -->
<hr>
<table class="table table-striped">
                      <thead>
                        <tr>
                          <th>Rm. No.</th>
                          <th>Rm. Type</th>
                          <th>Rate</th>
                          <th>Arvl. Date</th>
                          <th>Dept. Date</th>
                          <th>Bkg. ID</th>
                          <th>Nights</th>
                          <th>Admin</th>
                          <th>Entr On</th>
                          <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
              <?php foreach($arrivalListModel as $k => $v){?>
                        <tr>
                          <th scope="row"><?php echo  $v['room']['room_number'];?></th>
                          <td><?php echo  $v['room_type']['type'];?></td>
                          <td><?php echo  $v['tariff']['tariff'];?></td>
                          <td><?php echo  $v['time_in'];?></td>
                          <td><?php echo  $v['time_out'];?></td>
                          <td><?php echo  $v['booking_id'];?></td>
                          <td><?php $date1 = strtotime($v['time_out']); $date2 = strtotime($v['time_in']);
                          echo floor(($date1-$date2)/(60 * 60 * 24));  ?></td> 
                          <td><?php echo  $v['user_id'];?></td>
                          <td><?php echo  $v['datetime'];?></td>
                          <td><?php echo  $v['status']['status_title'];?></td>
                        </tr>
                            <?php }?>
                      </tbody>
                    </table>

</div>
</div>
</div>
</div>