<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Button ;
use yii\bootstrap\Modal;
use scotthuangzl\googlechart\GoogleChart;
use yii\widgets\ActiveForm;
use app\models\StatusType;
use yii\web\View;
    
use kartik\datetime\DateTimePicker;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reservations';
$this->params['breadcrumbs'][] = $this->title;
?>
 <div class="">
	 
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
     <button class="pull-right btn btn-danger reservemodalclick" id="" data-url="<?php echo Url::to(['create']); ?>" >Create</button>
                    <h2><i class="fa fa-building"></i> <?= Html::encode($this->title) ?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
 <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        
                      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#bookings" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Bookings</a>
                        </li>
                        <li role="presentation" class=""><a href="#report" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Reports</a>
                        </li>
                        <li role="presentation" class=""><a href="#statistics" role="tab" id="profile-tab3" data-toggle="tab" aria-expanded="false">Statistics</a>
                        </li>
                      </ul>
                      <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="bookings" aria-labelledby="home-tab">
                       
<div class="reservations-index">

  <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Booking ID</th>
                          <th>Room No.</th>
                          <th>Guest</th>
                          <th>Tariff</th>
                          <th>Time In</th>
                          <th>Time Out</th>
                          <th>Amount Paid</th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
              <?php foreach($viewModel as $k => $v){?>

                        <tr>
                          <td><?php echo  $v['booking_id'];?></td>
              <td><?php echo  $v['room']['room_number'];?></td>
              <td><?php echo  $v['person']['surname'].' '. $v['person']['first_name'];?></td>
                          <td><?php echo  $v['tariff_id'];?></td>
                          <td><?php echo  $v['time_in'];?></td>
                          <td><?php echo  $v['time_out'];?></td>
                          <td><?php echo  $v['amount_paid'];?></td>
              <td><?php echo  $v['status']['status_title'];?></td>
                          <td><?php echo Html::a(' <i class="fa fa-eye hvr-bounce-in"></i>', ['room-booking/view', 'id' => $v['booking_id']], ['class' => 'reservemodalclick']) ?></td>
                        </tr>
                            <?php }?>
                            </tbody>
                            </table>

</div>
</div>

                        <div role="tabpanel" class="tab-pane fade" id="report" aria-labelledby="profile-tab"> 
                        <div class="col-md-12">
                        <div class='col-sm-6'>
                    Start Date
                          <div class="control-group">
                            <div class="controls">
                              <div class="col-md-11 xdisplay_inputx form-group has-feedback">
                                <input type="text" class="form-control has-feedback-left" id="single_cal3" placeholder="First Name" aria-describedby="inputSuccess2Status3">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="inputSuccess2Status3" class="sr-only">(success)</span>
                              </div>
                            </div>
                          </div>
                </div>
                <div class='col-sm-6'>
                    End Date
                          <div class="control-group">
                            <div class="controls">
                              <div class="col-md-11 xdisplay_inputx form-group has-feedback">
                                <input type="text" class="form-control has-feedback-left" id="single_cal4" placeholder="First Name" aria-describedby="inputSuccess2Status4">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="inputSuccess2Status4" class="sr-only">(success)</span>
                              </div>
                            </div>
                          </div>
                </div>
                <input type="hidden" value="3" id="test" name="test">
                
                        <font size="+1">
                        <?php echo $this->registerJs(
    "var startDate = document.getElementById('test').value;
    alert(startDate); return startDate;",
    View::POS_READY,
    'my-button-handler'
);?>
                        <li><?php echo Html::a(' Arrival List Report', ['arrival-list-report', 'startDate' => $this->registerJs("document.getElementById(\'single_cal3\');",
    View::POS_READY)]) ?></li>
                        <li><?php echo Html::a(' Availability Chart', ['availability-chart']) ?></li>
                        <li><?php echo Html::a(' Monthly Inventory Report', ['monthly-inventory-report']) ?></li>
                        <li><?php echo Html::a(' Reservation Report', ['reservation-report']) ?></li>
                        </font>
                        </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="statistics" aria-labelledby="profile-tab"> 
              <div class="col-md-6">
              <?= GoogleChart::widget(array('visualization' => 'ColumnChart',
                'data' => array(
                    array('Reservations', 'Reservations Per Month'),
                    array('Jan', (int)$totalJanRes),
                    array('', 0),
                    array('Feb', (int)$totalFebRes),
                    array('', 0),
                    array('Mar', (int)$totalMarRes),
                    array('', 0),
                    array('Apr', (int)$totalAprRes),
                    array('', 0),
                    array('May', (int)$totalMayRes),
                    array('', 0),
                    array('June', (int)$totalJunRes),
                    array('', 0),
                    array('July', (int)$totalJulRes),
                    array('', 0),
                    array('Aug', (int)$totalAugRes),
                    array('', 0),
                    array('Sep', (int)$totalSepRes),
                    array('', 0),
                    array('Oct', (int)$totalOctRes),
                    array('', 0),
                    array('Nov', (int)$totalNovRes),
                    array('', 0),
                    array('Dec', (int)$totalDecRes)
                ),
                'options' => array('title' => 'Yearly Reservations (Bar Chart)', 'width' => '500', 'height' => '400', 'class' => 'pull-right'))); ?>
                </div>
                <div class="col-md-6"> 
              <?=GoogleChart::widget(array('visualization' => 'PieChart',
                'data' => array(
                    array('Reservations', 'Reservations Per Month'),
                    array('January', (int)$totalJanRes),
                    array('February', (int)$totalFebRes),
                    array('March', (int)$totalMarRes),
                    array('April', (int)$totalAprRes),
                    array('May', (int)$totalMayRes),
                    array('June', (int)$totalJunRes),
                    array('July', (int)$totalJulRes),
                    array('August', (int)$totalAugRes),
                    array('Sepember', (int)$totalSepRes),
                    array('October', (int)$totalOctRes),
                    array('November', (int)$totalNovRes),
                    array('December', (int)$totalDecRes)
                ),
                'options' => array('title' => 'Yearly Reservations (Pie Chart)',  'height' => '400')));
                ?>
                </div>
                <div class="col-md-12">
                        <?php echo Html::a(' JANUARY', ['reservations', 'month' => 'JANUARY'], ['class' => 'btn btn-danger']) ?>
                        
                        <?php echo Html::a(' FEBRUARY', ['reservations', 'month' => 'FEBRUARY'], ['class' => 'btn btn-warning']) ?>
                       
                        <?php echo Html::a(' MARCH', ['reservations', 'month' => 'MARCH'], ['class' => 'btn btn-danger']) ?>
                       
                        <?php echo Html::a(' APRIL', ['reservations', 'month' => 'APRIL'], ['class' => 'btn btn-warning']) ?>
                       
                        <?php echo Html::a(' MAY', ['reservations', 'month' => 'MAY'], ['class' => 'btn btn-danger']) ?>
                       
                        <?php echo Html::a(' JUNE', ['reservations', 'month' => 'JUNE'], ['class' => 'btn btn-warning']) ?>
                        
                        <?php echo Html::a(' JULY', ['reservations', 'month' => 'JULY'], ['class' => 'btn btn-danger']) ?>

                        <?php echo Html::a(' AUGUST', ['reservations', 'month' => 'AUGUST'], ['class' => 'btn btn-warning']) ?>

                        <?php echo Html::a(' SEPTEMBER', ['reservations', 'month' => 'SEPTEMBER'], ['class' => 'btn btn-danger']) ?>

                        <?php echo Html::a(' OCTOBER', ['reservations', 'month' => 'OCTOBER'], ['class' => 'btn btn-warning']) ?>

                        <?php echo Html::a(' NOVEMBER', ['reservations', 'month' => 'NOVEMBER'], ['class' => 'btn btn-danger']) ?>

                        <?php echo Html::a(' DECEMBER', ['reservations', 'month' => 'DECEMBER'], ['class' => 'btn btn-warning']) ?>
                    </div>
                    </div>

</div>
</div>
</div>
</div>
</div>
</div>

<?php
//this should be put directly inside the user index page (best practice should be the last thing in the page 
Modal::begin([
            'header' =>'<h1 id="modalheader"></h1>',
            'id' => 'reservemodal',
            'size' => 'modal-lg',  
        ]);
echo "
<div id='wait'><center><img src='../web/admin/images/ajax-loader.gif' /></center></div>";
        echo "<div id='reservemodalcontent'></div>";
        Modal::end();
    ?>
