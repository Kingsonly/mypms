<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Reservations */

$this->title = 'Monthly Inventory Report';
?>
<div class="">
     
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel" style="border-width:10px; border-style:ridge">
                  <div class="x_content">
<br>
<div class="col-md-12">
    <div class="col-md-4">
    	<div class="pull-left">
    		Printed By: <?= Yii::$app->user->identity->person->surname.' '.Yii::$app->user->identity->person->first_name ?><br>
    		Printed Date: <?= date('Y-m-d H:i:s') ?>
    	</div>
    </div>
    <div class="col-md-4"><center>DBI GUEST HOUSE<br>MONTHLY INVENTORY REPORT</center></div>
    <div class="col-md-4">
    	<div class="pull-right">Page 1</div></div>
    	</div>
    	<br>
    	<br>
    	<hr>
    	Month As On <?= date('M Y'); ?>&nbsp; Booking Status: All
<hr>
<table class="table table-striped">
                      <thead>
                        <tr>
                          <th>Room Type</th>
                          <?php 
                          $month = date('m');
                          if ($month = 9 || $month = 4 || $month = 6 || $month = 11) {
                          	$lastDay = 30;
                          } if ($month = 2) {
                          	$lastDay =29;
                          } if ($month != 9 || $month != 4 || $month != 6 || $month != 11 || $month != 2){
                          	$lastDay = 31;
                          }
                          	for ($i=1; $i <= $lastDay; $i++) { 
        $day1 = strtotime(date('Y-m-').'01');
        $day2 = strtotime(date('Y-m-').'02');
        $day3 = strtotime(date('Y-m-').'03');
        $day4 = strtotime(date('Y-m-').'04');
        $day5 = strtotime(date('Y-m-').'05');
        $day6 = strtotime(date('Y-m-').'06');
        $day7 = strtotime(date('Y-m-').'07');
        $day8 = strtotime(date('Y-m-').'08');
        $day9 = strtotime(date('Y-m-').'09');
        $day10 = strtotime(date('Y-m-').'10');
        $day11 = strtotime(date('Y-m-').'11');
        $day12 = strtotime(date('Y-m-').'12');
        $day13 = strtotime(date('Y-m-').'13');
        $day14 = strtotime(date('Y-m-').'14');
        $day15 = strtotime(date('Y-m-').'15');
        $day16 = strtotime(date('Y-m-').'16');
        $day17 = strtotime(date('Y-m-').'17');
        $day18 = strtotime(date('Y-m-').'18');
        $day19 = strtotime(date('Y-m-').'19');
        $day20 = strtotime(date('Y-m-').'20');
        $day21 = strtotime(date('Y-m-').'21');
        $day22 = strtotime(date('Y-m-').'22');
        $day23 = strtotime(date('Y-m-').'23');
        $day24 = strtotime(date('Y-m-').'24');
        $day25 = strtotime(date('Y-m-').'25');
        $day26 = strtotime(date('Y-m-').'26');
        $day27 = strtotime(date('Y-m-').'27');
        $day28 = strtotime(date('Y-m-').'28');
        $day29 = strtotime(date('Y-m-').'29');
        $day30 = strtotime(date('Y-m-').'30');
        $day31 = strtotime(date('Y-m-').'31');
        $day32 = strtotime(date('Y-m-').'32');
                          echo '<th>'.$i.'<br><span style="font-size: 6px;">'.date("D", ${'day'.$i}).'</font></th>';
                          // echo '<th>'.$i.'</th>';
                          } ?>
                          <th>TOTAL</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th>Classic</th>
                          <td><?php echo $roomInventoryReport1[0]; ?></td>
                          <td><?php echo $roomInventoryReport1[1]; ?></td>
                          <td><?php echo $roomInventoryReport1[2]; ?></td>
                          <td><?php echo $roomInventoryReport1[3]; ?></td>
                          <td><?php echo $roomInventoryReport1[4]; ?></td>
                          <td><?php echo $roomInventoryReport1[5]; ?></td>
                          <td><?php echo $roomInventoryReport1[6]; ?></td>
                          <td><?php echo $roomInventoryReport1[7]; ?></td>
                          <td><?php echo $roomInventoryReport1[8]; ?></td>
                          <td><?php echo $roomInventoryReport1[9]; ?></td>
                          <td><?php echo $roomInventoryReport1[10]; ?></td>
                          <td><?php echo $roomInventoryReport1[11]; ?></td>
                          <td><?php echo $roomInventoryReport1[12]; ?></td>
                          <td><?php echo $roomInventoryReport1[13]; ?></td>
                          <td><?php echo $roomInventoryReport1[14]; ?></td>
                          <td><?php echo $roomInventoryReport1[15]; ?></td>
                          <td><?php echo $roomInventoryReport1[16]; ?></td>
                          <td><?php echo $roomInventoryReport1[17]; ?></td>
                          <td><?php echo $roomInventoryReport1[18]; ?></td>
                          <td><?php echo $roomInventoryReport1[19]; ?></td>
                          <td><?php echo $roomInventoryReport1[20]; ?></td>
                          <td><?php echo $roomInventoryReport1[21]; ?></td>
                          <td><?php echo $roomInventoryReport1[22]; ?></td>
                          <td><?php echo $roomInventoryReport1[23]; ?></td>
                          <td><?php echo $roomInventoryReport1[24]; ?></td>
                          <td><?php echo $roomInventoryReport1[25]; ?></td>
                          <td><?php echo $roomInventoryReport1[26]; ?></td>
                          <td><?php echo $roomInventoryReport1[27]; ?></td>
                          <td><?php echo $roomInventoryReport1[28]; ?></td>
                          <td><?php echo $roomInventoryReport1[29]; ?></td>
                          <td><?php echo $roomInventoryReport1[30]; ?></td>
                          <td><?php echo array_sum($roomInventoryReport1); ?></td>
                          </tr>
                          <tr>
                          <td>Superior</td>
                          <td><?php echo $roomInventoryReport2[0]; ?></td>
                          <td><?php echo $roomInventoryReport2[1]; ?></td>
                          <td><?php echo $roomInventoryReport2[2]; ?></td>
                          <td><?php echo $roomInventoryReport2[3]; ?></td>
                          <td><?php echo $roomInventoryReport2[4]; ?></td>
                          <td><?php echo $roomInventoryReport2[5]; ?></td>
                          <td><?php echo $roomInventoryReport2[6]; ?></td>
                          <td><?php echo $roomInventoryReport2[7]; ?></td>
                          <td><?php echo $roomInventoryReport2[8]; ?></td>
                          <td><?php echo $roomInventoryReport2[9]; ?></td>
                          <td><?php echo $roomInventoryReport2[10]; ?></td>
                          <td><?php echo $roomInventoryReport2[11]; ?></td>
                          <td><?php echo $roomInventoryReport2[12]; ?></td>
                          <td><?php echo $roomInventoryReport2[13]; ?></td>
                          <td><?php echo $roomInventoryReport2[14]; ?></td>
                          <td><?php echo $roomInventoryReport2[15]; ?></td>
                          <td><?php echo $roomInventoryReport2[16]; ?></td>
                          <td><?php echo $roomInventoryReport2[17]; ?></td>
                          <td><?php echo $roomInventoryReport2[18]; ?></td>
                          <td><?php echo $roomInventoryReport2[19]; ?></td>
                          <td><?php echo $roomInventoryReport2[20]; ?></td>
                          <td><?php echo $roomInventoryReport2[21]; ?></td>
                          <td><?php echo $roomInventoryReport2[22]; ?></td>
                          <td><?php echo $roomInventoryReport2[23]; ?></td>
                          <td><?php echo $roomInventoryReport2[24]; ?></td>
                          <td><?php echo $roomInventoryReport2[25]; ?></td>
                          <td><?php echo $roomInventoryReport2[26]; ?></td>
                          <td><?php echo $roomInventoryReport2[27]; ?></td>
                          <td><?php echo $roomInventoryReport2[28]; ?></td>
                          <td><?php echo $roomInventoryReport2[29]; ?></td>
                          <td><?php echo $roomInventoryReport2[30]; ?></td>
                          <td><?php echo array_sum($roomInventoryReport2); ?></td>
                          </tr>
                          <tr>
                          <td>Business Suite</td>
                          <td><?php echo $roomInventoryReport3[0]; ?></td>
                          <td><?php echo $roomInventoryReport3[1]; ?></td>
                          <td><?php echo $roomInventoryReport3[2]; ?></td>
                          <td><?php echo $roomInventoryReport3[3]; ?></td>
                          <td><?php echo $roomInventoryReport3[4]; ?></td>
                          <td><?php echo $roomInventoryReport3[5]; ?></td>
                          <td><?php echo $roomInventoryReport3[6]; ?></td>
                          <td><?php echo $roomInventoryReport3[7]; ?></td>
                          <td><?php echo $roomInventoryReport3[8]; ?></td>
                          <td><?php echo $roomInventoryReport3[9]; ?></td>
                          <td><?php echo $roomInventoryReport3[10]; ?></td>
                          <td><?php echo $roomInventoryReport3[11]; ?></td>
                          <td><?php echo $roomInventoryReport3[12]; ?></td>
                          <td><?php echo $roomInventoryReport3[13]; ?></td>
                          <td><?php echo $roomInventoryReport3[14]; ?></td>
                          <td><?php echo $roomInventoryReport3[15]; ?></td>
                          <td><?php echo $roomInventoryReport3[16]; ?></td>
                          <td><?php echo $roomInventoryReport3[17]; ?></td>
                          <td><?php echo $roomInventoryReport3[18]; ?></td>
                          <td><?php echo $roomInventoryReport3[19]; ?></td>
                          <td><?php echo $roomInventoryReport3[20]; ?></td>
                          <td><?php echo $roomInventoryReport3[21]; ?></td>
                          <td><?php echo $roomInventoryReport3[22]; ?></td>
                          <td><?php echo $roomInventoryReport3[23]; ?></td>
                          <td><?php echo $roomInventoryReport3[24]; ?></td>
                          <td><?php echo $roomInventoryReport3[25]; ?></td>
                          <td><?php echo $roomInventoryReport3[26]; ?></td>
                          <td><?php echo $roomInventoryReport3[27]; ?></td>
                          <td><?php echo $roomInventoryReport3[28]; ?></td>
                          <td><?php echo $roomInventoryReport3[29]; ?></td>
                          <td><?php echo $roomInventoryReport3[30]; ?></td>
                          <td><?php echo array_sum($roomInventoryReport3); ?></td>
                        </tr>
                        <tr></tr>
                        <tr><td>Total</td>
                        <td><?php echo ($roomInventoryReport1[0] + $roomInventoryReport2[0] + $roomInventoryReport3[0]); ?></td>
                        <td><?php echo ($roomInventoryReport1[1] + $roomInventoryReport2[1] + $roomInventoryReport3[1]); ?></td>
                        <td><?php echo ($roomInventoryReport1[2] + $roomInventoryReport2[2] + $roomInventoryReport3[2]); ?></td>
                        <td><?php echo ($roomInventoryReport1[3] + $roomInventoryReport2[3] + $roomInventoryReport3[3]); ?></td>
                        <td><?php echo ($roomInventoryReport1[4] + $roomInventoryReport2[4] + $roomInventoryReport3[4]); ?></td>
                        <td><?php echo ($roomInventoryReport1[5] + $roomInventoryReport2[5] + $roomInventoryReport3[5]); ?></td>
                        <td><?php echo ($roomInventoryReport1[6] + $roomInventoryReport2[6] + $roomInventoryReport3[6]); ?></td>
                        <td><?php echo ($roomInventoryReport1[7] + $roomInventoryReport2[7] + $roomInventoryReport3[7]); ?></td>
                        <td><?php echo ($roomInventoryReport1[8] + $roomInventoryReport2[8] + $roomInventoryReport3[8]); ?></td>
                        <td><?php echo ($roomInventoryReport1[9] + $roomInventoryReport2[9] + $roomInventoryReport3[9]); ?></td>
                        <td><?php echo ($roomInventoryReport1[10] + $roomInventoryReport2[10] + $roomInventoryReport3[10]); ?></td>
                        <td><?php echo ($roomInventoryReport1[11] + $roomInventoryReport2[11] + $roomInventoryReport3[11]); ?></td>
                        <td><?php echo ($roomInventoryReport1[12] + $roomInventoryReport2[12] + $roomInventoryReport3[12]); ?></td>
                        <td><?php echo ($roomInventoryReport1[13] + $roomInventoryReport2[13] + $roomInventoryReport3[13]); ?></td>
                        <td><?php echo ($roomInventoryReport1[14] + $roomInventoryReport2[14] + $roomInventoryReport3[14]); ?></td>
                        <td><?php echo ($roomInventoryReport1[15] + $roomInventoryReport2[15] + $roomInventoryReport3[15]); ?></td>
                        <td><?php echo ($roomInventoryReport1[16] + $roomInventoryReport2[16] + $roomInventoryReport3[16]); ?></td>
                        <td><?php echo ($roomInventoryReport1[17] + $roomInventoryReport2[17] + $roomInventoryReport3[17]); ?></td>
                        <td><?php echo ($roomInventoryReport1[18] + $roomInventoryReport2[18] + $roomInventoryReport3[18]); ?></td>
                        <td><?php echo ($roomInventoryReport1[19] + $roomInventoryReport2[19] + $roomInventoryReport3[19]); ?></td>
                        <td><?php echo ($roomInventoryReport1[20] + $roomInventoryReport2[20] + $roomInventoryReport3[20]); ?></td>
                        <td><?php echo ($roomInventoryReport1[21] + $roomInventoryReport2[21] + $roomInventoryReport3[21]); ?></td>
                        <td><?php echo ($roomInventoryReport1[22] + $roomInventoryReport2[22] + $roomInventoryReport3[22]); ?></td>
                        <td><?php echo ($roomInventoryReport1[23] + $roomInventoryReport2[23] + $roomInventoryReport3[23]); ?></td>
                        <td><?php echo ($roomInventoryReport1[24] + $roomInventoryReport2[24] + $roomInventoryReport3[24]); ?></td>
                        <td><?php echo ($roomInventoryReport1[25] + $roomInventoryReport2[25] + $roomInventoryReport3[25]); ?></td>
                        <td><?php echo ($roomInventoryReport1[26] + $roomInventoryReport2[26] + $roomInventoryReport3[26]); ?></td>
                        <td><?php echo ($roomInventoryReport1[27] + $roomInventoryReport2[27] + $roomInventoryReport3[27]); ?></td>
                        <td><?php echo ($roomInventoryReport1[28] + $roomInventoryReport2[28] + $roomInventoryReport3[28]); ?></td>
                        <td><?php echo ($roomInventoryReport1[29] + $roomInventoryReport2[29] + $roomInventoryReport3[29]); ?></td>
                        <td><?php echo ($roomInventoryReport1[30] + $roomInventoryReport2[30] + $roomInventoryReport3[30]); ?></td>
                        <td><?php echo (array_sum($roomInventoryReport1) + array_sum($roomInventoryReport2) + array_sum($roomInventoryReport3)); ?></td>
                        
                        </tr>
                      </tbody>
                    </table>

</div>
</div>
</div>
</div>