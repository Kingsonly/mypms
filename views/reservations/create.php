<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Reservations */

$this->title = 'Book A Reservation';
$this->params['breadcrumbs'][] = ['label' => 'Reservations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reservations-create">
    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
		'personModel' => $personModel,
        'emailModel' => $emailModel,
        'telephoneModel' => $telephoneModel,
        'addressModel' => $addressModel,
        'statesModel' => $statesModel,
        'countriesModel' => $countriesModel,
        'statusTypeModel' => $statusTypeModel,
        'guestModel' => $guestModel,
		'roomModel' => $roomModel,
    ]) ?>

</div>
