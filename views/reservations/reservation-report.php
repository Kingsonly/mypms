<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Reservations */

$this->title = 'Reservation Report';
?>
<div class="">
     
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel" style="border-width:10px; border-style:ridge">
                  <div class="x_content">
<br>
<div class="col-md-12">
    <div class="col-md-4">
    	<div class="pull-left">
    		Printed By: <?= Yii::$app->user->identity->person->surname.' '.Yii::$app->user->identity->person->first_name ?><br>
    		Printed Date: <?= date('Y-m-d H:i:s') ?>
    	</div>
    </div>
    <div class="col-md-4"><center>DBI GUEST HOUSE<br>RESERVATION REPORT</center></div>
    <div class="col-md-4">
    	<div class="pull-right">Page 1</div></div>
    	</div>
    	<br>
    	<br>
    	<hr>
    	For the Month of <?= date('M, Y'); ?> 
<hr>
<table class="table table-striped">
                      <thead>
                        <tr>
                          <th>Rsrv. No.</th>
                          <th>Rsrv. Date</th>
                          <th>Guest</th>
                          <th>Arvl.</th>
                          <th>Dept.</th>
                          <th>Nights</th>
                          <th>Rm. No.</th>
                          <th>Total Amt.</th>
                          <th>Amt. Paid</th>
                          <th>User</th>
                        </tr>
                      </thead>
                      <tbody>
              <?php foreach($reservationReport as $k => $v){?>
                        <tr>
                          <th  scope="row"><?php echo  $v['booking_id'];?></th>
                          <td><?php echo  $v['datetime'];?></td>
              <td><?php echo  $v['person']['surname'].' '. $v['person']['first_name'];?></td>
                          <td><?php echo  $v['time_in'];?></td>
                          <td><?php echo  $v['time_out'];?></td>
                          <td><?php $date1 = strtotime($v['time_out']); $date2 = strtotime($v['time_in']);
                          echo floor(($date1-$date2)/(60 * 60 * 24));  ?></td> 
                          <td><?php echo  $v['room']['room_number'];?></td>
                          <td><?php echo  $v['tariff']['tariff'];?></td>
                          <td><?php echo  $v['amount_paid'];?></td>
                          <td><?php echo  $v['user_id'];?></td>
                        </tr>
                            <?php }?>
                      </tbody>
                    </table>

</div>
</div>
</div>
</div>