<?php

use yii\helpers\Html;
use scotthuangzl\googlechart\GoogleChart;


/* @var $this yii\web\View */
/* @var $model app\models\Reservations */

$this->title = 'Reservations';
$month = $_GET['month'];
?>
<div class="">
     
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?= $month ?> <?= Html::encode($this->title) ?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <div class="month-reservations">

      <?= GoogleChart::widget(array('visualization' => 'ColumnChart',
                'data' => array(
                    array('Reservations', 'Reservations Per Week'),
                    array('1st - 7th', (int)$firstWeek),
                    array('', 0),
                    array('8th - 14th', (int)$secondWeek),
                    array('', 0),
                    array('15th - 21st', (int)$thirdWeek),
                    array('', 0),
                    array('22nd - 28th', (int)$fourthWeek),
                    array('', 0),
                    array('Last Week', (int)$lastWeek)
                ),
                'options' => array('title' => 'Monthly Reservations (Bar Chart)', 'height' => '400'))); ?>
</div>

</div>
</div>
</div>
</div>
