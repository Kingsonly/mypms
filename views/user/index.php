<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Button ;
use yii\bootstrap\Modal;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Administrators');
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="">
              <div class="col-md-12 col-sm-12 col-xs-12">
            
                <div class="x_panel">
                  <div class="x_title">
     <button class="pull-right btn btn-danger adminmodalclick" data-url="<?php echo Url::to(['create']); ?>" >
Create Admin
</button>
                  <h2><i class="fa fa-users"></i> <?= Html::encode($this->title) ?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
<div class="userdb-index">
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          	<th>SN</th>
							<th>Full Name</th>
							<th>Username</th>
                          	<th>Role</th>
							<th>Level</th>
							<th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
              <?php $i=1; foreach($viewModel as $k => $v){?>
              <tr>
								<td><?php echo  $i;?></td>
                          		<td><?php echo  $v['person']['surname'].' '.$v['person']['first_name'] ;?></td>
                          		<td><?php echo  $v['username'];?></td>
                          		<td><?php echo  $v['role']['role_description'];?></td>
                          		<td><?php echo  $v['statusType']['status_title'];?></td>
								<td><?php echo Html::a(' <i class="fa fa-eye hvr-bounce-in"></i>', ['view', 'id' => $v['user_id']], ['class' => 'adminmodalclick']) ?>
                         &nbsp;&nbsp;<?php echo Html::a(' <i class="fa fa-pencil hvr-bounce-in"></i>', ['update', 'id' => $v['user_id']]) ?></td>
							<td></td>
						</tr>
                          
                            <?php $i++; }?>
                            </tbody>
                            </table>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            


<?php
//this should be put directly inside the user index page (best practice should be the last thing in the page 
Modal::begin([
            'header' =>'<h1 id="modalheader"></h1>',
            'id' => 'adminmodal',
            'size' => 'modal-lg',  
        ]);
echo "
<div id='wait'><center><img src='../web/admin/images/ajax-loader.gif' /></center></div>";
        echo "<div id='reservemodalcontent'></div>";
        Modal::end();
    ?>
