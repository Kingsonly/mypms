<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Userdb */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Userdb',
]) . $model->user_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Userdbs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->user_id, 'url' => ['view', 'id' => $model->user_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="userdb-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
		'personModel' => $personModel,
		'emailModel' => $emailModel, 
		'telephoneModel' => $telephoneModel, 
		'addressModel' => $addressModel, 
		'roleModel' => $roleModel, 
		'statusTypeModel' => $statusTypeModel, 
		'statesModel' => $statesModel, 
		'countriesModel' => $countriesModel, 
    ]) ?>

</div>
