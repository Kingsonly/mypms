<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Userdb */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="userdb-form">

				
    <?php $form = ActiveForm::begin(); ?>
	<?= $form->field($personModel, 'surname')->textInput(['maxlength' => true]) ?>
	
	<?= $form->field($personModel, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
	
	<?=$form->field($model, 'access_level')->dropdownList(ArrayHelper::map($roleModel->find()->all(), 'role_id', 'role_description'));?>
	
	<?= $form->field($emailModel, 'address')->textInput(['maxlength' => true]) ?>
	
	<?= $form->field($telephoneModel, 'number')->textInput(['maxlength' => true]) ?>
	
	<?= $form->field($addressModel, 'address')->textInput(['maxlength' => true]) ?>
	
	<?=$form->field($addressModel, 'state_id')->dropdownList(ArrayHelper::map($statesModel->find()->all(), 'state_id', 'state_name'));?>
	
    <?=$form->field($addressModel, 'country_id')->dropdownList(ArrayHelper::map($countriesModel->find()->all(), 'country_id', 'country_name'));?>

    <?=$form->field($model, 'status_id')->dropdownList(ArrayHelper::map($statusTypeModel->find()->where(['status_category'=>'user'])->all(), 'status_id', 'status_title'));?>
    
	

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-danger' : 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
