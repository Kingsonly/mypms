<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Userdb */

$this->title = Yii::t('app', 'Create Userdb');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Userdbs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="userdb-create">

    <?= $this->render('_form', [
        'model' => $model,
		'personModel' => $personModel,
		'emailModel' => $emailModel, 
		'telephoneModel' => $telephoneModel, 
		'addressModel' => $addressModel, 
		'roleModel' => $roleModel, 
		'statusTypeModel' => $statusTypeModel, 
		'statesModel' => $statesModel, 
		'countriesModel' => $countriesModel, 
    ]) ?>

</div>
